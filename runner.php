<?php
/*
wrapper for commandline scripts.
Executes a script in a loaded application environment.

Invocation:
php runner.php script_path
*/

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/setup.php';
require_once __DIR__ . '/src/container.php';

include $argv[1];