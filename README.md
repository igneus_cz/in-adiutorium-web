# In adiutorium website

This is the source of the [In adiutorium](http://www.inadiutorium.cz)
website.

You might want to use it to build a similar site with your own
content (I don't think it would be a very good idea, though)
or to run your own clone of the website when it's original author
is dead or permanently offline.

## Requirements

* PHP ~> 5.6
* composer

## Installation

* `$ composer install` to install dependencies
* configure your webserver to make the directory [public](./public)
  root of a new site; enable the site
* make sure that directory `tmp/cache` is writable by the user under
  whom the webserver is running
  and that the directory `blog/vygenerovane` is writable by the user
  who will run the `blog_update.php` script (see below)

## "Usage"

Most of the content is in XML files:

* public/knihy.xml - Books
* public/odkazy.xml - Links
* public/knihovna.xml - Description of downloads
* blog/*.xml - Blog posts

When adding a new blog post, execute
`$ php src/scripts/blog_update.php`
to refresh blog index.

News are in a plaintext file novinky.txt

Compiled pdfs of music sheets from the
[In adiutorium project](https://github.com/igneus/In-adiutorium)
are expected to be found in `public/materialy`.

[International bibliography of music for the Liturgy of the Hours](https://github.com/igneus/loth-music-bibliography)
is expected to live in `public/bibliography`.

## XML everywhere

The reader might be scandalized or even disgusted by the fact
that the website isn't backed by a relational database or other
efficient data storage and instead reads and processes a few
XML files over and over, occasionally performing XPath queries
on them.

The scandalized reader is definitely right in some sense.
However, due to the very low load the site faces,
together with small amount of data contained, there are
no performance problems that would require a more efficient
data storage. So the website is optimized rather for it's author's
happiness and ease of maintenance rather than rendering speed.

## History

I started the website in late 2010, with the attitude "minimum energy
investments to deliver the content". It started as a single
page, then grew both in extent and functionality, and quickly became
ugly mess.

Then I finally had options to run web applications built
on technologies other than PHP and decided to rewrite the website
from scratch in Ruby.
First on [Sinatra](https://bitbucket.org/igneus_cz/in-adiutorium-web/commits/branch/sinatra-restart) (2013) - "it is only a small simple
website, isn't it?" -
later on [Rails](https://bitbucket.org/igneus_cz/in-adiutorium-web/commits/branch/rails-restart) (2015).
But none of these rewrite attempts made it to production.
Still later I figured out it might be better to stay with the old
codebase and improve it gradually instead of throwing it away.
