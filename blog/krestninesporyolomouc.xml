<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Křestní nešpory (7): a co Morava?</titulek>

  <tags>křestní nešpory, olomoucký ritus, pražský ritus</tags>
  <kategorie>Rubriky</kategorie>
  <datum>11.3.2024 18:58</datum>
  <souvisejiciNoty>velikpruvod</souvisejiciNoty>

  <souhrn></souhrn>

  <text>
    <p>
      Velikonoce se blíží a tak je vhodná doba
      po delší době opět zvednout téma křestních nešpor
      a doplnit něco málo k seriálu,
      který tu o nich před několika lety vycházel.
    </p>
    <p>
      Liturgické předpisy, počínaje VPDMC 213, opakovaně
      vyzývají ke slavení nešpor slavnosti Zmrtvýchvstání Páně
      jako "tzv. křestních nešpor, při nichž se za zpěvu žalmů
      koná průvod ke křestnímu pramenu".
      Zatímco VPDMC a <em>Caeremoniale episcoporum</em> zůstávají
      při nabádání, aby se tato tradice zachovávala tam, kde dosud
      trvá, okružní list <em>Paschalis sollemnitatis</em>
      zřejmě <a href="/blog.php?clanek=krestninespory1.xml">připouští i její zavedení</a>
      tam, kde třeba už dávno zanikla, nebo kde dokonce ani není
      z dřívějška doložena.
    </p>
    <p>
      Protože se liturgické zákonodárství ve věci křestních nešpor
      odvolává na místní zvyklosti a neexistuje žádný oficiální
      římský scénář, jak by se ony nešpory s průvodem ke křestnímu pramenu
      měly konat, seznámili jsme se jednak s tradicí
      křestních nešpor v <a href="/blog.php?clanek=krestninespory2.xml">premonstrátském řádu</a>
      (viz také <a href="/blog.php?clanek=krestninesporystrahov2022.xml">novější vývoj</a>),
      jednak s prameny dokládajícími předtridentskou liturgickou
      praxi v <a href="/blog.php?clanek=krestninespory3.xml">pražské církevní provincii</a>.
      Na závěr jsem, opřen primárně o ony pražské prameny,
      <a href="/blog.php?clanek=krestninespory4.xml">předložil</a>
      <a href="/blog.php?clanek=krestninespory5.xml">návrh</a>,
      jak by v Čechách bylo možné křestní nešpory obnovit
      v rámci platných liturgických předpisů.
    </p>
    <p>
      Moravský čtenář se právem mohl ptát, zda se na Moravě
      křestní nešpory vůbec slavily, zda se jejich řád něčím lišil
      od toho užívaného v Čechách,
      a co z případných rozdílů by bylo možné a vhodné
      zohlednit v dnešní liturgické praxi.
      Odpověď jsem až dosud zůstával dlužen.
      I to, co následuje, je třeba číst zatím jen jako odpověď
      velmi předběžnou, protože stojí na opravdu skromné
      pramenné základně.
    </p>
    <h3>Předtridentský olomoucký ritus</h3>
    <p>
      Tištěný olomoucký breviář z r. 1499
      (<a href="https://books.google.cz/books?id=2wRiOMEukxAC&amp;hl=cs&amp;pg=RA1-PA136-IA1#v=onepage&amp;q&amp;f=false"><em>Breviarium Olomucense</em>, Strassburg: Johann Grüninger 1499, f. 136v</a>)
      má nešpory slavnosti Zmrtvýchvstání Páně v podobě, která v hrubých obrysech odpovídá
      variantě pražských nešpor určené pro soukromou
      modlitbu mimo kostel.
      Jsou zachovány dobově obvyklé strukturní zvláštnostni oktávu
      Zmrtvýchvstání (začíná se zpěvem kyrie; capitulum, veršík
      a hymnus jsou nahrazeny zpěvy graduale a aleluja z mešního propria),
      ale průvod ke křtitelnici se nekoná.
    </p>
    <p>
      Křestní nešpory se vším všudy (a s rubrikami
      zahrnujícími nejeden zajímavý ceremoniální detail!)
      však najdeme ve starším breviáři rukopisném.
      Co vedlo k tomu, že byl při redakci tištěného breviáře
      průvod ke křtitelnici vypuštěn?
      Přestal se v průběhu 15. stol. konat i v katedrále?
      Nebo byl tištěný breviář koncipován primárně pro potřeby
      nejširších kruhů
      diecésního duchovenstva a se slavením křestních nešpor
      mimo katedrálu se nepočítalo?
    </p>
    <p class="petit">
      Pro pražskou diecési jsem ve starším článku mohl prokázat,
      že s křestními nešporami počítaly i některé rukopisy
      specificky určené pro liturgický provoz mimo katedrálu. Pro diecési olomouckou
      zatím podobný důkaz předložit nemohu.
    </p>
    <blockquote>
      <p>
        <strong>[1]</strong>
        K nešporám:<br/>
        <strong><em>Kyrieleison.</em></strong><br/>
        Antifona <em>Alleluia alleluia alleluia alleluia</em>,
        žalm <em>Dixit Dominus</em>.<br/>
        Antifona <em>Alleluia alleluia alleluia alleluia [dopl.: alleluia]</em>,
        žalm <em>Confitebor</em>.<br/>
        Antifona <em>Alleluia alleluia alleluia [dopl.: alleluia]</em>,
        žalm <em>Beatus vir</em>.<br/>
      </p>
      <p>
        <strong>Graduale</strong> <em>Haec dies</em> s veršem <em>Confitemini</em>,<br/>
        <strong>Aleluja</strong> s verši <em>Pascha nostrum</em> a <em>Epulemur</em>.
      </p>
      <p>
        Hned následuje antifona k Magnificat:
        <em>Surrexit enim sicut dixit Dominus
        et praecedet vos in Galilaeam, alleluia, ibi eum videbitis, alleluia alleluia alleluia</em> a opakuje se.<br/>
        <em>Dominus vobiscum. Oremus.</em>
        Orace <em>Deus qui hodierna
        <!-- plný text orace doplněn z téhož folia vlevo nahoře -->
        die [per unigenitum tuum
        aeternitatis nobis aditum devicta morte reserasti:
        vota nostra, quae praeveniendo aspiras,
        etiam adiuvando prosequere. Per eundem.]</em>
      </p>
      <p>
        <strong>[2]</strong>
        Po <em>Amen</em> hned následuje
        antifona <em>Alleluia alleluia alleluia</em>,
        žalm <em>Laudate pueri</em>.
        Za zpěvu tohoto žalmu chór sestoupí ke [křestnímu] prameni.
        Napřed jdou korouhve, svíce, mezi nimi i požehnaná
        svíce velikonoční (tzn. paškál), také svaté křižmo, olej a kadidlo.
      </p>
      <p>
        Následuje antifona <em>Alleluia alleluia alleluia</em>,
        žalm <em>In exitu Israel de Aegypto</em>.
        Během tohoto žalmu kněz oblečený v liturgickém rouchu (presbyter indutus <em>apparatu</em>)
        se svou asistencí podle obyčeje <strong>sedmkrát obejde pramen.</strong>
        Korouhve a svíce, o nichž výše, jdou před nimi.<br/>
        Po zakončení žalmu kněz, stoje vedle pramene, říká verš
        <em>Domine, apud te est fons vitae, alleluia.</em><br/>
        Orace <em>Deus, qui renatis ex aqua et spiritu sancto
        regni caelestis pandis introitum: auge super famulos tuos
        gratiam, quam dedisti, ut nullis bonis priventur promissis,
        sed a cunctis purgentur peccatis. Per Christum.</em>
        Chór odpoví <em>Amen</em>.
        Tato orace se u pramene říká po celý oktáv.
      </p>
      <p>
        <strong>[3]</strong>
        Pak, zatímco se procesí vrací do chóru,
        začnou antifonu <em>Christus resurgens [...] V. Dicant nunc Iudaei [...] alleluia alleluia</em>.<br/>
        Verš <em>In ressurrectione tua Christe, alleluia.</em><br/>
        Orace <em>Deus, qui nos resurrectionis dominicae annua sollemnitate laetificas:
        concede propitius, ut per temporalia festa quae agimus pervenire ad gaudia aeterna mereamur. Per eundem.</em>
        (Bože, jenž nás obveseluješ každoroční slavností zmrtvýchvstání našeho Pána, dej, ať slavením těchto časných svátků dojdeme do radosti věčné; dnes kolekta středy ve velikonočním oktávu.)<br/>
        [Pak] <em>Benedicamus domino, alleluia</em> [dopl.: pětkrát!].
      </p>
      <p>
        Tento pořádek se o nešporách zachovává až do soboty,
        jen každý den s vlastním veršem graduale, aleluja a antifonou
        k Magnificat.
        Jestliže někdo říká nešpory mimo procesí
        ke křtitelnici, ať čte žalmy podle obvyklého pořádku
        (<em>psalmi legantur per ordinem</em> - podle všeho se tím chce říci, že se má číst všech pět žalmů najednou, bez dělení na zkrácenou nešporní psalmodii a žalmy k procesí - srov. tištěný breviář),
        pak graduale s jeho veršem, aleluja s veršem náležejícím
        danému dni.
        Pak ať říká antifonu k Magnificat.
        Ta se až do středy opakuje (pozn.: jde o upřesnění způsobu provedení, ne o to, že by se měla čtyři dny říkat stejná).
        Připojí oraci bez závěru, pak ať říká antifonu
        <em>Christus resurgens</em> vč. jejího verše [<em>Dicant nunc</em>],
        připojí veršík <em>In resurrectione</em>,
        oraci <em>Deus qui nos resurrectionis</em>
        a uzavře <em>Per eundem.</em>
        <em>Benedicamus Domino, alleluja.</em> [dopl.: pětkrát!]
        A víc nic.
      </p>
      <p class="petit">
        (<a href="https://www.manuscriptorium.com/apps/index.php?direct=record&amp;pid=VMO___-VMO___K_24076_____143YBTD-cs">CZ-OLm K-24076</a>, f. 157r;
        liturgické texty, k nimž zde není uveden překlad, se shodují s pražskými a překlady jsou ve starším článku)
      </p>
    </blockquote>
    <p>
      Textový repertoár je podobný pražskému, ale nápadným
      rozdílem je <strong>chybějící zastavení ve "středu kostela"</strong>
      (cestou od křitelnice)
      s veršem a orací připomínající umučení Páně.
      Zastavení u křtitelnice proto také nemá extra zpěv
      (v Praze antifona <em>Vidi aquam</em>),
      ale zpívá se tu druhý z žalmů k procesí,
      zatímco se křtitelnice sedmkrát slavnostně obchází.
      Při procesí zpět do chóru se pak zpívá antifona
      <em>Christus resurgens</em>, stejně jako v Praze.
    </p>
    <p>
      K tomu, <strong>s jakými nápěvy se v Olomouci velikonoční nešpory zpívaly,</strong>
      zatím nemohu říci nic. Digitalisovaný
      antifonář přesvědčivě identifikovaný jako olomoucký diecésní
      neznám žádný, studovat fysické exempláře jsem dosud nebyl
      a v dohledné době se k tomu nedostanu.
    </p>
    <p class="petit">
      Antifonář
      <a href="https://www.manuscriptorium.com/apps/index.php?direct=record&amp;pid=AIPDIG-VMO___K_14892_____2I4Z3A2-cs">CZ-OLm K 14892</a>,
      obsahující hlavně matutina a laudy vybraných svátků,
      podle všeho olomoucký (ani pražský) diecésní <em>není</em> -
      viz mj. f. 95v antifony nedělních nešpor;
      90v sestavu antifon slavnosti Všech svatých.
      Ale když je teď rukopis v Olomouci uložen, možná tu i tak stojí za zmínku,
      že (1) na f. 6v-7r byla původně zapsána sestava pěti alelujatických antifon
      k velikonočním nešporám, až na pár detailů v nápěvech odpovídající obvyklému pražskému
      pořádku, ale (2) první dvě antifony jsou vyškrábány a
      nahrazeny jediným desetinásobným aleluja.
    </p>
    <p>
      Něco nám o nápěvech můžou napovědět počty opakování aleluja
      v jednotlivých antifonách k žalmům.
      Pro pražské prameny je typická sestava antifon
      4-5-4-3-3.
      Melodie pro alelujatické antifony jsou vzaté z antifon
      figurujících v různých místních tradicích
      v nedělních nešporách:
    </p>
    <ul class="petit">
      <li><em>Dixit Dominus</em></li>
      <li><em>Fidelia omnia</em></li>
      <li><em>Potens in terra</em></li>
      <li><em>Sit nomen Domini</em></li>
      <li><em>Nos qui vivimus</em></li>
    </ul>
    <p class="petit">
      (Mimochodem: pražské nedělní nešpory mají třetí a pátou
      antifonu jinou, takže nápěvový odkaz mezi antifonami
      běžných nedělí a "neděle neděl" tu fungoval jen zčásti.)
    </p>
    <p>
      Rukopisný olomoucký breviář K-24076 má sestavu 4-4-3-3-3,
      kterou však dodatečná aleluja doplněná v marginálních přípiscích
      opravují na "pražský" formát 4-5-4-3-3.
      (Shodné počty aleluja samozřejmě ještě nutně neznamenají
      shodné nápěvy.)
      Tištěný breviář z r. 1499 pak má sestavu 4-4-5-4-3.
      Je samozřejmě otázka, jak pečlivý který nenotovaný pramen v reprodukci
      počtu opakování aleluja je, ale dosavadní zkušenost mi velí
      nepředpokládat automaticky nespolehlivost.
      Kdyby všechny tři verze (rukopisný breviář podle první ruky,
      rukopisný breviář s pozdějšími přípisky, tištěný breviář)
      byly spolehlivými otisky dobové praxe, resp. notovaných
      pramenů, ukazovalo by to
      na značnou proměnlivost olomouckého repertoáru velikonočních nešpor,
      např. vlivem používání antifonářů dovážených z různých církevních center.
      Byl bych nakloněn za nejspolehlivější prohlásit
      rukopisný breviář ve znění dodatečných oprav
      (tzn. možná stejné alelujatické antifony jako v Praze)
      a obě zbylé verze označit za pravděpodobné chyby.
      Solidnější však bude zůstat při tom, že nevíme,
      a počkat, co řeknou zatím neprostudované prameny.
    </p>
    <h3>Jak křestní nešpory slavit dnes</h3>
    <p>
      Vzhledem k velmi podobnému textovému repertoáru je
      možné vyjít ze starších návrhů pro Čechy
      (<a href="/blog.php?clanek=krestninespory4.xml">první verze</a>;
      její <a href="/blog.php?clanek=krestninespory4.xml">modlitelný český překlad a doplňky</a>)
      a volitelně zohlednit některá z moravských specifik:
    </p>
    <ul>
      <li>v průvodu ke křtitelnici se nesou i posvátné oleje</li>
      <li>u křtitelnice se nezpívá <em>Vidi aquam</em>, ale za zpěvu druhého z žalmů k procesí se křtitelnice sedmkrát obchází</li>
      <li>nekoná se zastavení ve středu kostela / u kříže, antifona <em>Christus resurgens</em> se začíná při odchodu od křtitelnice a jde se rovnou zpátky do chóru</li>
      <li>po návratu do chóru se říká jiná orace - <em>Deus, qui nos resurrectionis</em> (pro oficiální český překlad viz kolektu středy ve velikonočním oktávu)</li>
    </ul>
    <p>
      Sedmeré obcházení křtitelnice v slavnostním procesí
      je prostorově hodně náročné a současníkovi
      dost kulturně vzdálené, proto mnohde bude vhodnější
      zvolit pražskou podobu zastavení, kde se křtitelnice
      okuřuje za zpěvu antifony <em>Vidi aquam</em>.
      Druhý žalm se pak může
      zpívat cestou zpátky do chóru a antifona <em>Christus resurgens</em>
      vynechat, příp. připojit po dokončení žalmu.
    </p>
    <h3>Křestní nešpory <em>secundum Olejníkum</em></h3>
    <p>
      Chorální nápěvy - jak pro slavení se zpěvy latinskými,
      tak s českými - zájemce najde v odkazovaných starších článcích.
      Ale to moravského čtenáře patrně neuspokojí, protože
      olomoucká arcidiecése mezi všemi ostatními diecésemi
      zemí koruny české vyniká vlastním, tiskem vydaným českým vesperálem,
      podle kterého se v olomoucké katedrále pravidelně zpívá,
      kněžský dorost z celé Moravy v Olomouci studuje
      a ve zpívání Olejníkových nešpor se cvičí, ...
      zkrátka návrh "olomouckých" křestních nešpor by byl k ničemu,
      neposkytl-li by důstojné řešení, jak průvod ke křtitelnici
      vkusně odbavit <em>na závěr nešpor zpívaných z Olejníkova vesperálu.</em>
    </p>
    <p>
      K průvodu jsou potřeba dvě alelujatické antifony k žalmům,
      volitelně antifona <em>Vidi aquam</em> (jen pokud se přikloníme
      k pražské podobě zastavení u křtitelnice) a
      procesionální antifona <em>Christus resurgens</em>.
      S <em>Vidi aquam</em> není problém, samozřejmě se sáhne po
      <a href="https://kancional.cz/587">Olejníkově zpracování</a>.
      Pro antifony k žalmům zpívaným při průvodu pak
      vyjdeme z postřehu, že alelujatické antifony (nejen) pražských
      křestních nešpor vznikly přetextováním melodií antifon,
      se kterými se ten který žalm zpíval v cyklu žaltáře.
      I my se zařídíme podobně jako naši středověcí předchůdci:
      vezmeme antifonu k příslušnému žalmu,
      text nahradíme vícenásobným opakováním aleluja,
      melodii podle potřeby upravíme zejm. svázáním not do melismat
      či rozvázáním/přeskupením melismat stávajících,
      aby s novým textem dávala smysl.
      Protože nemáme Olejníkovo zpracování antifon k příslušným
      žalmům z oficia (anžto nejde o žalmy zařazené v pokoncilním breviáři
      do nedělních druhých nešpor), sáhneme po antifonách k týmž žalmům
      do žaltářů mešních:
    </p>
    <div class="obrazek vlevo">
      <img src="/blog/prilohy/krestninesporyolomouc/velikonoce_pruvod_olejnik.png" alt="Alelujatické antifony vzniklé přetextováním antifon J. Olejníka" />
      <p class="tac">
        (<a href="/blog/prilohy/krestninesporyolomouc/velikonoce_pruvod_olejnik.ly">zdrojový kód</a>;
        původní Olejníkovy antifony:
        <a href="https://www.zaltar.cz/OL264.html">OL264</a>,
        <a href="https://www.zaltar.cz/OL163.html">OL163</a>)
      </p>
    </div>
    <p>
      (Kdo by jako druhý žalm chtěl místo žalmu 66
      zpívat raději žalm 111 nebo 112,
      tomu příprava alelujatické antifony zůstane domácím úkolem.)
    </p>
    <p>
      Za antifonu <em>Christus resurgens</em> žádnou olejníkovskou
      náhradu nemáme, ale vzhledem k tomu, že se k ní nezpívá žalm,
      nemusela by jedna chorální antifona (třeba navíc zpívaná
      v latinském originálu) působit obtíže, i když svým hudebním
      charakterem bude vyčnívat. Výše byla zmíněna i možnost
      úplně ji vynechat, příp. lze pomýšlet na náhradu nějakým
      "jiným vhodným zpěvem".
    </p>
  </text>
</clanek>
