<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Directorium chori ... Congregationis Oratorii, 1753</titulek>
  
  <tags>oratoriáni</tags>
  <kategorie>Knihy</kategorie>
  <datum>25.9.2012 21:55</datum>
  <!-- <souvisejiciNoty></souvisejiciNoty> -->
  
  <souhrn>
    Zpěvy původem z galikánské Francie. - Chorální, nikoli však
    gregoriánské. - Výrazné zjednodušení zpívaného oficia.
  </souhrn>
  
  <text>
    <div class="obrazek vpravo" style="width: 360px;">
      <img src="/blog/prilohy/directoriumchori_paris1753b.png" alt="stránka s nápěvy žalmů" style="width: 350px;" />
      <p>
	Directorium chori sive brevis psalmodiae ratio ad usum presbyterorum
	Congregationis Oratorii D.N.J.C.", Paříž 1753. Nápěvy žalmů
	k nešporám.
      </p>
    </div>

    <p>
      Čas od času ke mně z té či oné strany dolehne útržek informace
      o barokním úpadku gregoriánského chorálu ... o pokusech
      chorálu pevně určit délku not ... skládat nový ... o tom, jak vznikl
      dnes všeobecně známý tonus simplex antifony Salve regina ...
      Naneštěstí jsem ještě o této epoše/větvi vývoje liturgické hudby
      nenašel žádné obsáhlejší solidní informace, jen ony útržky.
      Nedávno jsem narazil na jednom z blogů, které sleduji,
      na článek 
      <a href="http://musicologicus.blogspot.cz/2012/08/parisian-plainchant-of-17th-and-18th.html">Parisian Plainchant of the 17th and 18th Century</a>.
      Ten mě podnítil k pokusu najít nějaké ukázky "francouzského barokního
      chorálu" na internetu.
      Ten pokus přinesl dvojí objev: 
      <a href="http://imslp.org/wiki/Lamentationes_Jeremiae_prophetae_%28Nivers,_Guillaume-Gabriel%29">Niversovy "Lamentationes Jeremiae prophetae" 
      (Paříž 1719)</a>
      a
      <a href="http://books.google.cz/books?id=iDXwOdkUYfEC">
      "Directorium chori sive brevis psalmodiae ratio ad usum presbyterorum
      Congregationis Oratorii D.N.J.C." (Paříž 1753)</a>.
      Zatímco první knížka obsahuje jen lamentace zpívané dříve
      v matutinu Svatého týdne, druhá umožňuje udělat si dosti jasnou
      celkovou představu o chórovém oficiu francouzských oratoriánů 17. stol.
      Proto ji chci krátce představit.
    </p>

    <h5>Typ knihy</h5>
    <p>
      Nejprve je nutné zodpovědět otázku, jaký typ liturgické knihy
      vlastně máme před sebou. Ačkoli tomu název nenasvědčuje,
      po podrobném pročtení celého obsahu knihy soudím, že jde
      o kompletní sbírku liturgického zpěvu k oficiu
      s několika připojenými popěvky ke specifickým obřadům (procesní
      zpěvy ke dnům, na něž je předepsané nějaké žehnání, jako jsou
      Hromnice nebo Květná neděle; dále k výstavu Nejsvětější Svátosti), 
      tedy víceméně "antifonář+procesionál".
    </p>
    
    <h5>Hudební charakteristiky "oratoriánského chorálu"</h5>
    <p>
      Knize je předeslán úvod, cenný tím, že podrobně vysvětluje
      pravidla pro interpretaci dále používané notace, jakož i
      některé zásady tvůrce (tvůrců?) obsažených zpěvů.
      Budiž řečeno narovinu: nemáme před sebou pokleslou edici
      zpěvů z repertoáru gregoriánského chorálu, ale z velké části
      autonomní autorský počin, odpovídající jednak na požadavky
      dobového vkusu (proti gregoriánskému chorálu, resp. proti
      jeho soudobé všeobecné interpretační praxi, se výslovně a
      velmi kriticky vymezuje), jednak na potřeby apoštolsky zaměřené
      kongregace, která sice zachovávala povinnost chóru, ale
      neřadila ji vysoko ve svém žebříčku priorit.
    </p>
    <p>
      (Zde musím otevřeně přiznat, že moje vědomosti o oratoriánech
      jsou velmi omezené - vycházejí v zásadě jen z vybraných článků
      v <a href="http://www.newadvent.org/cathen/">The Catholic Encyclopedia</a>,
      především <a href="http://www.newadvent.org/cathen/11272a.htm">The Oratory of Saint Philip Neri</a> a
      <a href="http://www.newadvent.org/cathen/11274a.htm">French Congregation of the Oratory</a>.
      Kdyby tedy někdo v mém textu našel zásadní faktografické chyby,
      budu rád za upozornění v komentářích.)
    </p>
    <p>
      Jak vyplývá z kritiky v předmluvě, běžná praxe interpretace
      gregoriánského chorálu v době vzniku knihy četla každou notu
      jako notu přesné jednotkové délky a nepřipouštěla žádnou dynamiku,
      žádnou změnu rychlosti či síly.
      Takový zpěv působil ve své době odpudivě. (A působil by, počítam,
      stejně odpudivě i na nás. Dnes obvyklý způsob interpretace chorálu
      je jiný.)
    </p>
    <p>
      "Oratoriánský chorál" zná oproti soudobému gregoriánskému
      noty různé hodnoty. Jeho notace vychází z tehdejší podoby
      notace kvadratické (mj. se píše do čtyřlinkové osnovy a používá
      běžné klíče C a F), má ale dva typy not, které za pomoci doplňkových
      znamének umožňují vyjádřit šest různých hodnot:
    </p>
    
    <div class="obrazek vpravo" style="width: 327px;">
      <img src="/blog/prilohy/directoriumchori_paris1753_typynot.png" alt="noty" style="width: 317px;" />
      <p>
	typy not
      </p>
    </div>

    <ul>
      <li>obdélník - dlouhá
      <ul>
	<li>s předcházejícím křížkem - nejdelší</li>
	<li>s následnou tečkou - prodloužená dlouhá</li>
	<li>prostý - dlouhá</li>
      </ul>
      </li>
      <li>kosočtverec - krátká
      <ul>
	<li>skloněný doleva s následnou tečkou - prodloužená krátká</li>
	<li>skloněný doleva - krátká</li>
	<li>stojící na rohu - nejkratší</li>
      </ul>
      </li>
    </ul>

    <p>
      Převládající typ not (obdélníky/kosočtverce) 
      dále umožňuje určit relativní "tempo" konkrétního zpěvu:
      zda je spíše rychlý či pomalý.
    </p>
    <p>
      Velký důraz je položen na to, aby délka
      noty odpovídala délce slabiky, která se na ni zpívá.
      Je to dobře vidět zejm. v notovaných ukázkách žalmů, kde je vždy
      prodloužena přízvučná slabika.
    </p>

    <h5>Repertoár</h5>
    <p>
      Když jsem studovanou knihu výše označil jako "antifonář a procesionál",
      překvapí její rozsah: má jen něco přes 200 stran. Na první pohled je
      jasné, že jsem se v určení buďto zmýlil, nebo otcové oratoriáni
      přistoupili v rámci repertoáru zpěvů oficia k masivním škrtům.
    </p>
    <p>
      Mnou zvolené označení "antifonář" možná není úplně šťastné. Chtěl jsem
      jím vyjádřit, že kniha obsahuje úplný soubor zpěvů potřebných
      ke slavení oficia. Potud je název vhodný. Nakolik jsme ale zvyklí
      ve středověkých antifonářích nacházet většinou pouze
      notované antifony a responsoria (+ příp. hymny) a v antifonářích 
      moderních často vedle notovaných zpěvů i všechny texty oficia, directorium
      žádným antifonářem není. Je to hubená sbírka zpěvů používaná jako
      doplněk k breviáři. Antifony, podle kterých antifonáře dostaly jméno, 
      neobsahuje téměř žádné.
    </p>
    <p>
      Co tedy obsahuje? Především jsou podány nápěvy žalmů. Po většinu roku 
      se vystačí
      s cca sedmi: jeden je na žalmy kompletáře (s. 17),
      tři další na žalmy ostatních hodinek (s. 1). V nešporách a laudách
      se zpívají první dva žalmy na první nápěv, třetí a čtvrtý
      na druhý a pátý žalm na třetí.
      V matutinu se vždy první z těchto nápěvů použije pro všechny
      žalmy prvního nokturnu, druhý pro žalmy druhého atd.
      V době velikonoční se v nešporách všechny žalmy zpívají na jiný
      nápěv (s. 15).
      Nad to zpěvník obsahuje dva nápěvy vyhrazené kantiku Magnificat
      a užívané v závislosti na liturgické době. Specifikem 
      první verze Magnificat oproti běžné
      psalmodii je, že se kantikum zřejmě nezpívalo po verších,
      ale dílem po strofách (s. 10n). Podobně jsou dále dva nápěvy
      pro Benedictus (s. 50) a jeden pro Nunc dimittis (s. 20).
    </p>
    <p>
      Pro matutinum jsou ve zpěvníku tři nápěvy žalmu Venite 
      (žalm 95/94 zpívaný na úplném začátku hodinky),
      antifony k němu pro liturgické doby a svátky a hymnus Te Deum. 
      Nápěvy žalmu jsou
      velice prosté, bez ozdob, pro žalm Venite, jakožto sólový kus
      kantora, jinak typických. Antifony mají všechny stejnou,
      maximálně jednoduchou, "psalmodickou" melodii.
      Te Deum je zřejmě upravenou verzí soudobého gregoriánského.
    </p>
    <p>
      Pro dobu velikonoční je ve zpěvníku antifona <em>Haec dies</em>
      ("Toto je den, který učinil Hospodin..."; s. 16)
      a závěrečné <em>Benedicamus Domino</em> (nepodařilo se mi ho teď
      znovu najít).
    </p>
    <p>
      Dále najdeme sbírku responsorií
      k malým hodinkám podle dob a svátků (s. 66) 
      a výběr hymnů (s. 85nn a ).
    </p>
    <p>
      Nechybí závěrečné mariánské antifony (s. 22), přičemž
      Salve regina některými rysy připomíná dnes všem katolíkům 
      dobře známý "tonus simplex".
    </p>
    <p>
      A s tím otcové oratoriáni podle všeho, pokud jde o oficium,
      vystačili na celý liturgický rok. Nebo s tím přinejmenším počítal
      autor naší knížky...
    </p>

    <h5>Další zjednodušení oproti plnému chorálnímu oficiu</h5>
    <p>
      Všechny ostatní, výše nezmíněné zpěvy oficia se přednášejí
      recto tono (s. x). Verše a capitula se zpívají rovněž
      recto tono, ale se závěrečným poklesem o tercii.
      To je proti klasické podobě chorálního oficia
      opravdu výrazné zjednodušení.
      Odpadá tím velké množství originálních melodií (antifony),
      stejně jako přednes některých textů na ustálené melodické
      formule (lekce a požehnání matutina, veršíky s neumou, 
      závěrečný verš <em>Benedicamus Domino</em>, ...)
    </p>

    <h5>Shrnutí a hodnocení: Zpívané oficium "s nižší spotřebou"</h5>
    <p>
      Nechci a ani nemohu studovanou knihu hodnotit z hlediska
      krásy obsažených zpěvů.
      Mohu, ale nechci ji hodnotit z hlediska účelnosti uspořádání
      obsahu a didaktické efektivity.
      Podívejme se na ni jako na specifický projekt zpívaného oficia.
    </p>
    <p>
      V kongregaci, pro jejíž potřeby vznikl, byla zachovávána povinnost
      chórové modlitby a bylo živé vědomí, že se oficium má zpívat.
      K tomu přistoupila na jedné straně nechuť ke gregoriánskému chorálu,
      na druhé straně silné pastorační zaměření institutu, které neskýtalo
      dobré podmínky k nácviku a realizaci hudebně náročné podoby
      liturgie. (Pokud jde o liturgii hodin, na níž se měli zpěvem
      podílet sami otcové. Oratoriánská liturgie pro lid byla naopak
      hudebním bohatstvím pověstná, neobešla se však bez pěveckých sborů a
      instrumentálních těles.)
    </p>
    <p>
      Provedené zjednodušení výrazně a ve více rozměrech redukuje
      hudební rozmanitost oficia: 
      ruší rozmanitost "společných nápěvů" ve prospěch zpěvu recto tono; 
      z antifony dělá recitativ namísto melodického prvku;
      snižuje množství nápěvů psalmodie a "matematicky" linkuje pravidla jejich použití.
      Hudební svéráz liturgické doby nebo dne není zcela zrušen,
      je však napříště omezen na hymny, responsoria malých hodinek
      a závěrečnou mariánskou antifonu. (Pokud pomineme několik
      výjimek vztahujících se na Svatý týden, Velikonoční oktáv a celou
      dobu velikonoční.)
    </p>
    <p>
      Takto radikálně zjednodušené zpívané oficium má 
      (už i oproti oficiu využívajícímu třeba jen všechny možnosti 
      zatím dostupného nedělního-svátečního svazku pokoncilního 
      Antiphonale Romanum!) výrazně snížené nároky na učení.
      Do větší části zpěvu se může snadno zapojit i náhodný příchozí,
      objem potřebného nácviku s komunitou je minimální.
    </p>

    <h5>Transposice do našich podmínek</h5>
    <p>
      Není náhodou, že některá společenství dnes používají velice podobný model.
      (Srov. články <a href="/blog.php?clanek=mundeleinpsalter.xml">The Mundelein Psalter</a> a
      <a href="/blog.php?clanek=wyd2011madrid.xml">Denní modlitba církve na WYD v Madridu</a>.)
      Překvapuje mě ale, když se někde toto "laciné" uspořádání
      denní modlitby církve užívá jako slavnostní, "zpívané", k vyzdvižení
      svátků oproti všedním dnům, kdy se oficium recituje.
      Maximálně prostá hudební podoba právě otevírá možnost zpívat
      každý den.
    </p>
    <p>
      Kde se o nějakém podobném redukovaném tvaru zpívaného oficia uvažuje,
      doporučil bych oproti oratoriánskému directoriu vynalézavěji
      pracovat s nápěvy žalmů. Rozhodně jich vzít o něco více (ať už sadu
      chorálních nápěvů, ke které jsem přilnul já, či Korejsovu,
      Olejníkovu, Šmolíkovu nebo jinou) a nepoužívat je se železnou
      pravidelností den za dnem stejně.
      Je možné pevně přiřadit
      jednotlivým žalmům "jejich" nápěvy, jako se to děje
      mj. v mém zpěvníčku <a href="/noty.php#nespornizpevy">Nešporní zpěvy</a>.
      Zrovna tak je ale možné připravit několik schémat používaných
      podle liturgické doby a svátku. 
      ("mezidobí - férie - ranní chvály: k žalmům nápěvy A, C, A, 
      k Benedictus X" apod.)
      Právě tuto možnost považuji za velmi vhodnou.
    </p>
    
  </text>
</clanek>
