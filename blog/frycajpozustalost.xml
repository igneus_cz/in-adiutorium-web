<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Z pozůstalosti Tomáše Fryčaje</titulek>

  <tags>Tomáš Fryčaj</tags>
  <kategorie>Jiné</kategorie>
  <datum>1.12.2016 20:19</datum>
  <!-- <souvisejiciNoty></souvisejiciNoty> -->

  <souhrn></souhrn>

  <text>
    <h3>Neslavný konec jednoho plagiátu</h3>
    <p>
      Začátkem léta jsem zjistil, že na Masarykově univerzitě
      v Brně vznikla nová
      <a href="https://is.muni.cz/th/427761">bakalářská práce</a>
      k osobě <a href="https://cs.wikipedia.org/wiki/Tom%C3%A1%C5%A1_Fry%C4%8Daj">Tomáše Fryčaje</a>,
      zpřístupňující soupis jeho pozůstalosti.
      Vlastní edici historického
      pramene, příliš krátkého, než aby naplnil minimální rozsah
      bakalářské práce, je předeslán životopisný úvod.
      Když jsem do něj nahlédl, zjistil jsem, že autorka v přehledu
      Fryčajova díla pro oddíl věnovaný Katolickému kancionálu
      téměř slovo od slova převzala většinu
      <a href="/blog.php?clanek=lidofic07frycaj.xml">mého staršího článku o nešporách v témže kancionálu obsažených</a>,
      aniž uvedla zdroj.
      Patrně ji vůbec nenapadlo přemýšlet, jestli kancionál
      obsahuje i něco jiného než nešpory, jestli je namístě věnovat
      právě nešporám tolik pozornosti, a jestli jí někdo uvěří,
      že "liturgickou pitvu" jejich obsahu, která se navíc do celku
      práce vůbec nehodí, provedla sama.
    </p>
    <div class="edit">
      <p>
        [EDIT 26. 5. 2019]
        Odkaz na pomlouvanou kvalifikační práci v mezičase
        od zveřejnění článku "zvětral",
        doplňuji proto aktuální odkaz a úplnou citaci:
      </p>
      <p>
        <a href="https://is.muni.cz/th/k43b0/">ŠTEFINOVÁ, Martina:
        <em>Inventár majetku z pozostalosti obřanského farára Tomáša Fryčaja</em></a>,
        Brno 2017.
        Dostupné z:
        &lt;<a href="https://is.muni.cz/th/k43b0/">https://is.muni.cz/th/k43b0/</a>&gt;.
        Bakalářská práce. Masarykova univerzita, Filozofická fakulta.
        Vedoucí práce Dalibor Havel.
      </p>
    </div>
    <p>
      Automatický systém kontroly prací MU, posudek oponentky ani
      vedoucího práce plagiát nezachytil. V tomto ohledu
      tedy překlad do slovenštiny nebyl špatným tahem.
      Byl jsem na vážkách, zda je správné incident vůbec řešit,
      když má daná bakalářská práce podle posudků beztak bídné
      vyhlídky.
      Když jsem však četl, jak oponentka "střílí" právě i do
      "mé" části textu, zželelo se mi článku,
      jenž měl být, špatně pochopený a znetvořený,
      bez fundované obhajoby vystaven drtivé kritice,
      a zjištěné neakademické
      zacházení s prameny jsem na příslušném místě oznámil.
    </p>

    <h3>Z obsahu inventáře</h3>
    <p>
      Výše uvedené ale nic nemění na záslužnosti samotného
      zveřejnění dosud nepublikovaného historického pramene.
      Jde o dva seznamy podmnožin pozůstalosti. V jednom jsou
      sepsány knihy, ve druhém obrazy. Je zajímavé, že zatímco
      soupis obrazů obsahuje odhady ceny a místy poznámky,
      že zemřelý ten který kus odkázal určité osobě nebo instituci,
      inventář knih tyto údaje nemá. Zdá se, že účel
      každého soupisu, resp. zamýšlený další osud sepsaných předmětů,
      byl trochu jiný. Je také otázkou, proč
      byly sepsány (resp. v archivu se dochovaly) právě jen
      knihy a obrazy a ne všechny, nebo alespoň všechny
      hodnotnější předměty z pozůstalosti.
      Snad se počítalo s možností převzetí celých souborů
      nějakou kulturní institucí.
    </p>
    <p>
      Pozůstalostní inventář může o obřanském faráři prozradit nemálo
      zajímavých skutečností. Někdo povolanější z něj jistě
      v budoucnu vytěží cenná upřesnění celkového obrazu Tomáše
      Fryčaje, ale i pohled do intelektuálního, duchovního
      či úředního života kněze dané doby. Já chci dále nabídnout jen
      několik postřehů z oblasti tematického zaměření tohoto webu.
    </p>
    <h4>Hudebniny</h4>
    <ul>
      <li>1. Fryčaj Tomáš: Katolický kancionál, Brno 1835</li>
      <li>25. Kancionál (český), <a href="https://books.google.cz/books?id=EtljAAAAcAAJ">1794</a></li>
      <li>51. Úplná knížka k čtení, k modlení a zpívání (2 díly), Brno 1802</li>
      <li>113. Lese- und Gebethbuch für  Gesangen [titul téměř jistě porušený - správně snad <a href="https://books.google.cz/books?id=4lFhAAAAcAAJ">für Gefangene</a>], Wien 1822</li>
    </ul>
    <p>
      Zde můžeme uzavřít, že Fryčaj zřejmě nebyl žádný hudebník,
      protože všechny jím vlastněné hudebniny jsou zpěvníky.
      Navíc, nakolik je mi známo, převážně nenotované.
      Ovšem u sestavitele významného moravského kancionálu,
      byť by šlo i jen o redakci jeho textové složky,
      bychom jistě čekali větší sbírku starších českých zpěvníků.
    </p>
    <p>
      Č. 1 je osmé vydání Fryčajova Katolického kancionálu.
      O č. 51 se mi zatím nepodařilo zjistit vůbec nic.
    </p>
    <p>
      Pokud je správná výše navrhovaná oprava porušeného titulu č. 113,
      má tato položka zřejmě souvislost s tím, že majitel, než
      se stal farářem v Obřanech, působil mj. ve sféře vězeňství.
      Č. 25 je, nakolik lze soudit podle roku vydání,
      evangelický kancionál Elsnerův. To je nález na jednu stranu
      nesamozřejmý, na druhou stranu však není ani příliš
      překvapivý, protože v soupisu nalezneme i spisy
      Komenského, a to nejen <em>Janua linguarum</em> (č. 140),
      ale i ryze nábožensky zaměřenou <em>Praxis pietatis</em> (č. 31).
      Není vyloučeno, že znalejší oko by našlo i další nekatolická
      díla.
    </p>
    <h4>Knihy liturgické</h4>
    <ul>
      <li>9. <em>Officium defunctorum</em>, 2 exempláře: <a href="http://www.digitalniknihovna.cz/mzk/view/uuid:beb10540-2334-11e2-8579-005056827e52">Brno 1807</a>, <a href="http://www.digitalniknihovna.cz/mzk/view/uuid:5e3bea80-2335-11e2-85be-005056827e51">Olomouc 1818</a></li>
      <li>56. <em>Triduum sacrum celebratum
      in sodalitate majore academica ...</em>, <a href="https://books.google.cz/books?id=RDrAMwEACAAJ">Wien 1770</a></li>
      <li>79. <em>Breviarium ecclesiasticum, pars <a href="https://books.google.cz/books?id=nKkWyRbsfNcC">aestivalis</a> et <a href="https://books.google.cz/books?id=MkKzz-SehhAC">hiemalis</a></em>, Embricae 1726 (kompletní breviář ve dvou svazcích; Bohatta, č. [2217])</li>
      <li>82. <em>Horae diurnae Breviarii Romani</em>, Antwerpen 1717 (Bohatta nezná - nejbližší u něj zaznamenané vydání téhož titulu z téhož místa je r. 1714, č. [669])</li>
    </ul>
    <p class="petit">
      Bohatta Hans: <em>Bibliographie der Breviere 1501-1850,</em> 2. unveränderte Aufl., Stuttgart - Nieuwkoop: Hiersemann - de Graaf 1963.
    </p>
    <p>
      Č. 9 jsou samostatně vydané hodinky za zemřelé
      ve dvou exemplářích různého roku a místa vydání, oba jsou však
      domácího - moravského - původu.
      To, že se separát s hodinkami za zemřelé tiskl opakovaně
      na různých místech <em>v rámci Moravy</em>, svědčí o tom,
      že šlo o texty často a široce užívané.
      Důvody uvádí úvod obou vydání (viz odkazy výše):
      oficium za zemřelé se říkalo nebo zpívalo při pohřbech,
      textů bylo potřeba dost pro zpěváky. Standardní knihou
      pro pohřební zpěvy by byla moravská provinční
      agenda, ta však byla značně objemná, těžká, a v neposlední řadě
      drahá.
    </p>
    <p>
      Č. 56, obsahující obřady Velikonočního tridua,
      je podle titulu zřejmě spíše zpěvníček nebo neoficiální příručka
      pro účastníky bohoslužeb než liturgická kniha v přísném
      slova smyslu.
    </p>
    <p>
      Č. 82 jsou <em>Horae diurnae Breviarii Romani</em>,
      tedy jednosvazkové vydání breviáře, neobsahující texty
      matutina. (Moderní obdobou je breviář bez modlitby se čtením,
      tkzv. "laický".)
      Dané vydání sice Bohattova bibliografie nezachytila,
      ale odjinud víme,
      že existovalo, a některé exempláře se dochovaly do dnešních dnů.
      Z <a href="https://www.abebooks.co.uk/Horae-Diurnae-Breviarii-Romani-decreto-Sacrosancti/15060168784/bd">fotodokumentace a popisu</a>
      je zřejmé, že to byl krásný tisk v neméně krásné vazbě.
    </p>
    <p>
      <em>Breviarium ecclesiasticum</em> pod č. 79
      není standardní potridentský
      římský breviář, ale jeden z neogalikánských breviářů reformních.
      Zásady jeho sestavení jsou shrnuty v předmluvě otištěné
      na začátku každého svazku.
      Namátkou jmenujme snahu o tematickou distribuci žalmů
      do jednotlivých hodinek (oproti převážně kontinuálnímu
      cursu římského breviáře), o co možná málo proměnlivou
      délku psalmodie v jednotlivých hodinkách,
      zjednodušení kalendáře, vypuštění méně významných svátků,
      či recitaci feriálních žalmů i o svátcích.
      Některé z těchto zásad se později uplatnily i při breviářových
      reformách 20. století.
    </p>
    <p>
      Za zmínku stojí, že obě vydání breviáře, která Fryčaj na konci
      života vlastnil (dvousvazkový kompletní breviář
      a <em>Horae diurnae</em>),
      pocházejí z doby poměrně dlouho
      před jeho narozením (1759). I vzhledem k tomu, že neogalikánské
      <em>Breviarium ecclesiasticum</em> není standardní římský
      breviář, jaký byl v té době kněz olomoucké církevní provincie
      povinen užívat, se zdá, že breviáře zachycené
      v pozůstalostním soupisu majitel neměl k denní potřebě,
      ale byly součástí jeho knihovny ze zájmu historického,
      jako památka na předchozího majitele, nebo, a to snad nejspíše,
      ze zájmu bibliofilského, jako krásné tisky.
      To by ovšem znamenalo, že soupis nezachycuje všechny knihy,
      které obřanský farář vlastnil a užíval, protože bychom museli
      počítat alespoň ještě s jedním vydáním breviáře, užívaným
      pro každodenní modlitbu.
    </p>
    <p>
      Je příznačné, že v osobním vlastnictví kněze nacházíme
      liturgické knihy v zásadě jen pro "soukromý" provoz - tedy
      pro oficium. Knihy nezbytné pro veřejně vykonávané liturgické
      funkce byly, stejně jako je to obvyklé dnes,
      součástí vybavení farnosti.
      Z tohoto rámce vyčnívají jen hodinky za zemřelé, které, ač
      formálně také patří k oficiu, je třeba nahlížet spíš
      jako příručku k pohřebním obřadům.
    </p>
    <h3>Závěrem</h3>
    <p>
      Náš výběrový pohled na soupis pozůstalosti Tomáše Fryčaje
      nepřinesl žádné významnější zjištění, zato vyvolává nejednu
      otázku:
      kam se poděly knihy, jejichž existenci musíme předpokládat,
      ale v seznamu nejsou zachyceny?
      Co (pokud něco) znamenají v dané době Komenského
      spisy nebo evangelický kancionál v knihovně moravského
      katolického kněze? Do jaké míry Katolický kancionál čerpá
      ze starších sbírek? Ze kterých?
      (Tyto otázky jsou jistě dávno
      zodpovězeny a akorát ilustrují nízkou míru autorovy obeznámenosti
      s dobovým kontextem a související literaturou.)
      Jak široká byla recepce potridentských reformních
      breviářů na území Habsburské monarchie?
    </p>
  </text>
</clanek>
