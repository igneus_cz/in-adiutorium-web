<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Slavnost Panny Marie Karmelské na slánském karmelu</titulek>

  <tags>Panna Maria Karmelská, Slaný, kláštery, bosí karmelitáni</tags>
  <kategorie>Ze života</kategorie>
  <datum>17.7.2020 02:36</datum>
  <!-- <souvisejiciNoty></souvisejiciNoty> -->

  <souhrn></souhrn>

  <text>
    <p>
      Poté, co jsem jako gymnasista zjistil, že v nedalekém
      Slaném je klášter bosých karmelitánů, často a rád jsem tam
      z Kladna jezdil (za méně příznivého počasí autobusem,
      od jara do podzimu na kole) a pravidelné, návštěvníkům
      zvenčí přístupné nešpory po páteční adoraci byly mou úplně
      první zkušeností s (částečně) zpívanou liturgií hodin.
      Tehdy ovšem hudební stránka oficia ještě nebyla něco,
      na čem by mi zvlášť záleželo.
    </p>
    <p>
      Když jsem před pár dny na Facebooku zachytil informaci,
      že součástí oslav řádové titulární slavnosti
      Panny Marie Karmelské ve Slaném budou mj. "slavnostní nešpory",
      rozhodl jsem se (když teď zase bydlím celkem nedaleko)
      po letech se vrátit na místo činu a zjistit, čemu tam dnes
      říkají "slavnostní nešpory". Nešpory největšího řádového
      svátku patrně lze považovat za ukázku nejvyšší míry
      slavnostnosti, jaké se v daném místě lze nadát.
      Také jsem byl zvědavý na karmelitánské texty svátku
      (které, k mé velké žalosti, nejsou veřejně dostupné).
    </p>
    <p>
      Nešpory uzavíraly poměrně dlouhý blok programu:
      po poutní mši (kterou jsem zmeškal) následovala přednáška
      o Panně Marii ve světle čtyř evangelií,
      pak obřad přijetí nových členů do škapulířového bratrstva,
      a úplně nakonec ony nešpory, na které tak celkem pochopitelně
      zůstal již jen zlomek lidu.
      Po ceremoniální stránce to byly nešpory úplně prosté - žádný vstupní
      průvod, žádné okuřování oltáře při Magnificat,
      žádná liturgická roucha.
      Karmelitáni rozesazení (spolu s pár světskými kněžími
      a bez řeholnic, vč. přítomných karmelitek)
      po obou stranách presbytáře neměli chórové pláště.
      Začátek byl vysloveně neformální, když se mezi návštěvníky
      distribuovalo omezené množství knížek s řádovými texty
      a domlouvala se čísla stránek;
      český hymnus byl po pár taktech uťat
      a nahrazen <a href="http://www.karmel.cz/index.php?option=com_content&amp;view=article&amp;id=284:flos-carmeli&amp;catid=49:texty-z-tradice&amp;Itemid=182">antifonou</a>/<a href="https://en.wikipedia.org/wiki/Flos_Carmeli">sekvencí</a>/hymnem <em>Flos Carmeli</em>.
    </p>
    <p>
      Ani po hudební stránce nešpory nevybočovaly z toho,
      co z dřívějška z kláštera znám.
      Zpíval se hymnus, žalmy (chorální nápěvy I.D2, VI.F, III.h
      - s recitací na c),
      responsorium (nechorální nápěv místní provenience),
      Magnificat (IV.E se slavnostní mediací, stejně jako o všech
      nešporách, jimž jsem kdy ve Slaném byl přítomen)
      a Otčenáš.
      Zpívalo se na dva chóry a bez jakéhokoli doprovodu
      (to zmiňuji proto, že dřív bývalo zvykem i při všednodenních
      pátečních nešporách některé části doprovázet na citeru).
    </p>
    <p>
      Bylo mi líto recitovaných antifon, zvlášť o tak velkém svátku.
      V té souvislosti stojí za zmínku, že většina antifon druhých
      nešpor slavnosti byla posbírána z různých míst Denní modlitby
      církve, nejsou to tedy texty pro svátek zvlášť psané
      a nápěvy pro ně již existují.
      Texty jsem neměl před sebou, ale nakolik jsem je dobře
      slyšel a zapamatoval si je,
      k prvnímu žalmu a NZ kantiku se říkaly antifony charakteru
      společných textů o Panně Marii (jedna víceméně souhlasí
      s antifonou k tercii z těchto společných textů, druhou
      jsem si nezapamatoval, ale byla mi povědomá a nebyla-li
      vzata přímo ze společných textů, tak nejspíš z nějakého
      mariánského svátku);
      s druhým žalmem pak antifona, která je ve "světském" breviáři pro památku
      Panny Marie Karmelské k Magnificat.
      Jen antifona k Magnificat byla vlastním textem v silném
      slova smyslu - poměrně dlouhý slavnostní text
      typu <em>"Dnes ... dnes ... dnes ..."</em>
      (srov. mj. antifony k Magnificat druhých nešpor
      slavností Narození Páně, Zjevení Páně,
      v předkoncilním breviáři též antifonu téhož určení
      pro slavnost sv. Petra a Pavla).
    </p>
    <p class="petit">
      Bosí karmelitáni nepatří k řádům proslulým pěstováním
      chorálního oficia, ale řádové proprium s nápěvy vydané
      měli a zájemce ho dnes najde online:
      <a href="https://media.musicasacra.com/pdf/carmelite/proprium.pdf"><em>Proprium missarum et officiorum Ordinis Carmelitarum Discalceatorum,</em> Romae 1959</a>.
      Svátek Panny Marie Karmelské začíná na s. (43)
      a má pěkné vlastní texty, které také částečně čerpají
      z (tehdejší podoby) společných textů o Panně Marii,
      ale zdaleka ne tak výrazně.
      Hodně jich je věnováno symbolice hory Karmel.
      Čekal jsem, že tam najdu předlohu oné dnešní antifony
      <em>"Dnes,"</em> ale nenašel jsem.
      Podle všeho je to pokoncilní kreace.
    </p>
    <p>
      Závěr: ta část mého já, která miluje věci, na které je spolehnutí,
      a které jsou i po letech při (dobrém, osvědčeném) starém,
      může být spokojená: zpívané nešpory u slánských karmelitánů
      byly tak, jak jsem je znával.
      Ale když byly předem inzerovány nešpory "slavnostní",
      dle mého soudu to byla klamavá reklama.
    </p>
  </text>
</clanek>
