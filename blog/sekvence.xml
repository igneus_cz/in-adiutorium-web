<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Sekvence v denní modlitbě církve</titulek>

  <tags>
    hymny, sekvence, červený hymnář, zelený hymnář,
    Liturgia horarum,
    křestní nešpory, pražský ritus, Jistebnický kancionál,
    chorál český
  </tags>
  <kategorie>Texty</kategorie>
  <datum>2.6.2024 22:08</datum>
  <souvisejiciNoty>sekvence</souvisejiciNoty>

  <souhrn></souhrn>

  <text>
    <p class="petit">
      (Článek jsem původně psal k vydání na Obrácení svatého Pavla,
      pak ho odložil na Uvedení Páně do chrámu, pak na Letnice,
      ale jak už to tak často bývá, dopsán je a vychází nakonec až
      po Navštívení Panny Marie, kdy je jeho prakticky zaměřená
      část pro letošek passé.)
    </p>
    <h3>Předtridentský pražský breviář</h3>
    <p>
      <a href="https://books.google.cz/books?id=oRVkAAAAcAAJ&amp;hl=cs&amp;pg=PT126#v=onepage&amp;q&amp;f=false">Na nečíslovaném listu</a>
      na konci pražského breviáře vytištěného v Norimberku r. 1502
      se mezi jiným dodatkovým materiálem nečekaně nacházejí
      dvě <em>sekvence,</em> tedy zpěvy určené pro mši a v breviáři nečekané:
      <a href="https://cantusindex.org/id/ah50269"><em>Dixit Dominus ex Basan</em></a>
      ke svátku Obrácení svatého Pavla
      a <a href="https://cantusindex.org/id/ah53099"><em>Concentu parili</em></a> k Očišťování Panny Marie.
      Proč byly takto k breviáři přilepeny? A proč právě tyto dvě?
      Jistě nejde např. o provizorní způsob distribuce nově zaváděných
      mešních textů do farních kostelů, protože obě
      byly v Čechách na začátku 16. stol. dávno standardní součástí
      repertoáru.
    </p>
    <p>
      Řešení záhady se nachází v rubrice ve formuláři druhého dotčeného
      svátku, <a href="https://books.google.cz/books?id=oRVkAAAAcAAJ&amp;hl=cs&amp;pg=PR271#v=onepage&amp;q&amp;f=false">f. 271r</a>.
      <!-- v BP1517 f. 133r -->
      Středověké pražské rubriky příležitostně věnují pozornost
      nejen tomu, aby se pokud možno (i kalendářním kolizím navzdory)
      slavily všechny svátky,
      ale také aby kvůli kalendářní nepřízni nezůstávaly nezpívány
      některé důležitější kusy chorálního repertoáru.
      Což platí právě i pro naše dvě sekvence.
      V době předpostní a postní se sekvence ve mši nezpívaly,
      a proto rubrika stanoví, aby se v letech,
      kdy Obrácení svatého Pavla nebo Očišťování Panny Marie
      padne už do období po Devítníku, jejich sekvence
      zazpívaly alespoň v nešporách místo hymnu.
      Redakce breviáře z r. 1502 pak se postarala, aby
      i ten, kdo se oficium modlí mimo chór, měl příslušné texty
      po ruce a neměl výmluvy onu rubriku nedodržet.
    </p>
    <p>
      Jiným případem je
      velikonoční sekvence <em>Victimae paschali laudes</em>, která
      se v některých diecésích zpívala kromě mše také v rámci
      "křestních nešpor" ve velikonočním oktávu (spolu s dalšími
      zpěvy - kyrie, graduale, aleluja - které rovněž normálně patří
      k repertoáru mše, nikoli oficia).
      Pražský breviář,
      kodifikující primárně liturgii svatovítské katedrály,
      <a href="/blog.php?clanek=krestninespory3.xml">v křestních nešporách se sekvencí nepočítá</a>,
      ale to, že tato praxe i do Čech alespoň okrajově pronikla,
      máme dosvědčeno
      v <a href="http://www.manuscriptorium.com/apps/index.php?direct=record&amp;pid=AIPDIG-NMP___II_C_7______4347P97-cs">Jistebnickém kancionálu</a>
      (rubrika se řádem velikonočních nešpor f. 92r, notovaná sekvence f. 92v).
    </p>
    <h3>Římský breviář</h3>
    <p>
      S přijetím tridentských liturgických knih
      byl bohatý středověký repertoár sekvencí vyřazen z užívání
      a napříště většina katolického Západu ve mši zpívala
      jen jejich
      <a href="https://www.newliturgicalmovement.org/2022/05/what-really-happened-to-sequences.html">velmi omezený římský výběr</a>:
    </p>
    <ul>
      <li><em>Victimae paschali laudes</em> (Zmrtvýchvstání Páně)</li>
      <li><em>Veni sancte Spiritus</em> (Seslání Ducha svatého)</li>
      <li><em>Lauda, Sion, Salvatorem</em> (Boží Tělo)</li>
      <li><em>Dies irae</em> (za zemřelé)</li>
    </ul>
    <p>
      V oficiu pak se sekvence nezpívaly žádné.
      Až <a href="https://www.newadvent.org/cathen/14151b.htm">v 18. stol.</a>
      byl na celou církev rozšířen svátek Sedmi bolestí Panny Marie,
      slavený v pátek před Květnou nedělí.
      Jako hymny byly pro jeho formulář použity tři díly
      (pro matutinum, ranní chvály a nešpory)
      sekvence <em>Stabat Mater dolorosa</em>,
      která se zpívala i ve mši svátku.
    </p>
    <!--
        Malina 358n o Stabat Mater mluví bez dalšího jako o hymnu;

        Bäumer 524 jen zmiňuje zavedení svátku a repertoáru se nevěnuje;
        354 letmá zmínka, že Stabat Mater je sekvence a figuruje
        i v oficiu
    -->
    <h3>Liturgia horarum</h3>
    <!--
      Salve dies bez komentáře
      https://archive.org/details/HymniInstBR1968/page/n41/mode/2up
      TDH s. 41

      Lux iucunda bez komentáře
      https://archive.org/details/HymniInstBR1968/page/n133/mode/2up
      TDH s. 131

      Dies irae
      https://archive.org/details/HymniInstBR1968/page/n313/mode/2up
      TDH s. 70

      Stabat Mater
      https://archive.org/details/HymniInstBR1968/page/n235/mode/2up
      TDH s. 214
      naznačuje, že zařazení do breviáře je víceméně záchranou repertoáru
    -->
    <p>
      Liturgická reforma po Druhém vatikánském koncilu
      sekvence jako žánr zpěvů mešního propria dále odsunula na okraj:
      povinné zůstávají jen dvě, pro slavnost
      Zmrtvýchvstání Páně a Seslání Ducha svatého;
      sekvence <em>Dies irae</em> je z mešního repertoáru
      vyřazena úplně, zbylé dvě - božítělová a pro památku Panny Marie
      Bolestné - jsou napříště nepovinné.
    </p>
    <p class="petit">
      (<em>Ordo lectionum missae</em>,
      <a href="https://archive.org/details/OLM1969/page/340/mode/2up">editio typica 1969, s. 341-342</a>;
      <a href="https://archive.org/details/OLM1981/page/448/mode/2up">editio typica altera 1981, s. 449-451</a>)
    </p>
    <p>
      Několik sekvencí však bylo nově
      pojato ve funkci hymnů do reformovaného breviáře.
      V notovaném hymnáři (<em>Liber hymnarius</em>, Solesmes <sup>1</sup>1983, <sup>2</sup>2019)
      na první pohled vyčnívají svou hudební formou:
      zatímco hymnus je strofická píseň, sekvence se obvykle
      skládá z dvojic paralelních frází, přičemž každá dvojice
      má jiný nápěv.
    </p>
    <table>
      <thead>
        <tr>
          <td>incipit</td>
          <td>příležitost</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><a href="http://cantusindex.org/id/ah54154"><em>Lux iucunda, lux insignis</em></a></td>
          <td>Seslání Ducha svatého, MČ</td>
        </tr>
        <tr>
          <td><a href="http://cantusindex.org/id/ah54178"><em>Dies irae</em></a></td>
          <td>férie 34. týdne v mezidobí, MČ, RCH, N (části)</td>
        </tr>
        <tr>
          <td><a href="http://cantusindex.org/id/ah54146"><em>Salve dies, dierum gloria</em></a></td>
          <td>žaltář, neděle sudých týdnů, MČ</td>
        </tr>
        <tr>
          <td><a href="http://cantusindex.org/id/ah54201"><em>Stabat Mater dolorosa</em></a></td>
          <td>Panny Marie Bolestné (15. září), MČ, RCH, N (části)</td>
        </tr>
      </tbody>
    </table>
    <p class="petit tac">
      MČ = modlitba se čtením,
      RCH = ranní chvály,
      MU = modlitba uprostřed dne,
      N = nešpory<br/>
      (stránky odkazují do <em>Liber hymnarius</em>)
    </p>
    <p>
      V případě <em>Dies irae</em> jde o záchranu populárního
      textu, při reformě mešní liturgie zcela vyřazeného z repertoáru,
      jeho obsazením do nové liturgické
      role.
      (<a href="https://forum.musicasacra.com/forum/discussion/comment/252349#Comment_252349">Lentini A.: <em>Te decet hymnus</em></a>, Vatikán 1984, s. 70)
      Dlužno dodat, že je to role nově pro něj vytvořená:
      zatímco eschatologický ráz byl textům závěru
      liturgického roku vlastní i před reformou,
      to, že poslední týden má zvláštní hymny, je podle všeho inovace
      bez jakékoli opory v dosavadní liturgické tradici.
    </p>
    <p>
      <em>Stabat Mater</em> se zdá být podobný případ.
      Ze mše sice, narozdíl od <em>Dies irae</em>, vypovězena není,
      ale počítá se s tím, že ve všednodenní mši bez zpěvu bude dlouhá
      nepovinná sekvence obvykle spíš vynechávána, a jako hodnotnému
      a populárnímu kusu
      liturgické poesie je jí zjednáno nové pevné místo v oficiu.
      Zároveň se tím navazuje na repertoár zrušeného svátku
      Sedmi bolestí Panny Marie v závěru doby postní, o němž výše.
      (Srov. Lentini: <em>Te decet hymnus</em>, s. 214)
    </p>
    <p>
      Zbylé dvě sekvence jsou do hymnáře vybrány z předtridentského
      repertoáru,
      snad do jisté míry i z nouze o texty, když byl pokoncilní hymnář
      koncipován jako rozsáhlejší než kterýkoli jednotlivý liturgický
      hymnář před ním a zároveň byly na texty vybírané
      do modernisovaného oficia kladeny specifické nároky.
      (Srov. <a href="https://archive.org/details/HymniInstBR1968"><em>Hymni instaurandi Breviarii Romani</em></a>, Vatikán 1968, Introductio, čl. 48)
    </p>
    <h3>Denní modlitba církve / červený hymnář</h3>
    <p>
      Pár česky přebásněných sekvencí najdeme i v hymnáři Denní
      modlitby církve. Narozdíl od latinského hymnáře
      tu nijak nevyčnívají svou hudební formou,
      protože jsou všechny upravené jako strofické písně.
    </p>
    <p>
      O památce Panny Marie Bolestné a ve 34. týdnu v mezidobí,
      kdy <em>Liturgia horarum</em> distribuuje díly sekvence
      na všechny hlavní hodinky, má český breviář z příslušné sekvence
      vždy jen jeden vybraný výňatek a ostatní hodinky pokrývá jinými
      písněmi. Zbylé dvě sekvence z latinského breviáře reprodukovány
      nejsou, protože hymnář čerpá primárně z existujícího písňového
      repertoáru, do kterého nepronikly.
      (<em>Salve dies, dierum gloria</em>
      je podle <a href="http://www.musmed.fr/CMN/tableAH.htm"><em>Analecta hymnica</em></a> 54
      součástí specificky francouzského repertoáru a mimo zemi původu
      se nerozšířila;
      <em>Lux iucunda, lux insignis</em>
      sice je doložena i v bohemikálních pramenech, ale nepatří tu
      k široce rozšířeným.)
    </p>
    <p class="petit">
      Rozšíření sekvencí v rukopisech českého původu
      bylo v nedávných letech mapováno v jednom z podprojektů projektu
      <a href="https://www.smnf.cz/">"Staré mýty, nová fakta"</a>.
      Výsledkem je databáze
      <a href="http://www.hymnologica.cz/sequences">Index Sequentiarum Bohemiae Medii Aevi</a> -
      a ovšem i to, že jsou teď v <a href="http://cantusindex.org/">CantusIndexu</a>,
      kam je tato integrována, české prameny sekvencí značně
      nadreprezentované.
    </p>
    <p>
      Jedna sekvence je naopak v Denní modlitbě církve
      oproti latinskému breviáři přidána:
      o nešporách svátku Navštívení Panny Marie
      se zpívá překlad sekvence pražského arcibiskupa Jana z Jenštejna,
      který je daného svátku původcem a zároveň autorem jeho
      prvotního liturgického repertoáru.
    </p>
    <table>
      <thead>
        <tr>
          <td>incipit</td>
          <td>latinská předloha</td>
          <td>příležitost</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Den hněvu ten, ten hrozný den</td>
          <td><em>Dies irae</em></td>
          <td>férie 34. týdne v mezidobí, MU</td>
        </tr>
        <tr>
          <td>Stála Matka žalostivá</td>
          <td><em>Stabat Mater dolorosa</em></td>
          <td>Panny Marie Bolestné, N</td>
        </tr>
        <tr>
          <td>Sluší se, by v tuto chvíli</td>
          <td><a href="http://cantusindex.org/id/ah48391"><em>Decet huius cunctis horis</em></a></td>
          <td>Navštívení Panny Marie, RCH</td>
        </tr>
      </tbody>
    </table>
    <h3>Zelený hymnář</h3>
    <p>
      I zelený hymnář reprodukuje dvě sekvence z latinského breviáře
      a přidává k nim dvě další.
      Ani tady nechybí (v jiném překladu)
      sekvence <em>Stabat Mater</em>, přičemž je
      podána v úplnosti, distribuovaná do jednotlivých hodinek
      jako v latinském breviáři.
      Pro ranní chvály Navštívení Panny Marie se opět sahá
      pro (jinou) sekvenci Jana z Jenštejna.
      Pro slavnost Seslání Ducha svatého se nabízí k modlitbě se čtením
      sekvence odpovídající latinskému breviáři a nadto k ranním
      chválám <em>Veni Sancte Spiritus</em>.
      (Tj. stejná sekvence, kterou téhož rána uslyší
      nebo budou zpívat všichni, kdo se zúčastní nedělní mše.
      Takové zdvojení se mi zdá málo vhodné, ale určité historické
      precedenty má - viz výše o <em>Stabat Mater</em>
      a o <em>Victimae paschali laudes</em>.)
    </p>
    <table>
      <thead>
        <tr>
          <td>incipit</td>
          <td>latinská předloha</td>
          <td>příležitost</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Svaté světlo, vzácný plamen</td>
          <td><em>Lux iucunda, lux insignis</em></td>
          <td>Seslání Ducha svatého, MČ</td>
        </tr>
        <tr>
          <td>Svatý Duchu, sestup k nám<br/>(nebo) <a href="https://kancional.cz/422A">Přijď, ó Duchu přesvatý</a></td>
          <td><a href="https://cantusindex.org/id/ah54153"><em>Veni Sancte Spiritus</em></a></td>
          <td>Seslání Ducha svatého, RCH</td>
        </tr>
        <tr>
          <td>Zdrávas, Matko Slova spásná</td>
          <td><a href="https://cantusindex.org/id/ah48392"><em>Ave, Verbi Dei parens</em></a></td>
          <td>Navštívení Panny Marie, RCH</td>
        </tr>
        <tr>
          <td>Nuže, Matko, lásky zdroji; Stála Matka odhodlaně; Ať mě každý bol s ním sblíží</td>
          <td><em>Stabat Mater dolorosa</em></td>
          <td>Panny Marie Bolestné, MČ, RCH, N (části)</td>
        </tr>
      </tbody>
    </table>
    <p>
      Pro <em>Veni Sancte Spiritus</em> má hymnář text
      z větší části souhlasící s mešním lekcionářem
      a jako alternativu nabízí verzi z kancionálu;
      <em>Stabat Mater</em> se podává jen podle kancionálu
      a na verzi z lekcionáře se nehledí.
    </p>
    <h3>Jak české sekvence zpívat</h3>
    <p>
      Když upozorňujeme na to, že některé hymny Denní modlitby
      církve jsou ve skutečnosti zakuklené sekvence,
      na webu věnovaném hudební stránce oficia se nelze vyhnout
      otázce, zda je možné a vhodné tyto také zpívat v příslušné
      hudební formě.
      Rozhodl jsem se pro "průzkum bojem":
      vybral jsem obě sekvence Jenštejnovy a svatodušní <em>Lux iucunda</em>
      (t.j. takové, kde překlad napodobuje formu předlohy a zároveň nejde
      o texty široce známé a běžně zpívané)
      a <strong><a href="/noty#sekvence">opatřil jsem je nápěvem latinské předlohy</a></strong>,
      podle potřeby upraveným, kde nevycházely slabiky, nebo kde se
      s textem špatně snášel.
      Jestli to je zpívatelné a zpívání-hodné musí posoudit
      čtenář sám. Mně se výsledek zamlouvá víc, než jsem původně čekal.
    </p>
    <p>
      Každopádně naše sekvence jsou zpěvy výrazně náročnější
      než běžné hymny, a to nejen tím, že každé dvojverší
      má jiný nápěv, ale i rozsahem, melismaty a skoky.
      Sváteční repertoár pro zdatné zpěváky, ne písně pro lidový zpěv.
      Z toho lze vyjmout onu svatodušní sekvenci, která je krátká a
      snadná a s trochou nácviku by měla být v možnostech každého
      shromáždění, které dobře vychází s běžným repertoárem
      červeného hymnáře.
    </p>
    <p>
      Sluší se upozornit na to, že sekvence <em>Stabat Mater</em>,
      která má za sebou delší historii v římském breviáři,
      se v oficiu nezpívala na svůj nápěv obvyklý ze mše,
      ale (s ohledem na to, že její sloky mají pravidelnou strukturu)
      jako strofická píseň s jediným nápěvem pro všechny sloky,
      jak je to obvyklé u hymnů
      (<a href="https://archive.org/details/Antiphonarius1912/page/567/mode/2up">AR1912 s. 568</a>;
      srov. též <a href="https://books.google.cz/books?id=qtVDQ122UxEC&amp;hl=cs&amp;pg=PA259#v=onepage&amp;q&amp;f=false">Řezno 1882</a>,
      <a href="https://books.google.cz/books?id=pRzS_AIJW3IC&amp;hl=cs&amp;pg=PA876#v=onepage&amp;q&amp;f=false">Antverpy 1773</a>).
      <!-- mešní nápěv LU s. 1634 (v pdf s. 1874) -->
      Když se sekvence stává součástí oficia, přijímá
      (střízlivou, snadnou/lidovou) hudební formu strofické písně,
      přiměřenou dané liturgické funkci.
    </p>
    <p>
      Naproti tomu pokoncilní latinský hymnář (<em>Liber hymnarius</em>)
      právě i pro <em>Stabat Mater</em> podává primárně obvyklý nápěv
      užívaný pro mešní sekvenci, písňový
      se nabízí až jako druhá možnost. Pro ostatní hymny-sekvence
      pak se žádná zjednodušení hudební formy nenabízejí.
      Obě sekvence vybrané z předtridentského repertoáru
      (<em>Lux iucunda</em> a <em>Salve dies</em>)
      jsou ovšem určeny pro modlitbu se čtením, kterou
      "vyjma specialistů obvykle nikdo nezpívá"
      (Lentini: <em>Te decet hymnus</em>, s. XXIX).
    </p>
    <p>
      Tedy: jak ten, kdo by chtěl s pomocí našeho nového notového
      materiálu oficium dotčených svátků ozdobit
      sekvencí v příslušném hudebním hávu
      (za cenu toho, že hymnus nejspíš bude vyňat z lidového zpěvu
      a svěřen specialistům),
      tak ten, kdo zůstane při jednodušším, hymnovitém hudebním tvaru,
      se může odvolávat na nějaký liturgický precedens.
      U <em>Svaté světlo, vzácný plamen</em> tato otázka samozřejmě
      nevyvstává, jelikož má sloky proměnlivé délky
      a jinak než jako sekvence, s nápěvem, který nepravidelné
      formě textu činí zadost, se zpívat nedá.
    </p>
    <h3>Souhrn</h3>
    <p>
      Pokoncilní reforma breviáře pojala mezi hymny i omezené
      množství sekvencí - tedy textů, jejichž původní liturgické určení nebylo
      pro oficium, ale pro mši. Stejný jev je možné pozorovat
      i v obou oficiálních českých hymnářích.
      Takové užívání mešního repertoáru v oficiu
      není úplně  bez historických precedentů,
      přičemž několik příkladů skýtají i domácí liturgické prameny.
      Opatrně upozorňujeme na možnost některé z těchto sekvencí
      i v národním jazyce zpívat na nápěvy odpovídajícího
      druhu, od nápěvů hymnů charakteristicky odlišné.
    </p>
  </text>
</clanek>
