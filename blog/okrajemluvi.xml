<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Okraje stránek mluví</titulek>
  
  <tags></tags>
  <kategorie>Ze života</kategorie>
  <datum>22.12.2012 19:55</datum>
  <!-- <souvisejiciNoty></souvisejiciNoty> -->
  
  <souhrn>
    Pro liturgika jsou obzvlášť vzrušující poznámky ve starých liturgických
    knihách. ...
    Co se dočteme na okrajích stránek a mezi řádky?
  </souhrn>
  
  <text>
    <p>
      Nemám rád, když se čmárá do knížek. Považuji to za barbarství.
      (Není to přesvědčení na základě úvahy, ale preracionální opce. 
      I když <em>vím</em>, že v řadě situací je psaní a podtrhávání v knížce
      velice rozumné, efektivní, ekonomické, morálně nezávadné, ...
      mně se to prostě <em>nelíbí</em>.)
      Na druhou stranu ale poznámky a jiné čmáranice na okrajích stránek
      svědčí o tom, že knihu někdo používal, někdy o něm prozrazují zajímavé
      věci a případně umožňují i jakousi "komunikaci" přes propast
      řady generací. 
    </p>
    <p>
      Pro liturgika jsou obzvlášť vzrušující poznámky ve starých liturgických
      knihách. Liturgická kniha (zejména od unifikace latinské liturgie
      po Tridentském koncilu) nutně obsahuje jen základní všude platné
      schéma liturgických
      úkonů. Nás ale kromě schématu zajímá také to, jak bylo uváděno
      v život. Nejen, co říkají liturgické texty a rubriky např. o půlnoční
      mši slavnosti Narození Páně, ale také, jak se v určité době na určitém
      místě slavila.
    </p>
    <p>
      V <a href="/blog.php?clanek=slibychyby.xml">posledním článku</a> 
      jsem letmo zmínil knížečku <em>Officium in die Nativitatis D.N.J.C.</em> 
      (Ratisbonae: Pustet 1890), která nedávno přibyla do mé sbírky
      zpěvníků k oficiu. Co se dočteme na okrajích jejích stránek a mezi řádky?
    </p>
    <p>
      Na předsádce je kromě antikvářské poznámky rozmáchlá čmáranice
      barevnou pastelkou. Domnívám se, že by to mohlo být jedno slovo
      ledabyle napsané psacím písmem, které neovládám (např. psacím švabachem).
      Na to, aby mohlo být dětskou kresbou,
      vypadá příliš "písmovitě", cílevědomě a málo důkladně.
      Snad je to podpis majitele.
      Na titulní stránce nahoře je krasopisně červeným inkoustem vyvedená
      zkratka "K 15.", která se zdá svědčit pro zařazení do nějakého
      souboru - buďto knihovny, nebo vybavení kostela.
    </p>
    <p>
      Dále najdeme řadu vpisků přímo v liturgických textech.
      Ty spadají v zásadě do dvou druhů: přednesové značky (zanesené do textu
      pastelkami) a poznámky k průběhu slavení (tužkou na okrajích stránek).
      Výmluvný je fakt, že bylo vpisováno pouze na stránky matutina.
      Ostatní hodinky jsou čmáráním nedotčené.
      Knížka byla zřejmě používána jen při vánočním matutinu (které předcházelo
      půlnoční mši).
    </p>
    <p>
      Co nám vpisky prozradí?
      Pocitem zadostiučinění mě naplňují nalezené přednesové značky.
      Neznámý uživatel knížky, který žil na konci 19. století
      a měl tedy k latině (která tehdy byla pravidelnou součástí gymnasiálního
      vzdělání) mnohem blíže než my dnes, měl zapotřebí označkovat si texty
      žalmů pro zpěv.
      Latinské texty žalmů v chórových žaltářích se dodnes obvykle
      neznačkují, protože se má za to, že toho uživatel nemá zapotřebí.
      O svých obtížích se zpěvem z neznačkovaných latinských textů
      jsem psal <a href="/blog.php?clanek=ar12den1.xml">v srpnu během svého 
      experimentu s předkoncilním oficiem</a>
      a <a href="http://www.praytellblog.com/index.php/2012/09/16/chanting-abbots/">na blogu PrayTell</a>
      se lze dočíst, že podobné problémy mají dokonce i někteří ctihodní opati
      benediktinských komunit.
      Poučení ze stoletých čmáranců: není to problém nový, typický pro naši
      dobu latinou nedostatečně dotčených katolíků.
    </p>
    <p>
      Nejhojnější přednesovou značkou je podtržení slabiky modrou pastelkou.
      Podtržena je obvykle první slabika, kde se v příslušném poloverši
      měnila melodie, tedy buďto první přípravná nebo první přízvučná.
      Někde navíc najdeme červenou svislou čárku -
      podle všeho pauzu k nádechu ve velmi dlouhých verších.
      (Je vždy max. jedna ve verši, na místě, kde text snese cézuru.)
      Za povšimnutí stojí, že žalmy v naší knížce nikdy nemají tištěnu flexu.
    </p>
    <p>
      Zajímavější než přednesové značky jsou "scénické poznámky"
      připsané na okrajích stránek. Níže uvádím všechny.
    </p>
    <p>
      s. 19 (hymnus): <em>Ep.</em> <br/>
      s. 20 (in 1. nocturno 1. antiphona): <em>Canon 1.</em> <br/>
      s. 23 (lectio 1.): <em>Can. iunior</em> <br/>
      s. 27 (in 2. nocturno 1. antiphona): <em>Can. 4</em> <br/>
      s. 40 (lectio 8.): obtížně čitelná poznámka. Dvě nerozluštitelná slova,
      pak <em>Eppi ad Te D</em> <br/>
      s. 41 (lectio 9.): <em>Ep.</em> <br/>
      s. 42 (Te Deum): delší nečitelná poznámka
    </p>
    <p>
      Zdá se, že je v knížce zaznamenáno rozdělení rolí při pontifikální
      liturgii v katedrále. Zkratku Ep. čtu episcopus, biskup.
      Biskupovi přísluší ve vánočním matutinu intonovat hymnus,
      číst poslední-devátou lekci a možná intonovat Te Deum.
      Nerozluštěná poznámka u 8. lekce se ho týká, ale nezdá se být přiřazením
      role - spíše určuje, že se s biskupem má něco dít.
      (Někdo mu něco přinést?)
    </p>
    <p>
      Vedle biskupa čteme o třech různých kanovnících 
      (canonicus 1., canonicus iunior, canonicus 4).
      Tak ovšem vyvstává řada otázek.
      Kdo intonoval antifony, u kterých není nikdo napsán?
      Vždy kanovník jmenovaný u první antifony daného nokturnu?
      Další kanovník v pořadí? (Každý nokturn má tři žalmy, takže
      by v prvním intonovali canonicus 1, 2 a 3 a první antifona
      druhého nokturnu by vyšla akorát na napsaného k ní can. 4.)
      Nebo někdo jiný (např. člen chrámového sboru), kdo pro uživatele
      naší knížky z nějakého důvodu nebyl důležitý?
      Kdo četl lekce? Kdo předzpěvoval žalm Venite? (Oprava melodie
      zanesená na jednom místě tužkou by mohla nasvědčovat tomu,
      že tento úvodní žalm zpíval sám uživatel naší knížky.)
      ... ...
    </p>
    <p>
      Studium "scénických poznámek" 
      nás přivedlo k překvapivému zjištění o osudu knížky
      objevené v antikvariátu. Jestli správně interpretuji zkratky,
      sloužila (nebo <em>po</em>sloužila)
      kdysi při vánočním matutinu v některé katedrále a používal ji
      někdo, kdo potřeboval mít přehled o vykonavatelích jednotlivých
      liturgických rolí - tedy někdo jako ceremonář.
      Toto zjištění staví do překvapivého světla závěry učiněné
      z nalezených přednesových značek. V (blíže sotva určitelné)
      katedrále se na sklonku 19. století o Vánocích z celého oficia
      zpívalo jen matutinum a ceremonář zřejmě nebyl docela kovaný
      v psalmodii.
    </p>
  </text>
</clanek>