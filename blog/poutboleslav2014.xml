<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Národní svatováclavská pouť 2014</titulek>
  
  <tags>Stará Boleslav, sv. Václav, Národní svatováclavská pouť,
  Praha, katedrála sv. Víta</tags>
  <kategorie>Ze života</kategorie>
  <datum>28.9.2014 21:24</datum>
  <!-- <souvisejiciNoty></souvisejiciNoty> -->
  
  <souhrn>
    Další zažité nešpory pod drobnohledem.
  </souhrn>
  
  <text>
    <p>
      Po dvou letech jsem se znovu vypravil na pouť k místu "zrození pro nebe"
      svatého knížete Václava. (Hlouběji v archivu blogu je 
      <a href="/blog.php?clanek=poutboleslav2012.xml">zpráva z předloňské pouti</a>.)
      Liturgický program je zřejmě dlouhodobě ustálený. Oproti minule
      se nic zásadního nezměnilo. Bohužel se nezměnila ani moje schopnost
      brzy ráno vstávat do tmy a zimy, takže ani letos nemohu referovat
      o modlitbě se čtením a ranních chválách konaných ráno v souvislosti
      se dvěma ranními mšemi v kryptě svatováclavské baziliky.
    </p>

    <h3>První nešpory (krypta sv. Kosmy a Damiána, Stará Boleslav)</h3>
    <p>
      Nešpory, které mi posledně zavdaly důvod k rozsáhlejšímu rozboru
      a následně k přísnému "odsouzení", se letos neopakovaly.
      Chorální schola (převážně ženská) přijela z bavorského Řezna
      a nešpory byly latinsky.
      Noty jsou k mání 
      <a href="http://choral.sdh.cz/?a=1&amp;id=349">na webu choral.sdh.cz</a>.
      (Jsou evidentně pražské katedrální, nikoli řezenské provenience.)
    </p>
    <p>
      Co k nim lze říci? Zachovávají bez újmy strukturu pokoncilních nešpor
      (Bohu díky alespoň za to).
      Pokud jde o výběr textů (viz <a href="http://kpmk.eu/index.php/liturgicke-texty/147-texty-narodniho-propria-latinsky">oficiální latinské texty</a>):
      Žalmy jsou z druhých (místo z prvních) nešpor commune o jednom mučedníkovi.
      Antifony by měly být rovněž z commune, ale texty nesouhlasí ani 
      s breviářem, ani s náhradními antifonami pro zpívané latinské chorální
      oficium podle <em>Ordo cantus officii</em>.
      Jak antifony k žalmům, tak antifona k Magnificat (která je předepsaná 
      vlastní - <em>Consummatus in brevi</em>) jsou vybrané napříč několika
      commune. Často jde o antifony, které nenašly cestu ani do
      "tridentského" breviáře. Následují odkazy na jejich záznamy v databázi 
      CANTUS.
    </p>
    <ul>
      <li><a href="">Omnes sancti quanta passi sunt</a></li>
      <li><a href="http://cantusdatabase.org/node/377098">Martyrum chorus</a></li>
      <li><a href="http://cantusdatabase.org/node/375210">Corpora sanctorum</a></li>
      <li><a href="http://cantusdatabase.org/node/377498">O quam venerandus es</a> (text v databázi souhlasí jen z větší části)</li>
    </ul>
    <p>
      Místo předepsaného hymnu 
      <em>Salve decus Bohemiae</em> (vyskytujícího se v pražských formulářích 
      svátku od 17. stol.) je starobylejší <em>Dies venit victoriae</em>.
      Jediným zpěvem, který má text přesně jak má být, je responsorium,
      a to nepochybně proto, že je nově složené.
    </p>
    <p>
      Použité nešpory pravděpodobně vznikly původně jako nešpory druhé
      téhož svátku. Hymnus byl vzat z pramenů z doby, kdy se v Praze
      oficium ještě zpívalo. Antifony pak byly vybrány volně
      z pokladu gregoriánského chorálu.
      Z hlediska souladu s liturgickými předpisy na tom tedy nejsme o mnoho
      lépe než předloni.
    </p>
    <p class="edit">
      [EDIT 23. 10. 2022]
      Pokud jde o výtku nezákonnosti výběru antifon nesouhlasícího
      s tím, který předepisuje <em>Ordo cantus officii</em>,
      viz její uvedení na pravou míru
      v <a href="/blog.php?clanek=boleslav2020.xml">pozdějším článku</a>.
    </p>
    <p>
      Výše uvedené upozorňuje na jeden zatím nejen nevyřešený, ale, domnívám se,
      dokonce vůbec neřešený problém:
      zatímco nezpívatelnost latinského oficia reformovaného v návaznosti
      na 2. vatikánský koncil je pro texty společné celé církvi alespoň
      provizorně vyřešena dokumentem kongregace pro svátosti a bohoslužbu
      <em>Ordo cantus officii</em>, kde se pro každý zpěv bez tradiční
      chorální melodie určuje zpívatelná náhrada, pro svátky vlastní
      našemu česko-moravskému kalendáři takový dokument zatím nemáme.
      Občas někde k nějakým zpívaným latinským nešporám dojde
      a potom se zpívá, co sestavitel liturgického programu uzná za vhodné.
      Netvrdím, že to je vážný problém vyžadující rychlé řešení -
      ale úplně šťastné mi to nepřijde.
      Když se nechá stranou možnost chybějící zpěvy složit
      (proti které bych principielně také nebyl, zvláště pokud jde o novější
      svátky), pro staré svátky
      zůstává možnost vrátit se alespoň zčásti k jejich středověkému
      repertoáru. Např. se divím, proč ten, kdo sáhl po hymnu
      <em>Dies venit victoriae</em>, nevzal ze stejného pramene
      i antifonu k Magnificat.
    </p>

    <h3>Druhé nešpory (pražská katedrála)</h3>
    <p>
      byly podle všeho zpívány do puntíku stejně jako předloni i loni. 
      (Letopočet na notách hlásí dokonce 2004/5.)
      Mohu tak ke zprávě
      z předloňska akorát doplnit některé chybějící informace:
    </p>
    <p>
      Jedna antifona k žalmu byla dvouhlasá, zbylé jednohlasé; 
      antifona k Magnificat pak asi čtyřhlasá.
      Nápěvy žalmů byly jiné než obvyklé "svatovítské nedělní"; 
      <a href="/blog.php?clanek=nesporyvitceske.xml">trpí sice podobnými neduhy, ale alespoň nejsou tak ponuré</a>.
      Při procesí po nešporách se opět zpívalo "Buď pozdraven, klenote Čech"
      (překlad latinského hymnu Salve, decus Bohemiae;
      v zeleném hymnáři jako hymnus k obojím nešporám).
    </p>
    <p>
      Letošní svatováclavské nešpory v katedrále měly jeden půvabný rys:
      zároveň s nimi probíhala jakási paralelní bohoslužba ve svatováclavské
      kapli a v jednu chvíli se psalmodie mísila s dosti hlasitým
      zpěvem svatováclavského chorálu odtamtud. Tak se stalo o něco
      představitelnějším prostředí starého <em>pražského kostela</em>,
      kde byl liturgický provoz prakticky nepřetržitý a kněží sloužící
      u četných postranních oltářů
      měli přísně zakázáno modlit se nebo zpívat hlasitě, aby nerušili
      "hlavní linii" bohoslužeb odehrávající se ve svatovítském chóru.
      (Zda a jak se případně navzájem rušil chór svatovítský a mariánský,
      o tom jsem v publikacích, které mi zatím prošly rukama,
      nezachytil žádné zprávy.)
    </p>
  </text>
</clanek>
