<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Nešpory v předvečer Národní svatoludmilské pouti na Tetíně</titulek>

  <tags>Tetín, sv. Ludmila, benediktinky</tags>
  <kategorie>Ze života</kategorie>
  <datum>17.9.2021 21:50</datum>
  <!-- <souvisejiciNoty></souvisejiciNoty> -->

  <souhrn></souhrn>

  <text>
    <p>
      U příležitosti 1100. výročí mučednické smrti sv. Ludmily
      se letos pouť na Tetíně koná ve větším formátu než obvykle
      a v její předvečer se v pátek 17. 9. 2021 slavily i nešpory.
      Já tam nakonec nebyl, ale měl jsem možnost alespoň je sledovat
      v přenosu na TV Noe.
    </p>
    <p class="edit">
      [EDIT 10. 6. 2023]
      V archivu TV Noe je z nešpor k dispozici
      <a href="https://www.tvnoe.cz/porad/32380-svatoludmilska-pout-na-tetine-prijezd-lebky-sv-ludmily-na-tetin-modlitba-za-vlast-a-svatoludmilske-nespory">záznam</a>.
    </p>
    <p>
      Program začal v 18:30 příjezdem lebky sv. Ludmily
      v doprovodu motocyklů Hradní stráže.
      Relikviář z auta vyzvedli dva jáhni a v průvodu byl nesen
      ke kostelu sv. Ludmily.
    </p>
    <p class="petit">
      Dlužno říci, že to nesení patrně bylo dost nepohodlné
      a ne právě bezpečné, protože relikviář byl usazen na jakési
      desce nebo větším podstavci, postrádajícím jakákoli kloudná
      držadla na nošení. Zatímco lebka sv. Václava ceremoniální
      transporty absolvuje pravidelně a je pro ně vybavena
      speciálními nosítky, lebka sv. Ludmily se na cesty tak
      často nevydává a tomu zřejmě odpovídá i její vybavení.
    </p>
    <p>
      Před kostelem se průvod zastavil,
      místní dětská schola zpívala únavnou
      <a href="https://zpevnik.proscholy.cz/pisen/2213/podepira">hosanovku</a>
      k českým světcům, pak kardinál Duka předčítal dlouhou
      "modlitbu za vlast", zatímco v držení relikviáře
      spočívajícího na oné nepraktické podložce se střídali
      ostatní čeští a moravští biskupové.
      Konečně se průvod opět dal v pohyb a zatímco pokračoval
      do kostela, ženská schola uvnitř spustila litanie
      k národním patronům.
      Po litaniích následovaly samotné nešpory.
    </p>
    <p>
      Protože je to téma, které mi určitým způsobem leží v hlavě,
      všímám si často při veřejně a slavnostně konaných nešporách,
      jak byl vyřešen <a href="/blog.php?clanek=introit.xml">vstupní průvod</a>.
      I tady na malý vstupní průvod
      došlo, protože bylo třeba do liturgického prostoru uvést
      předsedajícího, který se během litanií v sakristii oblékal.
      To, že <em>Bože, pospěš mi na pomoc</em>
      začaly zpěvačky (dobře, nebyly to ledajaké zpěvačky, ale,
      jestli jsem přenos sledoval dobře, sama mnichovsko-bělohorská
      abatyše, ale stejně) začaly
      ve chvíli, kdy předsedající s přisluhujícími teprve
      vycházel ze sakristie, hodnotím jako určitou anomálii.
      Obvyklé a správné je nešpory začínat
      až když jsou všichni na svých místech, intonace
      <em>Bože, pospěš mi na pomoc</em> bývá jedním z předsednických
      vstupů. Ale je pravda, že úvodní verš vstupní průvod
      pokryl a nebylo tak třeba žádného zpěvu nebo jiného hudebního
      doprovodu speciálně pro ten účel.
    </p>
    <p>
      Jak jsem předpokládal, nešpory byly
      <a href="/blog.php?clanek=ludmila2015.xml">ty samé, které se každoročně slaví v basilice sv. Jiří na Pražském hradě</a>.
      Tedy nešpory na půdorysu pokoncilní liturgie hodin
      (hymnus <em>na začátku</em>, pak psalmodie, krátké čtení,
      responsorium, Magnificat, <em>přímluvy, Otčenáš,</em> orace)
      a s využitím jejích prvků (žalmy v neovulgátním znění,
      krátké čtení, přímluvy a orace v češtině),
      ale co do struktury psalmodie předkoncilní (světský cursus)
      a co do repertoáru vlastních zpěvů středověké.
    </p>
    <p>
      Už v minulosti jsem psal, že podle mého nejlepšího vědomí
      jsou takové nešpory v rámci platného liturgického zákonodárství
      nezákonné a slavit by se takto neměly.
      Shodou okolností byly včera večer přenášeny
      <a href="https://youtu.be/EKT04eKZI1E">nešpory z památky sv. Ludmily ze strahovského kláštera</a>:
      repertoár vlastních zpěvů téměř identický
      (hymnus ovšem bez polyfonních strof a bez závěrečné strofy
      z mnohočetných amen), ale v uspořádání zcela respektujícím
      pokoncilní breviář. Přizpůsobení psalmodii sestávající
      jen ze dvou žalmů a NZ kantika si pochopitelně vyžádalo
      redukci počtu antifon, ale třeba vypuštění oné antifony o tom,
      jak se sv. Ludmila z nebe postarala o odplatu svým vrahům,
      rozhodně nebylo ke škodě věci.
      Bylo by žádoucí, aby se ony svatoludmilské nešpory
      každoročně zpívané u sv. Jiří uvedly v soulad s liturgickými předpisy
      (alespoň když se konají ne jako koncert, ale jako liturgie)
      a strahovské řešení je dobrou ukázkou jedné z možných cest.
    </p>
    <p>
      V kázání p. biskup Wasserbauer objevoval ekumenický potenciál
      sv. Ludmily, s tím, že neví, jestli ji ctí i pravoslavní.
      Nedalo by moc práce vygooglit, že samozřejmě ano:
      pravoslavné oslavy budou
      v basilice sv. Jiří na Hradě i právě na Tetíně
      týden po těch katolických, a Ludmilin kult naprosto není
      omezen jen na pravoslavnou církev v českých zemích.
    </p>
    <p class="petit">
      Když se v souvislosti s oslavami jubilea dostalo určité
      publicity <a href="https://eshop.cirkev.cz/knihy/akathisty-k-ochrancum-zeme-ceske_8132">akathistu</a>
      ke sv. Ludmile od T. F. Krále, možná stojí za zmínku
      <a href="http://download.pravoslavi.cz/svati/ludmila-akathist-w.pdf">starší text stejného žánru a funkce</a>
      z pravoslavných kruhů.
    </p>
    <p>
      Kardinál Schönborn zase ve svém proslovu po nešporách
      prosil sv. Ludmilu o pomoc proti nacionalismu ohrožujícímu
      evropskou pospolitost. To je v legračním protikladu proti
      roli, jakou (alespoň ti staří) čeští světci vždy hráli
      pro formování nacionalismu českého.
      Být patronkou dobrých a otevřených mezinárodních vztahů
      rozhodně není úloha pro sv. Ludmilu v Čechách obvyklá
      a přirozená.
      Bude bezpečnější světcům, kteří za sebou mají stovky let ve funkci
      symbolických opor národní partikularity, ponechat jejich
      tradiční roli, a za svaté ochránce evropské integrace
      zvolit nějaké jiné světce, kteří se k tomu svým širokým
      mezinárodním přesahem dobře nabízejí.
    </p>
  </text>
</clanek>
