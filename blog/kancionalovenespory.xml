<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Možnosti záměny textů v nedělních nešporách</titulek>
  
  <tags></tags>
  <kategorie>Rubriky</kategorie>
  <datum>29.10.2012 22:15</datum>
  <souvisejiciNoty>kancinespor</souvisejiciNoty>
  
  <souhrn>
    Přehodnocení jednoho notového materiálu po novém přečtení příslušného
    oddílu Všeobecných pokynů k Denní modlitbě církve.
  </souhrn>
  
  <text>
    <p>
      Nedávno jsem, hledaje odpověď na rubricistickou otázku jistého člověka
      na jistém internetovém fóru, znovu prošel relevantní články
      Všeobecných pokynů k Denní modlitbě církve a došlo mi, že
      mé <a href="/noty.php#kancinespor">kancionálové nešpory</a> jsou s jejich
      ustanoveními v rozporu více, než jsem si při jejich tvorbě a ještě
      dlouho potom myslel. Protože jsem na tomto blogu, co se týče
      (ne)dodržování liturgických předpisů, nikoho nešetřil, nebudu šetřit ani
      sebe. Zároveň myslím, že by mohlo být užitečné nabídnout co možná
      přesnou a vyčerpávající odpověď na otázku, 
      <em>co se tedy kdy v nedělních nešporách změnit smí</em>.
      Protože nedělní druhé nešpory velmi pravděpodobně stále jsou tou
      hodinkou, která má největší šanci být slavena společně a slavnostně.
    </p>

    <h5>1. Možnosti záměny textů v nedělních druhých nešporách</h5>
    <p>
      Záměně textů jsou věnovány články VPDMC 246-252.
      Pro naše tázání je klíčový zejm. čl. 247:
    </p>
    <blockquote>
      <p>
	247. V oficiu 
	nedělí,<br/> 
	slavností,<br/> 
	svátků Páně, které jsou zapsány ve všeobecném kalendáři,<br/> 
	dále všedních dnů doby postní a Svatého týdne,<br/> 
	dnů ve velikonočním a vánočním oktávu<br/> 
	i všedních dnů od 17. do 24. prosince<br/> 
	nikdy nelze změnit ty texty, které jsou vlastní nebo pro toto slavení 
	přizpůsobené, jako jsou antifony, hymny, čtení,
	responsoria, modlitby a velmi často i žalmy.
      </p>
      <p>
	Nedělní žalmy příslušného týdne se mohou nahradit, je-li to vhodné, 
	nedělními
	žalmy jiného týdne, ba dokonce, jde-li o modlitbu s účastí lidu, i 
	jinými žalmy
	vybranými tak, aby byl lid postupně přiváděn k porozumění žalmům.
      </p>
    </blockquote>
    <p>
      Interpretuji tento článek tak, že jeho druhá část,
      umožňující záměnu nedělních žalmů, je normou
      obecnou, zatímco první část je normou speciální - a tedy má před
      obecnou přednost.
    </p>
    <p>
      Pro aplikaci je klíčové upřesnění, co jsou "texty, které jsou vlastní 
      nebo pro toto slavení přizpůsobené".
      "Vlastní texty" jsou, podle použití toho termínu v jiných částech dokumentu
      soudě, texty, které jsou v breviáři plně uvedeny v oddílu vyhrazeném danému
      dni a nejsou brány z formuláře společného pro více dnů. 
      Co jsou "texty pro toto slavení přizpůsobené", netuším.
      (Možná se tím myslí texty ze společných textů o svatých
      nebo ze společných textů určité doby.)
      Soudím tedy, že ve dnech ve článku 247 vyjmenovaných lze zaměnit 
      za jiné ty texty, které se berou ze žaltáře, a možná i ty,
      které jsou ze společných textů o svatých.
    </p>
    <p>
      V neděli, na kterou připadne slavnost nebo svátek Páně, tedy nelze
      v nešporách zaměnit nic - protože obě tyto kategorie oslav mívají
      všechny texty vlastní.
    </p>
    <p>
      Neděle doby adventní, postní a velikonoční mají rovněž
      všechny texty vlastní a proto v jejich nešporách také nelze
      zaměňovat nic. - K diskusi je snad akorát možnost záměny žalmů
      za žalmy jiné neděle (protože žalmy se berou z cyklu žaltáře).
      Z čistě liturgicko-právního hlediska by to, pokud vím, bylo
      legitimní. Je tu ale nanejvýš vhodné žalm od žalmu zvážit
      jeho případnou významovou provázanost s antifonou
      a tam, kde taková provázanost je, žalm pokud možno neměnit.
    </p>
    <p>
      Neděle v liturgickém mezidobí mají vlastních textů jen málo -
      antifony ke kantikům z evangelia a oraci. Je tudíž,
      je-li moje výše podaná interpretace "vlastních textů" správná,
      legitimní zaměnit všechno ostatní.
    </p>

    <h5>2. Kancionálové nešpory a liturgické předpisy</h5>
    <p>
      Drobný notový materiál, který jsem v červenci vytvořil a zveřejnil
      pod názvem <a href="/noty.php#kancinespor">kancionálové nešpory</a>,
      byl pokusem o rozvinutí možností, které ke slavení liturgie hodin
      nabízí Jednotný kancionál. Ten obsahuje Pololáníkovo zhudebnění
      druhých nešpor neděle 1. týdne žaltáře a antifony k Magnificat
      z pondělí téhož týdne. Z výkladu výše je zřejmé, že 
      tyto nešpory lze zcela legitimně 
      zpívat jako první či druhé o všech nedělích v mezidobí,
      až na tuto posledně zmíněnou antifonu,
      protože by se, přísně vzato, měla vždy vzít vlastní antifona
      těch kterých nešpor.
    </p>
    <p>
      Moje kancionálové nešpory obsahují
      pro dobu adventní, postní a velikonoční vždy antifony k žalmům
      a antifonu k Magnificat k použití s žalmy a kantiky
      z nešpor obsažených v kancionálu.
      Ovšem neděle v těchto dobách mají všechny tyto texty vlastní
      a není tedy možné je libovolně zaměňovat.
    </p>
    <p>
      Co udělám? Na stránce se stahováním opravím informace
      o materiálu, aby nenaváděly k liturgickým neřádům.
      A během několika týdnů opravím i jeho obsah, aby umožňoval
      řádně slavit druhé nešpory všech nedělí ze zmiňovaných dob,
      kdy se používají žalmy a kantikum z 1. týdne žaltáře.
    </p>
    <p>
      Nakonec musím své závěry poněkud relativisovat.
      Zmínil jsem, že "universální antifona k Magnificat", kterou
      náš kancionál obsahuje, není v souladu s předpisy.
      A kancionál má církevní schválení.
      Německý katolický kancionál Gotteslob jde ještě dál:
      pro každou liturgickou dobu obsahuje jeden nebo více formulářů
      nešpor, které někdy nemají s žádným formulářem obsaženým v breviáři
      zhola nic společného. Nebo mají, ale jen málo. A tak jako tak,
      když jsou podávány jako universální pro použití někdy v dané době,
      nejsou v souladu s předpisy. I Gotteslob má církevní schválení.
      Nelze se tudíž ubránit silnému dojmu, že jsem něco přehlédl.
    </p>
  </text>
</clanek>