<?xml version="1.0" encoding="utf-8" ?>
<!--
This stylesheet is only used on the local station, when writing a post,
to preview it in style and layout.
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <!-- <!DOCTYPE html 
	 PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> -->
    
    <!-- <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs"> -->
    <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Blog: <xsl:value-of select="//titulek" /> (In adiutorium)</title>
        
        <link rel="stylesheet" type="text/css" href="../public/grafika/style.less.css" title="výchozí styl" />
				<base href="../" />
      </head>
      
      <body>
        <div id="wrapper">
          <div id="content" class="blog">
            <div class="post">
              <div class="post-header">
                <p>blogový článek:</p>
                <h2><xsl:value-of select="//titulek"/></h2>
                
                <p>
                  <b>na webu:</b> <a href="#" id="weblink"></a><br/>

                  <span>
                    <xsl:if test="string(//datum)">
                      <xsl:attribute name="style">background-color: lime;</xsl:attribute>
                    </xsl:if>
                    <b>datum:</b> <xsl:apply-templates select="//datum"/>
                  </span>
                  <br/>
                  <b>kategorie:</b> <xsl:apply-templates select="//kategorie"/><br/>
                  <b>tags:</b> <xsl:apply-templates select="//tags"/><br/>
                  <b>související noty:</b> <xsl:apply-templates select="//souvisejiciNoty"/><br/>
                  <b>souhrn:</b> 
                  <div id="header"><xsl:apply-templates select="//souhrn"/></div>
                </p>
              </div>

              <xsl:apply-templates select="//text" />
            </div>
          </div>
        </div>
	<script type="text/javascript">
	  window.onload = function () {
      // construct link to the online version of a published post
      var blogUri = 'http://www.inadiutorium.cz/blog.php?clanek=';
      var postUri = blogUri + window.location.href.split('/').pop();
      var link = document.getElementById('weblink');
      link.setAttribute('href', postUri);
      link.textContent = postUri;

      // rewrite online image URLs to local ones
	    document.querySelectorAll('img').forEach(function (node) {
	      if (node.src.startsWith('file:///blog')) {
	        node.src = node.src.replace('file:///blog/', './blog/');
	      }
	    });

      // rewrite online blog post links to local ones
      document.querySelectorAll("a[href^='/blog.php?clanek=']").forEach(function (node) {
        node.href = node.href.replace(
          'file:///blog.php?clanek=',
          'blog/' // required because of the /head/base tag
        );
      });

      // rewrite online attachment links to local ones
      document.querySelectorAll("a[href^='/blog/']").forEach(function (node) {
        node.href = node.href.replace(
          'file:///blog/',
          'blog/' // required because of the /head/base tag
        );
      });
    };
	</script>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="text">
    <xsl:copy-of select="."/>
  </xsl:template>

</xsl:stylesheet>
