<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Změna plánu</titulek>
  
  <tags>Liturgia horarum</tags>
  <kategorie>Ze života</kategorie>
  <datum>5.12.2015 23:00</datum>
  <!-- <souvisejiciNoty></souvisejiciNoty> -->
  
  <souhrn>
  
  </souhrn>
  
  <text>
    <p>
      Neměním svoje plány rád - a také ne vždy se mi podaří včas
      postřehnout, že zvolená cesta není optimální a změna plánu
      by byla potřeba. Po týdnu
      <a href="/blog.php?clanek=predkoncilnipripravy.xml">oficia podle rubrik z r. 1960</a>
      však vidím, že se mi předkoncilní oficium nedaří dobře
      smířit s dalšími dvěma činnostmi, které jsou pro mě důležité -
      totiž se zaměstnáním a studiem.
    </p>
    <p>
      Podotýkám, že jsem se
      zdaleka nemodlil celé denní pensum, nýbrž jen jeho menší část:
      pravidelně ranní chvály, sextu, nešpory a kompletář,
      ve dny, kdy pracuji z domova, doplněné o nónu,
      a v neděli o matutinum. I tak je textový objem oproti
      doposud užívanému pokoncilnímu oficiu bez modlitby se čtením
      výrazně větší (ve všední den 16-19 žalmů či jejich částí
      oproti 10, přičemž části žalmů jsou v předkoncilním žaltáři
      obvykle delší) a když se k tomu přidá doba potřebná pro
      vyhledání všech částí oficia v knihách a přípravu zpěvů
      (navíc je běžné ve velkých hodinkách zpívat dvě až tři
      "antifony k Benedictus/Magnificat" - jednu opravdu s kantikem,
      ostatní na konci hodinky v rámci komemorací potlačených
      konkurenčních oslav), přechod na předkoncilní oficium byl
      pro můj rozvrh ranou, ze které by se nezotavil
      a doplatilo by na to především studium.
    </p>
    <p>
      Možným řešením by bylo ještě více omezit množství
      každodenně zpívaných hodinek. To ale nechci - na den
      strukturovaný modlitbou ráno, v poledne, večer a před spaním
      jsem dlouho zvyklý a, možno-li to tak říci, velmi se mi osvědčil.
      Další možností by bylo omezit míru zpěvu, neboť recitovaná
      hodinka nevyžaduje tolik přípravy a snáze se v případě potřeby
      urychlí. Pro mě však je hudební složka oficia důležitá
      a jeho nezpívaná forma těžce defektní. (Askety, kteří se léta
      nebo celý život věrně modlí breviář beze zpěvu, obdivuji;
      pro mě by to byl velice suchý chléb.)
    </p>
    <p>
      Rozsah oficia proto omezím návratem k jeho pokoncilní formě.
      Protože však považuji za důležité takříkajíc osvěžit
      své skladatelské snahy stále čerstvou vodou autentického
      gregoriánského chorálu, nevrátím se k českému breviáři,
      ale sáhnu po pokoncilním breviáři latinském.
    </p>
    <p class="petit">
      Jako rádobyrubricistu mě samozřejmě velice trápí, že mám pouze
      <em>editionem typicam</em> z r. 1975 a nejsem si vůbec jist, zda po vydání
      <em>editionis typicae alterae</em> je užívání staršího vydání při slavení
      oficia legální. Pravděpodobnější je, že nikoli.
      Že však jsem soukromá osoba k modlitbě oficia nevázaná, ...
      nebudu to v nejbližší době řešit.
    </p>
    <p>
      Jak známo, úplný oficiální antifonář k pokoncilní podobě římského
      oficia nebyl dosud vydán.
      O nedělích a svátcích se podržím vydaného vesperálu
      (<em>Antiphonale Romanum II</em>, Solesmes 2009),
      pro zbylé hodinky použiji <em>Liber hymnarius</em> (Solesmes 1983)
      a <a href="http://www.chantcafe.com/2011/03/peter-sandhofes-antiphonary.html">Sandhofeho antifonář</a>.
    </p>
    <p class="petit">
      Ten deklaruje, že je připraven podle <em>Ordo cantus officii</em>
      z r. 1983 - to však
      <a href="http://www.libreriaeditricevaticana.va/content/libreriaeditricevaticana/it/novita-editoriali/ordo-cantus-officii.html">bylo čerstvě nahrazeno novým předpisem</a>.
      Protože však nemám časovou kapacitu všechny zpěvy pro každý den
      podle něj vyhledat (ani přístup ke všem pro to potřebným
      pramenům), prozatím si udělím další úlevu z přísného zachovávání
      té nejtvrdší zákonické linie a budu zřejmě občas zpívat zpěvy
      zcela neodpovídající nejčerstvějším liturgickým normám.
    </p>
    <p>
      Holger Peter Sandhofe bohužel zemřel dříve, než mohl
      dokončit svazky velikonočního okruhu a sanktorálu.
      Otázku, jak si s tím poradit, prozatím nechávám otevřenou.
      Stále se mohu utéci k české verzi oficia a svým zpěvům.
      Dalším řešením by mohlo být sáhnout
      po <a href="http://dominican-liturgy.blogspot.cz/">dominikánském antifonáři P. Augustina Thompsona</a> -
      za předpokladu, že se texty zpěvů vždy nebo většinou shodují.
      (Zatím jsem neověřoval.) To by samozřejmě znamenalo
      infikovat se specifickou hudební tradicí dominikánského řádu,
      se kterým přitom osobně nemám nic společného.
    </p>
    <p>
      Takže - druhou adventní neděli dozpívám podle starých knih
      a od pondělka se vracím do vod známějších a bezpečnějších.
      Poněkud dostávají na frak všechny moje uštěpačné poznámky
      na adresu pokoncilní reformy oficia. I nadále jsem
      samozřejmě přesvědčen (a v případné diskusi na toto téma
      s tradicionalisty o to přesvědčenější, čím větší radost
      mi dělá možnost nařknout z modernismu samotného
      <em>Pia X.</em> :) ),
      že jediné pravé tradiční římské oficium zemřelo už roku 1911.
      Experiment ale ukazuje, že ani jeho forma stižená opakovaným
      zkracováním pro mě není reálně zvládnutelná a že ono kratinké
      pokoncilní oficium je dlouhé právě tak akorát.
    </p>
    <p>
      Svých dlouho spřádaných
      <a href="/blog.php?clanek=ponor.xml">plánů</a>
      na důkladné pobytí se staršími
      podobami oficia se nevzdávám, ale netuším, na kdy je vlastně
      odkládám. Snad na dobu, až dostuduji (ať šťastně nebo
      definitivně nešťastně) - za předpokladu, že pro mě v té době
      vize života rozděleného téměř beze zbytku mezi práci a oficium
      bude stále ještě přitažlivá.
      ("Autor je nejspíš trochu podivín." "Ano. Spíš trochu víc.")
    </p>
  </text>
</clanek>
