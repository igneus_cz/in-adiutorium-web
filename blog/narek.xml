<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Na sv. Řehoře z Nareku</titulek>

  <tags>překlad, LK ČBK</tags>
  <kategorie>Texty</kategorie>
  <datum>27.2.2024 18:44</datum>
  <souvisejiciNoty>rehornarek, cucitel</souvisejiciNoty>

  <souhrn></souhrn>

  <text>
    <p>
      V lednu 2021 byly do Všeobecného římského kalendáře
      nově zapsány nezávazné památky tří učitelů církve:
      sv. Hildegardy z Bingenu, sv. Jana z Avily
      (za učitele církve prohlášeni v roce 2012 Benediktem XVI.)
      a sv. Řehoře z Nareku
      (prohlášen v roce 2015 papežem Františkem).
      Současně se zapsáním do kalendáře světové církve byly
      vydány liturgické texty, následujícího roku byl zveřejněn
      jejich oficiální český překlad.
      Formuláře sv. Hildegardy a sv. Jana z Avily jsou
      v nejskromnějším možném formátu, formulář sv. Řehoře z Nareku
      je o stupínek bohatší: má antifony
      k evangelním kantikům, tyto však nejsou skutečně vlastní,
      nýbrž vzaté z commune učitelů církve.
    </p>
    <p>
      Asi je rozumné, že se Kongregace pro bohoslužbu při zavádění
      nových svátků drží zpátky co do stupně slavení a rozsahu
      vlastních textů, ale stejně se těžko bráním zklamání,
      že slavní autoři liturgické poesie, jako je sv. Hildegarda
      a sv. Řehoř z Nareku, nemají ani jednu opravdu vlastní,
      ze spisů nebo z lokálních liturgických textů vybranou antifonu.
    </p>
    <p>
      Formulář sv. Řehoře skýtá jednu zvláštnost:
      i když obě jeho antifony jsou, jak již bylo řečeno,
      vzaté ze společných textů o učitelích církve,
      antifona k Magnificat
      v českém překladu nepřebírá znění odpovídající antifony
      z Denní modlitby církve, ale je přeložena nově,
      těsněji při předloze.
    </p>
    <blockquote>
      <p>
        <sup>[1]</sup> O doctor óptime, Ecclésiae sanctae lumen,
        <sup>[2]</sup> sancte <strong>Gregóri,</strong>
        <sup>[3]</sup> divínae legis amátor,
        <sup>[4]</sup> deprecáre pro nobis Fílium Dei.
      </p>
      <p class="petit tac">
        (Liturgia horarum)
      </p>
      <!--
      <p>
        <sup>[1]</sup> Dokonalý učiteli, světlo svaté církve,
        <sup>[2]</sup> svatý <strong>[Řehoři],</strong>
        <sup>[3]</sup> milovníku božského zákona,
        <sup>[4]</sup> přimlouvej se za nás u Božího Syna.
      </p>
      <p class="petit tac">
        (Liturgie hodin: Žaltář, ČKCH 1977, společné texty o učitelích církve)
      </p>
      -->
      <p>
        <sup>[2]</sup> Svatý <strong>[Řehoři],</strong>
        <sup>[1]</sup> učiteli víry a světlo církve,
        <sup>[3]</sup> tys čerpal moudrost z Božího zákona;
        <sup>[4]</sup> přimlouvej se za nás u Božího Syna.
      </p>
      <p class="petit tac">
        (DMC, společné texty o učitelích církve)
      </p>
      <p>
        <sup>[1]</sup> Vynikající učiteli, světlo svaté církve,
        <sup>[2]</sup> svatý <strong>Řehoři,</strong>
        <sup>[3]</sup> milovníku božského zákona,
        <sup>[4]</sup> pros za nás u Božího Syna.
      </p>
      <p class="petit tac">
        (památka sv. Řehoře z Nareku)
      </p>
    </blockquote>
    <p>
      Různé překlady stejného liturgického textu jsou nepořádek,
      jehož předcházení se při překládání nových formulářů
      podle všeho věnuje značná péče
      (srov. ony před časem <a href="/blog.php?clanek=prekladmatkycirkve.xml">uniklé podklady pro schvalování jednoho z překladů biskupskou konferencí</a>)
      a je málo pravděpodobné, že by ten nový nekonkordantní překlad
      vznikl omylem.
      Co tedy znamená? Tlačí teď Řím na překlady vypracované striktně
      podle zásad
      instrukce <em>Liturgiam authenticam</em>, a to dokonce
      i v případě nových výskytů již dříve přeložených
      a schválených textů? Rozhodla se naše liturgická komise
      být v daném případě "papežštější než papež"?
      Nebo se potichu pracuje na revizi Denní modlitby církve
      a formulář už reflektuje její předpokládanou budoucí podobu?
    </p>
  </text>
</clanek>
