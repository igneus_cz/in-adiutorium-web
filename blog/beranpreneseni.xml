<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Kardinála Josefa Berana přenesení</titulek>

  <tags>Josef Beran, sv. Vojtěch, Praha, Strahovský klášter, katedrála sv. Víta</tags>
  <kategorie>Ze života</kategorie>
  <datum>22.4.2018 20:45</datum>
  <!-- <souvisejiciNoty></souvisejiciNoty> -->

  <souhrn></souhrn>

  <text>
    <p>
      V pátek a sobotu 20.-21. 4. proběhla repatriace těla
      kardinála Josefa Berana, 17. 5. 1969
      zesnulého v římském exilu
      a od té doby pohřbeného v kryptě Svatopetrské basiliky
      ve Vatikánu.
    </p>
    <p>
      Oficiální zprávy všechny mluví zcela náležitě o <em>repatriaci</em>
      a titul tohoto článku by, vzat doslova, byl nepatřičný:
      přenesení ostatků (translace) bylo v západní církvi
      až do zavedení jednotného a centralisovaného kanonisačního
      procesu de facto aktem prohlášení za svatého.
      K tomu tady samozřejmě nedochází. Ostatky někdejšího pražského
      arcibiskupa také nebyly uloženy na oltář/do oltáře,
      ale jen vystaveny k projevům úcty a
      připraveny k chystanému pohřbu do kamenného sarkofágu.
    </p>
    <p>
      Že však nejde o repatriaci zcela všední, je dáno tím,
      že je o kardinálu Beranovi veden kanonisační proces.
      Nejde tak o pouhý pohřeb významného pražského arcibiskupa,
      ale také o přinesení ostatků možného světce.
      To se zdá také být klíčem k tomu, jak byla celá událost
      liturgicky utvářena.
    </p>
    <h3>Kostel sv. Vojtěcha, Dejvice</h3>
    <p>
      V pátek
      od 18:00 bylo připraveno pásmo, představující kardinála Berana
      z různých úhlů pohledu. Uzavřeno bylo čtením jmen
      "mučedníků a obětí" nacistické a komunistické totality
      z řad kléru arcidiecése pražské (do velice široké kategorie
      "mučedníků a obětí" spadli jak umučení, popravení,
      zemřelí na následky nelidských podmínek věznění apod.,
      tak např. i oběti náletů).
      Na úvod zaznělo nějaké zpracování introitu
      <em>Réquiem aetérnam</em> (ze mše za zemřelé;
      byl to snad jediný zpěv tohoto určení, který za celé
      dva dny zazněl),
      později pozdvižení obočí vyvolalo responsorium
      <em>Ecce, quómodo móritur iustus</em>
      z předkoncilního matutina Bílé soboty:
      jeho text by se vedle Krista, na nějž ho aplikuje
      liturgie, samozřejmě dal aplikovat i na mučedníka,
      který "má podíl na utrpení Kristově", ale liturgie
      takovou aplikaci nezná. Jde o jeden ze zpěvů, které
      mají v liturgickém roce právě jedno své místo,
      a užívat je jinak je nepatřičné.
    </p>
    <p>
      V 19:00 dorazila kolona s rakví, kterou v liturgickém
      průvodu sedm seminaristů vneslo do kostela.
    </p>
    <p class="petit">
      Venku akci doprovázel dechový ansámbl;
      po vstupu průvodu do kostela spustil uvnitř sbor litanie
      a s (neméně zbožným)
      troubením zvenku to vytvořilo nepěknou směs,
      která se měla ještě jednou zopakovat v závěru.
      Nicméně a capella pějícímu sboru se sluší složit poklonu,
      že v tom souboji, co do akustického tlaku nerovném,
      intonačně nepodlehl. Nebo přinejmenším ne tak, aby to postřehl
      i hudebním sluchem nepříliš vybavený pisatel těchto řádků.
    </p>
    <p>
      Po litaniích, během nichž byla rakev vnesena do kostela
      a v něm umístěna, již následovaly nešpory,
      za předsednictví Beranova aktuálního nástupce na postu
      rektora pražského semináře, P. Kotase.
      Byly to nešpory zpívané, po hudební stránce poplatné zvyklostem
      pražského kněžského semináře: hymnus ze "zeleného hymnáře"
      ("Ty, jenž jsi slavná koruna", v hymnáři určený k ranním
      chválám;
      též <a href="http://kancional.cz/833">Kancionál, 833</a>),
      žalmy a kantika na Korejsovy nápěvy,
      antifony <em>recto tono</em>, a navíc zpívané jen malou mužskou
      schólou (pravděpodobně právě ze seminaristů složenou, ale
      na dění v presbytáři jsem neviděl).
      Místo responsoria zpíval sbor nějaké mně neznámé zpracování
      zpěvu <em>Ubi caritas</em> (určeného pro mandatum
      na Zelený čtvrtek; tento nešťastný nápad by mohl být
      <a href="/blog.php?clanek=nesporyvysehrad2015.xml">importem z Vyšehradu</a>).
      Sešity připravené pro lid zdaleka neobsahovaly notový záznam
      pro všechny zpěvy, což se vymstilo
      především při úvodním verši (za absence nějakého suverénního
      vůdčího hlasu ho ve všeobecné nejistotě každý zpíval,
      jak uznal za vhodné).
    </p>
    <p>
      Pokud jde o texty, základem byly nešpory ze svátku sv. Vojtěcha
      (které nemají vlastní antifony k žalmům, takže se berou
      ty ze společných textů o jednom mučedníkovi).
      Svátku odpovídala i červená liturgická barva.
      Ta jednoznačně patřila Vojtěchovi - kardinálu Beranovi
      jakožto zemřelému by patřila fialová; jako případný budoucí
      svatý duchovní pastýř a vyznavač by byl slaven v bílé.
      Čtení bylo (v souladu s VPDMC 46) zvoleno delší,
      po něm bylo místo homilie přečteno poslední veřejné kázání
      kardinála Berana před komunistickou internací.
      Celé nešpory sledovaly Denní modlitbu církve, jen úvodní verš byl,
      kdovíproč, ve znění charitního breviáře ("Bože, shlédni a pomoz").
      Responsorium již bylo pohaněno výše.
      Zbývá ještě zmínit pro danou příležitost psané přímluvy,
      z nichž poslední (zcela v duchu "repatriace" a beze stopy "přenesení")
      pro kardinála Berana prosila o věčnou spásu.
    </p>
    <p>
      Závěrem dal požehnání pan kardinál Duka (značně dlouhý
      improvisovaný text, zcela v jemu vlastním pravičáckém -
      nebo snad je v daném případě přeci jen spravedlivější říci
      prostě "vlasteneckém" - duchu)
      a za zpěvu mariánské antifony <em>Regína coeli</em>
      byla rakev opět vynesena ven, aby pokračovala ve své pouti
      k místu nového pohřbu. Venku opět spustily žestě, zatímco
      uvnitř se sbor jal pět <em>Těbě pojem</em> (liturgie sv. Jana
      Zlatoústého, zpěv po slovech ustanovení;
      pokoušet se hledat spojitost s příležitostí by patrně
      bylo zbytečné).
      Odvaha a udatnost pěvců si zasluhuje náležitý obdiv,
      ovšem estetický zážitek to byl opět hrozný.
    </p>

    <h3>Basilika Nanebevzetí Panny Marie, Strahov</h3>
    <p>
      Než jsem se pěšky přesunul na Strahov, nějakou dobu to trvalo,
      takže jsem ohlášenou "modlitební vigilii" neviděl od začátku.
      Přišel jsem však na první žalm "modlitební vigilie" zcela
      liturgické, totiž modlitby se čtením, opět ze svátku sv. Vojtěcha.
      Strahovská premonstrátská kanonie normálně slaví oficium latinsky
      (a neveřejně), toto mimořádné veřejné slavení se však
      odbývalo v češtině.
    </p>
    <p class="petit">
      Na Strahově se pravidelněji slaví část oficia i veřejně a česky -
      každou středu "nešporomše" podle VPDMC 96,
      o slavnostech po večerní mši kompletář. Nakolik jsem se však
      dobře zorientoval, nejde o součást liturgického života komunity,
      nýbrž liturgické nabídky strahovské farnosti.
    </p>
    <p>
      Namísto hagiografického čtení o sv. Vojtěchu se četlo opět
      Beranovo poslední veřejné kázání v Československu,
      zde o to příhodnější, že ho pronesl právě ve strahovské basilice.
      Responsorium ovšem bylo ponecháno ze svatovojtěšského formuláře.
      A právě toto responsorium bylo z celého pátečního večera
      snad nejsilnějším prvkem tíhnoucím v rámci výše naznačeného napětí
      repatriace-přenesení k pólu "přenesení" a volajícím
      <em>ecce, Adalbertus novus.</em>
    </p>
    <p>
      Po responsoriu zaznělo i Te Deum, jak to o svátku sv. Vojtěcha
      náleží. Tedy Te Deum ... ve skutečnosti šlo o píseň
      "Bože, chválíme tebe". (To <em>není</em> legální úprava.)
    </p>

    <h3>(21. 4.) Katedrála</h3>
    <p>
      Na webu věnovaném hudební stránce <em>liturgie hodin</em> dění
      následujícího dne již vybočuje z tematického zaměření.
      Přeci však je vhodné si ho všimnout alespoň nakolik
      bylo výrazem "repatriace" nebo "přenesení".
    </p>
    <p>
      V jedenáct hodin se velké množství lidu shromáždilo
      na strahovském nádvoří. Po nějakém úvodním zpěvu a čtení
      se dal v pohyb
      dlouhý průvod. Ze srovnání s tím, v němž,
      podle Kosmova vyprávění, do Prahy vnesli ostatky
      sv. Vojtěcha, (náležitě) vychází jako slavný pohřební průvod,
      ne translace relikvií: tam relikviář s Vojtěchovými ostatky
      nesli "sám kníže a biskup", tady rakev vezl pohřební vůz,
      tažený šestispřežím ušlechtilých koní.
      Otřesné ovšem bylo, že po skončení litanií, které provázely
      první část průvodu, na tyto navázaly (spíše pod/průměrně
      interpretované) sacropopové písničky protestantské provenience.
    </p>
    <p class="petit">
      Slyšel jsem jednu nebo dvě řežábkoviny, ale jmenovat už bych
      je nemohl.
    </p>
    <p>
      Mše byla opět ze svátku sv. Vojtěcha, jehož lebka byla
      v presbytáři vystavena.
      Koncelebrovali ji zástupci biskupů z Polska, Maďarska a Slovenska -
      tedy postkomunistických zemí střední Evropy,
      až druhotně také zemí spojených se životem a kultem sv. Vojtěcha
      (z těch by chybělo Německo, kde studoval, a kde byl,
      díky přátelství s císařem Otou III., ponejprv ctěn jako svatý).
    </p>
    <p>
      Zmínku zasluhují přímluvy, které (srov. ty z pátečních nešpor)
      prosily mj. za zdárné zakončení Beranova kanonisačního procesu.
    </p>
    <p>
      Po mši byla rakev v liturgickém průvodu přenesena do kaple
      sv. Anežky České. Tam nynější biskup litoměřický přečetl
      dopis, který kdysi Beranovi do exilu adresoval jeho
      předchůdce, biskup Štěpán Trochta. Ten v něm
      vyjadřuje srdečné přání, aby jeho metropolitovi byl dopřán
      vytoužený návrat do vlasti, ale také otevřeně tematisuje možnost,
      že toto přání zůstane nenaplněno - a že se naplní jiným,
      vznešenějším způsobem, podobně, jako se do své diecése
      vrátil sv. Vojtěch. (Přenesení!)
    </p>
    <p class="petit">
      Tady, na samý závěr, pan arcibiskup uděloval požehnání.
      Latinsky. Trklo mě, že Boží požehnání bylo svoláváno
      na přímluvu
      <em>... sancti <strong>Adalberti,</strong> episcopi et martyris ...</em>:
      zatímco odnepaměti byl sv. Vojtěch v latinských liturgických
      textech jmenován svým biřmovacím jménem "Adalbertus",
      <a href="http://www.kpmk.eu/index.php/liturgie/liturgicke-texty/147-texty-narodniho-propria-latinsky">pokoncilní texty</a>
      se od této zvyklosti odklánějí. Napříště je Vojtěch
      i v latinských textech "S. Vojtěch" (v ostatních pádech
      skloňováno podle vzoru servus).
      V liturgické praxi se však tato reforma zatím neměla šanci
      prosadit, jistě i proto, že příslušné texty dosud nevyšly
      v oficiálním vydání.
    </p>

    <h3>Koncept</h3>
    <p>
      Náš článek z "beranovské anabáze" zachycuje jen část -
      chybí jak římská fáze, tak pondělní závěr. Přeci však
      jde o část representativní, protože jistě nejhojněji
      navštívenou a nejslavnostnější.
    </p>
    <p>
      Beranova repatriace nebyla co do liturgického obsahu
      (texty, zpěvy, akce)
      ztvárněna jako převoz zemřelého, ale jako oslava světce.
      Protože někdejšímu pražskému arcibiskupovi zatím
      liturgická úcta nepřísluší, byl převoz ostatků zasazen
      do jakési mimořádné vnější oslavy svátku sv. Vojtěcha.
    </p>
    <p class="petit">
      Mimořádné proto, že v pražské katedrále se
      i svátky populárních světců slaví vždy jen přímo v den,
      na který podle kalendáře
      připadnou, bez ohledu na to, zda jde o den pracovní.
      Všeobecná ustanovení o liturgickém roku a kalendáři
      navíc (čl. 58) počítají se slavením populárních svátků
      o blízké neděli a možnost slavení v jiný den (sobotu) neznají.
      Jde ovšem o řešení v pastorační praxi dosti rozšířené,
      což by mohlo znamenat, že pisateli těchto řádků nějaký
      důležitý kus platného liturgického práva uniká.
    </p>
    <p class="petit">
      Výše zmiňované ustanovení se však vztahuje pouze
      na mši. V případě oficia zřejmě šlo o aplikaci
      (značně nekonkrétního) ustanovení
      o "votivním oficiu" (VPDMC 245).
    </p>
    <p>
      Dramaturgie celé slavnosti vedle opakovaně zmiňovaného
      napětí "repatriace - přenesení"
      vykazuje ještě jedno, totiž "Vojtěch Slavníkovec - Josef Beran".
      Oba tyto oblouky se podařilo udržet ve vzduchu,
      žádný se úplně nezřítil.
      V určitých momentech přitom obě postavy (především skrze
      kontinuitu úřadu) jakoby na chvíli splynuly:
      máme na mysli nešporní hymnus (... Biskupem buď nám, Vojtěše,
      dál ve svých následnících ...)
      a ještě více responsorium modlitby se čtením, které na Beranovo
      závažné kázání odpovědělo slovy určenými pro sv. Vojtěcha:
      "Přijď, otče biskupe, svou navštiv domovinu *
      a rukama svých synů dál pracuj na své vinici."
    </p>
    <p>
      Výpověď celé slavnosti je jasná. Teď nezbývá než přát si,
      aby se smělému <em>"tvrzení"</em> dostalo shůry <em>potvrzení,</em>
      a na hrobě Božího služebníka, krajanům citelně přiblíženém,
      "chromí chodili, hluší slyšeli a malomocní byli očišťováni".
    </p>
  </text>
</clanek>
