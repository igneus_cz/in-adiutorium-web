<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Vlastní texty řeholních společností</titulek>

  <tags>řádová propria</tags>
  <kategorie>Projekt</kategorie>
  <datum>17.4.2016 00:48</datum>
  <souvisejiciNoty>omievzen</souvisejiciNoty>

  <souhrn>
    Komentář k nové sekci not ke stažení.
  </souhrn>

  <text>
    <p>
      Na stránce s notami ke stažení jsem dnes založil novou sekci
      pro vlastní texty řeholních společností
      a umístil do ní zhudebněné oficium sv. Evžena de Mazenod,
      zakladatele řádu <em>Misionářů oblátů Panny Marie Neposkvrněné</em>.
      K tomu se sluší dodat pár slov na vysvětlenou.
    </p>
    <p>
      Breviářová propria řeholních společností mě zajímají,
      takže je aktivně vyhledávám;
      v <a href="/blog.php?clanek=radovapropria.xml">jednom starším blogovém článku</a>
      je (čas od času aktualisovaný) seznam těch, která jsou volně
      dostupná na internetu.
      Kdo by zapátral na githubu v repozitáři projektu In adiutorium,
      zjistil by, že jsem
      <a href="https://github.com/igneus/In-adiutorium/commits/master/reholni">na zhudebnění některých z nich</a>
      začal pomýšlet už dosti dávno - v dubnu 2013 jsem udělal první
      kroky ke zpracování oficia slavnosti sv. Norberta podle českých
      textů premonstrátského řádu, ještě o něco dříve jsem začal
      pracovat na oficiu slavnosti sv. Dominika podle vlastních
      textů Řádu kazatelů.
      Výběr samozřejmě není náhodný: u dominikánů byl kdysi v noviciátu
      můj dědeček; jméno svatého Norberta pak nosím jako jedno
      z křestních.
    </p>
    <p>
      Z výše řečeného je zřejmé, že moje práce na zhudebnění
      vlastních textů některých řeholních společností je vedená
      čistě osobním zájmem. Nejde o zpěvy složené na přání nebo
      na objednávku a neočekávám, že budou někdy někde zpívány.
      Ani já je nikdy v rámci normálního běhu oficia zpívat nebudu.
      Skládám je pro radost ze skládání a pro bližší kontakt
      s dotčenými liturgickými texty a se svatými, k jejichž oslavě
      byly složeny.
    </p>
    <p>
      Když jsem před lety začal s texty o svatých zakladatelích
      dominikánů a premonstrátů, záhy jsem narazil
      na překážku, která mou práci v tom směru nadlouho zastavila:
      oba řády jsou starobylé (premonstrátský vznikl ve 12.,
      dominikánský ve 13. stol.) a k podstatným součástem
      jejich způsobu života patří chór. Každý má svou svébytnou
      chorální tradici. Budu jejich české liturgické texty
      zhudebňovat úplně nezávisle, "na zelené louce", jako jsem
      naložil s většinou textů Denní modlitby církve, nebo bych se měl
      spíše pokusit o zhudebnění navazující na danou řádovou tradici?
      Tu otázku jsem tenkrát nerozřešil a sv. Norberta i sv. Dominika
      jsem tudíž nechal čekat.
    </p>
    <p>
      To byl výchozí bod pro moje nepříliš dávné rozhodnutí
      zpracovat oficia svatých řádu Misionářů oblátů.
      (Ve skutečnosti je jediným světcem, jehož oficium je vystrojeno
      vlastními zpěvy, zakladatel - sv. Evžen.)
      Tato řeholní společnost mně osobně není nijak blízká.
      Ale především - nejen že nemá chórovou povinnost:
      jsem si téměř jist, že nemá <em>vůbec žádnou</em>
      tradici zpívaného oficia.
      Vůbec bych se nedivil, kdyby se ukázalo, že zhudebnění
      kompletního oficia sv. Evžena existuje pouze v českém
      jazyce a pouze od dnešního večera. Tak otázku, zda a jak bych
      se měl v hudebním výrazivu vázat na řádovou tradici,
      mohu nechat úplně stranou.
    </p>
    <p class="edit">
      [EDIT 4. 9. 2016] Jen pro pořádek:
      domněnka, že obláti nemají vůbec
      žádnou tradici zpívaného oficia, se ukázala být mylná.
      "V neděli ... v 18:15 pak máme zpívané
      nešpory s varhanním doprovodem ..."
      (<a href="http://oblati.cz/povolani.html">http://oblati.cz/povolani.html</a>)
    </p>
    <p>
      Podle téže logiky vyhlašuji program na dobu příští:
      až se mi zase mezi opravováním starších nepříliš povedených
      zpěvů zasteskne po zhudebňování nových, dosud nedotčených
      textů, pokračovat budu texty salesiánskými.
      Předběžné ohledání ukázalo, že tím budu mít postaráno
      o zábavu na delší dobu, protože salesiánský kalendář
      je bohatší než oblátský a breviářové texty obsahují několik plně
      vystrojených oficií, jakož i řadu skromněji vypravených
      památek "o dvou vlastních antifonách".
    </p>
    <p class="petit">
      Správně bych jistě měl zohlednit rčení
      <em>Jesuita non cantat, neque rubricat</em>
      a v rámci "nesmyslného" přednostního zhudebňování textů
      nezpívajících řádů dát přednost řádu <em>příslovečně</em> nezpívajícímu -
      Tovaryšstvu Ježíšovu. Jeho proprium breviáře ale, pokud
      je mi známo, veřejně dostupné není.
    </p>
    <p>
      Závěrem:
      To, co dělám, dělám v dobré víře, že tím nikomu neškodím.
      Kdyby se některá z dotčených řeholních společností cítila
      poškozena tím, že jsem neuměle zhudebnil a zveřejnil její
      (již předtím všeobecně veřejně dostupné) liturgické texty,
      samozřejmě jsem připraven příslušné dílo či díla stáhnout
      z nabídky a upustit od jejich dalšího šíření.
    </p>
  </text>
</clanek>
