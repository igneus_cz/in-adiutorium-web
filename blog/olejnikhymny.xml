<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Olejníkovy nešpory: hymny a žalmy</titulek>

  <tags>Josef Olejník, hymny, psalmodie</tags>
  <kategorie>Knihy</kategorie>
  <datum>2.1.2017 01:42</datum>
  <!-- <souvisejiciNoty></souvisejiciNoty> -->

  <souhrn></souhrn>

  <text>
    <p>
      Moje knihovna se nedávno rozrostla o varhanní doprovod
      Olejníkových <em>Nedělních nešpor</em>. Konečně si tak mohu
      na "olomoucký katedrální vesperál" udělat tak úplný náhled,
      jaký je jen hudebníkovi mých bídných kvalit
      skrz notový záznam možný.
      Když jsem tu psal o <em>Nedělních nešporách</em>
      <a href="/blog.php?clanek=olejniknedelninespory2013.xml">naposledy</a>,
      o tom, že bych si snad někdy mohl sám přehrát jejich doprovod,
      se mi ještě ani nezdálo. Dnes již je tomu jinak:
      v půli třetího roku hodin klavíru, jakkoli nejsem nijak dobrým
      žákem, přeci už jsou Olejníkovy nešpory literaturou
      docela dobře přístupnou. A že je doprovod hudebně zajímavější
      a "chutnější" než samotné melodie, které jediné jsem dosud
      znal ze zpěvníku, ochotně jím po chvilkách listuji
      a všímám si věcí, které mi dříve ucházely.
      Proto jsem se rozhodl své postřehy systematicky prověřit a
      sepsat.
    </p>
    <h3>Hymny</h3>
    <p>
      Z dřívějšího nedosti zevrubného listování a zpívání jsem měl
      zato, že hymny zahrnuté
      v <em>Nedělních nešporách</em> jsou vesměs standardní texty
      z Denní modlitby církve, s nápěvy podle "červeného hymnáře"
      a doprovodem buďto reprodukovaným z doprovodu k Mešním zpěvům,
      nebo od P. Olejníka nově složeným. Ukazuje se však, že situace
      je o mnoho pestřejší.
    </p>
    <p>
      Publikace obsahuje celkem 54 notovaných hymnů.
      Když od toho odečteme sedm dublet, zbyde 47 jedinečných skladeb.
      Z toho dále mezi dodatkovými hymny v závěru
      svazku jsou tři (č. 901-903) sborové úpravy hymnů dříve
      již otištěných v úpravě pro lidový zpěv a varhany.
      Několik textů je také otištěno vícekrát s různými nápěvy.
      Jedinečných zhudebněných textů by tedy bylo ještě o něco méně
      než výše uvedených 47.
    </p>
    <p>
      Většina hymnů je otištěna u jednotlivých nešporních
      formulářů nebo v úvodu jednotlivých liturgických období.
      Na konci jsou pod čísly 901-910 přidány pro tři hymny
      úpravy pro smíšený sbor a dále sedm hymnů ze zeleného
      hymnáře jako alternativa pro vybrané příležitosti.
      Posledně jmenované jsou všechny beze změny přetištěny
      z <a href="/knihy#doprzelenyhymnar">varhanního doprovodu k zelenému hymnáři</a>.
    </p>
    <p>
      Když jsem všechny hymny prošel, ukázalo se, že text i nápěv
      podle červeného hymnáře má 23 z nich.
      Devět dalších má text podle červeného hymnáře
      (tj. ten samý, jako je v breviáři), ne však shodný nápěv.
      V jednom případě je vzata známější varianta z Kancionálu
      (č. 100/1, <em>Vítej, milý Jezu Kriste</em>),
      v jednom dalším (č. 711, <em>Korouhve královské jdou vpřed</em>)
      je velice doslovně reprodukován nápěv odpovídajícího latinského
      hymnu (ovšem jiná varianta, než je v červeném hymnáři).
      Ostatní P. Olejník nově zhudebnil.
      Zbylé texty jsou ze zeleného hymnáře. Výjimku tvoří hymny
      č. 100/2 (<em>To slovo spásy lidí všech</em>) a
      300/2 (<em>Laskavý Stvořiteli náš</em>), které v žádném ze dvou
      standardních hymnářů nefigurují. Našli bychom je
      jen v charitním breviáři ze sedmdesátých let a v jeho dítku -
      současném breviáři českých dominikánů.
    </p>
    <p>
      V jednom případě si zatím nejsem docela jist autorem:
      Č. 100/3, <em>Všichni věrní křesťané,</em> má jiný nápěv,
      než je v červeném hymnáři, ale není jisté, že je Olejníkův -
      jeho charakter je takový, že by klidně mohl být vybraný
      z nějakého staršího pramene.
      Na údaje o autorství otištěné u jednotlivých hymnů se bohužel
      nelze spolehnout,
      protože jsou často evidentně nepřesné,
      zejména, jde-li o rozlišení,
      zda z uváděného pramene pochází jen text, nebo i zhudebnění.
    </p>
    <p>
      Pro mě zvlášť zajímavým případem jsou nová zhudebnění
      hymnů z červeného hymnáře. Můžeme si všimnout,
      že s výjimkou mariánského <em>Zdrávas, hvězdo moří</em> (č. 719)
      jsou všechny soustředěny ve formulářích vánočního a
      velikonočního cyklu. Bez přesnějších údajů o době a okolnostech
      zpracování jednotlivých nešporních formulářů se můžeme jen
      dohadovat, zda skladatel dotyčné hymny opatřil novými nápěvy
      o své vůli (třeba proto, že měly zvlášť inspirativní texty,
      nebo se mu nápěv v hymnáři nelíbil), či z nutnosti
      (např. v době přípravy daného formuláře neměl
      po ruce notovaný hymnář).
      Bylo by nasnadě očekávat, že se nového zpracování dostalo
      hymnům, jejichž nápěvy v červeném hymnáři jsou v rámci
      soudobého "běžného katolického kostelního repertoáru",
      nakolik je representován např. Kancionálem, výrazně neobvyklé
      nebo náročné. Zdá se ale, že tento ohled nehrál
      rozhodující úlohu. (Viz např. ponechání č. 716.)
      Snad nová zhudebnění
      hymnů z červeného hymnáře spadají do doby, kdy ještě nebylo
      na obzoru vydání uceleného vesperálu. Když se pak skladatel,
      již na sklonku života, ocitl před rozsáhlým úkolem zhudebnit
      všechny nešpory, které během roku
      mohou připadnout na nedělní večer, hymny, jako texty již
      zhudebněné, nechal stranou, a věnoval se jen textům
      "potřebnějším".
    </p>
    <p>
      Je třeba zdůraznit, že jsem si všímal pouze textů a
      <em>nápěvů</em>. O tom, jestli jsou všechny harmonisace
      Olejníkovy, nebo jsou některé převzaty z existujících sbírek
      (což by bylo u hymnů převzatých beze změn z červeného hymnáře
      veskrze pochopitelné), tak nemohu říci nic.
    </p>
    <table>
      <tr class="petit">
        <td>zdroje</td><td>číslo</td><td></td>
      </tr>
      <tr>
        <td>čK</td><td>100/1</td><td>Vítej, milý Jezu Kriste</td>
      </tr>
      <tr>
        <td>-O</td><td>100/2</td><td>To slovo spásy lidí všech</td>
      </tr>
      <tr>
        <td>čO?</td><td>100/3</td><td>Všichni věrní křesťané</td>
      </tr>
      <tr>
        <td>čO</td><td>200/1</td><td>Kriste, světa spasiteli</td>
      </tr>
      <tr>
        <td>čč</td><td>200/2</td><td>Pán Kristus se narodil</td>
      </tr>
      <tr>
        <td>čO</td><td>201</td><td>Z Hvězdy vyšlo Slunce</td>
      </tr>
      <tr>
        <td>čO</td><td>204</td><td>Rok nový již k nám přichází</td>
      </tr>
      <tr>
        <td>čO</td><td>205</td><td>Zdráva buď, přeslavná</td>
      </tr>
      <tr>
        <td>čč</td><td>207</td><td>Raduj se, všechno stvoření</td>
      </tr>
      <tr>
        <td>čč</td><td>209</td><td>Ctěme posla nebeského</td>
      </tr>
      <tr>
        <td>čč</td><td>300/1</td><td>Otec, Syn i Duch svatý</td>
      </tr>
      <tr>
        <td>-O</td><td>300/2</td><td>Laskavý stvořiteli náš</td>
      </tr>
      <tr>
        <td>čO</td><td>306</td><td>Korouhve královské; zhudebnění Olejníkovo, ale alespoň začátek se lehce přiznává k chorální předloze</td>
      </tr>
      <tr>
        <td>zO</td><td>400/1</td><td>U Beránkovy hostiny</td>
      </tr>
      <tr>
        <td>zO</td><td>400/2</td><td>U Beránkovy hostiny, nápěv jako č. 306</td>
      </tr>
      <tr>
        <td>čč</td><td>400/3</td><td>Kristovo vzkříšení</td>
      </tr>
      <tr>
        <td>čO</td><td>400/4</td><td>Přijď, Tvůrce, Duchu (mírně pozměněný text)</td>
      </tr>
      <tr>
        <td>čč</td><td>400/5</td><td>Duchu Tvůrce, přijď (nezměněný text)</td>
      </tr>
      <tr>
        <td>čč</td><td>534</td><td>Mocný všech věcí králi</td>
      </tr>
      <tr>
        <td>čč</td><td>535</td><td>Ó Nejsvětější Trojice božská</td>
      </tr>
      <tr>
        <td>čč</td><td>536</td><td>Kriste, rozkoši andělská</td>
      </tr>
      <tr>
        <td>čč</td><td>601 =603</td><td>Stvořiteli světa, Bože</td>
      </tr>
      <tr>
        <td>čč</td><td>602 =604</td><td>Pravý Bože na výsosti</td>
      </tr>
      <tr>
        <td>čč</td><td>701</td><td>Aj, Král slávy</td>
      </tr>
      <tr>
        <td>čč</td><td>702</td><td>Tvé světlo v noci světa bdí</td>
      </tr>
      <tr>
        <td>čč</td><td>703</td><td>Slavíce svátek milého</td>
      </tr>
      <tr>
        <td>čč</td><td>704 =705</td><td>Knížete apoštolského</td>
      </tr>
      <tr>
        <td>čč</td><td>706 =707</td><td>Vy naše knížata kněžská</td>
      </tr>
      <tr>
        <td>čč</td><td>708</td><td>Ó Ježíši velebný</td>
      </tr>
      <tr>
        <td>čč</td><td>709 =710</td><td>Zdrávas, Maria Panno</td>
      </tr>
      <tr>
        <td>č-</td><td>711</td><td>Korouhve královské; nápěv velmi doslovně chorální, ne shodný s tím v červeném hymnáři (srov. výše č. 306!)</td>
      </tr>
      <tr>
        <td>čč</td><td>712</td><td>Veselte se, slovanští</td>
      </tr>
      <tr>
        <td>čč</td><td>713</td><td>Svatý Václave, kníže náš</td>
      </tr>
      <tr>
        <td>čč</td><td>714 =715</td><td>Svaté Trojici jasné (jedna drobná úprava melodie)</td>
      </tr>
      <tr>
        <td>čč</td><td>716</td><td>Z hlubokosti volajících</td>
      </tr>
      <tr>
        <td>čč</td><td>717 =718</td><td>Ejhle, stánek Boží s lidmi</td>
      </tr>
      <tr>
        <td>čO</td><td>719</td><td>Zdrávas, hvězdo moří</td>
      </tr>
      <tr>
        <td></td><td>901<br/>902<br/>903</td><td>(sborové úpravy)</td>
      </tr>
      <tr>
        <td>zO</td><td>904</td><td>Ty, Tvůrce všeho, Bože náš</td>
      </tr>
      <tr>
        <td>zO</td><td>905</td><td>Půst čtyřicetidenní jsi</td>
      </tr>
      <tr>
        <td>zO</td><td>906</td><td>Jeruzalém, obec slavná</td>
      </tr>
      <tr>
        <td>zO</td><td>907</td><td>Ježíši Kriste, všehomíra vládce</td>
      </tr>
      <tr>
        <td>zO</td><td>908</td><td>Pane, zůstaň s námi, když se připozdívá</td>
      </tr>
      <tr>
        <td>zO</td><td>909</td><td>Já o studnici vím</td>
      </tr>
      <tr>
        <td>zO</td><td>910</td><td>Hvězdy dvě se z východu</td>
      </tr>
      <tr>
        <td colspan="3">
          <p>
            První sloupec zkratkami uvádí vždy zdroj textu
            (první znak) a nápěvu (druhý znak):
          </p>
          <ul>
            <li>č = <a href="/knihy#hymnarcerveny">"červený hymnář"</a></li>
            <li>z = <a href="/knihy#hymnarzeleny">"zelený hymnář"</a></li>
            <li>O = složil Josef Olejník</li>
            <li>K = Kancionál</li>
            <li>- = jiný</li>
          </ul>
        </td>
      </tr>
    </table>
    <h3>Nápěvy žalmů</h3>
    <p>
      Až když se na <em>Nedělní nešpory</em> dívám očima neumělého
      varhaníka, který musí počítat s časem na nacvičení doprovodu
      každé jednotlivé části každých nešpor, všiml jsem si, že autor
      s nápěvy žalmů zachází (k varhaníkovu prospěchu)
      ještě mnohem úsporněji, než jsem měl zato:
      V drtivé většině formulářů se první žalm zpívá vždy na ten samý
      nápěv, podobně i druhý žalm (vždy na stejný nápěv, odlišný
      od prvního).
      Zatímco žalmy mají nápěvy přiřazeny podle své posice
      v nešporní psalmodii, každé novozákonní kantikum má svůj
      pevně přiřazený nápěv a na ten se zpívá, kdykoli
      v některých nešporách přijde na řadu.
      Nemá ale každé svůj jedinečný: jen kantika ze Zj 19
      a z 1 Tim 3 mají nápěvy na míru, pro ostatní se dohromady
      vystačí se čtyřmi dalšími (resp. se dvěma, pokud zanedbáme
      výjimečné formuláře, o nichž dále).
    </p>
    <p>
      Editor <em>Nedělních nešpor</em>, p. Oldřich Heyl,
      ve své předmluvě upozorňuje na to, že některé z nejstarších
      do knihy zahrnutých
      nešpor jsou co do nápěvů atypické. Jako příklady uvádí
      variantu A nešpor slavnosti Zmrtvýchvstání Páně a slavnost
      sv. Václava. Můžeme upřesnit, že atypických formulářů
      je ve skutečnosti (jen) pět:
    </p>
    <ul>
      <li>slavnost Zmrtvýchvstání Páně, varianta A</li>
      <li>slavnost Seslání Ducha svatého</li>
      <li>slavnost sv. Václava, (jen) druhé nešpory</li>
      <li>Vzpomínka na všechny věrné zemřelé</li>
      <li>nešpory ze společných textů o Panně Marii</li>
    </ul>
    <p>
      Přitom nejvýrazněji atypické jsou poslední dva jmenované
      formuláře - jejich nápěvy žalmů a kantik jsou buďto jedinečné,
      nebo sdílené nanejvýš s jedním dalším formulářem,
      který ovšem vždy patří mezi výše vyjmenované formuláře
      atypické.
      Ostatní tři formuláře naproti tomu sdílejí některé nápěvy
      s typickým schématem a zdá se tedy, že snad jsou předstupni jeho
      formování.
    </p>
    <p>
      Dohromady se pro žalmy a kantika celých
      <em>Nedělních nešpor</em> užívá 15 nápěvů, ovšem pro "typické"
      nešpory, kterých je většina, jich stačí pouhých šest.
      Zbylé se vyskytují jen ve výše jmenovaných formulářích
      atypických.
      Níže nabízím přehled, vždy
      spolu s odkazem na první stránku, kde se ten který nápěv
      v knize vyskytuje.
    </p>
    <p>
      K nápěvům pro žalmy a novozákonní kantika přistupuje
      dalších 14 nápěvů Magnificat. Většina je jedinečná,
      jen nápěv Magnificat č. 806 je shodný s "prvním typickým žalmem".
      Distribuci jednotlivých Magnificat mezi typické a atypické
      nešpory jsem nesledoval. Je možné, že i tady by se ukázalo,
      že pro typické nešpory se všech 14 kusů nevyužívá.
    </p>
    <table>
      <tr class="petit">
        <td>strana</td><td>na nápěv se zpívá</td>
      </tr>
      <tr>
        <td>11</td><td>první žalm typických nešpor</td>
      </tr>
      <tr>
        <td>13</td><td>druhý žalm typických nešpor</td>
      </tr>
      <tr>
        <td>14</td><td>Zj 19</td>
      </tr>
      <tr>
        <td>77</td><td>Flp 2; Ef 1; Zj 15; Zj 4</td>
      </tr>
      <tr>
        <td>85</td><td>Kol 1; 1 Petr 2; Zj 4 (s. 707 - má stejnou melodii, ale jiný doprovod!)</td>
      </tr>
      <tr>
        <td>136</td><td>1 Tim 3</td>
      </tr>
      <tr>
        <td>241</td><td>atyp (jen Zmrtvýchvstání Páně A)</td>
      </tr>
      <tr>
        <td>337, 735</td><td>atyp (Seslání Ducha svatého, Věrných zemřelých)</td>
      </tr>
      <tr>
        <td>339</td><td>atyp (Seslání Ducha svatého)</td>
      </tr>
      <tr>
        <td>703, 761</td><td>atyp (sv. Václav, mariánské)</td>
      </tr>
      <tr>
        <td>705</td><td>atyp (sv. Václav)</td>
      </tr>
      <tr>
        <td>737</td><td>atyp (Věrných zemřelých)</td>
      </tr>
      <tr>
        <td>739</td><td>Flp 2 - atyp (Věrných zemřelých)</td>
      </tr>
      <tr>
        <td>759</td><td>atyp (mariánské)</td>
      </tr>
      <tr>
        <td>763</td><td>Ef 1 - atyp (mariánské)</td>
      </tr>
    </table>
    <h3>Závěr</h3>
    <p>
      Jako neumělému varhaníkovi se mi <em>Nedělní nešpory</em>
      po provedeném průzkumu nápěvů žalmů
      jeví ještě podstatně přístupnější
      a "lidovější". Zdá se, že naučit se doprovodit
      alespoň všechny "typické" nešpory není až tak
      nezvládnutelný úkol, jak by se na první pohled mohlo zdát.
    </p>
    <p>
      To, co bylo zjištěno o hymnech, vyvolává otázky.
      Jestliže v hymnech <em>Nedělních nešpor</em> máme před sebou
      de facto "hymnář olomoucké katedrály", kdo jednotlivé hymny
      (z obou hymnářů a charitního breviáře) vybíral?
      Sám P. Olejník? Někdo další? Co ho při výběru vedlo?
      To se pravděpodobně nikdy nedozvíme, ale zajímalo by mě to moc.
    </p>
  </text>
</clanek>
