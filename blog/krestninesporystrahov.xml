<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Křestní nešpory strahovských premonstrátů</titulek>
  
  <tags>křestní nešpory, Praha, kláštery, Strahovský klášter, premonstráti</tags>
  <kategorie>Ze života</kategorie>
  <datum>10.4.2015 21:30</datum>
  <!-- <souvisejiciNoty></souvisejiciNoty> -->
  
  <souhrn></souhrn>
  
  <text>
    <p>
      "Křestní nešpory," totiž nešpory ve velikonočním oktávu
      rozšířené o průvod ke křtitelnici,
      patří ke zvláštnostem premonstrátské liturgie.
      V minulosti jsem se na ně nejednou chystal, ale opravdu se na jedny dostat
      se mi podařilo až dnes.
      Protože jde zároveň o mou vůbec první přímou skušenost se slavením
      liturgie hodin ve strahovském klášteře, bude potřeba další návštěvy
      někdy mimo velikonoční oktáv, abych ověřil, co všechno bylo zvláštností
      křestních nešpor a co je společné všem nešporám slaveným v dané komunitě.
      Že premonstráti řádové zvláštnosti v liturgii hodin (i v té pokoncilní) 
      mají, to si věrní čtenáři pamatují 
      <a href="/blog.php?clanek=tenebrae.xml">od jednoho z minulých článků (neračte přehlédnout komentáře)</a>.
      Zatím nabízím prostě popis toho, co jsem viděl a slyšel.
      Jisté je, že křestní nešpory křestními nešporami dělá již zmíněný
      průvod ke křtitelnici. (Srov. také <a href="http://www.praytellblog.com/index.php/2014/04/03/non-solum-baptismal-vespers/">nedávný článek o křestních nešporách na blogu PrayTell;</a>
      premonstrátská řádová tradice v něm však není zmíněna, autor
      ji patrně nezná.)
    </p>
    <p>
      Pootevřenou mříží jsem se do baziliky Nanebevzetí Panny Marie protáhl
      na poslední chvíli. Chórové lavice však byly k mému překvapení prázdné!
      Až s úderem páté v sakristii kantoři spustili Kyrie a bratři vstoupili
      do kostela v liturgickém průvodu.
      (Vstup za zpěvu Kyrie - další dříve neviděné řešení "introitu" k nešporám!)
      Dalším překvapením pro mě byla relativně nízká účast - jistě ne více
      než 10 členů komunity. Zřejmě projev toho, že premonstráti nejsou
      mnišský řád v benediktinské tradici charakterisované oním 
     "před službou Boží se nesmí dávat přednost ničemu," (RB 43)
     a mnozí mají povinnosti, které jim neumožňují každodenní účast
     na nešporách v chóru.
    </p>
    <p>
      Po Kyrie jsem nezaznamenal žádné další "úvodní rity",
      následovala rovnou psalmodie.
    </p>
    <p>
      Celé nešpory byly zpívané, a to latinsky, chorálně. Pokud jsem dobře
      poslouchal, psalmodie byla v alternaci kantoři/ostatní, ne 
      v alternaci stran chóru.
    </p>
    <p>
      Před prvním žalmem zazněla jedna z alelujatických antifon
      (ne "epická antifona o vzkříšení" předepsaná v breviáři).
      Po prvním žalmu (nejsem si jist, zda to byl žalm <em>Dixit Dominus</em>,
      který se na daném místě modlíme my nepremonstráti, nebo jiný) 
      následovala orace a výzva k průvodu. Za zpěvu žalmu
      <em>In exitu,</em> responsoriálně prokládaného další alelujatickou antifonou,
      se všichni, s rozsvíceným paškálem a kadidelnicí, ubírali
      do kaple sv. Norberta (sloužící zároveň jako kaple eucharistická
      a křestní). Tam bylo přečteno evangelium (o Nikodémovi - tedy
      perikopa nijak zvlášť velikonoční, ale typicky křestní)
      a pronesena krátká promluva.
      Nato se všichni za zpěvu kantika <em>Salus et gloria et virtus</em>
      vrátili do chóru.
    </p>
    <p>
      Tady bylo přečteno capitulum a kantoři zazpívali <em>Haec dies</em>,
      ovšem ne jako samostatnou antifonu, jak je obvyklé v římské liturgii,
      nýbrž jako dvakrát zopakovanou antifonu s vloženým veršem
      (podobně melismatickým jako antifona sama, pročež jsem mu nerozuměl;
      jen hrubě hádám, že to mohlo být 
      <em>"Confitemini Domino, quoniam bonus,"</em> snad
      ještě s delším pokračováním.
    </p>
    <p>
      Až tady přišel na řadu hymnus, následovaný veršíkem.
      Umístění hymnu a po něm následující veršík odpovídají uspořádání římské
      liturgie před poslední reformou. Premonstráti by za jeho zachování
      patrně inkasovali kladné body od prof. Dobszaye, který přestrukturování
      římských nešpor ostře kritisoval.
    </p>
    <p>
      Antifona k Magnificat byla nejnejistěji zazpívanou částí celých
      nešpor, což ovšem nikoho nepřekvapí, jelikož antifonu k evangelnímu
      kantiku je nutné nacvičit na každý den jinou.
      Při Magnificat nejprve celebrant velmi důkladně okouřil oltář,
      následně turifer celebranta a jen tak tak stihl okouřit ještě i kantory,
      patrně jako reprezentanty komunity nebo lidu vůbec.
      Žalmový nápěv Magnificat VIII. modu byl drobně odlišný od všeobecně
      známého v tom, že druhý poloverš nezačíná na recitandě,
      ale asi o malou tercii níže. Zvláště krátké verše díky této ozdobě
      zněly, přišlo mi, o kousek libozvučněji.
    </p>
    <p>
      Pak už přímluvy, Otčenáš, orace, požehnání, a bratři se za zpěvu
      antifony <em>Regina coeli</em> (nějaký relativně jednoduchý nápěv,
      ale podle všeho řádový, nesouhlasící s jednodušším nápěvem
      obvyklým v římských chorálních knihách posledního století -
      a dokonce ho ani vzdáleně nepřipomínající)
      odebrali zpátky, odkud přišli.
    </p>
    <p>
      Závěrem jen upozorním na to, že křestní nešpory se v římské církvi
      po liturgické reformě těší určité "zvláštní přízni liturgického
      zákonodárce": i když nejsou součástí římské liturgie (v tom smyslu,
      že liturgické knihy římského obřadu pro ně neobsahují zvláštní rubriky
      ani liturgické texty), jsou porůznu "příznivě zmiňovány".
      Viz např. VPDMC 213, ale i další dokumenty a knihy zmiňované ve výše
      zmiňovaném článku na blogu PrayTell.
      Srov. také závěr článku Štěpána Horkého <a href="http://www.teologicketexty.cz/casopis/2012-2/Impulzy-slovanske-bohosluzby-pro-soucasnou-liturgickou-praxi.html">Impulzy slovanské bohoslužby pro současnou liturgickou praxi (TT 2012/2)</a>,
      kde je popisována i realizace křestních nešpor v rámci liturgického
      života (nepromonstrátské) farnosti.
    </p>
    <p>
      Konečně, kdo by chtěl křestní nešpory na vlastní oči a uši zažít,
      má letos ještě šanci zítra a pozítří. Pak zase napřesrok.
    </p>
  </text>
</clanek>
