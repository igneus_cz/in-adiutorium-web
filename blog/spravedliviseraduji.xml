<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Spravedliví se radují</titulek>

  <tags>responsoria, de lege ferenda</tags>
  <kategorie>Texty</kategorie>
  <datum>1.2.2022 03:12</datum>
  <souvisejiciNoty>czena, cmuz</souvisejiciNoty>

  <souhrn>
    K jednomu responsoriu, které v "laickém breviáři" nenajdete.
  </souhrn>

  <text>
    <p>
      Když jsem se pustil do zhudebnění Denní modlitby církve,
      zhudebňovaným "libretem" mi po první tři až čtyři roky
      byl tkzv. "laický breviář" - jednosvazkový diurnál - resp.
      jedno jeho konkrétní vydání: to z r. 1994, protože
      pro studentský rozpočet byl nový breviář, když bylo možné
      zadarmo získat starší, nesmyslně vysoký zbytečný výdaj.
      Proto jsem v prvních letech vpodstatě ignoroval zpěvy
      modlitby se čtením
      a do jejich systematického doplňování do již existujících materiálů
      jsem se pustil až poté, co jsem v roce 2013 začal vydělávat
      a mohl si pořídit nové úplné čtyřsvazkové vydání.
    </p>
    <p>
      Má tkzv. "kněžský" breviář
      oproti "laickému" - kromě textů modlitby se čtením -
      ještě nějaký další exklusivní obsah?
      Otázka, kterou mě snad nikdy nenapadlo položit si.
      Až jsem si během ranních chval
      všiml, že v mých <a href="/noty#cmuz">notách společných textů o svatých mužích</a>
      chybí responsorium <em>Spravedliví se radují</em>,
      které breviář (třetí svazek čtyřsvazkového) předepisuje jako závaznou variantu
      pro společné svátky více svatých.
      Jak to?!
    </p>
    <blockquote>
      <p class="sc">Zpěv po krátkém čtení</p>
      <p>
        <strong>O.</strong> Měl Boží zákon ve svém srdci, [...]
      </p>
      <p><em>
        O více svatých:
      </em></p>
      <p>
        <strong>O.</strong> Spravedliví se radují *
        a hledí na Boží tvář. Spravedliví.<br/>
        <strong>V.</strong> Veselí se, jásají radostí *
        a hledí na Boží tvář. Sláva Otci. Spravedliví.
      </p>
      <p class="petit">
        (Denní modlitba církve, svazek 3, KNA 2005, s. 1770)
      </p>
    </blockquote>
    <p>
      Nejjednodušším vysvětlením samozřejmě je, že jsem ho
      přehlédl, nebo se před lety až tak nestaral, jestli
      zhudebním všechny alternativní zpěvy toho kterého formuláře.
      Ale nahlédnutí do onoho diurnálu z roku 1994 ukázalo,
      že z jeho textů jsem tehdy nevynechal nic.
      Responsorium <em>Spravedliví se radují</em> tam v ranních chválách
      společných textů o svatých mužích prostě není.
      Následné srovnání jednotlivých svazků úplných vydání
      (DMC 2005; 1989; latinská editio typica 1971;
      editio typica altera dosud nevlastním, ale na její obsah lze nepřímo usuzovat z DMC, která vznikla na jejím základě)
      pak ukázalo překvapivou skutečnost, že (od začátku a konsistentně
      mezi vydáními) toto responsorium obsahují pouze svazky
      pro liturgické mezidobí. Svazek adventně-vánoční ani
      postně-velikonoční ne.
    </p>
    <p>
      To, že responsorium <em>Spravedliví se radují</em> chybí
      v jednosvazkových vydáních Denní modlitby církve
      (není ani v tom nejnovějším z r. 2007), nejsnáze
      vysvětlíme jako redakční chybu při kompilaci
      materiálu napříč čtyřmi svazky do svazku jediného,
      popř. jako vědomé zjednodušení (než psát výstřední rubriku
      "o více svatých, ale jen mimo dobu adventní, vánoční, postní a velikonoční",
      radši to responsorium úplně vynecháme).
      Jak ovšem vysvětlit to, že úplná vydání mají pro liturgické mezidobí
      extra responsorium pro svátky více svatých,
      příhodně mluvící o svatých v plurálu, narozdíl od singulárně
      formulovaného <em>Měl Boží zákon ve svém srdci</em>,
      ale svazky pro "silné doby" se omezují na responsorium jediné,
      právě ono singulární?
      Jistě to není obsahem, protože text responsoria
      <em>Spravedliví se radují</em> neobsahuje nic,
      co by se nedalo zpívat i v adventu, o Vánocích,
      v době postní, a beze všeho by pro něj šla obvyklým způsobem
      připravit i velikonoční varianta s aleluja.
    </p>
    <p class="petit">
      Když je řeč o velikonočních variantách responsorií,
      nemůžu si na okraj nepostěžovat, že způsob úpravy textů
      responsorií ranních chval a nešpor
      pro velikonoční variantu s aleluja je v Denní modlitbě církve
      charakteristicky jiný než v její latinské předloze, takže
      jsou česká alelujatická responsoria oproti odpovídajícím
      latinským výrazně ukecanější -
      až je příslušný tradiční nápěv (stavěný pro rozumně krátké texty),
      když se pro ty dlouhé české texty použije,
      nejednou napnut daleko za meze své elasticity.
    </p>
    <p>
      Jako další možné vysvětlení se nabízí, že v době adventní,
      vánoční, postní a velikonoční snad žádné oslavy více svatých,
      pro něž přichází v úvahu společné texty o svatých mužích,
      nejsou. Proti tomu však jest, že takové zacházení se společnými
      texty by nebylo obvyklé ani rozumné, protože
      <em>commune sanctorum</em> je vždy koncipováno tak,
      aby pokrylo nejen potřeby liturgického kalendáře, který je
      v dané liturgické knize (nebo jejím svazku) otištěn
      a se kterým ostatní obsah knihy primárně počítá,
      ale potřeby pokud možno všech možných kalendářů, se kterými by kniha
      mohla být v budoucnu používána. Tzn. i potřeby svátků více
      svatých mužů, které možná budou zavedeny v budoucnu a padnou
      do doby adventní nebo velikonoční, jakož i svátků více
      svatých mužů, které se třeba slaví nebo budou slavit jen
      v jednotlivé diecési, městě, či jen v jediném kostele kvůli
      ostatkům v něm chovaným.
    </p>
    <p>
      Za nejpravděpodobnější vysvětlení považuji to,
      že v době přípravy prvního vydání <em>Liturgiae horarum</em>
      na to, že by se vedle responsoria <em>Měl Boží zákon ve svém srdci</em>
      (resp. jeho latinského ekvivalentu)
      hodil ještě druhý text pro společné oslavy více světců, prostě přišli pozdě.
      Editio typica vycházela v průběhu roku 1971 v číselném pořadí
      svazků, tzn. od adventu přes půst a Velikonoce k liturgickému
      mezidobí. Je známé, že se až v průběhu tisku přišlo na určité
      závažnější nedostatky, které se narychlo opravovaly
      (doložit citací to teď hned nemůžu, ale nejpravděpodobněji
      jsem to četl u <a href="https://aleph.nkp.cz/F/?func=direct&amp;doc_number=005476568&amp;local_base=SKC">Campbella</a>,
      příp. u <a href="https://aleph.nkp.cz/F/?func=direct&amp;doc_number=005916266&amp;local_base=SKC">Bugniniho</a>).
      A zdá se, že tohle je podobný případ. Nejde o úplně hrubý
      nedostatek (v liturgii není nijak neslýchané zpívat
      biblické texty v singuláru i o svátku více světců),
      ale o drobné vylepšení, ke kterému však nejspíš došlo až v době,
      kdy první dva svazky nového breviáře byly již vytištěné
      a nešlo do nich zasahovat. Ale následující vydání mohla a měla
      toto vylepšení aplikovat již do všech svazků, protože
      žádný rozumný důvod pro jeho omezení na liturgické mezidobí neexistuje.
    </p>
    <p>
      Každopádně objev tohoto dosud mi skrytého responsoria
      značně nahlodal mou víru, že mám Denní modlitbu církve
      zhudebněnou kompletně, a v to, že ve věcech, jako je obsah
      společných textů o svatých, se lze (s výjimkou postních
      a velikonočních specifik) spolehnout na identitu textů
      mezi svazky. Kompletní korektura zhudebněných textů, až na ni časem
      dojde, tak bude ještě úmornější záležitost, než jsem
      dosud předpokládal.
    </p>
  </text>
</clanek>
