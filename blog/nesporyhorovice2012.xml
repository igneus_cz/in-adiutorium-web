<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Nešpory mezi koncertem a liturgií. Hořovice, 1. neděle adventní 2012</titulek>
  
  <tags>Hořovice, advent, Ó antifony</tags>
  <kategorie>Ze života</kategorie>
  <datum>31.7.2013 23:30</datum>
  <!-- <souvisejiciNoty></souvisejiciNoty> -->
  
  <souhrn>
    rozbor nahrávky nešpor nalezené na Youtube
  </souhrn>
  
  <text>
    <p>
      Před několika dny jsem našel na Youtube záznam nešpor z letošní první
      adventní neděle z Hořovic.
      Zkoušel jsem kontaktovat někoho ze jmenované farnosti, kdo by
      mi k těm nešporám mohl prozradit některé detaily, na které jsem
      upřímně zvědavý, ale nedostalo se mi zatím žádné odpovědi a já
      už nevydržím nechávat si ten krásný objev déle pro sebe.
      Kdyby se tedy někdo z oslovených později přeci jen ozval, článek
      doplním, nebo napíši další jako pokračování.
    </p>

    <iframe width="420" height="315" src="http://www.youtube.com/embed/qL86fxp5EPE" frameborder="0"> </iframe>

    <h5>Uspořádání liturgického prostoru</h5>

    <p>
      Nahrávka nepříliš rozšířené a známé "liturgické akce",
      jako jsou nešpory, se může stát cenným výukovým materiálem.
      Z této perspektivy doporučuji čtenářově pozornosti způsob,
      jak se v rámci nešpor, které sledujeme, pracuje s liturgickým
      prostorem. Návštěvníci bohoslužby, kterých zřejmě nebylo
      mnoho, sedí v lavicích, jak je obvyklé.
      Celebrant zaujímá své místo v presbytáři.
      Sbor, který je jednoznačně "hlavním tahounem" této liturgie,
      je rozestaven v půlkruhu před presbytářem.
    </p>
    <p>
      Tomuto uspořádání bych vytkl dvojí. Podivným dojmem působí
      izolace celebranta za "zdí" tvořenou sborem.
      Zároveň se sbor svým umístěním ocitá v centru pozornosti víc,
      než je žádoucí (pokud jsou nešpory liturgií a ne koncertem;
      na koncertě to naopak pochopitelně žádoucí je).
      Preferované uspořádání je méně koncentrované na sbor
      a otevřené "směrem k Bohu",
      ať tak, že sbor tvoří dva chóry rovnoběžné s osou kostela,
      ať tak, že zpívá v lavicích pro lid.
      Zvolené, z mého pohledu "liturgicky neideální" uspořádání
      je však výhodné pro dirigování a zřejmě i s ohledem na šíření
      zvuku do chrámové lodi. 
      Zároveň při prostorových dispozicích daného kostela
      by např. zmíněné uspořádání do dvou rovnoběžných chórů bylo těžko možné.
    </p>

    <h5>Identifikace textů a zpěvů</h5>

    <ul>
      <li>04:30 Ejhle, Hospodin přijde</li>
      <li>07:13 Deus, in adiutórium meum inténde</li>
      <li>08:04 "hymnus" Poslán jest od Boha anděl</li>
      <li>10:02 antifona Aj anděl Páně</li>
      <li>10:50 žalmy 113, 19, 117 (na konci se opakuje antifona)</li>
      <li>17:56 krátké čtení Řím 13,11b-12</li>
      <li>18:30 zpěv po krátkém čtení Solis ...?</li>
      <li>19:26 antifona "Ó Vycházející"</li>
      <li>20:03 Magnificat (na konci se opakuje antifona)</li>
      <li>27:25 přímluvy, Otčenáš, závěrečná modlitba, požehnání</li>
      <li>30:23 Benedicámus Dómino</li>
      <li>30:49 "závěrečná mariánská antifona" Zdráva buď, Panno Maria</li>
    </ul>

    <p>
      Nešpory otevírá adventní zpěv <b>Ejhle, Hospodin přijde</b>
      (Jednotný kancionál, 101B). To je začátek nešporám cizí -
      zařazen byl zřejmě z přesvědčení, že když při liturgii probíhá nějaký
      přesun, např. nástup celebranta a sboru, musí k tomu znít hudba
      a pokud možno nějaký zpěv. -- Toto přesvědčení je vzásadě správné,
      hudební struktura mše v římském ritu mu dává zcela za pravdu.
      Na otázku, proč liturgie hodin s nějakým vstupním zpěvem
      odpovídajícím mešnímu introitu nepočítá a na začátku předpokládá již
      přítomného předsedajícího začínajícího "Bože, pospěš mi na pomoc",
      nedokážu dát uspokojivou odpověď. Myslím, že je to jeden z jejích
      přetrvávajících monastických rysů. Zatímco v klášteře je přirozené,
      že hebdomadář začíná hodinku poté, co se bratři shromáždí k modlitbě,
      bez nějakého velkého úvodu, pro nešpory v kontextu
      nemonastickém - tedy obvykle výjimečné a slavnostní - 
      by něco na způsob introitu bylo vhodné.
    </p>
    <p>
      Mimochodem, "Ejhle, Hospodin přijde" je překlad nešporní antifony
      z 1. neděle adventní. Rozšířené nápěvy tu věrněji, tu volněji
      reprodukují starý nápěv latinského originálu.
      (Dnes tuto antifonu zpíváme jako druhou v prvních nešporách.
      Když jsem pro ni před půlrokem psal nápěv, vůbec mi nedošlo,
      že mám před sebou modernější text notoricky známého "Ejhle",
      a tak má antifona v mém zpracování mělodii úplně jinou, v modu I.)
      Zajímalo by mě, ale nevím, jak se antifona z oficia dostala
      na začátek rorátní mše.
    </p>
    <p>
      Po "Ejhle" celebrant začíná úvodní verš <b>Deus, in adiutórium meum
      inténde</b>. I když většina textů bude v češtině,
      zazní během nešpor latina ještě několikrát: jmenovitě při
      zpěvu po krátkém čtení, Magnificat, při požehnání a závěrečném
      Benedicámus Dómino. Nic proti tomu - je to vkusné, liturgickými
      předpisy výslovně legitimované (VPDMC 276), ... Kdyby se mi ale
      přeci jen podařilo spojit se s tím, kdo ony nešpory připravoval,
      na důvod pro zařazení latinských zpěvů bych se určitě zeptal.
      Byly ve hře motivy čistě estetické? "Archeologické"? Nebo nějaké jiné?
    </p>
    <p>
      Těm, kdo znají český breviář, nemusím říkat,
      že <b>hymnus</b> Poslán jest od Boha anděl
      v něm nenajdeme. Je volně vybraný z pokladu českých adventních/mariánských
      písní.
    </p>
    <p>
      Následuje psalmodie, pro mě jakožto amatérského skladatele
      a badatele tradičně nejvíc vzrušující část celých nešpor:
      předně tu máme krásnou <b>antifonu</b> Aj anděl Páně.
      Protože její jazyk nasvědčuje tomu, že už se na ni nevztahují
      autorská práva skladatele, drze jsem ji odposlechl a níže nabízím
      zapsanou.
      Velice by mě zajímal pramen, z něhož pochází. Vím zatím pouze,
      že žádná z mnou dosud zhudebněných adventních antifon
      současného breviáře nemá odpovídající text. Nevím, jde-li
      o překlad některé z antifon starší podoby oficia či zpěv jiného původu.
      Mám vážné podezření, že by mohlo jít o "antifonu před Rorate"
      nebo úryvek chorálu
      z některého starého českého rorátníku, ale dosud jsem toto podezření
      neověřoval.
    </p>
    <img src="/blog/prilohy/nespory_horovice2012/ant_ajandelpane.png" alt="antifona Aj anděl Páně" />
    <p>
      Antifona je pouze jedna, na způsob "antiphona super psalmos",
      a zpívá se před prvním žalmem a po konci posledního.
      <b>Žalmy</b> se všechny zpívají na chorální nápěv odpovídající
      antifoně, tedy V.a. Použitý text je z liturgického překladu,
      s několika nepatrnými zásahy (jedna změna slovosledu; úprava dělení
      na poloverše). Melodický vzorec je na text nasazen tak,
      že obvykle, ale nikoli vždy, souhlasí hlavní či vedlejší slovní přízvuky 
      se zamýšlenýmy
      přízvuky hudebními. Předpokládám, že text byl značkován podle citu
      a ne podle nějakých přesných pravidel.
    </p>
    <p>
      Uchu znalému žaltáře neunikne, že žalmy nejsou vzaté z 2. nešpor
      1. neděle žaltáře, jak by se slušelo, ba ani z jiné neděle nebo
      jakéhokoli jiného breviářového formuláře. Jsou volně vybrané.
      Žalm 19, v současném breviáři vždy dělený na poloviny,
      které se navíc obvykle nezpívají v rámci jedné hodinky,
      se zpívá v kuse. Úplně chybí novozákonní kantikum - místo něj máme
      třetí žalm.
      (Protože něco podobného <a href="/blog.php?clanek=poutboleslav2012.xml">jsme už jednou potkali</a>,
      říkám si, jestli i za těmito nešporami tak či onak nestojí trapisté :) )
    </p>
    <p>
      <b>Krátké čtení</b> není z 2. nešpor 1. neděle adventní, ale z ranních chval
      téže neděle. To naznačuje, že se tvůrce "liturgického programu"
      na breviář ohlížel, ale zacházel s ním volně.
    </p>
    <p>
      Následuje <b>zpěv po krátkém čtení</b>. Schválně nepíši "responsorium",
      protože formálně o responsorium nejde.
    </p>
    <p>
      <b>Antifona k Magnificat</b> je překladem antifony "O Oriens" -
      jedné ze všeobecně známých "Ó-antifon". Tato konkrétní se zpívá
      k Magnificat 21.12.
      Pro srovnání podávám i znění odpovídající latinské antifony
      podle římských chorálních knih z 20. stol.
      (Nezištná reklama: za to, že jsem nemusel opisovat a mohl si rovnou
      stáhnout obrázek, vděčím 
      <a href="http://gregobase.selapa.net/">GregoBase</a>.
      Doporučuji používat...)
      Porovnáním zjistíme, že překlad je "slovo za slovo",
      melodie však je, při zachování většiny strukturních tendencí,
      výrazně odlišná, takže se domnívám, že tvůrce českého překladu
      měl před sebou jinou melodii latinské předlohy.
      Zrovna tak je ovšem možné, že k hudební předloze přistupoval
      svobodněji než k textu a melodii upravil.
      "Česká" melodie se od srovnávané "latinské" nápadně odlišuje
      dekompozicí nebo vypuštěním některých melismat a zejm.
      nápadnou preferencí intervalu d-f oproti d-e.
    </p>
    <img src="/blog/prilohy/nespory_horovice2012/ant_ovychazejici.png" alt="antifona Ó Vycházející" />
    <img src="/blog/prilohy/nespory_horovice2012/ant_ooriens.png" alt="antifona O Oriens" />
    <p>
      Kantikum <b>Magnificat</b> se zpívalo latinsky,
      na chorální nápěv I.D. Ten sice antifoně, přísně vzato, neodpovídá,
      ale žalm v dórské tónině s antifonou v hypodórské zní dobře.
      Choralistu trkne, že se verše kantika zpívají bez initia,
      u evangelních kantik obvyklého.
      To je zřejmě potřeba připsat tomu, že se k vedoucí sboru nedostala
      část know-how ohledně zpěvu hodinek. Samozřejmě to je nepodstatná drobnost.
    </p>
    <p>
      <b>Prosby a závěrečná modlitba</b> jsou v rámci celých nešpor
      vlastně jediným kusem textu, který byl vzat z toho formuláře,
      ze kterého vzat být měl - tedy z 2. nešpor 1. neděle adventní.
    </p>
    <p>
      Po požehnání následuje <b>Benedicámus Dómino</b>.
      To podle současného breviáře není standardní součástí nešpor,
      ale v roce 2009 vydaný pokoncilní římský vesperál pro neděle a svátky
      tento zpěv uvádí jako možnost "propuštění" následujícího po požehnání. 
      (Ritus conclusionis se dělí na oddíly <em>benedictio</em> 
      a <em>dimissio</em>. <em>Benedictio</em>
      je pevné, <em>dimissio</em> volitelné. Benedicámus je jednou
      z možných podob <em>dimissio</em>.)
      Nápěv Benedicámus Dómino, který v závěru našich nešpor zazní,
      ovšem v novém vesperálu není. Našel jsem ho v Antiphonale Romanum 1912
      na s. 50* s určením "In Dominicis per annum". (Adventní neděle
      tam mají svůj vlastní, jiný, ale to je asi celkem jedno.)
    </p>
    <p>
      Nakonec se zpívá píseň <b>Zdráva buď, Panno Maria</b> - jedna z písní,
      které jsou v oficiálním českém hymnáři předepsané jako alternativy
      závěrečných mariánských antifon.
    </p>

    <h5>Hodnocení liturgického "programu"</h5>
    
    <p>
      Z čistě zákonického hlediska souladu s liturgickými předpisy
      jsou zachycené nešpory "nedovolené". Dovolím si nerozebírat to kus
      po kusu a normu po normě, protože jde o téměř identický případ
      jako při <a href="/blog.php?clanek=poutboleslav2012.xml">svatováclavských 
      nešporách staroboleslavských minulého roku</a>.
    </p>
    <p>
      Jako člověk hodně zaměřený na slovo
      jsem nešťastný, že mi polyfonní pasáže hymnu a zpěvu po krátkém čtení
      nebyly srozumitelné, od toho ale přímým účastníkům nešpor
      pravděpodobně odpomohly rozdané sešity.
    </p>
    <p>
      Hodnotit nešpory co do uměleckých kvalit skladeb a výkonu sboru
      nejsem kompetentní. Jako nekompetentní hudební laik nicméně
      neskrývám nadšení.
    </p> 
    <p>
      Ožehavou otázkou je hodnocení volně sestaveného liturgického formuláře
      z pohledu liturgického.
      Texty jsou zdařile vybrané s ohledem na liturgickou dobu - advent.
      Méně se už bere zřetel na to, že jde o druhé nešpory nedělní -
      chybí žalm 110, zpěv po čtení je zcela zaměřený na Pannu Marii, ...
      Výběr krátkého čtení konečně není úplně šťastný s ohledem na denní
      dobu. Jistěže sv. Pavel o spánku a o "hodině, kdy je třeba se z něj 
      probudit" píše v obrazném slova smyslu a myslí na probuzení duchovní.
      Avšak to, že tato slova čteme na začátku dne, v ranních chválách,
      kdy je Pavlův obraz jaksi hmatatelnější, není náhodné.
      Když si ještě všimneme, že tu o 2. nešporách 1. neděle adventní
      zaznívá antifona k Magnificat, která v řádném uspořádání breviáře
      patří do posledního předvánočního týdne a je výrazem jeho vystupňovaného
      očekávání, můžeme uzavřít, že rozebírané nešpory jsou spíše
      "adventními nešporami" než "2. nešporami 1. neděle adventní".
      Pro ty, kdo nemají příležitost slavit v adventu společně liturgii
      hodin jindy než právě o těchto jedněch nešporách, se někdo
      pokusil o výběr, do nějž podle uvážení zařadil texty z různých míst
      breviáře i odjinud.
    </p>
    <p>
      Moc by mě zajímalo, čím se neznámý kompilátor nechal vést při výběru
      žalmů. Žalm 113 je snad zařazen pro své celkem přímočaré mariánské
      konotace; Žalm 19 je s dobou adventní spojen v uších a srdcích
      snad všech Čechů díky výše zmiňovanému zpěvu Ejhle, Hospodin přijde,
      kde se z něj zpívají verše;
      Žalm 117 by v našem kontextu, orámovaném antifonou o Gabrielovi,
      mohl mít místo jako odkaz na univerzální význam spásy přinášené
      zvěstovaným Spasitelem; zrovna tak ale k jeho zařazení mohl vést
      prozaičtější důvod: nešpory mají mít tři žalmy a předchozí dva
      jsou již dosti dlouhé ...
    </p>
    <p>
      Své hodnocení jsem shrnul do nadpisu článku heslem
      "nešpory mezi koncertem a liturgií".
      Nešpory zachycené na videu podle všeho jsou společným slavením
      liturgie hodin ve farnosti - nakolik je zachováno základní liturgické
      schéma. Zároveň ale jde do jisté míry i o "koncert" -
      tomu nasvědčuje
      charakteristické uspořádáním prostoru a výběr "zajímavých kousků"
      bez ohledu na liturgické předpisy a širší souvislosti.
    </p>

    <h5>Pramen, z něhož se málo pije</h5>

    <p>
      Závěrem chci zvláště zdůraznit jeden výše jen letmo zmíněný
      rys rozebíraných nešpor: pro část zpěvů se sahá do pokladnice
      české chorální tradice. To je v mých očích chvályhodné.
      Bohoslužebný zpěv v národním jazyce,
      zpěv oficia nevyjímaje, má u nás dlouhou historii, nejen
      díky utrakvistickému hnutí. Avšak zatímco staré české graduály
      jsou předmětem soustavného vědeckého bádání, jakkoli omezeného,
      a zpěvy z rorátníků péčí chrámových hudebníků znovu
      zaznívají do adventních rán, staré české zpěvy hodinek
      leží v rukopisech, jak se zdá, prakticky bez povšimnutí.
      Ví se o nich, ale právě jen to.
      Právě dnes, když vznikají nová zhudebnění textů liturgie hodin
      v národním jazyce, je čas také otevřít staré rukopisy
      a studovat, jak zpívali hodinky česky naši předkové.
      A nejen studovat, ale také zpívat - samozřejmě při plném vědomí
      rozdílu mezi liturgií na jedné straně a 
      koncertem nebo liturgicko-archeologickým
      experimentem na straně druhé.
    </p>
  </text>
</clanek>
