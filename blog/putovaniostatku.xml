<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Oficium při putování ostatků</titulek>

  <tags>relikvie</tags>
  <kategorie>Rubriky</kategorie>
  <datum>6.10.2024 19:50</datum>
  <souvisejiciNoty>optomasaq</souvisejiciNoty>

  <souhrn></souhrn>

  <text>
    <p>
      Ve dnech 19. 10. - 1. 11. navštíví vybraná místa v naší zemi
      <a href="https://www.op.cz/putovani-vzacne-relikvie-sv-tomase-akvinskeho-po-ceske-republice/">ostatky sv. Tomáše Akvinského</a>.
      Pokud je mi známo, slavení oficia v přítomnosti
      putujících ostatků svatých není nikde soustavně upraveno.
      V roce 2016 však Kongregace pro bohoslužbu vydala
      <em>notificatio</em> k putování význačných ostatků
      blahoslavených (<a href="https://www.cultodivino.va/it/rivista-notitiae.html">Notitiae</a> 2016, s. 36-38).
      Část se týká právě jen blahoslavených,
      ale pokyny ohledně oficia jsou zcela obecné povahy,
      a platí-li i pro blahoslavené, je tím spíše třeba vztáhnout je
      na svaté:
    </p>
    <table>
      <tbody>
        <tr>
          <td>
            <p>
              Pro Missae et Divini Officii celebratione, textus (in variis linguis)
              adhibendi sunt iam approbati, aut, cum desunt, debent desumi ex respectivo Communi sive Missalis Romani et Ordinis Lectionum Missae
              sive Liturgiae Horarum; si vero textus iam approbati desunt, formularia quae in Communi exstant adhibenda sunt.
            </p>
            <p class="petit">
              <em>... i
              testi (in varie lingue) già debitamente approvati, attingendo per i mancanti al Comune corrispondente ...;
              in mancanza di testi già approvati, si attinge
              ai formulari del Comune.</em>
            </p>
          </td>
          <td>
            Pro slavení mše a božského oficia se použijí již schválené
            texty (v různých jazycích),
            chybějící části se doplní z příslušných
            společných textů misálu a lekcionáře, resp. denní modlitby církve.
            Pokud schválené texty nejsou k disposici,
            použijí se formuláře ze společných textů.
          </td>
        </tr>
        <tr>
          <td colspan="2" class="petit">
            (Latinský text sám o sobě je nejasný.
            Obě podmínkové věty
            i jejich konsekventy jsou skoro totožné
            a není zřejmé, jestli neříkají dvakrát totéž,
            a pokud ne, pak na jaké přesně případy se vztahují.
            Překlad se opírá o italskou verzi.)
          </td>
        </tr>
        <tr>
          <td>
            Missa et Officium Beati nequeunt celebrari si, illis in diebus, celebrationes occurrant quae nn. 1-5 in “Tabula dierum liturgicorum secundum ordinem praecedentiae disposita” indicantur; si vero occurrant
            in diebus nn. 6-9 de dicta “Tabula”, Missa et Officium Beati, Episcopo
            dioecesano consentiente (cf. Institutio generalis Missalis Romani, n. 374),
            permittuntur. Diebus tamen dominicis (temporis Nativitatis et per annum), una tantum Missa in honorem Beati celebrari potest, dum aliae
            Missae et Liturgia Horarum de dominica esse debent.
          </td>
          <td>
            Mši a oficium o blahoslaveném není možné slavit,
            pakliže na ony dny připadají oslavy, které jsou
            v Tabulce liturgických dnů pod čísly 1-5;
            jestliže však jde o oslavy pod čísly 6-9 řečené Tabulky,
            se souhlasem diecésního biskupa (srov. VPŘM 374)
            se mše a oficium o blahoslaveném připouští.
            O nedělích (v době vánoční a v liturgickém mezidobí)
            se však smí slavit jen jedna mše ke cti blahoslaveného,
            zatímco ostatní mše a denní modlitba církve
            mají být z neděle.
          </td>
        </tr>
      </tbody>
    </table>
    <p>
      Tedy:
      v přítomnosti putujících ostatků se počítá se slavením (mše a)
      oficia o světci, pokud to připouští liturgický kalendář.
      Kalendářní hranice přípustnosti je narýsována
      restriktivněji než ve VPDMC 245 pro votivní oficia.
    </p>
    <p class="petit">
      (<em>Odiosa sunt restringenda</em>, takže by se asi dalo argumentovat, že tato restriktivnější
      hranice platí právě jen pro ostatky blahoslavených a pro svaté
      se smí postupovat podle benevolentnějšího ustanovení VPDMC 245.
      Ale to ať případně dělá, koho by restriktivnější
      pravidla tísnila. Mě netísní a přijdou mi rozumná.)
    </p>
    <p>
      Ze třinácti dnů nadcházejícího putování
      vylučují oficium o sv. Tomáši tři:
      dvě neděle v liturgickém mezidobí a slavnost Všech svatých.
      O svátku sv. Šimona a Judy (28. 10.) by pak byl potřeba souhlas
      diecésního biskupa.
    </p>
    <table class="tac" style="margin: 0 auto;">
      <thead>
        <tr>
          <th>ne</th>
          <th>po</th>
          <th>út</th>
          <th>st</th>
          <th>čt</th>
          <th>pá</th>
          <th>so</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td colspan="6"></td>
          <td title="Nový Dvůr">19. 10.</td>
        </tr>
        <tr>
          <td title="Nový Dvůr, Plzeň" style="background-color: #fcc;">20</td>
          <td title="České Budějovice">21</td>
          <td title="Praha, Salvátor">22</td>
          <td title="Praha, Ignác">23</td>
          <td title="Praha, dominikáni">24</td>
          <td title="Praha, dominikáni">25</td>
          <td title="Praha, dominikáni">26</td>
        </tr>
        <tr>
          <td title="?" style="background-color: #fcc;">27</td>
          <td title="?" style="background-color: #fec;">28</td>
          <td title="Olomouc, dominikáni">29</td>
          <td title="?">30</td>
          <td title="?">31</td>
          <td title="Brno, katedrála" style="background-color: #fcc;">1. 11.</td>
          <td></td>
        </tr>
      </tbody>
    </table>
    <p>
      Nic se nestanoví o tom, v jakém stupni se má oficium
      o svatém slavit, ale pokyny o doplnění vlastních textů
      částmi z commune mluví pro oficium odpovídající stupni,
      kdy se většina nebo všechny texty berou o svatém,
      tedy svátku nebo slavnosti.
    </p>
    <p>
      Blízkou analogií jsou předpisy o zohlednění význačných
      ostatků svatých ve vlastním kalendáři kostela.
      Instrukce <em>Calendaria particularia</em>,
      čl. 11, pro takové případy počítá standardně jen s oslavou
      ve stupni památky.
      (Notitiae 1970, s. 348nn.)
    </p>
    <hr/>
    <p class="petit">
      Nový notový materiál
      s <a href="/noty#optomasaq">dominikánskými texty o sv. Tomáši Akvinském</a>
      jsem připravil spíš <em>u příležitosti</em>
      než <em>pro příležitost</em> - že by z něj letos někdo
      zpíval neočekávám.
    </p>
    <p class="petit">
      Přítomnost ostatků každopádně nic nemění na tom, že není legální,
      aby nedominikáni jen tak používali řádové liturgické texty,
      zvlášť když formulář památky sv. Tomáše Akvinského je standardní
      součástí Denní modlitby církve.
      Na druhou stranu se však dá očekávat, že v okolí ostatků
      svatého dominikána nebude nouze o jeho živé spolubratry -
      a tedy ani o příležitost zúčastnit se oficia slaveného
      podominikánsku.
    </p>
  </text>
</clanek>
