<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Brněnský výběr textů</titulek>

  <tags>Brno, kapituly, brněnská katedrální kapitula, hymny</tags>
  <kategorie>Knihy</kategorie>
  <datum>4.12.2021 21:50</datum>
  <!-- <souvisejiciNoty></souvisejiciNoty> -->

  <souhrn></souhrn>

  <text>
    <p>
      [<a href="https://www.akademiekp.cz/publikace-denni-modlitba-cirkve-vyber-textu-pro-vyznamne-dny-liturgickeho-roku/">Denní modlitba církve.
      Výběr textů pro významné dny liturgického roku,
      Brno:
      Akademie kanonického práva
      2019, 178 s.</a>]
    </p>
    <p>
      Brožura formátu A5 obsahuje texty Denní modlitby církve
      pro středně velký okruh příležitostí.
      (Viz na výše odkazované webové stránce zveřejněný obsah.)
      Z webu vydavatele víme, že
      <em>"publikace vznikla z podnětu Královské stoliční kapituly v Brně,
      která slaví některé dny liturgického roku modlitbou církve
      za přítomnosti Božího lidu."</em>
      Přímo v knížce žádné metainformace upřesňující její
      Sitz im Leben nenajdeme, ale výběr příležitostí se zdá
      potřebám brněnské katedrální kapituly dobře odpovídat
      (jediným pokrytým svátkem z cyklu sanktorálu je slavnost
      sv. Petra a Pavla, titulární svátek katedrály).
      Knížka je také vidět přímo v liturgickém provozu
      na <a href="https://youtu.be/e2aatjflC9E">záznamu modlitby se čtením a ranních chval v brněnské katedrále z letošního Velkého Pátku</a>.
    </p>
    <p>
      Obálka je vkusná a důstojná (byť na první pohled bych
      v takových deskách úplně nečekal liturgické texty),
      <strong>grafická úprava</strong> obsahu je však ryze účelová,
      bez větších estetických ambicí.
      Dvoubarevný tisk a formátování jednotlivých liturgických elementů
      víceméně odpovídá breviáři, až na bezpatkové písmo
      nadpisů a rubrik. Rušivě a nedůstojně působí
      šedě stínované záhlaví všech stránek.
    </p>
    <p>
      <strong>Hudební informace</strong> knížka obsahuje jen úplné
      minimum: u těch hymnů, jejichž (vlastní) nápěv lze najít
      v Kancionálu nebo v Mešních zpěvech, je na tento odkázáno.
      Nápěvy ostatních hymnů nejsou řešeny, i když pro většinu
      by se nějaký použitelný i jen ve jmenovaných dvou zpěvnících
      bez problémů našel.
    </p>
    <p>
      <strong>Výběr příležitostí</strong> pokrývá vrcholy temporálu,
      svátky týkající se daného kostela (výročí posvěcení, titulární slavnost),
      nešpory pro neděle v mezidobí a oficium za zemřelé.
      Charakteristické je, že kromě nedělních nešpor a nešpor
      z oficia za zemřelé jde vždy o modlitbu se čtením a ranní chvály,
      což vzbuzuje vážnou pochybnost o realističnosti tvrzení
      vydavatele, že
      <em>"v kostelích, kde se slaví liturgie hodin, může být tato publikace vhodnou pomůckou."</em>
      Výběr hodinek, které má ve zvyku slavit veřejně a za účasti lidu
      brněnská katedrální kapitula, je tak netypický, že je obtížné
      představit si, že by opravdu byl k potřebě ještě někde jinde.
    </p>
    <p>
      Asi to nejzajímavější na novodobém liturgickém prameni pro konkrétní
      instituci je, <strong>jaká rozhodnutí jeho redaktor či redaktoři
      udělali</strong> v rámci prostoru ponechaného v oficiálních liturgických
      knihách, jak je obecné konkretisováno.
    </p>
    <p>
      Jednou vrstvou přizpůsobení jsou kroky k blbuvzdornosti:
      úvodní dialog je otištěn celý na začátku každého formuláře;
      na konci každého žalmu je plný text Sláva Otci;
      responsoria jsou kompletně rozepsána tak, jak se opravdu
      recitují (nespoléhá se na to, že uživatel ví, jak které části
      správně opakovat, nebo že mu opakování stačí napovědět
      pomocí incipitů).
    </p>
    <p>
      Jiná přizpůsobení kodifikují konkrétní provozovací praxi.
      Sem můžeme zařadit to, že část prvního verše každého žalmu
      je tištěna kurzívou, nepochybně jako připomínka,
      že ji má recitovat sólově ten, kdo začínal antifonu.
      Úplné znění antifony je tištěno před žalmem i po něm,
      ale opakování po žalmu často má doplněnu jednu nebo více
      pomlček, patrně jako pomůcky, jak se má při sborové recitaci
      jednotně frázovat.
      Antifony, recitované ve Velikonočním triduu po krátkých
      čteních místo responsorií, mají oproti breviáři upřesněn konkrétní
      způsob provedení: nejprve má celý text recitovat předsedající,
      pak ho všichni zopakují.
      Kantikum podle Zj 19 v nedělních nešporách je tištěno rovnou
      bez těch částí, které rubriky breviáře při slavení beze zpěvu
      dovolují vynechat.
      Dialogické elementy místo obecných zkratek obvyklých v breviáři -
      V[erš] a O[dpověď] - často užívají zkratek kodifikujících
      rozdělení rolí: K[něz] a L[id].
      (Veršíky v modlitbě se čtením však kdovíproč ani nesledují
      Denní modlitbu
      církve, ani tuto konkretisaci rozdělení recitace mezi kněze
      a lid, ale mají zkratky
      V[ersus] a R[esponsum] jako latinský breviář.)
    </p>
    <p>
      Dále jsou tu provedené volby v rámci možností nabízených
      rubrikami.
      Ranní chvály jsou vždy spojeny s modlitbou se čtením,
      a to tak, že se říká jediný hymnus, vždy zásadně vzatý
      z ranních chval (srov. VPDMC 99).
      Pro každou příležitost je otištěn jeden hymnus
      (žádné alternativy na výběr), přičemž
      část hymnů je vybrána z červeného, část ze zeleného hymnáře.
      Pro neděle v liturgickém mezidobí jsou využity hymny
      druhých nedělních nešpor z obou hymnářů, takže každá ze čtyř
      nedělí v cyklu žaltáře má hymnus jiný.
    </p>
    <p class="petit">
      Pokud jde o výběr hymnů kombinující materiál obou oficiálních
      sbírek, dalo by se mluvit o kodifikovaném hymnáři
      brněnské katedrály, podobně, jako je v Olejníkových
      Nedělních nešporách
      <a href="/blog.php?clanek=olejnikhymny.xml">kodifikován hymnář katedrály olomoucké</a>.
    </p>
    <p>
      Jestliže jsme na jedné straně viděli řadu příležitostí,
      kde redakce z různých možností připouštěných breviářem závazně
      vybírá jednu, jsou i případy, kde jsou reprodukovány
      všechny možnosti breviářem nabízené. To se týká nejen
      výběru žalmů k invitatoriu, ale také dvou možností
      psalmodie ranních chval na Popeleční středu.
      (Záhadné je, z jakých důvodů knížka nabízí dvě varianty
      psalmodie i pro modlitbu se čtením ve čtvrtek Svatého týdne.)
      Na Štědrý den, kdy se pro modlitbu se čtením a ranní chvály
      berou feriální žalmy, redakce nehledá zkratky
      (byť ve VPDMC 247 nejspíš využitelná skulinka je,
      neboť feriální žalmy naprosto nejsou Štědrému dni
      vlastní ani přizpůsobené)
      a na čtyřiceti stranách otiskuje žalmy pro všechny možnosti -
      tedy až na tu jednu, kdy Štědrý den připadne na čtvrtou neděli adventní.
    </p>
    <p>
      Možná stojí za explicitní zmínku, že obsahem nejde
      o pouhou "knížku do lavice", která by vynechávala části
      příslušející lektorovi nebo předsedajícímu, ale pro knížku
      pro všechny liturgické role, obsahující všechny potřebné
      texty (až na evangelium vigilie Narození Páně - s. 52),
      včetně předsednických výzev před Modlitbou Páně a
      slavnostních požehnání.
    </p>
    <p>
      Tiráž tvrdí, že <strong>"liturgické texty se shodují s oficiálním
      zněním Denní modlitby církve."</strong>
      V celku je to pravda, ale na dva drobné hříchy v tomto směru
      jsem při zběžném pročítání narazil:
    </p>
    <blockquote>
      <strong class="petit">24. 12. uvedení do první modlitby dne:</strong>
      <p>
        Dnes poznáte, že přijde Pán, a zítra uvidíte jeho slávu.
        <u>Skutečně ji uvidíte.</u>
      </p>

      <strong class="petit">Výročí posvěcení kostela, ranní chvály, zpěv po krátkém čtení:</strong>
      <p>
        Bože, tvá církev je předobraz<u>em</u> nebeského Jeruzaléma *
        je to dům modlitby pro všechny národy.
        ...
      </p>
    </blockquote>
    <p>
      Zatímco drobná jazyková úprava responsoria je pochopitelná
      a snadno může jít i o neúmyslnou chybu přepisu, neústrojné
      prodloužení štědrodenního invitatoria je pro mě záhadné.
      Kdo a proč by něco takového chtěl?
    </p>
    <p>
      Jediným projevem ryzí <strong>liturgické tvořivosti</strong> vykračující za mantinely rubrik
      jsou antifony k Magnificat nedělních nešpor.
      Antifony pro 32 nedělí krát tři roční cykly v knížce
      celkem pochopitelně otištěny nejsou.
      Rubrika sice správně uvádí, že
      <em>"Antifona je vlastní pro každou neděli"</em>,
      ale vzápětí (snad hlavně pro ty, co si cestou na nešpory
      breviář zapomenou na faře) nezákonně dovoluje, že se
      <em>"může ... použít obecná antifona,"</em>
      vzatá z kantika samotného:
    </p>
    <blockquote>
      <p class="petit tac"><strong>neděle 1. týdne žaltáře</strong></p>
      <p>
        Můj duch jásá v Bohu, mém Spasiteli.
      </p>
      <p class="petit">
        (Doslova shodná s antifonou k Magnificat úterý lichých týdnů žaltáře.)
      </p>
      <p class="petit tac"><strong>neděle 2. a 4. týdne</strong></p>
      <p>
        Veliké věci mi učinil ten, který je mocný, jeho jméno je svaté.
      </p>
      <p class="petit">
        (Srov. antifonu k Magnificat, středa lichých týdnů žaltáře -
        stejný text, lehce odlišný slovosled.)
      </p>
      <p class="petit tac"><strong>neděle 3. týdne</strong></p>
      <p>
        Hospodin pamatoval na své milosrdenství.
      </p>
      <p class="petit">
        (Srov. antifonu k Magnificat, pátek lichých týdnů žaltáře.)
      </p>
    </blockquote>
    <p>
      Nechat si tisknout výběr liturgických textů pro praktickou
      potřebu v knižní podobě není téměř nikdy jenom akt prostého
      namnožení textů (které se ostatně v době, kdy v každé druhé
      katolické kapse je mobilní telefon s breviářovou aplikací,
      pomalu stává přežitkem),
      ale také projev institucionalisace. Zřetelný krok od nahodilého
      a příležitostného k řádnému a pravidelnému.
      Kodifikován je výběr příležitostí, ale také řada
      praktických detailů a dramaturgických rozhodnutí, která
      oficiální liturgické knihy přenechávají tomu, kdo slavení
      utváří. Tak se i na (oproti středověké liturgii) omezené
      hrací ploše novodobé římské liturgie profiluje
      liturgický úzus dané církevní instituce.
      Když jsem se až dosud nevypravil do Brna,
      abych Královskou stoliční kapitulu sv. Petra a Pavla
      zastihl při činu, jsem rád, že alespoň mám v knihovně
      její "chórovou knížku".
    </p>
  </text>
</clanek>
