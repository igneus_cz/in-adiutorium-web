<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Povýšení svatého kříže, Milevsko 13. 9. 2023</titulek>

  <tags>premonstráti, Milevsko, Povýšení svatého kříže, relikvie, gregoriánský chorál</tags>
  <kategorie>Ze života</kategorie>
  <datum>17.9.2023 22:19</datum>
  <!-- <souvisejiciNoty></souvisejiciNoty> -->

  <souhrn></souhrn>

  <text>
    <h3>"Svatý hřeb"</h3>
    <p>
      V létě 2020 geofysikální průzkum kostela sv. Jiljí
      v areálu milevského premonstrátského kláštera odhalil dosud
      neznámé prostory v hmotě severní zdi chrámové lodi,
      interpretované jako trezorová místnost a přístupová chodba
      k ní. Z nově objevené místnosti vybíhá menší dutina,
      snad po vyhnilém trámu, a hluboko v ní
      sonda s kamerou našla trosky dřevěné schránky s ozdobami
      z drahých kovů. Při rozebírání trosek po jejich vyzvednutí pak
      byl mezi nimi nalezen železný předmět hřebovitého tvaru
      ozdobený zlatým křížkem. Objevitelé nešli daleko pro senzační
      interpretaci: jde o relikvii, část hřebu
      považovaného za jeden z těch, které se staly nástroji
      Kristova umučení. Hluboko do dutiny
      za tajnou místností byla vzácná relikvie ukryta snad v obavách
      z drancování v období husitských válek.
    </p>
    <p>
      Provedené expertízy přinesly data, která připravenou legendu
      neproblematisovala, a církevní i světští představitelé se
      pustili do práce, aby objev pokud možno obrátili ve zdroj příjmů
      pro klášter i region. Zrodila se značka "Svatý hřeb z Milevska"
      (<a href="https://www.strahovskyklaster.cz/soutez-na-celkovy-koncept-vizualniho-stylu-pro-prezentaci-relikvie-svateho-hrebu-z-milevska">1</a>,
      <a href="https://www.strahovskyklaster.cz/slavnostni-nespory-k-ucteni-svateho-hrebu">2</a>).
    </p>
    <h3>Nešpory</h3>
    <p>
      13. 9. 2022 byl hřeb v milevském klášteře
      <a href="https://www.strahovskyklaster.cz/slavnostni-nespory-k-ucteni-svateho-hrebu">poprvé představen veřejnosti</a> -
      od 16:00 se konala tisková konference o objevu a výsledcích
      provedených znaleckých zkoumání,
      v 17:00 navázaly první nešpory Povýšení svatého kříže, slaveného
      tu nově, podle všeho právě s ohledem na relikvii, jako slavnost.
      Skromnou ukázku z těch nešpor pro budoucnost uchoval
      <a href="https://youtu.be/ne1nBhrM84Q?si=T2AvooBbbCyvK3yP">sestřih Jaroslava Mareše</a> (0:36 vstupní průvod, 5:18 okuřování oltáře, 11:46 snad přímluvy / závěr nešpor).
    </p>
    <p class="petit">
      Že se relikvie spojená s umučením Páně slaví o tematicky
      příbuzném svátku již figurujícím v liturgickém kalendáři,
      to dobře odpovídá dnes platným předpisům
      (srov. <em>Notificazione su alcuni aspetti dei calendari e dei testi liturgici propri</em>,
      <a href="http://www.cultodivino.va/content/dam/cultodivino/notitiae/1997/372-374.pdf#page=14">Notitiae 1997, s. 284-297</a>, čl. 6).<br/>
      Do r. 1914 se v českých zemích slavil také samostatný svátek
      Kopí a hřebů Páně.
      (K okolnostem jeho zrušení Stejskal F.: <a href="http://depositum.cz/knihovny/ckd/tiskclanek.php?id=c_5452"><em>Kalendář církevní v Čechách nově upraven</em></a>, ČKD 1914/4, s. 360)
    </p>
    <p>
      První nešpory Povýšení svatého kříže se v Milevsku
      slavily i letos, za pěvecké a ceremoniální podpory
      ze Strahova a za předsednictví českobudějovického
      biskupa, v jehož diecési se klášter nachází.
      Premonstráti tentokrát sami zveřejnili několik videoukázek:
    </p>
    <ul>
      <li><a href="https://fb.watch/n6Jvryo6oQ/">responsorium <em>Nos autem gloriari oportet</em></a> (<a href="https://cantusindex.org/id/007238">CANTUS 007238</a>)</li>
      <li><a href="https://cantusindex.org/id/007238">Magnificat</a> (česky, nápěv II.D se slavnostní mediací) a antifona <em>Crux Iesu Christi</em> (<a href="https://cantusindex.org/id/001963">CANTUS 001963</a>)</li>
      <li><a href="https://fb.watch/n6KmTOBJr6/">otčenáš, orace, požehnání, Benedicamus Domino, Salve Regina</a></li>
    </ul>
    <p>
      Za pozornost stojí, jak se při zpěvu českého textu Magnificat
      na nápěv se slavnostní mediací ve více verších nedostává
      slabik. (Já v česky zpívaném oficiu slavnostní mediace nikdy
      nepoužívám a do českého antifonáře s nimi nepočítám.)
    </p>
    <p>
      Antifona k Magnificat nesouhlasí s žádnou z těch,
      které má pro svátek Povýšení svatého kříže solesmeské
      Antiphonale Romanum II, a nakolik se dá soudit podle
      CantusIndexu, je specifická pro premonstrátskou tradici.
    </p>
    <p>
      Že se na konci nešpor po požehnání místo
      Ite in pace (Jděte ve jménu Páně) zpívá Benedicamus Domino
      je věc rubrikám pokoncilního breviáře cizí,
      ale v již zmíněném solesmeském antifonáři dovolená,
      hlavně jako možnost použít tradiční nápěvy.
      (Jak efektní některá sváteční zpracování jsou je na záznamu
      z Milevska dobře předvedeno.)
      Pro premonstráty ovšem Benedicamus v nešporách pokoncilního
      oficia žádnou novinkou
      nedávného data není, ti si ho kodifikovali už v roce 1988
      v <em>Thesaurus liturgiae Praemonstratensis</em>
      (viz na konci nečíslovaných stránek se zpěvy nedělních nešpor).
    </p>
    <h3>Závěrem</h3>
    <p>
      Nikdo mě nebude podezírat, že bych neměl rád relikvie,
      zvlášť stanou-li se záminkou k úpravě místního liturgického
      kalendáře a dokonce ke slavnostním nešporám.
      Přeci by mi ale v tomto případě bylo milejší ze strany
      církevních představitelů víc zdrženlivosti a historické přísnosti
      - i kdyby to znamenalo méně nešpor.
    </p>
    <p>
      Samozřejmě dnes u mnoha relikvií nemůžeme
      dojít až k evidenci jejich pravosti, ale můžeme se opírat
      alespoň o delší historii zdokumentované úcty k nim.
      Domnělá relikvie, která je spektakulárně objevena v 21. století
      a není ji možné přesvědčivě spojit s jednoznačnými historickými
      doklady, že, kde, kdy a kým jí skutečně byla prokazována úcta
      jakožto hřebu z Kristova kříže, může být opatrně
      zpřístupněna soukromé úctě, kdyby jí snad někdo chtěl
      úctu prokazovat
      (a příp. pak lze škálovat míru oficiálního církevního schválení
      a liturgického slavení, opírajícího se primárně o onu lidovou
      úctu a případné zázraky, ne o jistotu historické pravosti
      relikvie).
      Hned kolem ní, jen na základě vývodů z toho mála, co o ní
      s jistotou víme, slavit pontifikální nešpory a udělovat
      s ní požehnání, je projev lehkověrnosti, která
      církvi nesluší a na vážnosti nepřidává.
    </p>
  </text>
</clanek>
