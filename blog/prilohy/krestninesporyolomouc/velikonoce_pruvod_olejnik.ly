\version "2.24.1"

\header {
  title = "Křestní nešpory"
  subtitle = "Alelujatické antifony k průvodu podle antifon J. Olejníka"
}

\score {
  \relative c' {
    \cadenzaOn
    \override Score.TimeSignature.stencil = ##f

    e4 c f8[( d]) d4 \bar "|" d8 d e d e f g4 g4 r \bar "||"
  }
  \addlyrics {
    A -- le -- lu -- ja a -- le -- lu -- ja a -- le -- lu -- ja.
  }
  \header {
    piece = \markup{Aleluja \italic{Pán ho posadil} (OL264) - Žalm 113}
  }
}

\score {
  \relative c'' {
    \cadenzaOn
    \override Score.TimeSignature.stencil = ##f

    \key g \major
    g4 g8[( d]) g a b d c[( b]) a4 b8 c a( g) g2 \bar "||"
  }
  \addlyrics {
    A -- le -- lu -- ja a -- le -- lu -- ja a -- le -- lu -- ja.
  }
  \header {
    piece = \markup{Aleluja \italic{Jásejte Bohu} (OL163) - Žalm 66}
  }
}