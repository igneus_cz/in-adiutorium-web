\version "2.19.32"

\layout {
  indent = 0\cm
}
\paper {
  #(set-paper-size "a5")

 myStaffSize = #20
  #(define fonts
    (make-pango-font-tree
                          "Domine"
                          "VL Gothic"
                          "Courier"
     (/ myStaffSize 20)))
}

choralniRezim = {
  % nepsat predznamenani tempa (neni tempo)
  \override Score.TimeSignature #'stencil = ##f

  % noty bez nozicek
  \override Stem #'transparent = ##t

  % nozky maji nulovou delku a tak neovlivnuji legatove cary
  % (tento radek resi problem "vznasejicich se car")
  \override Stem #'Y-extent = ##f

  % nedelat taktove cary
  \cadenzaOn

  % vzdycky vypsat becka
  % #(set-accidental-style 'forget) % for Lily 2.14
  \accidentalStyle "forget" % 2.15
}

Dagger = \markup { \char ##x02020 }

barMin = {
  \once \override Staff.BarLine #'bar-extent = #'(1.5 . 2.5)
  \bar "|"
}
barMaior = {
  \once \override Staff.BarLine #'bar-extent = #'(-1.5 . 1.5)
  \bar "|"
}
barMax = { \bar "|" }
barFinalis = { \bar "||" }

% ------------------------------------------------------

\score {
  \relative c' {
    \choralniRezim
    f4 f f f f f f f f g f \barMaior
    g g g g g g a bes a g2 f \barFinalis
  }
  \addlyrics {
    Dám vám pas -- tý -- ře po -- dle své -- ho srd -- ce
    a bu -- dou vás pást ro -- zum -- ně a moud -- ře.
  }
  \header {
    piece = "1. ant."
  }
}

\score {
  \relative c'' {
    \choralniRezim
    a4 a a \bar "" a a a a g \barMin
    a a \bar "" a a \bar "" a a a \bar "" a g a f \barMaior
    a a \bar "" a a \bar "" a a a a f g a g f \barFinalis
  }
  \addlyrics {
    Du -- še má, chval Hos -- po -- di -- \markup{na! \Dagger}
    Po -- kud ži -- ji, chci chvá -- lit Hos -- \markup\underline{po} -- di -- na,_*
    po -- kud bu -- du, chci o -- pě -- vo -- \markup\underline{vat} své -- ho Bo -- ha.
  }
  \header {
    piece = "Žalm 146, 1"
  }
}