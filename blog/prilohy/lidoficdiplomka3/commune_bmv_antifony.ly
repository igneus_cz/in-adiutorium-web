% must be compiled using lyp, compilation by lilypond alone will fail

\version "2.21.0"

% require lyp packages
\require "inadiutorium-chant"
\require "inadiutorium-blog"

\header {
  title = "Společné texty o Panně Marii"
  subtitle = "Antifony k nešporám"
}

\paper {
  scoreTitleMarkup = \markup\small{
    \fill-line {
      " "
      " "
      \concat{
        \fromproperty #'header:psalmus ",  "
        \fromproperty #'header:modus " " \fromproperty #'header:differentia
      }
    }
  }
}

\layout {
  \override Score.TimeSignature #'stencil = ##f
  \override Staff.NoteHead.style = #'baroque

  \context {
    \Staff
    \remove Custos_engraver
  }
}

notime = {
  \time 2/2
  \cadenzaOn
}

orelStyle = {
  \notime
  \accidentalStyle "forget"
  \autoBeamOff
}

eichlerStyle = {
  \notime
  \accidentalStyle "forget"
  \stemUp
}

div_ib = {
  \once \override Staff.BarLine #'bar-extent = #'(1.7 . 2.3)
  \bar "|"
}
div_i_ii = {
  \once \override Staff.BarLine #'bar-extent = #'(1 . 2)
  \bar "|"
}
div_i_iib = {
  \once \override Staff.BarLine #'bar-extent = #'(0.5 . 2)
  \bar "|"
}
div_ib_iii = {
  \once \override Staff.BarLine #'bar-extent = #'(0 . 1.5)
  \bar "|"
}
div_ib_iiib = {
  \once \override Staff.BarLine #'bar-extent = #'(-0.5 . 1.5)
  \bar "|"
}
div_i_iii = {
  \once \override Staff.BarLine #'bar-extent = #'(0 . 2)
  \bar "|"
}
div_ii_iii = {
  \once \override Staff.BarLine #'bar-extent = #'(0 . 1)
  \bar "|"
}
div_ii_iiib = {
  \once \override Staff.BarLine #'bar-extent = #'(-0.5 . 1)
  \bar "|"
}
div_iib_iii = {
  \once \override Staff.BarLine #'bar-extent = #'(0 . 1.5)
  \bar "|"
}
div_ii_iv = {
  \once \override Staff.BarLine #'bar-extent = #'(-1 . 1)
  \bar "|"
}
div_ii_ivb = {
  \once \override Staff.BarLine #'bar-extent = #'(-1.5 . 1)
  \bar "|"
}
div_iib_iv = {
  \once \override Staff.BarLine #'bar-extent = #'(-1 . 0.5)
  \bar "|"
}
div_iib_ivb = {
  \once \override Staff.BarLine #'bar-extent = #'(-1.5 . 0.5)
  \bar "|"
}
div_iii_iv = {
  \once \override Staff.BarLine #'bar-extent = #'(-1 . 0)
  \bar "|"
}
div_iii_ivb = {
  \once \override Staff.BarLine #'bar-extent = #'(-1.5 . 0)
  \bar "|"
}
div_iii_v = {
  \once \override Staff.BarLine #'bar-extent = #'(-2 . 0)
  \bar "|"
}

\bookpart {
  \header {
    subtitle = "Svatojanský kancionál"
    composer = "Ferdinand Lehner (1868)"
    poet = "Vincenc Bradáč (1863)"
  }

\score {
  \relative c'' {
    \notime
    \key g \major
    g2 \div_ii_iiib g( c) a g( a c) \div_ii_iii c1 \bar "||" % opraveno, v prameni je koncova cara
    b2 \div_iib_iii c \div_i_iii d c( a) c \div_ib_iii
    c( b) a \div_ii_iiib g( fis g a) g g \bar "|."
    g2 a b\breve
  }
  \addlyrics {
    Když sto -- lo -- val král,
    nard můj vy -- dá -- val
    vů -- ni pří -- jem -- nou.
  }
  \header {
    modus = "[III/I]"
    differentia = ""
    psalmus = "Žalm 109"
  }
}

\score {
  \relative c' {
    \notime
    \key f \major
    c2 d f( g) \div_ii_iv f( g) g \bar "||"
    f \div_ii_iiib g a( g f) \div_iib_iv f \bar "|"
    g4 \div_ib_iii d2( g) e f \div_iib_iv d( c) d( c) \div_iii_iv
    bes4 c2(d f e) f \div_iii_iv f1 \bar "|."
    f2 a c\breve
  }
  \addlyrics {
    Le -- vi -- ce je -- ho
    pod hla -- vou mou
    a pra -- vi -- ce je -- ho
    o -- bej -- me mne.
  }
  \header {
    modus = "[IV/V]"
    differentia = ""
    psalmus = "Žalm 112"
  }
}

\score {
  \relative c'' {
    \notime
    \key g \major
    g2 g \div_ii_iii c1 \div_i_ii c2 a \div_i_ii c1 b \bar "||"
    a2( g) a \div_ii_iiib a a b a g \bar "|"
    b b \div_ii_iiib a c b \div_ii_iiib g \div_ii_iiib a1 \div_i_iii
    a4 \div_i_iii g2 fis \div_ib_iii g( a g) \div_iib_iv e \div_iii_ivb fis4 \div_ii_iiib g2( a) g g1 \bar "||"
    g2 a b\breve
  }
  \addlyrics {
    Čer -- ná jsem a -- le krás -- ná
    dce -- ry Je -- ru -- sa -- lem -- ské
    pro -- tož mi -- lo -- val mne král
    a u -- ve -- dl mne na lož -- ce své.
  }
  \header {
    modus = "[III/I]"
    differentia = ""
    psalmus = "Žalm 121"
  }
}



\score {
  \relative c' {
    \notime
    \stemUp
    \key es \major
    f2 f( bes) \div_ii_iii g( f es) \div_iib_iv f4 g2 f4 f2 \bar "||"
    f f \div_iib_iv g f4 f2 \div_iib_ivb f4 \div_iib_iv g2 es4 es2 \bar "|"
    es( g) \div_ii_iv g( bes g) bes( a g) f4 es2( d) \div_iii_ivb es( f g bes) \div_ii_iiib g4 \div_ii_iiib g2 \bar "|."
    c2 bes c\breve
  }
  \addlyrics {
    Zi -- ma již po -- mi -- nu -- la,
    prš -- ka pře -- sta -- la a o -- de -- šla,
    vstaň pří -- tel -- ky -- ně má a pojď.
  }
  \header {
    modus = "[VIII/IV]"
    differentia = ""
    psalmus = "Žalm 126"
  }
}



\score {
  \relative c' {
    \notime
    \key f \major
    c2( d f) f \div_iii_ivb f4 f g2 a \div_ii_iiib g1 \bar "||"
    f4 \div_iib_iv g2( a g) f( g) g1 \div_i_iii g2 d( g) f( e f) \div_iib_ivb d( c d4 c) \bar "|"
    bes2 c( f) \div_ii_iv d1 f \div_i_iib f2 f4 f1 \bar "||"
    f2 a c\breve
  }
  \addlyrics {
    Slič -- ná u -- či -- ně -- na jsi
    a pří -- jem -- ná v_ra -- dos -- tech svých,
    sva -- tá Bo -- ží ro -- dič -- ko.
  }
  \header {
    modus = "[IV/V]"
    differentia = ""
    psalmus = "Žalm 147"
  }
}

\markup\justify\small{
  Antifona k Magnificat se textem částečně přizpůsobovala
  jednotlivým svátkům (pokud neměly antifonu vlastní).
  Ve Svatojanském kancionálu
  (a tedy i v Lehnerově zhudebnění)
  není její obecná podoba, ale jen varianta uzpůsobená
  pro svátek Jména Panny Marie.
}

\score {
  \relative c' {
    \notime
    \stemUp
    e2( f g) d( c) \div_iii_ivb d d( e) e \bar "||"
    g( a) a( g e f e4 d) \div_iib_ivb e2( f g f e4) e2 \div_ii_iiib d4 \div_ii_iv f2 a g \bar "|"
    g f1 e2 \div_iii_ivb d e4 f2 e4 e2 \bar "|"
    f( d) \div_ii_iv c d \div_iib_iv e f( g f d4) d2 \bar "|"
    d( f) a( g) \div_ii_iv g( f4 e) \div_iii_ivb d2( f e f e4 d) \bar "|"
    g2 g a( bes) \div_ii_iii a \div_ii_iiib g( f e) \div_iib_iv d( c d e) f( e) \bar "|"
    d( e) g( a) c( a) \div_ii_iv g( f e) d( c) d( f e f) e4( d) d2 \div_i_iib
    d2( f a) a( g f) g \div_ii_iv g( a g) e1 \bar "|"
    c2( d f4) f2 \div_ii_iiib e( f) f \div_iib_iv f4 f e2 \div_iib_iv d( c) \div_iib_ivb d( e) d( c) c \bar "|"
    c c( g' f4) g2 \div_ib_iii a g \div_iib_ivb g g g \div_ii_ivb f( e) d \div_iii_v c d \div_iib_ivb e( f) g( a g) g \bar "|."
    g2 a c\breve
  }
  \addlyrics {
    Sva -- tá Ma -- ri -- a,
    při -- spěj bíd -- ným ku po -- mo -- ci,
    po -- ma -- hej má -- lo -- my -- sl -- ným,
    dej bíd -- ným o -- křá -- ti,
    pro -- siž za lid,
    při -- mlou -- vej se za kněz -- stvo
    o -- ro -- důj za za -- slí -- be -- né
    po -- hla -- ví žen -- ské:
    nech -- ať vši -- ckni za -- ku -- sí tvé po -- mo -- ci
    kdož -- ko -- li sla -- ví
    sva -- té -- ho jmé -- na tvé -- ho
    pa -- mát -- ku.
  }
  \header {
    modus = "[IV/VIII]"
    differentia = ""
    psalmus = "Magn."
  }
}

\markup\small{
  Ferdinand Lehner:
  \italic{sv. Řehoře Velkého Nešporní Antifony,}
  Praha 1868,
  s. 61n.
}

} % bookpart

\bookpart {
  \header {
    subtitle = "Cesta k věčné spáse"
    composer = "Karel Eichler (1915)"
  }

\score {
  \relative c' {
    \key cis \phrygian
    \eichlerStyle
    e4 e( a fis) e fis( a \grace fis8) a4 \div_ib_iiib
    e( fis) a( gis) e( fis gis) e fis \div_ii_iiib
    e( cis) e( fis) e( d cis) \bar "||"
    e e( d) cis cis \bar "|."
  }
  \addlyrics {
    Když sto -- lo -- val král,*
    nard svůj % sic
    vy -- dá -- val vů -- ni svou.
    Al -- le -- lu -- ja.
  }
  \header {
    modus = "III"
    differentia = "2"
    psalmus = "Žalm 109"
  }
}

\score {
  \relative c' {
    \key g \major
    \eichlerStyle
    d4 e e g( a) a \div_ib_iiib g( a) b( a) g a \div_ib_iiib
    a e a fis g e( d) \div_ib_iiib c( d e f) g g e \bar "|"
    c( d) e( g) e e \bar "|."
  }
  \addlyrics {
    Le -- vi -- ce je -- ho* pod hla -- vou mou,
    a pra -- vi -- ce je -- ho
    o -- be -- jme mne.
    Al -- le -- lu -- ja.
  }
  \header {
    modus = "IV"
    differentia = "B2"
    psalmus = "Žalm 112"
  }
}

\score {
  \relative c' {
    \key cis \phrygian
    \eichlerStyle
    e4 e gis a fis a gis \div_ib_iiib
    e fis fis gis fis e e \div_ib_iiib
    gis fis fis a gis e fis \div_ib_iiib
    e fis d e cis \div_ib_iiib
    d b cis( d e fis) e( d) cis cis \bar "||"
    e e( d) cis cis \bar "|."
  }
  \addlyrics {
    Čer -- ná jsem, a -- le krá -- sná,*
    dce -- ry je -- ru -- sa -- lem -- ské,
    pro -- to mi -- lo -- val mě král
    a u -- ve -- dl mne
    do po -- ko -- je své -- ho.
    Al -- le -- lu -- ja.
  }
  \header {
    modus = "III"
    differentia = "1"
    psalmus = "Žalm 121"
  }
}

\score {
  \relative c' {
    \key e \mixolydian
    \eichlerStyle
    e4( a) fis e( d) e fis fis e \div_ib_iiib
    gis a b a fis a a gis gis \div_ib_iiib
    a( b a gis) fis gis a fis fis( e d) \div_ib_iiib
    fis( gis a gis fis gis) e \bar "||"
    fis( gis) fis e e \bar "|."
  }
  \addlyrics {
    Již zi -- ma po -- mi -- nu -- la,*
    prš -- ka pře -- sta -- la a o -- de -- šla:
    vstaň, pří -- tel -- ky -- ně má, a pojď.
    Al -- le -- lu -- ja.
  }
  \header {
    modus = "VIII"
    differentia = "1"
    psalmus = "Žalm 126"
  }
}

\score {
  \relative c' {
    \key g \major
    \eichlerStyle
    d4 e g g g a( b) a \div_ib_iiib
    g( a) b g a \div_ib_iiib
    e a( g) fis( g) e( d) \div_ib_iiib
    c! d e g e e e \bar "||"
    c( d) e( g) e e \bar "|."
  }
  \addlyrics {
    Slič -- na u -- či -- ně -- na jsi
    a pří -- jem -- nou v_ra -- do -- stech svých,
    sva -- tá Bo -- ží Ro -- dič -- ko.
    Al -- le -- lu -- ja.
  }
  \header {
    modus = "IV"
    differentia = "B2"
    psalmus = "Žalm 147"
  }
}

\markup\justify\small{
  Antifona k Magnificat je, narozdíl od obou zbylých verzí,
  ze druhých nešpor.
}

\score {
  \relative c'' {
    \key g \mixolydian
    \eichlerStyle
    g4 g g( c) a g( a) f g a g a g( \grace f8) f4 \div_ib_iiib
    g( a) g a( g f) g( a) f f \div_ib_iiib
    f g f g a a c( d) c c( b c d c) a( c) b g \bar "||"
    a( b) a g g \bar "|."
  }
  \addlyrics {
    Bla -- ho -- sla -- ve -- nou mne na -- zý -- va -- ti bu -- dou
    vši -- chni ná -- ro -- do -- vé,
    ne -- bo na po -- ní -- že -- ní děv -- ky
    vzhlé -- dl Bůh.
    Al -- le -- lu -- ja.
  }
  \header {
    modus = "VIII"
    differentia = "1"
    psalmus = "Magnificat"
  }
}

\markup\small{Karel Eichler: Cesta k věčné spáse, sv. 3, Brno 1915, s. 1121-1132.}

} % bookpart

\bookpart {
  \header {
    subtitle = "Český kancionál"
    composer = "Dobroslav Orel (1912, 1921)"
    poet = "Jan Hejčl"
  }

\score {
  \relative c' {
    \orelStyle
    \key fis \dorian
    e8 fis a gis[ fis] a b cis4 \bar "|"
    b8 b cis a a gis4 fis \bar "||"
  }
  \addlyrics {
    V_ne -- bi, kde král sto -- lu -- je,*
    nar -- dem tvá pro -- sba dý -- chá.
  }
  \header {
    modus = "II"
    differentia = "D"
    psalmus = "Žalm 109"
  }
}

\score {
  \relative c' {
    \orelStyle
    \key cis \phrygian
    e8 fis gis a fis e[ d] cis b cis d e4 \bar "|"
    a8 b cis b a fis[ e] e d cis4 \bar "||"
  }
  \addlyrics {
    Le -- vi -- ce Bo -- ha pod hla -- vou tvo -- jí jest,*
    pra -- vi -- ce je -- ho tě ob -- jí -- má.
  }
  \header {
    modus = "III"
    differentia = "a"
    psalmus = "Žalm 112"
  }
}

\score {
  \relative c' {
    \orelStyle
    \key d \lydian
    d8[ e] fis gis a4 a \div_i_iib
    gis8 a b4 a \div_i_iib
    e8 fis g fis e d d4 \div_ii_iv
    a'8 d cis d b a8 a4 \div_ii_iv
    e8 g fis e fis e d d4 \bar "||"
  }
  \addlyrics {
    Jsi pl -- ná ho -- ře,*
    a -- však krás -- ná
    me -- zi vše -- mi že -- na -- mi,
    pro -- to mi -- lo -- val tě Bůh
    a do svých sí -- del u -- ve -- dl.
  }
  \header {
    modus = "V"
    differentia = ""
    psalmus = "Žalm 121"
  }
}

\score {
  \relative c' {
    \orelStyle
    \key f \lydian
    f8 g a g a c bes a4 \div_i_iib
    a8 g f e d e d c4 \div_ii_iv
    g'8 f g a g f f4 \bar "||"
  }
  \addlyrics {
    Zi -- mní do -- by již mi -- nu -- ly,*
    ja -- ra krá -- sy ti na -- sta -- ly,
    vstaň a pojď, má pří -- tel -- ko!
  }
  \header {
    modus = "VI"
    differentia = "F"
    psalmus = "Žalm 126"
  }
}

\score {
  \relative c' {
    \orelStyle
    \key d \mixolydian
    d8[ g] fis g fis4 e \div_i_iib
    d8 e g a b4 \div_ii_iv
    c8 b a[ g] fis g4 \div_ii_iv
    b8 a g a fis e d4 \bar "||"
  }
  \addlyrics {
    Krá -- sná a sli -- čná*
    u -- či -- ně -- na jsi
    ve svých pů -- va -- bech,
    sva -- tá Bo -- ží Ro -- di -- čko!
  }
  \header {
    modus = "VII"
    differentia = "a"
    psalmus = "Žalm 109"
  }
}

\score {
  \relative c' {
    \orelStyle
    \key d \dorian
    f8 g g a f4 \div_ib
    a8 a a b[ c] a4 \div_ii_iv
    c8 c d bes a g a4 a \div_ii_iv
    c8 c c c d bes a[ b!] c4 \div_ii_iv
    a8 a a a g f g[ a] a4 \div_ii_iv
    a8 b c c bes a g f4 \bar "|"
    bes8 g g f a f e d4 \div_ii_iv
    f8 g a a g a b![ c] a4 \div_ii_iv
    a8 g a bes a g f g f[ e] d4 \bar "||"
  }
  \addlyrics {
    Sva -- tá Ma -- ri -- a,
    u -- bo -- hým při -- spěj,
    ma -- lo -- my -- sl -- ných se u -- jmi,
    sl -- zí -- cí zas k_srd -- ci při -- viň,
    za lid vě -- ří -- cí se při -- mluv,
    du -- cho -- vní rač pod -- po -- ro -- vat;
    za že -- ny pros za -- svě -- ce -- né,
    po -- moc tvou ať vši -- cni cí -- tí,
    kdož -- ko -- liv sla -- ví tvou sva -- tou sla -- vnost.
  }
  \header {
    modus = "I"
    differentia = "g"
    psalmus = "Magnificat"
  }
}

\markup\small{
  Český kancionál, Praha 1921, s. 644-657.
}

} % bookpart