\version "2.12.3"

\include "spolecne.ly"

\paper {
  % neroztahovat noty na celou stranku, ale nechat misto dole:
  ragged-bottom = ##t 
}

dvaleluja = \lyricmode { A -- le -- lu -- ja, a -- le -- lu -- ja. }
slavaRespText = \lyricmode {
  Slá -- va Ot -- ci i Sy -- nu_* i Du -- chu sva -- té -- mu.
}

alelResp = { d4 c d a b c d4. d }

\score {
  \relative c'' {
    \choralniRezim
    
    % R
    \neviditelna d
    d4. a4( b) c( d) d4. \bar "|"
    \alelResp \bar "||"
    
    %V
    \neviditelna d
    d4 f f e d e( f) d d \bar "|"
    \neviditelna d
    \alelResp \bar "||"
    
    % Slava
    d4 d d d d c d \bar "|" d4 c a b c d \bar "||"
  }
  \addlyrics {
    \Response Pán vstal z_hro -- bu._*
    \dvaleluja
    \Verse On za nás u -- mřel na kří -- ži.
    \Response \dvaleluja
    \slavaRespText
  }
  \header {
    piece = "úsporný zápis:"
  }
}

\pageBreak

\score {
  \relative c'' {
    \choralniRezim
    
    % R
    \neviditelna d^"Kantor:"
    d4. a4( b) c( d) d4. \bar "|"
    \alelResp \bar "||" \break
    
    % R
    \neviditelna d^"Všichni:"
    d4. a4( b) c( d) d4. \bar "|"
    \alelResp \bar "||" \break
    
    %V
    \neviditelna d^"Kantor:"
    d4 f f e d e( f) d d \bar "|"
    \neviditelna d^"Všichni:"
    \alelResp \bar "||" \break
    
    % Slava
    d4^"Kantor:"
    d d d d c d \bar "|" d4 c a b c d \bar "||" \break
    
    % R
    \neviditelna d^"Všichni:"
    d4. a4( b) c( d) d4. \bar "|"
    \alelResp \bar "||"
  }
  \addlyrics {
    \Response Pán vstal z_hro -- bu._*
    \dvaleluja
    \Response Pán vstal z_hro -- bu._*
    \dvaleluja
    \Verse On za nás u -- mřel na kří -- ži.
    \Response \dvaleluja
    \slavaRespText
    \Response Pán vstal z_hro -- bu._*
    \dvaleluja
  }
  \header {
    piece = "a takhle se responsorium zpívá:"
  }
}