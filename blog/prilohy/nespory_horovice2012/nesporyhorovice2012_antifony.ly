\include "spolecne.ly"

\header {
  tagline = ""
}

\score {
  \relative c' {
    \choralniRezim
    \key f \major
    f4 a bes c c \barMin
    d f e d( c) c \barMin
    a c d c bes \barMaior
    a g f f g a( bes a) g( f) f \barMax
    f \[ a( c4. \] \[ d4 c) \] c \barMin c c d d c a \barMaior
    bes a g f e g a( f) f \barMax
    c' f e d c c \barMin
    f, bes a bes c c c d d c bes( a) bes c \barMin
    f, g a bes( a) g( f) f \barFinalis
  }
  \addlyrics {
    Aj an -- děl Pá -- ně
    Ma -- ri -- i Pan -- ně
    zvěs -- to -- val jest řka:
    poč -- neš a po -- ro -- díš sy -- na.
    A na -- zveš jmé -- no je -- ho Je -- žíš,
    jež se vy -- klá -- dá Spa -- si -- tel.
    Ten bu -- de ve -- li -- ký
    a Syn Nej -- vyš -- ší -- ho Bo -- ha vše -- mo -- hou -- cí -- ho
    slav -- ně slou -- ti bu -- de.
  }
  \header {
    quid = "ant."
    modus = "V"
    differentia = "a" 
    psalmus = ""
    id = ""
    piece = \markup {\sestavTitulekBezZalmu}
  }
}

\score {
  \relative c' {
    \choralniRezim
    d4( f) f( e) d4. f4( d) c( d) d4. \barMin
    d4 d d f( e) d( f) d4. c \barMaior
    d4 c( d) d d f g a( g a bes a) \barMaior
    a g f( e) d f d( f) d4.( c) \barMaior
    d4 f f( e) d \barMin f( d) c( d) d( c4. a) \barMin
    c4( d) f c c( f) d \barFinalis
  }
  \addlyrics {
    Ó Vy -- chá -- ze -- jí -- cí
    v_bles -- ku svět -- la věč -- né -- ho
    a Slun -- ce spra -- ved -- nos -- ti,
    při -- jdiž a o -- svě -- tiž ty,
    jež -- to se -- dí v_tem -- nos -- tech
    a v_stí -- nu smr -- ti.
  }
  \header {
    quid = "ant."
    modus = "II"
    differentia = "D" 
    psalmus = ""
    id = ""
    piece = \markup {\sestavTitulekBezZalmu}
  }
}