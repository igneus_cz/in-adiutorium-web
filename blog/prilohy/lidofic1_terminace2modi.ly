choralniRezim = {
  % nepsat predznamenani tempa (neni tempo)
  \override Score.TimeSignature #'stencil = ##f

  % noty bez nozicek
  \override Stem #'transparent = ##t

  % nozky maji nulovou delku a tak neovlivnuji legatove cary
  % (tento radek resi problem "vznasejicich se car")
  \override Stem #'Y-extent = ##f
  
  % nedelat taktove cary    
  \cadenzaOn
  
  % vzdycky vypsat becka
  % #(set-accidental-style 'forget) % for Lily 2.14
  \accidentalStyle "forget" % 2.15
}

\markup\large\bold{terminace modu II:}
\markup{}

\score {
  \relative c' {
    \choralniRezim
    f\breve e4 c-| d d \bar "||"
  }
  \addlyrics {
    "Zasedej po" mé \markup\underline{pra} -- vi -- ci
  }
  \header {
    piece = "Antiphonale Romanum 1912"
  }
  \layout{}
}

\score {
  \relative c' {
    \choralniRezim
    f\breve e4-| c d  \bar "||"
  }
  \addlyrics {
    "Zasedej po mé" \markup\underline{pra} -- vi -- ci
  }
  \header {
    piece = "Hejčl+Orel"
  }
  \layout{}
}