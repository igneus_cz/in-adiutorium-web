% je potreba, aby soubor byl umisten do slozky se zdrojovymi
% kody projektu In adiutorium:
\include "spolecne.ly"

\markup\italic{antifona původně:}

\score {
  \relative c' {
    \choralniRezim
    d4 d c d e( f) f \barMin f g f d e d4.( c) \barMaior
    d4 d e( f) f f e f d( c) d \barFinalis
  }
  \addlyrics {
    V_Bo -- ha dů -- vě -- řu -- ji a ne -- bu -- du se bát,
    co mi mů -- že u -- dě -- lat člo -- věk.
  }
  \header {
    quid = "2. ant."
    modus = "II"
    differentia = "D" 
    psalmus = "Žalm 56"
    id = "up-ant2"
    piece = \markup{\sestavTitulek}
  }
}

\markup\italic{nápěv žalmu:}

\score {
  \relative c'' {
    \override Score.TimeSignature #'stencil = ##f
    \key bes \major
    \cadenzaOn
    bes\breve a8 bes-| \grace \parenthesize g g4 \bar "|"
    a\breve f8-| \grace \parenthesize g g4 \bar "|."
  }
  \layout {
    ragged-last = ##f
  }
  \header {
    cislo = "IV"
    modus = "II (II.D)"
    transposice = "tónika d → g"
    piece = \markup {
      \line { 
        \bold{\fromproperty #'header:cislo } 
        " "
        \column { 
          \line { "modus: " \fromproperty #'header:modus }
          \line { "transposice antifony: " \fromproperty #'header:transposice }
        }
      }
    }
  }
}

\markup\italic{antifonu adekvátně transponujeme:}

\score {
  {
    \key bes \major
    \transpose d g {
      \relative c' {
        \choralniRezim
        d4 d c d e( f) f \barMin f g f d e d4.( c) \barMaior
        d4 d e( f) f f e f d( c) d \barFinalis
      }
    }
  }
  \addlyrics {
    V_Bo -- ha dů -- vě -- řu -- ji a ne -- bu -- du se bát,
    co mi mů -- že u -- dě -- lat člo -- věk.
  }
  \header {
    quid = "2. ant."
    modus = "II"
    differentia = "D" 
    psalmus = "Žalm 56"
    id = "up-ant2"
    piece = \markup{\sestavTitulek}
  }
}