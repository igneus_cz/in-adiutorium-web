\version "2.19.32"

choralniRezim = {
  % nepsat predznamenani tempa (neni tempo)
  \override Score.TimeSignature #'stencil = ##f

  % noty bez nozicek
  \override Stem #'transparent = ##t

  % nozky maji nulovou delku a tak neovlivnuji legatove cary
  % (tento radek resi problem "vznasejicich se car")
  \override Stem #'Y-extent = ##f

  % nedelat taktove cary
  \cadenzaOn

  % vzdycky vypsat becka
  % #(set-accidental-style 'forget) % for Lily 2.14
  \accidentalStyle "forget" % 2.15
}

barMin = {
  \once \override Staff.BarLine #'bar-extent = #'(1.5 . 2.5)
  \bar "|"
}
barMaior = {
  \once \override Staff.BarLine #'bar-extent = #'(-1.5 . 1.5)
  \bar "|"
}
barMax = { \bar "|" }
barFinalis = { \bar "||" }

\score {
  \relative c'' {
    \choralniRezim
    g4 a c\breve b4 c d-! c \barMaior
    c\breve b4 c a-! g \barFinalis
  }
  \header {
    modus = "VIII"
    differentia = "G"
    piece = "VIII.G"
  }
}

\score {
  \relative c'' {
    \choralniRezim
    a4( c) d\breve c4 d e-! d \barMaior
    d\breve c4 d c-! a \barFinalis
  }
  \header {
    modus = "IV"
    differentia = "A"
    piece = "IV.A"
  }
}

\score {
  \relative c' {
    \choralniRezim
    c4 d f\breve g4-! d \barMaior
    f\breve c4(-! d) d \barFinalis
  }
  \header {
    modus = "II"
    differentia = "D"
    piece = "II.D"
  }
}