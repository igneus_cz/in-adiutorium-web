\version "2.24.1"

\header {
  title = "Korouhve královské jdou vpřed"
}

\paper {
  % Add space for instrument names
  indent = 15\mm

  markup-system-spacing.basic-distance = #10
  system-system-spacing.basic-distance = #14

  top-margin = 1.5\cm
  bottom-margin = 1\cm

  #(define fonts
     (make-pango-font-tree
      "Charis SIL"
      "VL Gothic"
      "Courier"
      0.9))
}

highlightColour = #(rgb-color 1.0 0.9 0.5)

global = {
  \cadenzaOn
  \override Score.TimeSignature.stencil = ##f
  \autoBeamOff
}

% pitch as printed in the hymnal, transposed to our standard chant pitch
VoiceI = \transpose e d \relative c'' {
  \global
  \key e \dorian
  g8 a[ b] b b[ a g] a a[ b a] g fis( e4) \bar "|"
  a8 a[ b] a a[ e] g fis[ g] e e4 \bar "|"
  e8 g e d8*2 g8 g[ b c] a g4 \bar "|"
  g8 b[ a] b g[ e] g fis[ g] e e4 \bar "|."
}

verseVoiceI = \lyricmode {
  Ko -- rouh -- ve krá -- lov -- ské jdou vpřed,
  kříž zá -- ří oh -- něm ta -- jem -- ství:
  ten, kte -- rý stvo -- řil tě -- lo, svět,
  svým tě -- lem, při -- bit, na něm tkví.
}

VoiceII = \relative c' {
  \global
  f8 g[ a]

  % single highlighted note (=> horizontal bracket can't be used)
  % based on https://lsr.di.unimi.it/LSR/Item?id=699
  bes
  -\tweak layer #-1
  -\markup {
    \with-dimensions #'(0 . 0) #'(0 . 0)
    \with-color #highlightColour
    \filled-box #'(-2 . 2) #'(-1 . 6) #0
  }

  a[ g f] g g[ a g] f e[ d8*2] \bar "|"
  g8 g\startGroup a8*2 f8[ d]\stopGroup f e[ f] d c[\startGroup d]\stopGroup \bar "|"
  d d\startGroup f d[ c]\stopGroup f f[ g a] g g[\startGroup f]\stopGroup \bar "|"
  f f[\startGroup a] \grace{ bes[ a g]\stopGroup } a f[ d] f e[ f] d c[\startGroup d]\stopGroup \bar "||"
}

verseVoiceII = \lyricmode {
  Ve -- xíl -- la Re -- gis pró -- de -- unt;
  Ful -- get Cru -- cis my -- sté -- ri -- um,
  Qua Vi -- ta mor -- tem pér -- tu -- lit,
  Et mor -- te vi -- tam pró -- tu -- lit.
}

VoiceIII = \relative c' {
  \global
  f8 g[ a] a a[ g f] g g[ a g] f e( d4)
  g8 g[ a] g g[ d] f e[ f] d d4
  d8 f d d[\startGroup c]\stopGroup f f[ a bes] g g(\startGroup f4*1/2)\stopGroup
  f8 a[ g] a f[ d] f e[ f] d d4
}

VoiceIPart = \new Staff \with {
  instrumentName = "hymnář"
} { \VoiceI }
\addlyrics { \verseVoiceI }

VoiceIIPart = \new Staff \with {
  instrumentName = \markup\column{
    \with-url "https://archive.org/details/antiphonale-romanum-1912/page/339/mode/2up" "AR1912"
    "(a Olejník)"
  }
} { \VoiceII }
\addlyrics { \verseVoiceII }

VoiceIIIPart = \new Staff \with {
  instrumentName = \markup\with-url "https://melodiarium.musicologica.cz/view_source.php?ID_Pramen=26&picture=129" "Roh"
} { \VoiceIII }

\score {
  <<
    \VoiceIPart
    \VoiceIIIPart
    \VoiceIIPart
  >>
  \layout {
    \context {
      \Score
      \remove "Bar_number_engraver"
    }
    \context {
      \Voice
      \consists "Horizontal_bracket_engraver"

      % display horizontal brackets as coloured background boxes
      % slightly modified https://lsr.di.unimi.it/LSR/Snippet?id=960
      \override HorizontalBracket.stencil =
        #(grob-transformer 'stencil
          (lambda (grob orig)
            (let* ((X-ext (ly:stencil-extent orig X))
                   (Y-ext (cons -4 4)))
              (stencil-with-color (ly:round-filled-box X-ext Y-ext 0) highlightColour))))
      \override HorizontalBracket.layer = #-10
      \override HorizontalBracket.outside-staff-priority = ##f
      \override HorizontalBracket.Y-offset = #0
      \override HorizontalBracket.shorten-pair = #'(-.95 . -1.65)
    }
  }
}
