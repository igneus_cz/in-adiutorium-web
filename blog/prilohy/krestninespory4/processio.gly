\header
title: Velikonoční nešpory - zpěvy k procesí

\markup
Většina zpěvů je nekriticky přepsána
z Kolínského antifonáře, KNM sig. XII~A~22,
poslední staročeská antifona pak
z Jistebnického kancionálu, KNM sig. II~C~7.
Kde jsou v notovém záznamu divisiones, jsou doplněny při transkripci
jako pomůcka pro zpěváky, v pramenech nejsou.

První tři alelujatické antifony byly původně určeny k žalmům
nešpor, čtvrtá a pátá k žalmům procesí.
Tady se nabízejí k volnému výběru všechny,
mj. proto, že poslední antifona má nápěv známé antifony
\emph{Nos qui vivimus,} a patří, stejně jako ona, k žalmu 114.
Podle našeho návrhu se však žalm 114 při procesí zpívat nedá
a může proto být vhodné vybrat také jinou antifonu,
která ho neevokuje.

\score
book: f. 134v
mode: 7
annotation: VII c2
c3 h h ij i iHG fh gf e
f f d f hf gf e e ::
Al -- le -- lu -- ia al -- le -- lu -- ia
al -- le -- lu -- ia al -- le -- lu -- ia.

\score
book: f. 134v
mode: 4
annotation: IV E
c4 e h gh h h g e f g fe df f
f f c!df f ef gFE d e ::
Al -- le -- lu -- ia al -- le -- lu -- ia al -- le -- lu -- ia
al -- le -- lu -- ia al -- le -- lu -- ia.

\score
book: f. 134v
mode: 8
annotation: VIII G
c4 g fh j jg ij hg hg f
g fh j jg ij h g g ::
Al -- le -- lu -- ia al -- le -- lu -- ia
al -- le -- lu -- ia al -- le -- lu -- ia.

\score
book: f. 136r
mode: 7
annotation: VII c
c3 i h iji h f gh f ed ef f e e ::
Al -- le -- lu -- ia al -- le -- lu -- ia al -- le -- lu -- ia.

\score
book: f. 136r
annotation: per.
c4 c d fg g h i h gf gh h g g ::
Al -- le -- lu -- ia al -- le -- lu -- ia al -- le -- lu -- ia.

\markup
\pagebreak
V latinské i české versi antifony \emph{Christus resurgens}
je vynechán protižidovský verš \emph{Dicant nunc.}

\score
book: f. 136r
mode: 1
c4 c df ed d!efe dc f!ghg edf dce e
dhg/hjIHgh fdf c df e!fgf/df/dcd ;
dds e!fg ge fgFEdf dcd c!df f f efd d!efd d ::
de!fg gf/hfg ge/fgFEdf f f c!dfd!f f efd d ;
de!fg gf/hf/gFEdf f f c!df f ef d ::
Chri -- stus re -- sur -- gens ex mor -- tu -- is
iam non mo -- ri -- tur,
mors il -- li ul -- tra non do -- mi -- na -- bi -- tur.
Quod e -- nim vi -- vit, vi -- vit De -- o,
al -- le -- lu -- ia al -- le -- lu -- ia

\score
book: Jistebnický kancionál, f. 92r
mode: 1
c4
c df fEDC f!ghg ed!fd/ce e ,
dhg hi/jIH/gh fe/df cdf ef/gFED fd ,
dds efg ge fge/df dcd c d f f f efd d ::

d e f g gf/hfg/FE/df f ,
c!dfd!f f efd d ,
de fg gf hf g fe df f cdf f ef d ::

Kri -- stus vstalť jest z_mrt -- vých,
již ni -- kdy ne -- u -- mře,
smrt ví -- ce je -- mu ne -- bu -- de pa -- no -- va -- ti.

Neb kte -- rýž -- to živ jest,
živ jest v_Bo -- hu.
Ve -- sel -- me se, ra -- duj -- me se, a -- le -- lu -- ja.