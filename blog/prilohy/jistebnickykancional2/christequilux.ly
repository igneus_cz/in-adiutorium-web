\version "2.24.1"

\header {
  tagline = ##f
}

\paper {
  evenHeaderMarkup = \markup {} % get rid of page numbers

  scoreTitleMarkup = \markup\fill-line\small {
    ""
    \fromproperty #'header:piece
  }

  ragged-bottom = ##t

  system-system-spacing.padding = #3
  %annotate-spacing = ##t

  #(define fonts
    (make-pango-font-tree
                          "Charis SIL"
                          "VL Gothic"
                          "Courier"
     1))
}

\layout {
  \override Stem.stencil = ##f
  \override Score.TimeSignature.stencil = ##f
  indent = 0
}

\score {
  \relative c' {
    \cadenzaOn
    \slurDotted
    d4 d( e f) e( d c) d e f e d \bar ","
    \parenthesize e \parenthesize e e e( d e) c d e( f d) e \bar "|"
    e g \parenthesize g g( f) e( d e) f e d \bar ","
    d( e) f e( d c) d e f e d \bar "||"
  }
  \addlyrics {
    \set ignoreMelismata = ##t

    Kris -- te, jenž jsi svět -- lem i ta -- ké dnem na -- zván,
    _ _ noč -- nie tem -- nos -- ti za -- pu -- zu -- ješ sám,
    svět -- lo _ z_svět -- la že jsi, myť vě -- ří -- me,
    svět -- lo bla -- ho -- sla -- ve -- né o -- zna -- mu -- ješ.
  }
  \addlyrics {
    \override Lyrics.LyricText.font-shape = #'italic

    Chris -- te qui lux es et di -- es,
    noc -- tis te -- ne -- bras de -- te -- gis,
    lu -- cis -- que lu -- men cre -- de -- ris,
    lu -- men be -- a -- tum prae -- di -- cans.
  }
  \header {
    piece = "T+N: Jistebnický kancionál, f. 55v"
  }
  \layout {

  }
}

\pageBreak

\score {
  \relative c' {
    \cadenzaOn
    e4 e d( c) d e f e d \bar "'"
    d d f e c d f e \bar ","
    f g f e d f e d \bar "'"
    e e d( c) d e f e d \bar "||"
  }
  \addlyrics {
    Kris -- te, ty jsi den a svět -- lo,
    o -- svě -- cu -- ješ noč -- ní tem -- no,
    vě -- ří -- me, žes Svět -- lo svě -- tel,
    všech vě -- ří -- cích o -- svě -- ti -- tel.
  }
  \header {
    piece = "T: DMC N: Liber hymnarius, Solesmes 1983, s. 51"
  }
}
