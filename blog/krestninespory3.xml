<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Křestní nešpory (3): pražský ritus</titulek>

  <tags>křestní nešpory, pražský ritus, Jistebnický kancionál</tags>
  <kategorie>Rubriky</kategorie>
  <datum>2.2.2017 23:40</datum>
  <!-- <souvisejiciNoty></souvisejiciNoty> -->

  <souhrn></souhrn>

  <text>
    <p>
      Pokračujeme v krátkém seriálu o křestních nešporách
      (předchozí díly:
      <a href="/blog.php?clanek=krestninespory1.xml">1</a>,
      <a href="/blog.php?clanek=krestninespory2.xml">2</a>).
      Třetí díl bude věnován velikonočním nešporám
      v předtridentském pražském ritu.
    </p>
    <h3>Velikonoční nešpory podle pražského breviáře</h3>
    <p>
      Pro někoho může být překvapením, že velikonoční nešpory
      s průvodem ke křtitelnici, velice podobné minule představeným
      nešporám premonstrátským, byly součástí středověkého pražského
      ritu. I když je otázkou, kdy se takto naposledy slavily
      přímo ve svatovítské
      katedrále, jejich "scénář" byl součástí pražského breviáře
      až do přijetí jednotného breviáře tridentského.
    </p>
    <p class="petit">
      Trapná věc: rok zavedení tridentského breviáře v pražské
      provincii jsem míval někde poznamenán, ale najít ho teď nemohu.
      Doplním ho později, po návštěvě knihovny.
      Prozatím postačí terminus ad quem:
      1663 vycházejí první pražská <em>Officia propria,</em>
      tj. svazeček svátků vlastních pražské provincii, jako doplněk
      k již zavedenému římskému breviáři.
    </p>
    <blockquote>
      <p>
        <b>[1]</b>
        Druhé nešpory: velikonoční <em>Kyrie eleison</em>, celé.<br/>
        Antifona <em>Alleluia alleluia alleluia alleluia.</em>
        Žalm <em>Dixit Dominus.</em><br/>
        Antifona <em>Alleluia alleluia alleluia alleluia alleluia.</em>
        Žalm <em>Confitebor.</em><br/>
        Antifona <em>Alleluia alleluia alleluia alleluia.</em>
        Žalm <em>Beatus vir.</em><br/>
        Následuje graduale s veršem.<br/>
        <em>Alleluia. Pascha nostrum.</em>
        Verš <em>Epulemur.</em>
        Bez hymnu, capitula a verše hned prelát začne antifonu
        k Magnificat <em>Surrexit enim sicut dixit Dominus,
        praecedet vos in Galileam, alleluia. Ibi eum videbitis,
        alleluia alleluia alleluia.</em>
        (Pán vstal, jak řekl; předejde vás do Galileje, aleluja.
        Tam ho uvidíte, aleluja ...)
        Tato antifona se zpívá dvakrát před kantikem a dvakrát po něm.
        Následuje orace <em>Deus qui hodierna.</em> (se zakončením)
        <em>Per eundem.</em> Chór <em>Amen.</em>
      </p>
      <p>
        <b>[2]</b>
        Jeden, podle pořádku začínajících, vstoje hned začne antifonu
        <em>Alleluia alleluia alleluia.</em>
        Žalm <em>Laudate pueri.</em>
        Zpívajíce tento žalm, sestoupí (všichni) pravou stranou k prameni
        (tj. křtitelnici) se svícemi, korouhvemi,
        požehnanou svící (paškál) a subdiakonem,
        oblečeným do tunicely (subtili induto), nesoucím plenář.
        Když stojí u pramene a chór dokončil žalm s antifonou,
        začne prelát nebo kněz, který (bude) pronášet oraci, antifonu
        <em>Vidi aquam.</em> Okuřuje pramen, potom chór.
        Když je to hotovo, postaví se na své místo a říká verš
        <em>Domine, apud te est fons vitae, alleluia.</em>
        (Pane, u tebe je pramen života, aleluja.)
        <em>Oremus. Deus qui renatis. Per Christum Dominum nostrum. Amen.</em>
      </p>
      <p>
        <b>[3]</b>
        Jeden hned začne (antifonu) <em>Alleluia alleluia alleluia.</em>
        Žalm <em>In exitu Israel.</em>
        Zpívajíce tento žalm se ubírají do "středu kostela"
        (medium ecclesiae).
        Tam po dokončení žalmu a antifony kněz říká verš
        <em>Dicite in nationibus, alleluia. Quia regnavit Dominus,
        alleluia.</em>
        (Říkejte mezi národy, aleluja. Pán se ujal vlády, aleluja.)
        Orace <em>Solita nos, quaesumus Domine, pietate custodi:
        ut qui Unigeniti tui passione sumus redempti:
        eius resurrectione laetemur. Per eundem Christum.</em>
        (Ochraňuj nás, Pane, svou obvyklou laskavostí, abychom se,
        když jsme byli vykoupeni utrpením tvého Jednorozeného,
        radovali z jeho vzkříšení. Skrze téhož Krista ...)
      </p>
      <p>
        <b>[4]</b>
        Precentoři hned začnou antifonu <em>Christus resurgens
        ex mortuis iam non moritur, mors illi ultra non dominabitur.</em>
        (Kristus vstávající z mrtvých již neumírá, smrt nad ním již
        nemá vládu.)
        Za zpěvu antifony se z obou stran vrátí do chóru.
        Tam, ještě než se zpívá <em>Quod enim vivit</em>,
        starší, stojící uprostřed chóru, zazpívají verš
        <em>Dicant nunc Iudaei, quo milites custodientes sepulcrum
        perdiderunt regem ad lapidis positionem.
        Quare non servabant petram iustitiae?
        Aut sepultum reddant, aut resurgentem adorent nobiscum
        dicentes:</em>
        Chór: <em>Quod enim vivit, vivit Deo, alleluia alleluia.</em>
        Kněz říká verš <em>In resurrectione tua, Christe.</em>
        Orace <em>Concede, quaesumus, omnipotens Deus,
        ut qui dominicae resurrectionis solemnia colimus:
        innovatione tui Spiritus a morte animae resurgamus.
        Per eundem.</em>
        (Všemohoucí Bože, dej, ať my, kdo slavíme slavnost
        zmrtvýchvstání Páně, obnoveni tvým Duchem vstaneme
        ze smrti duchovní. Skrze téhož ...)
        <em>Dominus vobiscum.</em>
        Libovolné <em>Benedicamus Domino</em> s <em>alleluia</em>.
      </p>
      <p>
        Tento nešporní pořádek se nemění až do soboty - kromě toho,
        že se každý den říká vlastní
        versiculus generalis (míněn patrně verš graduale)
        a antifona k Magnificat.
      </p>
      <p class="petit">
        (Breviarium Pragense, Nürnberg 1502,
        fol. <a href=" http://daten.digitale-sammlungen.de/bsb00018698/image_185">CLXXII recto</a> a dále. Překlad vlastní.
        Pravopis latinsky reprodukovaných incipitů nekriticky
        modernisován. V závorkách jsou doplňky a poznámky
        překladatele.)
      </p>
    </blockquote>
    <p>
      Dále na fol. <a href="http://daten.digitale-sammlungen.de/bsb00018698/image_186">CLXXII verso</a>
      je zvlášť popsán řád nešpor pro toho, kdo by se je modlil
      mimo kostel.
      Jde o zjednodušení řádu popsaného výše. První tři žalmy
      se neříkají s vlastními antifonami, ale pod jedinou antifonou
      <em>super psalmos</em>.
      Po Magnificat a kolektě dne se vynechá celý procesní materiál
      a rovnou se připojí antifona
      <em>Christus resurgens</em>, verš <em>In resurrectione tua</em>,
      orace <em>Concede, quaesumus, omnipotens Deus</em>
      a nešpory se standardně ukončí <em>Benedicamus.</em>
      Rubrika se uzavírá vysvětlivkou ohledně dvou v této
      zkrácené formě bez náhrady ztracených nešporních žalmů:
    </p>
    <blockquote>
      <p>
        Dva žalmy, <em>Laudate</em> a <em>In exitu</em>,
        totiž nejsou součástí nešpor, ale návštěvy (křestního) pramene.
      </p>
    </blockquote>
    <p>
      Tato poznámka je pro chápání celého formuláře
      klíčová. Křestní nešpory podle ní sestávají ze dvou poměrně
      samostatných rituálních jednotek: vlastních nešpor, zkrácených
      z obvyklých pěti na tři žalmy a s poněkud netypickou
      skladbou zpěvů (úvodní kyrie; po psalmodii,
      místo obvyklé kombinace capitulum + hymnus + verš,
      se zpívá graduale a aleluja) a následného průvodu
      se zastaveními u křestního pramene, <em>in medio ecclesiae</em>
      a nakonec zpátky v chóru.
    </p>
    <p>
      Když srovnáme pražské velikonoční nešpory se starým pořádkem
      premonstrátským, podaným
      <a href="/blog.php?clanek=krestninespory2.xml">v minulém článku</a>,
      můžeme si všimnout, že tam je toto rozdělení zachováno
      ještě přísněji: na konci vlastních nešpor, před začátkem
      procesí, a pak na konci každého zastavení, se zpívá
      <em>Benedicamus Domino</em>, kterým se normálně hodinky ukončují.
    </p>
    <h3>Exkurs: Křestní nešpory mimo katedrálu</h3>
    <p>
      Slavily se velikonoční nešpory s průvodem ke křtitelnici
      v Čechách i mimo pražskou katedrálu? Pražský breviář,
      jak byl tištěn, reflektuje katedrální liturgii.
      Tomu, jak se jednotlivé
      obřady mají přizpůsobit podmínkám méně významných (a personálně,
      prostorově i materiálně hůře disponovaných) kostelů,
      se většinou výslovně nevěnuje.
      Můžeme si o tom však udělat obrázek
      na základě obsahu liturgických rukopisů typově odpovídajících
      pražskému ritu a určených pro liturgický provoz mimo katedrálu.
      Letmý průzkum, jehož výsledky následují, se zaměřil
      speciálně na antifonáře.
    </p>
    <p class="petit">
      Antifonáře obsahující kompletní velikonoční nešpory
      podle výše podaného uspořádání, vč. všech zpěvů k procesím:
    </p>
    <ul class="petit">
      <li><a href="http://www.manuscriptorium.com/apps/index.php?direct=record&amp;pid=AIPDIG-MVCHK_HR_4_II_A_4_2F5P07D-cs">Muzeum východních Čech v Hradci Králové, sig. Hr-4 (II A 4)</a>, od fol. 117r</li>
      <li>"Kolínský antifonář" -
      <a href="http://www.manuscriptorium.com/apps/index.php?direct=record&amp;pid=AIPDIG-NMP___XII_A_22____0YK1T6C-cs">Knihovna Národního muzea v Praze, sig. XII.A.22</a>, od fol. 134v</li>
    </ul>
    <p class="petit">
      <a href="http://www.manuscriptorium.com/apps/index.php?direct=record&amp;pid=AIPDIG-ZMP___504_C_004___107P3BA-cs">Knihovna Západočeského muzea v Plzni, sig. 504 C 004</a>
      - výtah zpěvů (většinou jen jejich intonací)
      pro významnější svátky,
      a to obvykle jen pro nešpory, řídčeji také pro matutinum,
      výjimečně i pro laudy.
      Nejde o knihu pro zpěváky, ale zřejmě pro vyšší kleriky,
      jejichž pěvecké vstupy spočívaly hlavně v intonacích
      významnějších zpěvů. Nechybějí však ani delší sólové zpěvy,
      jako v matutinech žalm Venite a v nešporách verše responsorií.
      Od fol. 26r najdeme typické pražské velikonoční nešpory.
      I když chybí antifona <em>Vidi aquam</em>, z rubriky je zřejmé,
      že se průvod ke křtitelnici konal, pravděpodobně i s antifonou,
      která ale uživatele rukopisu nezajímala - patrně proto, že ji
      zpíval někdo jiný.
    </p>
    <p class="petit">
      "Zimní antifonář plzeňského faráře Mikuláše",
      <a href="http://www.manuscriptorium.com/apps/index.php?direct=record&amp;pid=AIPDIG-NMP___XII_A_24____3UQDTL9-cs">Knihovna Národního muzea v Praze, sig. XII.A.24</a>,
      má od fol. 136r velikonoční nešpory odlišného obsahu:
      tři žalmy se zpívají pod jedinou antifonou
      ("výpravnou", zopakovanou z laud, nikoli alelujatickou),
      liší se i antifona
      k Magnificat a procesní materiál se omezuje pouze na antifonu
      <em>Christus resurgens</em>. Strukturou jsou tedy podobné
      výše podanému zjednodušenému pražskému pořádku pro nešpory
      "mimo kostel", výběrem zpěvů se však částečně liší.
    </p>
    <p class="petit">
      Hodí se nahlédnout i do utrakvistických knih pro liturgii
      v národním jazyce:
    </p>
    <p class="petit">
      Jistebnický kancionál,
      <a href="http://www.manuscriptorium.com/apps/index.php?direct=record&amp;pid=AIPDIG-NMP___II_C_7______4347P97-cs">Knihovna Národního muzea v Praze, sig. II C 7</a>,
      obsahuje velikonoční nešpory přeložené do češtiny.
      Začínají na fol. 92r. Stránka je sice
      porušená, ale zdá se, že z nešpor ztráta zasáhla
      recto jen nepodstatnou část rozsáhlé úvodní rubriky
      a verso část notace z prostředku jediné antifony.
      Rubrika dokládá
      kyrie na začátku nešpor, stejně jako graduale a aleluja,
      po němž se ovšem, <em>ut in missa</em>, má zpívat i sekvence
      <em>Victimae paschali laudes</em>.
      Z procesních zpěvů je zachována pouze antifona
      <em>Christus resurgens</em>, kterou tu najdeme v české verzi.
      Notovánu máme jedinou alelujatickou antifonu, dále antifonu k
      Magnificat, již zmíněnou procesní antifonu a sekvenci.
      To je náš známý pořádek velikonočních nešpor
      "mimo kostel", jen bohatší o sekvenci.
    </p>
    <p class="petit">
      Naproti tomu "Graduál a antifonář český",
      <a href="http://www.manuscriptorium.com/apps/index.php?direct=record&amp;pid=AIPDIG-MVCHK_HR42IIA44___2S3OMM0-cs">Muzeum východních Čech v Hradci Králové, sig. Hr-42 (II A 44)</a>,
      dokládá, že jeho utrakvističtí uživatelé starý pražský řád
      nezachovávali. Nešpory dne Zmrtvýchvstání (od fol. 549r)
      postrádají velikonoční strukturní zvláštnosti.
      Mají pět žalmů s alelujatickými antifonami,
      capitulum a hymnus jako každé jiné nešpory,
      žádné zpěvy k procesí.
      Na konci najdeme zpěv za dobrou úrodu, ale není úplně zřejmé,
      zda je ještě součástí formuláře velikonočních nešpor.
    </p>
    <p>
      I jen z tohoto malého vzorku, omezeného na to,
      co je z fondů českých
      knihoven a museí digitalisováno, si troufám uzavřít, že se
      <strong>v Čechách velikonoční nešpory konaly s průvodem ke křtitelnici
      i v některých kostelích mimo katedrálu.</strong>
    </p>
    <p>
      Proti takovému závěru je možné vznést dvě zásadní
      námitky:
      1. Vzhledem k tomu, že se tu vůbec neřeší stáří a provenience
      prozkoumaných rukopisů, nelze vyloučit, že ty, které obsahují
      kompletní velikonoční nešpory s průvodem, ve skutečnosti
      byly určeny právě pro provoz pražské katedrály.
      2. I pokud by se prokázalo, že pro katedrálu skutečně určeny
      nebyly,
      stále je dobře možné, že šlo o opisy z normativních pražských
      předloh, přičemž v liturgické praxi se některé části, mezi nimi
      snad i průvod velikonočních nešpor a jeho zpěvy, mimo katedrálu
      nikdy nepoužívaly.
    </p>
    <p>
      Elegantně odpovědět na obě námitky umožňuje
      výše obšírněji zmíněný rukopis
      <a href="http://www.manuscriptorium.com/apps/index.php?direct=record&amp;pid=AIPDIG-ZMP___504_C_004___107P3BA-cs">knihovny Západočeského muzea v Plzni, sig. 504 C 004</a>.
      Ad 1. Že rukopis nebyl určen pro liturgii pražské katedrály,
      je zřejmé z jeho rubrik. Na fol. 22r začíná dramaticky
      propracovaný závěr matutina pro velikonoční triduum -
      zdaleka ne totožný, ale podobný,
      jako ho známe z pražského breviáře
      (citovaný exemplář, fol. <a href="http://daten.digitale-sammlungen.de/~db/0001/bsb00018698/images/index.html?id=00018698&amp;groesser=&amp;no=&amp;seite=172">CLXV</a> verso).
      Charakteristicky se ovšem liší rozdělení rolí:
      zatímco pražský breviář zpěvy dělí mezi
      celé skupiny katedrálního kléru a závěr nechává zazpívat
      "preláta", zkoumaný pramen předpokládá tři chlapce,
      dva kaplany (<em>sacellanus</em>),
      "všechny kleriky", chór a arcijáhna. Jde tedy o knihu užívanou
      ve významném kostele, který byl sídlem arcijáhna,
      ovšem rozhodně ne v pražské katedrále.
      Ad 2. Že rituální obsah rukopisu byl v místě určení živý,
      vyplývá z jeho charakteru jako výběru zpěvů nebo jejich částí,
      které podle místních zvyklostí náležely vyšším klerikům.
      S velkou dávkou jistoty můžeme počítat s tím,
      že pokud se nějaký prvek do knihy dostal,
      bylo to proto, že v místě určení skutečně přicházel ke slovu.
    </p>
    <h3>Velikonoční nešpory na křesťanském Západě obecně</h3>
    <p>
      Pokud jde speciálně o průvod ke křtitelnici
      (resp. do baptisteria) v rámci velikonočních nešpor,
      A. King jeho vznik v již minule citovaném
      <a href="http://web.archive.org/web/20080316095635/http://www.premontre.org/subpages/scholastica/libking/king1.htm#APPENDIX%203">dodatku k <em>Liturgies of the Religious Orders</em></a>
      vysvětluje převzetím z jerusalémské liturgie, kde poutnice
      Egerie ve 4. stol. dosvědčuje <em>každodenní</em> průvod
      po nešporách na místo ukřižování Páně.
      Podobný každodenní obřad se později objevuje v Miláně,
      jeho místem se však již stává baptisterium.
      (Což si přímo říká o výklad, ale bylo by nezodpovědné pokoušet
      se o něj bez důkladnějšího studia pramenů.)
      Procesí do baptisteria se etablovalo i v Římě, avšak ne
      každodenně, nýbrž jen o vybraných svátcích.
    </p>
    <p>
      Patrně z Říma se velikonoční procesí do baptisteria
      rozšířila dále po Evropě. V souvislosti s tím nešpory,
      při nichž se procesí konalo, nabývaly rozmanitých podob.
      Pro ilustraci dobře poslouží ty, které sebral
      <a href="https://books.google.cz/books?id=id2pBPyQx_kC&amp;pg=PA494#v=onepage&amp;q&amp;f=false">E. Martene (s. 494nn)</a>.
      Dozvídáme se, že úvodní kyrie často (ale ne všude)
      doprovázelo slavnostní vstup biskupa. Někde naopak úplně chybí.
      Zkrácení nešpor na tři žalmy označuje Martene za zvyklost
      ve středověku prakticky všeobecnou, opět však jsou známa místa,
      kde se zpívalo vždy všech pět nešporních žalmů.
      Někde se žalmy zpívají každý se svou antifonou, jinde
      všechny pod jedinou.
      Náhradu capitula, hymnu a verše zpěvy graduale a aleluja
      můžeme označit za nejběžnější konfiguraci, ale i tady
      zjišťujeme různou praxi: někde chybí graduale,
      někde se po aleluja zpívá i sekvence.
      Antifona k Magnificat (ohledně jejíhož výběru v reprodukovaných
      formulářích nelze vysledovat významnější shodu)
      se někde třikrát opakuje.
      Ohledně průvodu panuje široká shoda na zastaveních
      u křtitelnice a u kříže (zavěšeného v chrámové lodi),
      ale různé je složení zpěvů k procesí. Někde úplně chybí
      žalmy a nacházíme místo nich tu sérii zpěvů z graduálu
      (patrně kvůli značné délce procesí), tu pouze vybrané antifony
      (snad naopak proto, že trasa procesí na víc nedávala čas).
      Někde se v průběhu průvodu zpívá jedno nebo dvě
      další Magnificat - buďto při procesí, nebo při některém
      zastavení.
    </p>
    <p class="petit">
      Martene Edmundus: <em>Tractatus de antiqua ecclesiae disciplina in divinis celebrandis officiis,</em> Lugduni 1706, 494nn.
    </p>
    <p>
      Durandus (<a href="https://books.google.cz/books?id=27CupttC_AEC&amp;pg=PA583#v=onepage&amp;q&amp;f=false">Rationale, lib. VI, cap. 89, par. 9</a>)
      dosvědčuje úvodní kyrie i tři žalmy pod jednou alelujatickou
      antifonou.
    </p>
    <p class="petit">
      Durandus Gulielmus: <em>Rationale divinorum officiorum</em>,
      Neapoli 1859.
    </p>
    <p>
      Z jeho alegorických výkladů alespoň drobná ukázka těch
      střízlivějších:
    </p>
    <blockquote>
      <p>
        Místo capitula následuje <em>Haec dies</em>,
        jako v ostatních hodinkách, protože tu není potřeba
        vyučování, ale jásot ...
        Místo hymnu následuje <em>Alleluja</em>, což je hymnus
        příslušející nebešťanům (illorum civium).
      </p>
      <p class="petit">
        (tamtéž, překlad vlastní)
      </p>
    </blockquote>
    <p>
      Procesí ke křtitelnici vykládá jako připomínku křtu,
      přičemž rozvíjí
      alegorii křtu jako průchodu Izraelitů Rudým mořem,
      v němž utonuli všichni naši nepřátelé, tj. "neřesti,
      hříchy a démoni".
      Dělá se zastavení "u Ukřižovaného, aby mu vzdali díky,
      protože on je to, kdo křtí ... a na kříži se nám dostalo
      vykoupení našich duší."
      Textovou látku procesí příliš nekomentuje, kromě toho,
      že dosvědčuje žalmy <em>Laudate, pueri</em> a <em>In exitu</em>
      na cestě ke křtitelnici a zpět.
      Cenná může být poznámka, že se procesí účastnil lid,
      "zvlášť muži a zvlášť ženy".
    </p>
    <p>
      Další osud velikonočních nešpor v římském ritu již dobře známe:
      jednotný tridentský breviář byl založen na breviáři římské
      kurie, sestaveném pro ne-katedrální a ne-farní kontext
      (tj. kostel bez křtitelnice). Průvod ke křtitelnici tu tedy
      nenajdeme.
      Ze strukturních zvláštností velikonočních nešpor zachovává
      pouze to, že capitulum, hymnus a verš nahrazuje (jen)
      "antifona" <em>Haec dies</em> - co do textové i hudební materie
      vlastně osamocené responsum stejnojmenného graduale.
      Starší podoba velikonočních nešpor žila dál v ritech
      řádů a diecésí, které podržely svůj předtridentský breviář:
      vedle premonstrátů to byli až do liturgické refomy
      karmelitáni původní observance, do 17. stol. dominikáni;
      do začátku 20. století diecése lyonská, až do reformy
      Braga a Rouen (A. King, op. cit.).
    </p>
    <p>
      Liturgická reforma Druhého vatikánského koncilu
      velikonoční nešpory dále připodobnila
      běžné struktuře a antifona <em>Haec dies</em> nahrazuje
      již jen responsorium po krátkém čtení.
    </p>
    <h3>Souhrn</h3>
    <p>
      Viděli jsme, že "tzv. křestní nešpory, při nichž se
      za zpěvu žalmů koná průvod ke křestnímu pramenu"
      (srov. VPDMC 213)
      se před přijetím jednotného tridentského breviáře slavily
      mj. i v Čechách, a to jak ve svatovítské katedrále,
      tak přinejmenším v některých významnějších farních kostelích.
      K jejich charakteristickým prvkům patří úvodní kyrie,
      zkrácení psalmodie na tři žalmy,
      graduale a aleluja na místě capitula, hymnu a verše.
      Po Magnificat orací končí vlastní nešpory
      a následuje procesí se zastavením u křtitelnice
      u kříže (rubriky pražského breviáře mluví o zastavení
      <em>in medio ecclesiae</em>, ale obsahově jde o zastavení
      u kříže a můžeme předpokládat, že šlo skutečně o místo,
      kde byl v chrámové lodi kříž zavěšen)
      a nakonec zpět v chóru.
      Jako zpěvy k procesí se zpívají zbylé dva nešporní žalmy.
      Jednotlivá zastavení jsou ve formátu komemorace:
      skládají se z antifony (ta při zastavení u kříže ovšem chybí),
      verše a orace.
    </p>
    <p>
      Seriál uzavře čtvrtý článek, v němž se pustím na tenký led
      a pokusím se navrhnout, jak by se, v souladu s ustanovením
      <em>Paschalis sollemnitatis</em> 98,
      mohly "křestní nešpory, při nichž se
      za zpěvu žalmů koná průvod ke křestnímu pramenu,"
      na území pražské církevní provincie obnovit a slavit
      v dnešní době.
    </p>
  </text>
</clanek>
