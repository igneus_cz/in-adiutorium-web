<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" encoding="utf-8" href="clanekblogu.xsl" version="1.0"?>

<clanek>
  <titulek>Vratké základy</titulek>

  <tags>gregoriánský chorál, chorál český, László Dobszay, invitatorium, psalmodie</tags>
  <kategorie>Projekt</kategorie>
  <datum>4.3.2024 22:40</datum>
  <souvisejiciNoty>chorinvit, psalmiviii</souvisejiciNoty>

  <souhrn></souhrn>

  <text>
    <p>
      Jak se projekt In adiutorium pomalu propracovává
      k <a href="/blog.php?clanek=adventfinal.xml">prvním rozsáhlejším výstupům</a>,
      které alespoň v mých očích dosahují
      kvality připouštějící liturgické využití,
      vidím rostoucí potřebu tematisovat některé známé nedostatky, které leží
      hluboko v základech projektu, postihují celý repertoár
      nebo jeho významnou část a hned tak (pokud vůbec)
      odstraněny nebudou.
    </p>
    <p>
      Ideální tvůrce českého antifonáře by byl znalec
      teorie, historie a interpretace gregoriánského chorálu -
      a celé šíře chorálního repertoáru oficia.
      Někdo jako hudebně vzdělaný a zkušený kantor solidně fungujícího
      benediktinského kláštera (jako tvůrci německého
      <em>Antiphonale zum Stundengebet</em>,
      Godehard Joppich a Rabanus Erbacher, tehdy mniši a kantoři
      benediktinského opatství Münsterschwarzach),
      nebo profesor středověké hudby s bohatou provozovací praxí
      liturgickou i koncertní
      (jako Lászlo Dobszay, tvůrce rozsáhlého korpusu
      chorálních zpěvů pro mši i oficium v maďarštině).
      Já jsem pochopitelně nikdy žádný znalec chorálu
      nebyl a ani dnes nejsem.
      V době, kdy jsem se do práce na zhudebnění Denní modlitby církve
      pouštěl, <strong>jsem o gregoriánském chorálu nevěděl téměř nic,</strong>
      a v doplňování vzdělání jsem vždycky byl <em>mnohem</em> liknavější než
      ve skládání nových českých zpěvů, na které tudíž po dlouhou
      dobu spíš než znalosti chorálu měly vliv moje mylné
      představy o něm.
    </p>
    <p class="petit">
      Na druhou stranu víme, že soudobí domácí znalci a praktici
      gregoriánského chorálu
      se od "chorálu v národním jazyce" vesměs drží co možná daleko.
      Možná moje ignorantství bylo skoro nezbytným předpokladem,
      bez kterého bych se do takového díla nejspíš nikdy nepustil.
    </p>
    <p>
      Jednu rozsáhlou skupinu ne-právě-žádoucích charakteristik
      našeho korpusu je možné shrnout pod heslo
      <strong>příliš povrchní znalost systému osmi modů.</strong>
      V každém úvodu do gregoriánského chorálu se student dočte,
      že každý modus charakterisuje jeho finála a tenor,
      o rozdělení modů na autentické a plagální a obvyklém rozsahu
      každého z nich, příp. nějaké přiřazení
      významů/afektů/charakteristik.
      Ale co už stručný úvod zprostředkovat nemůže je skutečnost,
      že stejně charakteristické jsou pro každý modus obvyklé
      postupy, ozdoby, kadence a celé nápěvové modely
      pro texty vhodné délky a struktury.
      Když jsem tak poprvé celý rok zpíval oficium podle předkoncilního
      antifonáře, jedním z velkých překvapení bylo, jak je velká část
      tradičního repertoáru snadná. I velmi omezeně hudební člověk
      jako já může s trochou praxe mnoho antifon zazpívat z listu,
      protože jsou stavěné z dobře známých, v antifonáři mnohonásobně
      opakovaných melodických prvků. Někdy jsem byl až pohoršen
      neoriginalitou (když se někdy dvě a více antifon v rámci jedné
      hodinky zpívá na stejný nápěvový model) nebo nápěvy, které mají
      charakter jednoduchých líbivých popěvků a zdály se postrádat
      důstojnost a hloubku, jakou jsem od chorálních nápěvů očekával.
      Moje nápěvy se do té doby hnaly za originalitou a
      vystižením smyslu daného textu a ve srovnání s tradičním repertoárem
      byly často přetížené melismaty, málo zpěvné a obsahovaly mnoho nezvyklých
      postupů. Dědictví té doby je v korpusu i nadále přítomno
      a asi vždycky do nějaké míry bude.
    </p>
    <p>
      Z podobného soudku je třeba zmínit
      <strong>neznalost struktury tradičního repertoáru.</strong>
      Některé liturgické doby mají nápadně vysokou koncentraci
      vybraných modelů antifon, podobně jsou určité modely
      charakteristické pro některé skupiny svátků.
      Jsou nápěvové modely, které se vyskytují prakticky jen
      v cyklu žaltáře.
      A jsou svátky, jejichž zpěvy mají charakter
      výrazně netypický a pro začátečníka interpretačně náročný.
      Ani toto jsem však nevěděl, dokud jsem nepřezpíval celý předkoncilní
      antifonář, a v mých nápěvech tudíž tyto makrostruktury
      pozorovatelné nejsou.
    </p>
    <p>
      Speciálním případem vztahujícím se k oběma právě probraným
      bodům jsou
      <strong>invitatoria modu I a VIII,</strong>
      která se v tradičním repertoáru vyskytují jen okrajově,
      a to snad výhradně v jeho pozdních vrstvách.
    </p>
    <p class="petit">
      <em>Antiphonale Romanum I</em>, Solesmis 2020, s. XIX.<br/>
      Srov. <a href="http://doi.org/10.5283/epub.25558">Hiley D.: <em>Western Plainchant</em></a>, Oxford: Clarendon Press 1993, s. 99.
    </p>
    <p>
      V mém korpusu českých zpěvů však jsou invitatoria modu I
      velice hojná, protože tento je mi odjakživa blízký a dobře
      se mi v něm nápěvy skládají. (A první antifonář se zpěvy matutina,
      ke kterému jsem se dostal a adaptoval jeden z jeho nápěvů
      žalmu Venite pro český text, byl jako na potvoru antifonář
      cisterciácký, který invitatoria modu I zná.)
      Invitatorií modu VIII jsem v minulosti také pár měl
      a postupně jsem je vymýtil, ale v modu I je jich příliš mnoho.
      Nápěvy, které považuji za kvalitní,
      pak čistě jen kvůli hudebně-historickým ohledům nezahazuji.
    </p>
    <p>
      Obdobným případem jsou <strong>antifony IV. transponovaného modu:</strong>
      v tradičním repertoáru jejich drtivou většinu
      tvoří variace na jediný masivně rozšířený nápěvový model,
      ale já jsem to dlouho nevěděl a mám tak značné množství
      antifon, které se k modu hlásí základními charaktristikami
      (finála, tenor, rozsah), ale k danému tradičnímu modelu nepatří.
    </p>
    <p>
      Když byla řeč o invitatoriích, je vhodné také zmínit, že
      <strong>moje invitatoria mají z velké části charakter podobný
      běžným antifonám k žalmům,</strong>
      zatímco tradiční invitatoria jsou vesměs výrazně melismatická.
      I to jsem však zjistil až pozdě; navíc se mi ani teď nedaří
      pro české texty skládat dobře znějící nápěvy odpovídajícího rázu.
      Kromě horší schopnosti českého textu nést melismata
      (popř. mé omezené schopnosti melismatické české zpěvy tvořit)
      to částečně souvisí i se strukturou textu:
      zatímco nejrozšířenější textový model latinských
      invitatorií ústí do stereotypní výzvy
      (citátu žalmu 95, 1) "pojďme, klanějme se",
      Denní modlitba církve při jejich překladu tuto výzvu
      obvykle klade na začátek (pro ilustraci viz příklady níže).
      To není bez důsledků pro možnosti zhudebnění.
    </p>
    <table class="petit">
      <thead>
        <tr>
          <th>příležitost</th>
          <th>Liturgia horarum</th>
          <th>Denní modlitba církve</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>advent do 16. 12.</td>
          <td>Regem ventúrum Dóminum, <em>veníte, adorémus.</em></td>
          <td><em>Pojďme, klaňme se</em> Králi, na kterého čekáme.</td>
        </tr>
        <tr>
          <td>společné texty o Panně Marii</td>
          <td>Christum, Fílium Maríæ, <em>veníte, adorémus.</em></td>
          <td>Oslavujme Pannu Marii; <em>klaňme se</em> Kristu, jejímu Synu!</td>
        </tr>
        <tr>
          <td>Krista krále</td>
          <td>Jesum Christum, regem regum, <em>veníte, adorémus.</em></td>
          <td><em>Pojďme, klaňme se</em> Ježíši Kristu; on je Král králů.</td>
        </tr>
      </tbody>
    </table>
    <p>
      Jak už bylo řečeno, v prvních letech, kdy vznikalo v relativně
      vysokém tempu velké množství zpěvů, jsem věděl jen velice málo
      o gregoriánském chorálu, a chyběl mi i základní přehled
      o novějších praktických edicích.
      Když jsem zkraje práce na projektu hledal
      <strong>nápěvy žalmů,</strong> vzal jsem, co bylo pohotově
      k mání, a základem mého korpusu českých antifon
      se tak stala <a href="http://musicasacra.com/pdf/tones.pdf">tabulka chorálních nápěvů psalmodie</a>
      nabízená na webu CMAA a reflektující předkoncilní římské chorální
      knihy, tedy <strong>stav bádání z počátku 20. stol.</strong>
      Dnešní chorální knihy ze Solesmes pracují s košatějším
      systémem nápěvů psalmodie, který má navíc nápěvy pro
      modus II recitující na kvartě, modus IV recitující na tercii,
      a pro pramody C, D a E. Dalším nápadným rozdílem je, že modus III
      recituje na kvintě (h).
    </p>
    <p class="petit">
      Tenor modu III byl předmětem vyhroceného sporu právě už
      na počátku 20. stol. při přípravě <em>Editio Vaticana</em>.
      Srov. <a href="https://www.liturgische-gregorianik.online/pdf/wagner.pdf">Wagner P.: <em>Der Kampf gegen die Editio Vaticana: Eine Abwehr</em></a>, Graz-Wien: Styria 1907, s. 31nn.
    </p>
    <p>
      Mně jednodušší a staletími liturgického provozu
      prověřený modální systém i dnes vyhovuje a dodatečné
      posolesmeštění korpusu nemám v plánu.
      Dá se ale předpokládat, že modální systém odlišný
      od chorálních knih vydávaných dnes v Solesmes
      bude částí církevně-hudební veřejnosti vnímán
      jako další z mnohých principielních nedostatků.
    </p>
    <p class="petit">
      Kdyby někdo chtěl naše nápěvy antifon používat
      v rámci modálního systému novodobých solesmeských knih,
      nevystačí jen s výměnou nápěvů psalmodie.
      Část antifon připsaných teď hlavně
      modům I. a IV. transponovanému by změnila příslušnost,
      ale zejména by bylo potřeba z velké části přepsat nápěvy antifon
      modu III, protože jsou všechny psány
      pro "pozdní" modus III, recitující na c, a s nápěvem
      psalmodie recitujícím na h by dobře nefungovaly.
    </p>
    <p>
      Přes sto let stará chorální edice není pro mě určující
      jen tím, že jsem v samých počátcích projektu přijal její sadu
      nápěvů psalmodie a s tím její pojetí systému osmi modů,
      ale také později, když jsem hledal praktickou zkušenost
      s gregoriánským chorálem, sáhl jsem po antifonáři pro denní
      hodinky z téhož období (1912), protože to byl jediný rozumně dostupný
      antifonář římského ritu pokrývající celý liturgický rok.
      Tak můj až dosud jediný pořádně známý (opakovaně během celého liturgického
      roku přezpívaný) chorální repertoár oficia je antifonář
      zredigovaný na základě stavu bádání z počátku 20. stol.
      a obsahující pořádnou vrstvu neogregoriánských kusů
      (nejen ve svátečních formulářích nově zavedených nebo
      přepracovaných v potridentském období, ale také v žaltáři,
      výrazně zasaženém breviářovou reformou Pia X.).
    </p>
    <p>
      To, že moje <a href="/blog.php?clanek=adaptacelatinskych.xml"><strong>nápěvy jsou</strong> (s nemnoha výjimkami)
      <strong>složeny se zřetelem jen na daný český text, bez ohledu na nápěv případné latinské předlohy,</strong></a>
      je v mých očích jak silnou stránkou, tak stránkou slabou -
      samozřejmě pokaždé v jiném ohledu.
      Silnou stránkou, protože textu nebylo činěno násilí a dostal
      nápěv sobě na míru, nebyl na něj pasován nápěv složený
      původně pro text v jiném jazyce a mající možná výrazně odlišné
      pro zhudebnění významné charakteristiky.
      Slabou stránkou, protože nápěvy nehlásají jednotu
      českého antifonáře s antifonářem latinským
      a nemluví hudebními jazyky jednotlivých koutů a historických
      vrstev chorálního
      repertoáru, ale jsou výrazně poznamenány omezeností svého
      (jediného) autora.
    </p>
    <p>
      Dnes už se snažím nápěvy latinských předloh brát v potaz,
      když opravuji méně vydařené kusy, ale systematické přepracování
      všech antifon, které mají tradiční latinskou předlohu, neplánuji.
      Český antifonář činící ve větší míře zadost tradičnímu
      chorálnímu repertoáru zůstane úkolem pro někoho jiného.
    </p>
    <p class="petit">
      I tady sehrála významnou roli nevědomost.
      Je dobře známé, že pokoncilní breviář obsahuje mnoho
      zpěvů s novými, v chorální tradici se nevyskytujícími texty,
      ale že obsahuje i velké množství zpěvů s texty tradičními
      mi zůstalo skryto prakticky až do doby, kdy byly texty
      nejnovějšího vydání latinského breviáře přidány do
      <a href="https://breviar.sk/la/">internetového breviáře Juraje Vidékyho</a>
      a já jsem je začal prohlížet s pomocí pracovní verze
      <a href="https://github.com/igneus/eantifonar2">rozšíření do prohlížečů</a>,
      které k textům antifon a responsorií doplňuje noty -
      k českým textům moje zhudebnění, k latinským noty
      z <a href="https://gregobase.selapa.net/">GregoBase</a>.
    </p>
    <p>
      Kdybych byl v počátcích projektu vybaven dostatečně širokou
      znalostí tradičního repertoáru, velmi pravděpodobně <strong>bych
      si neuložil některá pravidla,</strong> která jsou teď do korpusu
      českých chorálních zpěvů důkladně vepsána:
      mj. to, že díly jednoho žalmu se v rámci stejné hodinky
      <a href="/blog.php?clanek=napevydelenychzalmu.xml">zpívají pokud možno na stejný nápěv</a>,
      nebo že když dvě antifony sdílejí část textu,
      onen společný úryvek by měl být v obou opatřen
      v mezích možností stejným nápěvem.
      Tradiční repertoár nejen že si takové pravidlo
      (které se v mém provedení stalo příčinou nejedné opravdu
      kostrbatě zhudebněné a později předělávané antifony) neukládá,
      ale naopak: známe řadu příkladů, kdy dokonce i pro identický text
      existuje více různých nápěvů, a to nejen jako rozdíl mezi
      různými místními tradicemi, ale právě i v rámci stejné knihy.
    </p>
    <p class="petit">
      V antifonáři z r. 1912 viz např. antifony
      <em>Veni, sponsa Christi</em>
      (různé nápěvy v různých hodinkách commune svatých panen)
      a <em>Euge, serve bone et fidelis</em>
      (různé nápěvy odlišují svátky vyznavačů-biskupů od nebiskupů);
      v rukopisném podání třeba ty rukopisy, které mezi zpěvy pro oktáv Nanebevzetí
      uvádějí antifonu <a href="https://cantusindex.org/id/003708"><em>Maria virgo, semper laetare</em></a>
      dvakrát, pokaždé s jiným nápěvem.
    </p>
    <p>
      V počátcích projektu jsem delší dobu marně hledal informaci,
      <strong>jaký přesně má vztah differentia (zakončení nápěvu psalmodie)
      k nápěvu antifony.</strong>
      Vysvětlení jsem nakonec našel (tehdy zrovna v Erfurtu v universitní
      knihovně) v Antiphonale monasticum:
    </p>
    <blockquote>
      <p>
        Většina nápěvů psalmodie má různá zakončení
        neboli <em>differentiae</em>. Ta odpovídají různým
        melodickým figurám, kterými antifony začínají,
        tak, aby se usnadnil přechod ze závěru [žalmového] verše
        k antifoně, když se tato má opakovat.
      </p>
      <p class="petit">
        (<a href="https://archive.org/details/antiphonalemonasticum1934/page/n1243/mode/2up"><em>Antiphonale monasticum</em>, Parisiis-Tornaci-Romae: Desclée 1934, s. 1209</a>; překlad JP)
      </p>
    </blockquote>
    <p>
      Differentiae v té době existujících antifon jsem
      <a href="https://github.com/igneus/In-adiutorium/commit/1623773b6382ac11112673f2048eff14e0ae0b56">opravil</a>
      a napříště je vybíral tak, <em>aby žalmový verš končil na tónu,
      kterým antifona začíná, nebo (když taková differentia neexistuje)
      na tónu co nejbližším.</em>
      Kdybych v té době měl trochu nazpívaný nebo nastudovaný
      tradiční repertoár (nebo kdybych se tenkrát nespokojil
      s první nalezenou odpovědí, hledal dál a měl při tom hledání
      trochu štěstí), věděl bych, že ve skutečnosti je vztah
      mezi diferencí a začátkem nápěvu antifony složitější.
    </p>
    <p class="petit">
      Např. všechny následující antifony modu I by podle mého
      naivního systému měly diferenci <strong>D</strong>, ale v chorálních knihách
      mají <strong>f</strong> (<em><a href="https://gregobase.selapa.net/chant.php?id=10431">In tympano et choro</a></em>),
      <strong>g</strong> (<em><a href="https://gregobase.selapa.net/chant.php?id=2323">Nemo in eum misit manum</a></em>),
      <strong>a3</strong> (<em><a href="https://gregobase.selapa.net/chant.php?id=2521">Domine, nonne bonum</a></em>).
      <!--
          k tomu srov.
          <a href="https://books.google.cz/books?id=8PjxAAzSK8sC&amp;lpg=PA220&amp;pg=PA221#v=onepage">Apel W.: <em>Gregorian Chant</em>, Indiana University Press 1990, s. 221nn</a>
      -->
    </p>
    <p>
      Když jsem si toho později všiml, už jsem měl antifon příliš mnoho,
      zároveň jsem nikde nenašel přesná a úplná pravidla, kterými se
      přiřazování diferencí antifonám řídí
      (ani neměl data a nástroje umožňující ona pravidla snadno odvodit
      přímo z masy tradičních antifon),
      a rozhodl jsem se pokračovat s dosavadní ne-zcela-přesnou
      interpretací vztahu mezi diferencí a začátkem antifony
      a opravu provést až v rámci velké závěrečné redakce celého korpusu.
      Na tu dosud nedošlo.
    </p>
    <p>
      Dalším problémem, jehož definitivní vyřešení odkládám
      kamsi do mlhavé vzdálené budoucnosti, je
      <strong>nekonsistence v užívání rytmických znamének.</strong>
      (1) V úplných počátcích projektu jsem každé prodloužení
      důsledně zapisoval tečkou (která nemá význam jako v moderní
      notaci, ale jako <em>punctum mora</em> v někdejším
      <a href="https://archive.org/details/antiphonalemonasticum1934/page/n11/mode/2up">solesmeském systému rytmických znamének</a>).
      (2) Později jsem rytmická znaménka psát téměř úplně přestal,
      protože výskyt prodloužených not v mých antifonách je předvídatelný
      (jde převážně o závěry jednotlivých oddílů nápěvu)
      a zároveň se mi zdálo, že explicitní rytmisace nevhodně omezuje
      interpretaci orientovanou na text.
      (3) V posledních letech jsem pak opět začal rytmická znaménka
      psát a i zpětně doplňovat, ale jen tam, kde specifický rytmus,
      se kterým nápěv počítá, není evidentní a je důležitý
      (nápěv rytmisovaný jinak nedává smysl). V praxi se jedná
      zejména o to, když se má prodloužit některá jedna nota
      v rámci melismatu.
      Také jsem začal příležitostně používat
      horizontální episema (čárku nad/pod notou), obvykle v případech,
      kdy je vhodné učinit zadost přirozeně dlouhé slabice
      a zazpívat ji s o něco větší rytmickou hodnotou.
      Tři výše zmíněné rozdílné přístupy se nyní v korpusu vyskytují
      vedle sebe, v závislosti na tom, kdy byl ten který nápěv
      naposledy upravován, a kdy dojde
      k plošnému vynucení jednotného standardu zůstává ve hvězdách.
    </p>
    <p>
      Zmínit je tu třeba i použitý <strong>notační systém.</strong>
      Jeho základem je moderní notace přizpůsobená pro zápis chorálních
      melodií, zhruba tak, jak je zaužívána v některých zpěvnících
      a musikologických odborných publikacích.
      Zaznamenaná hudební informace se omezuje na melodii,
      její frázování, členění delších melismat a příp. explicitní rytmické hodnoty.
      Použitý notační systém pak dobře odpovídá tomu, jak o chorálních nápěvech
      přemýšlím. Kdyby se někdo sháněl po informaci, jak vypadá
      ta která antifona ve svatohavelských neumách nebo jaké neumy
      mají být v nějakém kritickém místě, musel bych odpovědět,
      že to nikdo neví (resp. každý si to může vymyslet, jak uzná
      za vhodné), protože koncepty gregoriánské semiologie v základech
      projektu In adiutorium nikdy neležely.
    </p>
    <p>
      Můžeme uzavřít, že kdyby se někdo pokoušel o mně a projektu
      In adiutorium tvrdit ono
      <em>"osvojil si hudební jazyk [gregoriánského chorálu] považovaný za mrtvý po stovky let"</em> (D. Farkas o Dobszayovi v <a href="https://apps.lfze.hu/netfolder/PublicNet/Doktori%20dolgozatok/farkas_domonkos/tezis_en.pdf#page=7">anglickém autoreferátu</a> <a href="https://apps.lfze.hu/netfolder/PublicNet/Doktori%20dolgozatok/farkas_domonkos/">své disertace</a>),
      nebyla by to pravda.
      Můj "chorál český" je "chorál" jen v tom smyslu, že jde o <em>jednohlasý
      liturgický zpěv složený v církevních tóninách.</em> Přebírá sice
      vybrané prvky z tradičního chorálního repertoáru
      (nápěvy psalmodie, nejobecnější pravidla systému osmi modů,
      několik nápěvových modelů antifon
      a krátkých responsorií, několik nápěvů konkrétních antifon),
      ale jako celek v hudebním jazyce (jazycích) gregoriánského chorálu
      formulován není. Jde nanejvýš o jeho vzdálenou nápodobu.
      Dnes se sice v zásadě hlásím k ideálu, který Dobszay
      formuloval ve svém manifestu
      <a href="https://egyhazzene.hu/wp-content/uploads/2018/12/dobszay_living_gregorian.pdf"><em>Living Gregorian</em></a>
      (též <a href="http://www.zborarcus.sk/clanky.php?p=zivy_gregorian">slovensky</a>),
      ale jednak jsem na něj narazil až <a href="/blog.php?clanek=nesporyzilina2013.xml">pozdě</a>,
      jednak jsem nikdy nedisponoval znalostmi gregoriánského
      chorálu v rozsahu, jaký si solidní uskutečnění tohoto ideálu
      v českém antifonáři žádá.
    </p>
  </text>
</clanek>
