<?php
// checks that all the links presented at /odkazy and /knihy work

define('DATA_DIR', PUBLIC_DIR);
define('LINKS_DATA_FILE', DATA_DIR . '/odkazy.xml');
define('BOOKS_DATA_FILE', DATA_DIR . '/knihy.xml');

function check_url($url, $verbose=false)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    $data = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($status != 200 || $verbose) {
        echo strval($status) . ' ' . $url . "\n";

        if ($status != 200) {
            return 1;
        }
    }

    return 0;
}

$checked = 0;
$errors = 0;
$verbose = true;
$check = function ($url) use ($verbose) {
    check_url($url, $verbose);
};

$xml = simplexml_load_file(LINKS_DATA_FILE);
foreach ($xml->xpath('//stranka') as $link) {
    $errors += $check($link->url->__toString());
    $checked++;

    foreach ($link->popis->xpath('.//a') as $a) {
        $errors += $check($a['href']);
        $checked++;
    }
}

$xml = simplexml_load_file(BOOKS_DATA_FILE);
foreach ($xml->xpath('//odkazy/url') as $l) {
    $errors += $check($l->__toString());
    $checked++;
}
foreach ($xml->text->xpath('//text//a[starts-with(@href, "http")]') as $a) {
    $errors += $check($a['href']);
    $checked++;
}

echo "$checked links checked, $errors errors found\n";
