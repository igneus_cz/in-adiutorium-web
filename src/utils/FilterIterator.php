<?php
namespace InAdiutorium\utils;

/* wraps another iterator, only yields items for which
a filter function returns true */
class FilterIterator implements \Iterator
{
    private $iterator;
    private $filter;

    public function __construct($iterator, $filter)
    {
        $this->iterator = $iterator;
        $this->filter = $filter;
    }

    public function rewind()
    {
        $this->iterator->rewind();

        if (!$this->filter_approved()) {
            $this->next();
        }
    }

    public function current()
    {
        return $this->iterator->current();
    }

    public function key()
    {
        return $this->iterator->key();
    }

    public function valid()
    {
        return $this->iterator->valid();
    }

    public function next()
    {
        do {
            $this->iterator->next();
        } while ($this->iterator->valid() && !$this->filter_approved());
    }

    private function filter_approved()
    {
        return call_user_func($this->filter, $this->iterator->current());
    }
}
