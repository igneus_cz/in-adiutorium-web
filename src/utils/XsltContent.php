<?php
namespace InAdiutorium\utils;

/* represents and eventually retrieves
   content contained in a XML document and transformed
   by a XSLT template */
class XsltContent
{
    private $xml_path;
    private $xslt_path;

    public function __construct($xml_path, $xslt_path)
    {
        $this->xml_path = $xml_path;
        $this->xslt_path = $xslt_path;
    }

    public function retrieve()
    {
        $xml = new \DOMDocument;
        $xml->load($this->xml_path);

        $xslt = new \DOMDocument;
        $xslt->load($this->xslt_path);

        $proc = new \XSLTProcessor;
        $proc->importStyleSheet($xslt);

        return $proc->transformToXML($xml);
    }
}
