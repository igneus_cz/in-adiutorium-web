<?php
namespace InAdiutorium\utils;

class CachedXsltContent
{
    private $xml_path;
    private $xslt_path;
    private $cache_dir;

    public function __construct($xml_path, $xslt_path, $cache_dir)
    {
        $this->xml_path = $xml_path;
        $this->xslt_path = $xslt_path;
        $this->cache_dir = $cache_dir;
    }

    public function retrieve()
    {
        $resource = new FileCache(
            new XsltContent($this->xml_path, $this->xslt_path),
            $this->cache_dir . '/' . basename($this->xml_path) . '.html',
            filemtime($this->xml_path)
        );
        return $resource->retrieve();
    }
}
