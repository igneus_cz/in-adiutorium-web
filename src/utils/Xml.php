<?php
namespace InAdiutorium\utils;

class Xml
{
    /*
      SimpleXMLElement->asXML() returns the whole element;
      this function strips the element itself and returns only
      it's content
    */
    public static function unwrap($xml_str)
    {
        $eol = "\n";
        $start = strpos($xml_str, $eol);
        $end = strrpos($xml_str, $eol);
        $length = $end - $start;
        return substr($xml_str, $start, $length);
    }
}
