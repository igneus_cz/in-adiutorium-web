<?php
namespace InAdiutorium\utils;

/* Caches a string in a file. */
class FileCache
{
    private $source;
    private $cache_file;
    private $invalid_timestamp;

    public function __construct($source, $cache_file, $invalid_timestamp)
    {
        $this->source = $source;
        $this->cache_file = $cache_file;
        $this->invalid_timestamp = $invalid_timestamp;
    }

    public function retrieve()
    {
        if ($this->valid()) {
            return file_get_contents($this->cache_file);
        }

        $content = $this->source->retrieve();
        $f = fopen($this->cache_file, 'w');
        fwrite($f, $content);
        fclose($f);

        return $content;
    }

    public function __toString()
    {
        return $this->retrieve();
    }

    public function valid()
    {
        return (
            file_exists($this->cache_file)
            && filemtime($this->cache_file) > $this->invalid_timestamp
        );
    }
}
