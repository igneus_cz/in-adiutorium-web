<?php
namespace InAdiutorium\utils;

/* represents and eventually retrieves
   remote content reachable by means of a HTTP GET request */
class HttpContent
{
    private $url;

    public function __construct($url)
    {
        $this->url = $url;
    }

    public function retrieve()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'User-Agent: HttpContent (libcurl php)',
        ));
        $body = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($status !== 200) {
            throw new HttpError($status);
        }

        return $body;
    }
}
