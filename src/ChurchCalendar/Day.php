<?php
namespace InAdiutorium\ChurchCalendar;

class Day
{
    public $season;

    public function __construct($json)
    {
        $this->season = $json->{'season'};
    }
}
