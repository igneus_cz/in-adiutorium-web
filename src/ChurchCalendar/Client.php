<?php
namespace InAdiutorium\ChurchCalendar;

use InAdiutorium\ChurchCalendar\Day;

use InAdiutorium\utils\FileCache;
use InAdiutorium\utils\HttpContent;
use InAdiutorium\utils\HttpError;

class Client
{
    private $cache_dir;

    public function __construct($cache_dir)
    {
        $this->cache_dir = $cache_dir;
    }

    public function today()
    {
        $resource = new FileCache(
            new HttpContent('http://calapi.inadiutorium.cz/api/v0/en/calendars/czech/today'),
            $this->cache_dir . '/calendar.today.json',
            strtotime('today')
        );
        $response_body = $resource->retrieve();
        $json = json_decode($response_body);

        return new Day($json);
    }
}
