<?php
namespace InAdiutorium\Counter;

/* counter storage:
   - all values are integers
   - when the db file does not exist, it is not an error and
   all values are 0
 */
class DbaStorage
{
    private $handle = false;

    public function __construct($path, $mode='r')
    {
        if (file_exists($path) || $mode == 'c') {
            $this->handle = dba_open($path, $mode);
        }
    }

    public function __destruct()
    {
        $this->free();
    }

    // close the internal storage
    public function free()
    {
        if ($this->isLoaded()) {
            dba_close($this->handle);
            $this->handle = false;
        }
    }

    public function fetch($key)
    {
        if (!$this->isLoaded()) {
            return 0;
        }

        if (!dba_exists($key, $this->handle)) {
            return 0;
        }

        return (int)dba_fetch($key, $this->handle);
    }

    public function firstKey()
    {
        if (!$this->isLoaded()) {
            return false;
        }
        return dba_firstkey($this->handle);
    }

    public function nextKey()
    {
        if (!$this->isLoaded()) {
            return false;
        }
        return dba_nextkey($this->handle);
    }

    public function total()
    {
        $total = 0;
        $key = $this->firstKey();
        while ($key) {
            $total += $this->fetch($key);
            $key = $this->nextKey();
        }
        return $total;
    }

    public function increment($key)
    {
        $count = $this->fetch($key) + 1;
        dba_replace($key, strval($count), $this->handle);
    }

    private function isLoaded()
    {
        return ($this->handle !== false);
    }
}
