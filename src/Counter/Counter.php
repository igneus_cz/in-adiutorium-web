<?php
namespace InAdiutorium\Counter;

define("COUNTER_DIR", ROOT_DIR . '/statistika');
define("GLOBAL_DB", COUNTER_DIR . '/statistikastahovani.gdbm');
define("OBSOLETE_FILES_LIST", ROOT_DIR . '/config/ignoruj.txt');
define("RECENTLY_DOWNLOADED_LIST", COUNTER_DIR . '/poslednistazeni.txt');

/* keeps track of downloaded files */
class Counter
{
    private $entries = array();
    private $day = 0;
    private $total = 0;
    private $day_total = 0;

    /* for displaying specify which day's entries should be loaded;
       for updating call without parameters */
    public function __construct($load_day=false)
    {
        $this->day = $load_day;

        if ($load_day !== false) {
            $this->loadEntries($load_day);
        }
    }

    public function increment($file)
    {
        $today = time();

        $this->incrementIn($file, GLOBAL_DB);
        $this->incrementIn($file, $this->dayDbPath($today));

        if ((time() - filemtime(RECENTLY_DOWNLOADED_LIST)) < 60) {
            $fw = fopen(RECENTLY_DOWNLOADED_LIST, "a"); // append
        } else {
            $fw = fopen(RECENTLY_DOWNLOADED_LIST, "w"); // overwrite
        }
        fputs($fw, $file."\n");
        fclose($fw);
    }

    public function entries($sort_by='downloads_day')
    {
        $sort_func = function ($a, $b) use ($sort_by) {
            return spaceship_desc($a->$sort_by, $b->$sort_by);
        };

        uasort($this->entries, $sort_func);
        return $this->entries;
    }

    public function lastDownloadTime()
    {
        return filemtime(GLOBAL_DB);
    }

    public function total()
    {
        return $this->total;
    }

    public function day_total()
    {
        return $this->day_total;
    }

    public function recentDaySummaries()
    {
        $summaries = array();
        for ($i = 0; $i <= 13; $i++) {
            $time = strtotime("$i days ago");
            $db = new DbaStorage($this->dayDbPath($time));
            $summaries[$time] = $db->total();
            $db->free();
        }
        return $summaries;
    }

    public function recentlyDownloaded()
    {
        return file(RECENTLY_DOWNLOADED_LIST);
    }

    private function loadEntries($day)
    {
        $obsolete_files = $this->loadObsoleteFiles();

        $db = new DbaStorage(GLOBAL_DB);
        $day_db = new DbaStorage($this->dayDbPath($day));

        $key = $db->firstKey();
        while ($key) {
            $entry_total = $db->fetch($key);
            $this->total += $entry_total;

            $entry_day = $day_db->fetch($key);
            $this->day_total += $entry_day;

            $entry = new Entry(
                $key,
                $entry_total,
                $entry_day,
                array_key_exists($key, $obsolete_files)
            );
            array_push($this->entries, $entry);

            $key = $db->nextKey();
        }
        $db->free();
        $day_db->free();
    }

    private function loadObsoleteFiles()
    {
        $obsolete = array();
        foreach (file(OBSOLETE_FILES_LIST) as $line) {
            $obsolete[rtrim($line)] = true;
        }
        return $obsolete;
    }

    private function incrementIn($file, $db_path)
    {
        $db = new DbaStorage($db_path, 'c');
        $db->increment($file);
        $db->free();
    }

    private function dayDbPath($time)
    {
        return COUNTER_DIR . '/stazeni_' . date('Y_m_d', $time) . '.gdbm';
    }
}
