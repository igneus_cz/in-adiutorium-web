<?php
namespace InAdiutorium\Counter;

class Entry
{
    public $file;
    public $downloads_total;
    public $downloads_day;
    public $obsolete;

    public function __construct($file, $total, $day, $obsolete=false)
    {
        $this->file = $file;
        $this->downloads_total = $total;
        $this->downloads_day = $day;
        $this->obsolete = $obsolete;
    }
}
