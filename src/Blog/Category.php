<?php
namespace InAdiutorium\Blog;

class Category
{
    public $title;
    public $slug;
    public $description;

    public function __construct($title, $slug, $description='')
    {
        $this->title = $title;
        $this->slug = $slug;
        $this->description = $description;
    }

    public function permalink($url_opts=array())
    {
        $url_opts = array_merge($url_opts, array('slug' => $this->slug));
        return url_for('blog_category', $url_opts);
    }
}
