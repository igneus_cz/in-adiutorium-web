<?php
namespace InAdiutorium\Blog;

class Tag
{
    public $title;
    public $slug;
    public $posts;

    public function postsCount()
    {
        return count($this->posts);
    }

    public function permalink($url_opts=array())
    {
        $url_opts = array_merge($url_opts, array('slug' => $this->slug));
        return url_for('blog_tag', $url_opts);
    }
}
