<?php
namespace InAdiutorium\Blog;

use InAdiutorium\Blog\Category;
use Symfony\Component\Yaml\Yaml;

class Repository
{
    private $xml;
    private $all_categories;
    private $categories_by_slug;
    private $categories_by_title;

    private $sluggifier;

    public function __construct($xml_path, $descriptions_file, $sluggifier)
    {
        $this->sluggifier = $sluggifier;

        $this->xml = @simplexml_load_file($xml_path);
        if ($this->xml === false) {
            throw new LoadError("Unable to load '$xml_path'.");
        }

        $descriptions = Yaml::parse(file_get_contents($descriptions_file));

        $this->all_categories = array();
        $this->categories_by_slug = array();
        $this->categories_by_title = array();
        foreach ($this->xml->seznamkategorii->children() as $node) {
            $title = $node->__toString();
            $desc = '';
            if (array_key_exists($title, $descriptions)) {
                $desc = $descriptions[$title];
            }
            $cat = new Category($title, $node['slug'], $desc);
            $this->all_categories[] = $cat;
            $this->categories_by_slug[$cat->slug->__toString()] = $cat;
            $this->categories_by_title[$cat->title] = $cat;
        }
    }

    public function latest($limit=0, $page=0, $load_contents=false)
    {
        $all = $this->postNodes();
        $nodes = $this->paginate($limit, $page, $all);
        return array(
            $this->buildPosts($nodes, $load_contents),
            $this->pagesCount($limit, $all)
        );
    }

    public function fromCategory($category, $limit=0, $page=0, $load_contents=true)
    {
        $all = $this->xml->xpath("/prehled/clanky/clanek[kategorie='" . $category->title . "']");
        $nodes = $this->paginate($limit, $page, $all);
        return array(
            $this->buildPosts($nodes, $load_contents),
            $this->pagesCount($limit, $all)
        );
    }

    public function havingTag($tag)
    {
        $r = array_map(function ($filename) {
            return $this->findByFilename($filename);
        }, $tag->posts);

        usort($r, function ($a, $b) {
            return spaceship($b->time, $a->time);
        });

        return $r;
    }

    public function findByFile($filename, $load_contents=true)
    {
        $filename = basename($filename);
        foreach ($this->postNodes() as $node) {
            if ($filename == basename($node->soubor)) {
                return $this->buildPost($node, $load_contents);
            }
        }

        return false;
    }

    public function findBySlug($slug, $load_contents=true)
    {
        $nodes = $this->xml->xpath("/prehled/clanky/clanek[@slug='" . $slug . "']");

        if (count($nodes) >= 1) {
            return $this->buildPost($nodes[0], $load_contents);
        }

        return false;
    }

    public function findByFilename($fname, $load_contents=true)
    {
        $nodes = $this->xml->xpath("/prehled/clanky/clanek[id='" . $fname . "']");

        if (count($nodes) >= 1) {
            return $this->buildPost($nodes[0], $load_contents);
        }

        return false;
    }

    public function allCategories()
    {
        return $this->all_categories;
    }

    public function findCategoryBySlug($slug)
    {
        if (array_key_exists($slug, $this->categories_by_slug)) {
            return $this->categories_by_slug[$slug];
        }

        return false;
    }

    public function findCategoryByTitle($title)
    {
        if (array_key_exists($title, $this->categories_by_title)) {
            return $this->categories_by_title[$title];
        }

        return false;
    }

    public function allTags()
    {
        $tags = $this->tagData()['tags'];

        $r = array();
        foreach ($tags as $title => $posts) {
            $r[] = $this->buildTag($title, $posts);
        }

        return $r;
    }

    public function tagCloud()
    {
        $tags = $this->allTags();
        $tags = array_filter($tags, function ($t) {
            return $t->postsCount() > 1;
        });
        usort($tags, function ($a, $b) {
            $r = spaceship($b->postsCount(), $a->postsCount());

            return $r == 0 ? strcasecmp($a->title, $b->title) : $r;
        });

        return $tags;
    }

    public function findTagBySlug($slug)
    {
        $data = $this->tagData();
        if (!array_key_exists($slug, $data['slugs'])) {
            return null;
        }

        $title = $data['slugs'][$slug];

        return $this->buildTag($title, $data['tags'][$title]);
    }

    private function tagData()
    {
        $path = BLOG_DIR . "/vygenerovane/tags.txt"; // TODO extract
        return unserialize(implode("\n", file($path)));
    }

    private function buildTag($title, $posts)
    {
        $t = new Tag;
        $t->title = $title;
        $t->slug = $this->sluggifier->slugify($title);
        $t->posts = $posts;

        return $t;
    }

    private function postNodes()
    {
        return $this->xml->clanky->children();
    }

    // builds Blog\Post from a simplexml node
    private function buildPost($node, $load_contents=false)
    {
        $content = '';
        if ($load_contents) {
            $post_xml = simplexml_load_file($node->soubor);
            $content = $post_xml->text->asXML();
            $content = preg_replace('/<\/?text>/', '', $content); // remove wrapping element <text>
        }

        return new Post(
            $node->titulek->__toString(),
            $content,
            $node->souhrn,
            $node->datum,
            (int)$node->cas,
            $this->categories_by_title[$node->kategorie->__toString()],
            basename($node->soubor->__toString()),
            $node['slug'],
            $node->tags->__toString() == '' ? array() : preg_split('/,\s*/', $node->tags->__toString())
        );
    }

    private function buildPosts($nodes, $load_contents=false)
    {
        $r = array();
        foreach ($nodes as $node) {
            $r[] = $this->buildPost($node, $load_contents);
        }
        return $r;
    }

    private function paginate($limit=0, $page=0, $items)
    {
        $start = 0;
        $end = 0;

        if ($limit > 0) {
            if ($page > 0) {
                $start = $limit * $page;
            }
            $end = $start + $limit;
        }

        $r = array();

        $i = -1;
        foreach ($items as $item) {
            $i++;
            if ($i < $start) {
                continue;
            }
            if ($end > 0 && $i >= $end) {
                break;
            }

            $r[] = $item;
        }

        return $r;
    }

    private function pagesCount($page_size, $items)
    {
        if ($page_size === 0) {
            return 1;
        }

        return ceil(count($items) / $page_size);
    }
}
