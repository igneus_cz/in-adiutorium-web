<?php
namespace InAdiutorium\Blog;

/* Refreshes the index of blog posts, relied upon by Blog\Repository. */
class Updater
{
    private $dir;
    private $index_path;
    private $sluggifier;

    // elements copied from each post to index
    private $copy_fields = array(
        "titulek",
        "datum",
        "kategorie",
        "souhrn",
        "soubor",
        "tags"
    );

    public function __construct($dir, $index_path, $sluggifier)
    {
        $this->dir = $dir;
        $this->index_path = $index_path;
        $this->sluggifier = $sluggifier;
    }

    public function update()
    {
        $categories = array();
        $posts_for_downloads = array();
        $tags = array();

        $dir = Dir($this->dir);

        // load all raw post files
        $posts = array();
        while ($c = $dir->read()) {
            $file_path = $this->dir . '/' . $c;
            if (!is_file($file_path)) {
                continue;
            }

            if (substr($c, -3) !== 'xml') { // XML?
                continue;
            }

            if ($c === 'prazdnakostra.xml') {
                continue;
            }

            $post = $this->loadPost($file_path, $categories, $posts_for_downloads, $tags);
            $posts[] = $post;

            echo str_pad($post['datum'], 16)
                . " "
                . $post['titulek']
                . " - "
                . basename($post['soubor'])
                . "\n";
        }
        $dir->close();

        // sort:
        uasort($posts, [$this, 'comparePostsByDate']);

        // create index
        $index = new \SimpleXMLElement("<prehled></prehled>");
        $index_root = $index->addChild("clanky");

        $i = 0;
        foreach ($posts as $c) {
            $i++;

            $el = $index_root->addChild("clanek");

            $date_slug = date('Ymd', $c['cas']);
            $title_slug = $this->sluggifier->slugify($c['titulek']);
            $el->addAttribute('slug', "$date_slug-$title_slug");

            foreach ($this->copy_fields as $ke) {
                $el->addChild($ke, $c[$ke]);
            }

            // elements not included in the post files
            $el->addChild('id', basename($c['soubor']));
            $el->addChild("cas", $c['cas']);
        }

        // index by category
        $categories_root = $index->addChild("seznamkategorii");
        foreach (array_keys($categories) as $k) {
            $el = $categories_root->addChild("kategorie", $k);
            $el->addAttribute("slug", $this->sluggifier->slugify($k));
        }

        echo "Zpracováno " . count($posts) . " článků.\n";

        $this->writeXML($this->index_path, $index);
        echo "Uložen archiv.\n";

        $fnc = fopen(BLOG_DIR . "/vygenerovane/clankyknotam.txt", "w");
        if (!$fnc) {
            throw new Exception("Nepodařilo se otevřít soubor clankyknotam.txt pro zápis.");
        }
        fputs($fnc, serialize($posts_for_downloads));
        fclose($fnc);
        echo "Uložena databáze článků k notám.\n";

        $tag_slugs = array();
        foreach ($tags as $t => $posts) {
            $tag_slugs[$this->sluggifier->slugify($t)] = $t;
        }
        $difference = count($tags) - count($tag_slugs);
        if ($difference != 0) {
            echo "POZOR, $difference štítků má kolidující slug a nejdou prohlížet!\n";
        }
        $ft = fopen(BLOG_DIR . "/vygenerovane/tags.txt", "w");
        fputs($ft, serialize(array('tags' => $tags, 'slugs' => $tag_slugs)));
        fclose($ft);
        echo "Uloženy štítky.\n";
    }

    public function loadPost($path, &$categories, &$posts_for_downloads, &$posts_for_tags)
    {
        $d = simplexml_load_file($path);
        if (!$d) {
            throw new Exception("Nepodarilo se nacist clanek ze souboru '$path'.");
        }

        if (strlen($d->kategorie) > 0) {
            $categories[(string)$d->kategorie] = true;
        }

        $post = array();

        foreach ($this->copy_fields as $ke) {
            $post[$ke] = $d->$ke;
        }
        $post["soubor"] = $path;

        $a = explode(" ", $d->datum);
        $post['cas'] = $this->parseCzechDate($d->datum);
        $post['mtime'] = filemtime($path);

        // souvisi clanek s nejakymi notami?
        if ($d->souvisejiciNoty) {
            $syms = explode(",", $d->souvisejiciNoty);
            foreach ($syms as $s) {
                $s = trim($s);
                if (!array_key_exists($s, $posts_for_downloads)) {
                    $posts_for_downloads[$s] = array();
                }
                $posts_for_downloads[$s][] = basename($path);
            }
        }

        if ($d->tags) {
            $tags = explode(",", $d->tags);
            foreach ($tags as $t) {
                $t = trim($t);
                if ($t == '') {
                    continue;
                }
                if (!array_key_exists($t, $posts_for_tags)) {
                    $posts_for_tags[$t] = array();
                }
                $posts_for_tags[$t][] = basename($path);
            }
        }

        return $post;
    }

    public function writeXML($path, $xml_tree)
    {
        $f = fopen($path, "w");
        if (!$f) {
            throw new Exception("Nepodařilo se otevřít soubor '$path' pro zápis.");
        }

        $xml = $xml_tree->asXML();
        // SimpleXML produces a XML declaration without encoding,
        // which is an issue here. Cut it out.
        $i = strpos($xml, "?>");
        $xml = substr($xml, $i+2);

        fputs($f, "<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
        fputs($f, $xml);
        fclose($f);
    }

    public function comparePostsByDate($a, $b)
    {
        return spaceship_desc($a['cas'], $b['cas']);
    }

    // a very forgiving date parsing method
    public function parseCzechDate($str)
    {
        $a = explode(" ", $str);
        @list($day, $month, $year) = explode(".", $a[0]);
        @list($hour, $minute) = explode(":", $a[1]);
        return mktime((int)$hour, (int)$minute, 0, (int)$month, (int)$day, (int)$year);
    }
}
