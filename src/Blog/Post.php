<?php
namespace InAdiutorium\Blog;

class Post
{
    public $title;
    public $content;
    public $summary;
    public $datetime;
    public $time;
    public $category;
    public $file;
    public $slug;
    public $tags;
    public $internal_id;

    public function __construct($title, $content, $summary, $datetime, $time, $category, $file, $slug, $tags)
    {
        $this->title = $title;
        $this->content = $content;
        $this->summary = $summary;
        $this->datetime = $datetime;
        $this->time = $time;
        $this->category = $category;
        $this->file = $file;
        $this->slug = $slug;
        $this->tags = $tags;
        $this->internal_id = basename($file);
    }

    public function anchor()
    {
        return $this->internal_id;
    }

    public function permalink()
    {
        return url_for('blog_post', array('slug' => $this->slug));
    }

    public function shortDate()
    {
        if (date("Y", $this->time) != date("Y")) {
            return date("d. m. Y", $this->time);
        }

        return date("d. m.", $this->time);
    }
}
