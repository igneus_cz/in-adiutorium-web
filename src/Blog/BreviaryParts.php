<?php
namespace InAdiutorium\Blog;

define("dSUNDAY", 0);
define("dMONDAY", 1);
define("dTUESDAY", 2);
define("dWEDNESDAY", 3);
define("dTHURSDAY", 4);
define("dFRIDAY", 5);
define("dSATURDAY", 6);

define("DAY", 3600*24);

/* computes old breviary parts */
class BreviaryParts
{
    public function breviaryPart($time)
    {
        $y = (int)date('Y', $time);
        if ($time < $this->firstSeptemberSunday($y)) {
            if ($time < $this->ashWednesday($y)) {
                return 'zimní';
            }

            if ($time < ($this->pentecost($y) + DAY)) {
                return 'jarní';
            }

            return 'letní';
        }

        if ($time < $this->firstAdventSunday($y)) {
            return 'podzimní';
        }

        return 'zimní';
    }

    /* days when breviary parts are changed */

    public function firstSeptemberSunday($year)
    {
        // Sunday after the last day of August
        return $this->weekdayAfter(mktime(1, 1, 1, 8, 31, $year), dSUNDAY);
    }

    public function firstAdventSunday($year)
    {
        $nat = $this->nativity($year);
        $nat_weekday = $this->weekday($nat);
        if ($nat_weekday == 0) {
            $nat_weekday = 7;
        } // Nativity on Sunday
        return $nat - (DAY * (3 * 7 + $nat_weekday));
    }

    public function nativity($year)
    {
        return mktime(0, 0, 1, 12, 25, $year);
    }

    public function ashWednesday($year)
    {
        return $this->easter($year) - 46 * DAY;
    }

    public function easter($year)
    {
        return easter_date($year); // built-in PHP function
    }

    public function pentecost($year)
    {
        return $this->easter($year) + 49 * DAY;
    }

    /* helpers */

    private function weekdayAfter($time, $weekday)
    {
        $w1 = $this->weekday($time);
        if ($w1 == $weekday) {
            return $time + 7*DAY;
        }

        if ($weekday > $w1) {
            // in the same week
            return $time+($weekday-$w1);
        }

        // the next week
        return $time + (7-$w1+$weekday)*DAY;
    }

    private function weekday($time)
    {
        return (int)date('w', $time);
    }
}
