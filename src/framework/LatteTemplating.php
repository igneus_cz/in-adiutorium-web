<?php
namespace InAdiutorium\framework;

use \Latte\Engine;
use \Latte\Loaders\FileLoader;

class LatteTemplating extends Engine
{
    public function __construct($cache_directory, $views_directory, $slugifier)
    {
        parent::__construct();

        $this->setTempDirectory($cache_directory);
        $this->setLoader(new FileLoader($views_directory));

        $this->addFilter('slugify', [$slugifier, 'slugify']);
    }
}
