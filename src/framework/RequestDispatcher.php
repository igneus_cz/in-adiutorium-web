<?php
namespace InAdiutorium\framework;

class RequestDispatcher
{
    private $container;
    private $url_generator;

    public function __construct($container, $url_generator)
    {
        $this->container = $container;
        $this->url_generator = $url_generator;
    }

    /* $parameters are parameters returned by the url matcher
       (as found in routing configuration) */
    public function dispatch($request, $parameters)
    {
        if (array_key_exists('script', $parameters)) {
            $this->scriptPage($parameters);
        } elseif (array_key_exists('_controller', $parameters)) {
            $this->controllerAction($request, $parameters);
        } elseif (array_key_exists('redirect', $parameters)) {
            $this->redirect($request, $parameters);
        } elseif (array_key_exists('file', $parameters)) {
            $this->file($request, $parameters);
        } else {
            die('Fatal routing error.');
        }
    }

    private function scriptPage($parameters)
    {
        http_response_code(200);
        require ROOT_DIR . '/legacy/' . $parameters['script'];
        exit;
    }

    private function controllerAction($request, $parameters)
    {
        $parts = explode('#', $parameters['_controller']);
        if (count($parts) >= 2) {
            list($cname, $action) = $parts;
        } else {
            $cname = $parts[0];
            $action = 'index';
        }
        $controller = $this->container->get($cname . '.controller');
        $controller->run($action, $parameters);
    }

    private function redirect($request, $parameters)
    {
        $target = $parameters['redirect'];
        if ($target[0] == '/') {
            $location = $target;
        } else {
            $location = $this->url_generator->generate($target, $parameters['parameters']);
        }

        if ($request->query->count() > 0) {
            $location .= '?' . http_build_query($request->query->all());
        }

        header("Location: " . $location);
        if (array_key_exists('status', $parameters)) {
            http_response_code($parameters['status']);
        } else {
            http_response_code(301);
        }
    }

    private function file($request, $parameters)
    {
        http_response_code(200);
        header("Content-Type: " . content_type($parameters['file']));
        header("X-Sendfile: " . $parameters['file']);
        //header("Content-type: application/octet-stream");
    }
}
