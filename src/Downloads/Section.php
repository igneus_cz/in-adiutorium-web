<?php
namespace InAdiutorium\Downloads;

use InAdiutorium\utils\Xml;

class Section
{
    private $xml_node;
    public $id;
    public $title;
    public $description;
    public $itemCount;

    public function __construct($xml_node)
    {
        $this->xml_node = $xml_node;
        $this->id = $xml_node['id'];
        $this->title = $xml_node->nazev;
        $this->description = Xml::unwrap($xml_node->popis->asXML());
        $this->itemCount = $this->item_count($xml_node);
    }

    public function contents()
    {
        foreach ($this->xml_node->children() as $c) {
            switch ($c->getName()) {
            case 'sekce':
                yield new Section($c); break;
            case 'vec':
                yield new Item($c); break;
            }
        }
    }

    private function item_count($node)
    {
        $count = 0;

        /* it would be probably more eficient to count nested
           items using an XPath query, but SimpleXML doesn't support
           XPath queries returning other results than XML elements */
        foreach ($node->children() as $x) {
            switch ($x->getName()) {
            case "sekce":
                $count = $count + $this->item_count($x);
                break;
            case "vec":
                $count++;
                break;
            }
        }

        return $count;
    }
}
