<?php
namespace InAdiutorium\Downloads;

use InAdiutorium\utils\Xml;
use InAdiutorium\News;

class Item
{
    public $xml_node;
    public $id;
    public $title;
    public $filename;
    public $status;
    public $description;
    public $text;
    public $relatedPosts;
    public $externalURL;
    private $source;

    public function __construct($xml_node, $related_posts=false)
    {
        $this->xml_node = $xml_node;
        $this->id = (string) $xml_node->symbol;
        $this->title = (string) $xml_node->nazev;
        $this->filename = $xml_node->soubor;
        $this->source = $xml_node->zdroj;
        $this->externalURL = $xml_node->urlSouboru;
        $this->status = (string) $xml_node->status;
        $this->description = $xml_node->popis;
        $this->text = Xml::unwrap($xml_node->komentar->asXML());
        $this->relatedPosts = $related_posts;
    }

    public function downloadLink()
    {
        if ($this->isExternal()) {
            return $this->externalURL;
        }

        return url_for('counter') . '?soubor=' . $this->filePath();
    }

    public function isExternal()
    {
        return (bool) $this->externalURL;
    }

    public function filePath()
    {
        if ($this->isExternal()) {
            return '';
        }

        return 'materialy/' . $this->filename;
    }

    public function fullFilePath()
    {
        return ROOT_DIR . '/public/' . $this->filePath();
    }

    public function lastModified()
    {
        @$m = filemtime($this->fullFilePath());
        if ($m === false) {
            // error, usually file not found
            return 0;
        }

        return $m;
    }

    /* file size in kilobytes */
    public function fileSize()
    {
        if ($this->isExternal()) {
            return 0;
        }

        @$s = filesize($this->fullFilePath());
        if ($s === false) {
            // error
            return 0;
        }

        return round($s / 1000);
    }

    public function newsAnchor()
    {
        return News\Item::anchorFromTime($this->lastModified());
    }

    public function githubSourceUrl()
    {
        if ($this->isExternal()) {
            return '';
        }

        $repo_baseuri = 'https://github.com/igneus/In-adiutorium/blob/master/';
        if ($this->source) {
            $source_fname = $this->source;
            if (strpos($source_fname, "http") === 0) {
                return $source_fname;
            }
        } else {
            $source_fname = substr($this->filename, 0, -4).".ly";
        }

        if (strpos($source_fname, 'commune_') === 0) {
            $source_fname = 'commune/'.$source_fname;
        } elseif (strpos($source_fname, 'hymny') !== false) {
            $source_fname = 'hymny/'.$source_fname;
        } elseif (preg_match('/^\d{4}\w+/', $source_fname) === 1) {
            $source_fname = 'sanktoral/'.$source_fname;
        }

        return $repo_baseuri . $source_fname;
    }
}
