<?php
namespace InAdiutorium\Downloads;

use InAdiutorium\Downloads\Section;
use InAdiutorium\Downloads\Item;
use InAdiutorium\Blog;
use InAdiutorium\framework\NotFoundException;

class Repository
{
    private $xml;
    private $files_dir;
    private $blog_repo;
    private $download_count_db;
    private $related_posts_file;

    public function __construct($xml_path, $files_dir, $blog_repo, $download_count_db, $related_posts_file)
    {
        $this->xml = simplexml_load_file($xml_path);
        $this->files_dir = $files_dir;
        $this->blog_repo = $blog_repo;
        $this->download_count_db = $download_count_db;
        $this->related_posts_file = $related_posts_file;
    }

    /* returns iterable sequence of Sections and Items */
    public function contents()
    {
        foreach ($this->xml->children() as $node) {
            if ($node->getName() != 'sekce') {
                continue;
            }

            yield new Section($node);
        }
    }

    public function findItem($id)
    {
        if (!$this->validItemId($id)) {
            throw new NotFoundException("Invalid item id '$id'.");
        }

        $result = $this->xml->xpath("//vec[symbol='$id']");
        if (count($result) == 0) {
            throw new NotFoundException("Item with id '$id' not found.");
        }
        $node = $result[0];

        $posts = false;
        $related_posts = $this->relatedPosts();
        if (array_key_exists($id, $related_posts)) {
            $posts = array();
            foreach ($related_posts[$id] as $filename) {
                $posts[] = $this->blog_repo->findByFilename($filename, false);
            }
            usort($posts, function ($a, $b) {
                return spaceship_desc($a->time, $b->time);
            });
        }

        return new Item($node, $posts);
    }

    public function seasonal($season)
    {
        $season_xpath = array(
            'advent' => "//sekce[@id='advent']",
            'christmas' => "//sekce[@id='vanoce']",
            'lent' => "//sekce[@id='pust']",
            'easter' => "//sekce[@id='velikonoce']",
            'ordinary' => "//sekce[@id='ordo' or @id='mezidobi']"
        );
        if (array_key_exists($season, $season_xpath)) {
            return $this->byXpath($season_xpath[$season] . "/vec");
        }

        throw new \InvalidArgumentException("Unknown season '$season'.");
    }

    public function updatedSince($time)
    {
        $results = array();
        $md = dir($this->files_dir);
        while (false !== ($fname = $md->read())) {
            $path = $this->files_dir . '/' . $fname;
            if (is_file($path) && filemtime($path) >= $time) {
                $item = $this->byXPath("//vec[soubor='$fname']")[0];
                if (null === $item) {
                    continue;
                }
                array_push($results, $item);
            }
        }
        $md->close();
        return $results;
    }

    public function byXPath($xpath)
    {
        $results = array();
        $nodes = $this->xml->xpath($xpath);
        foreach ($nodes as $n) {
            array_push($results, new Item($n));
        }
        return $results;
    }

    public function validItemId($id)
    {
        return preg_match('/^\w+$/', $id) === 1;
    }

    public function downloadCount($item)
    {
        $db = dba_open($this->download_count_db, "r");
        $count_db_filename = "materialy/" . $item->filename;
        @$count = dba_fetch($count_db_filename, $db);
        if ($count === false) {
            $count = 0;
        }
        dba_close($db);
        return $count;
    }

    public function relatedPosts()
    {
        return unserialize(implode("\n", file($this->related_posts_file)));
    }
}
