<?php
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

// TODO: replace with the spaceship operator once on PHP 7
function spaceship($a, $b)
{
    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}

function spaceship_desc($a, $b)
{
    return spaceship($b, $a);
}

function register_controller($container, $name)
{
    $service_name = $name . '.controller';
    $class_name = "InAdiutorium\\controllers\\" . ucfirst($name) . 'Controller';

    return $container
        ->register($service_name, $class_name)
        ->addArgument(new Reference('templating'))
        ->addArgument('%template_vars%');
}

$container = new ContainerBuilder();

/* values */

$container->setParameter('books_file', PUBLIC_DIR . '/knihy.xml');
$container->setParameter('books_template', PUBLIC_DIR . '/xsl/knihy.xsl');
$container->setParameter('links_file', PUBLIC_DIR . '/odkazy.xml');
$container->setParameter('links_template', PUBLIC_DIR . '/xsl/odkazy.xsl');
$container->setParameter('places_file', PUBLIC_DIR . '/mistalh.xml');
$container->setParameter('places_template', PUBLIC_DIR . '/xsl/mistalh.xsl');
$container->setParameter('events_file', ROOT_DIR . '/akce.xml');
$container->setParameter('cache_dir', ROOT_DIR . '/tmp/cache');
$container->setParameter('views_dir', ROOT_DIR . '/views');
$container->setParameter('blog_index', BLOG_DIR . '/vygenerovane/archivprehled.xml');
$container->setParameter('template_vars', array(
    'navigation' => array(
        array("root", "Domů"),
        array("downloads", "Noty ke stažení"),
        array("blog", "Blog"),
        array("books", "Knihy"),
        array("links", "Odkazy"),
        array("locations", "Místa"),
        array("about", "O webu")
    ),
    'toplevel_class' => '',
    'page_xml_source' => '',
    'site_title' => "In adiutorium",
    'site_title_full' => "In adiutorium - hudba k české liturgii hodin",
    'site_url' => "http://www.inadiutorium.cz",
    'owner_name' => 'Jakub Pavlík',
    'owner_firstname' => 'Jakub',
    'owner_surname' => 'Pavlík',
    'owner_email' => 'jkb.pavlik@gmail.com',
    'github_base_uri' => 'https://github.com/igneus/In-adiutorium',
));

/* services */

$container->register('templating', 'InAdiutorium\framework\LatteTemplating')
          ->addArgument('%cache_dir%')
          ->addArgument('%views_dir%')
          ->addArgument(new Reference('sluggifier'));

$container->register('sluggifier', 'Cocur\Slugify\Slugify');

$container->register('crawler_detector', 'Jaybizzle\CrawlerDetect\CrawlerDetect');

$container->register('gravatar', 'forxer\Gravatar\Gravatar');

$container->register('blog.updater', 'InAdiutorium\Blog\Updater')
          ->addArgument(BLOG_DIR)
          ->addArgument('%blog_index%')
          ->addArgument(new Reference('sluggifier'));

$container->register('blog.repository', 'InAdiutorium\Blog\Repository')
          ->addArgument('%blog_index%')
          ->addArgument(BLOG_DIR . '/kategorie.yaml')
          ->addArgument(new Reference('sluggifier'));

$container->register('downloads.repository', 'InAdiutorium\Downloads\Repository')
          ->addArgument(PUBLIC_DIR . '/knihovna.xml')
          ->addArgument(PUBLIC_DIR . '/materialy')
          ->addArgument(new Reference('blog.repository'))
          ->addArgument(ROOT_DIR . '/statistika/statistikastahovani.gdbm')
          ->addArgument(ROOT_DIR . '/blog/vygenerovane/clankyknotam.txt');

$container->register('downloads_attic.repository', 'InAdiutorium\Downloads\Repository')
          ->addArgument(PUBLIC_DIR . '/harampadi.xml')
          ->addArgument(PUBLIC_DIR . '/materialy')
          ->addArgument(new Reference('blog.repository'))
          ->addArgument(ROOT_DIR . '/statistika/statistikastahovani.gdbm')
          ->addArgument(ROOT_DIR . '/blog/vygenerovane/clankyknotam.txt');

$container->register('news.reader', 'InAdiutorium\News\Reader')
          ->addArgument(ROOT_DIR . '/novinky.txt');

$container->register('calendar', 'InAdiutorium\ChurchCalendar\Client')
          ->addArgument('%cache_dir%');

$container->register('books.content', 'InAdiutorium\utils\CachedXsltContent')
          ->addArgument('%books_file%')
          ->addArgument('%books_template%')
          ->addArgument('%cache_dir%');

$container->register('links.content', 'InAdiutorium\utils\CachedXsltContent')
          ->addArgument('%links_file%')
          ->addArgument('%links_template%')
          ->addArgument('%cache_dir%');

$container->register('places.content', 'InAdiutorium\utils\CachedXsltContent')
          ->addArgument('%places_file%')
          ->addArgument('%places_template%')
          ->addArgument('%cache_dir%');

$container->register('downloads_counter', 'InAdiutorium\Counter\Counter');

/* controllers */

register_controller($container, 'error');

register_controller($container, 'home')
    ->addMethodCall('setDownloadsRepo', array(new Reference('downloads.repository')))
    ->addMethodCall('setBlogRepo', array(new Reference('blog.repository')))
    ->addMethodCall('setNewsReader', array(new Reference('news.reader')))
    ->addMethodCall('setBooksFile', array('%books_file%'))
    ->addMethodCall('setLinksFile', array('%links_file%'))
    ->addMethodCall('setEventsFile', array('%events_file%'))
    ->addMethodCall('setLiturgicalCalendar', array(new Reference('calendar')));

register_controller($container, 'blog')
    ->addMethodCall('setRepo', array(new Reference('blog.repository')));

register_controller($container, 'downloads')
    ->addMethodCall('setRepo', array(new Reference('downloads.repository')))
    ->addMethodCall('setNewsReader', array(new Reference('news.reader')))
    ->addMethodCall('setCrawlerDetector', array(new Reference('crawler_detector')));

register_controller($container, 'attic')
    ->addMethodCall('setRepo', array(new Reference('downloads_attic.repository')))
    ->addMethodCall('setNewsReader', array(new Reference('news.reader')))
    ->addMethodCall('setCrawlerDetector', array(new Reference('crawler_detector')));

register_controller($container, 'news')
    ->addMethodCall('setNewsReader', array(new Reference('news.reader')))
    ->addMethodCall('setRepo', array(new Reference('downloads.repository')));

register_controller($container, 'antiphonal');
register_controller($container, 'eantiphonal');

register_controller($container, 'about')
    ->addMethodCall('setGravatarBuilder', array(new Reference('gravatar')));

register_controller($container, 'books')
    ->addMethodCall('setContent', array(new Reference('books.content')))
    ->addMethodCall('setSourcePath', array('%books_file%'));

register_controller($container, 'links')
    ->addMethodCall('setContent', array(new Reference('links.content')))
    ->addMethodCall('setSourcePath', array('%links_file%'));

register_controller($container, 'places')
    ->addMethodCall('setContent', array(new Reference('places.content')))
    ->addMethodCall('setSourcePath', array('%places_file%'));

register_controller($container, 'counter')
    ->addMethodCall('setCrawlerDetector', array(new Reference('crawler_detector')));
