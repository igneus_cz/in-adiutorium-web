<?php
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

use Symfony\Component\HttpFoundation\Request;

// build route served by an old-school standalone php script
function script_route($path, $script_path, $formats='html')
{
    return new Route(
        $path.".{_format}",
        array('_format' => 'html', 'script' => $script_path), // defaults
        array('_format' => $formats) // constraints
    );
}

// build route served by a controller
function controller_route($path, $controller, $formats='html')
{
    return new Route(
        $path.".{_format}",
        array('_format' => 'html', '_controller' => $controller), // defaults
        array('_format' => $formats) // constraints
    );
}

function file_route($path, $file)
{
    return new Route(
        $path,
        array('file' => $file) // defaults
    );
}

// build redirected legacy route
function legacy_route($path, $target_name, $parameters=array())
{
    return new Route($path, array('redirect' => $target_name, 'parameters' => $parameters));
}

function on_localhost()
{
    return $_SERVER['REMOTE_ADDR'] == '127.0.0.1';
}

// hostnames
$toplevel_domain = on_localhost() ? 'l' : 'cz';
$main_host = "www.inadiutorium.$toplevel_domain";
$ean_host =  "ean.inadiutorium.$toplevel_domain";
$antiphonal_host = "www.antifonar.$toplevel_domain";

$routes = new RouteCollection();

$routes->add('root', controller_route('/', 'home#index'));
$routes->add('downloads', controller_route('/noty', 'downloads#index'));
$routes->add('download_comment', controller_route('/noty/komentar', 'downloads#commentary'));
$routes->add('downloads_attic', controller_route('/noty/puda', 'attic#index'));
$routes->add('downloads_attic_show', controller_route('/noty/puda/{id}', 'attic#show'));
$routes->add('traffic', controller_route('/noty/stahovanost', 'counter'));
$routes->add('counter', controller_route('/noty/stahovanost/redirect', 'counter#count'));
$routes->add('counter_current', controller_route('/noty/stahovanost/nyni', 'counter#current'));
$routes->add('news', controller_route('/noty/novinky', 'news', 'html|rss'));
$routes->add('news_detail', controller_route('/noty/novinky/{date}', 'news#show'));
$routes->add('downloads_show', controller_route('/noty/{id}', 'downloads#show'));
$routes->add('blog', controller_route('/blog', 'blog#index', 'html|rss'));
$routes->add('blog_selected', controller_route('/blog/vyber', 'blog#selected'));
$routes->add('blog_abbreviations', controller_route('/blog/zkratky', 'blog#abbreviations'));
$routes->add('blog_archive', controller_route('/blog/archiv', 'blog#archive'));
$routes->add('blog_categories', controller_route('/blog/kategorie', 'blog#categories'));
$routes->add('blog_category', controller_route('/blog/kategorie/{slug}', 'blog#category', 'html|rss'));
$routes->add('blog_tags', controller_route('/blog/stitky', 'blog#tags'));
$routes->add('blog_tag', controller_route('/blog/stitky/{slug}', 'blog#tag', 'html|rss'));
$routes->add('blog_post', controller_route('/blog/{slug}', 'blog#show'));
$routes->add('blog_old', controller_route('/blog.php', 'blog#legacy'));
$routes->add('blog_biblio', controller_route('/blog/{slug}/biblio', 'blog#biblio', 'xml|bib'));
$routes->add('books', controller_route('/knihy', 'books#index'));
$routes->add('links', controller_route('/odkazy', 'links#index'));
$routes->add('locations', controller_route('/mista', 'places#index'));
$routes->add('about', controller_route('/owebu', 'about#about'));
$routes->add('english', controller_route('/english', 'about#english'));
$routes->add('ancient', controller_route('/stare', 'ancient_sources'));

// redirected legacy routes
$routes->add('index_old', legacy_route('/index.php', 'root'));
$routes->add('downloads_old', legacy_route('/noty.php', 'downloads'));
$routes->add('download_comment_old', legacy_route('/komentarknotam.php', 'download_comment'));
$routes->add('news_old', legacy_route('/novinky.php', 'news'));
$routes->add('news_old_rss', legacy_route('/rss_novinky.php', 'news', array('_format' => 'rss')));

$routes->add('blog_selected_old', legacy_route('/blogvyber.php', 'blog_selected'));
$routes->add('blog_archive_old', legacy_route('/blogarchiv.php', 'blog_archive'));
$routes->add('blog_rss_old', legacy_route('blog/vygenerovane/blog.rss', '/blog.rss'));
$routes->add('books_old', legacy_route('/knihy.php', 'books'));
$routes->add('links_old', legacy_route('/odkazy.php', 'links'));
$routes->add('locations_old', legacy_route('/mistalh.php', 'locations'));
$routes->add('about_old', legacy_route('/about.php', 'about'));
$routes->add('english_old', legacy_route('/english.php', 'english'));
$routes->add('manuscripts_old', legacy_route('/rukopisy/', 'manuscripts'));
$routes->add('traffic_old', legacy_route('/stahovaniprohlizeni.php', 'traffic'));
$routes->add('counter_old', legacy_route('/pocitacstazeni.php', 'counter'));

// a microsite having it's own hostname
$antiphonal_routes = new RouteCollection();
$antiphonal_routes->add('antiphonal_home', controller_route('/', 'antiphonal#index'));
$antiphonal_routes->setHost($antiphonal_host);

$ean_routes = new RouteCollection();
$ean_routes->add('ean_home', controller_route('/', 'eantiphonal#index'));
$ean_routes->setHost($ean_host);

$routes_top = new RouteCollection();

$routes->setHost($main_host);
$routes_top->addCollection($routes);
$routes_top->addCollection($ean_routes);
$routes_top->addCollection($antiphonal_routes);

$context = new RequestContext();
$request = Request::createFromGlobals();
$context->fromRequest($request);

$matcher = new UrlMatcher($routes_top, $context);
$generator = new UrlGenerator($routes_top, $context);

// routing helper to be used throughout the web
function url_for($route_name, $parameters=array())
{
    global $generator;
    return $generator->generate($route_name, $parameters);
}
