<?php
namespace InAdiutorium\News;

class Item
{
    public $time;
    // time as seconds since epoch
    public $date;
    // date as found in the data file
        public $content; // text content

    public function __construct($date, $content)
    {
        $this->date = $date; // date as string
        $this->time = $this->parseCzechDate($this->date);
        $this->content = $content;
    }

    public static function anchorFromTime($time)
    {
        return "n".date("d-m-Y", $time);
    }

    public function anchor()
    {
        return self::anchorFromTime($this->time);
    }

    public function permalink()
    {
        return url_for('news_detail', array('date' => date('Ymd', $this->time)));
    }

    public function github_path()
    {
        return "/commits/master@{".date('Y-m-d', strtotime('+1day', $this->time))."}";
    }

    public function mentionedDownloadIds()
    {
        $matches = array();
        preg_match_all('/<a href="#(\w+)"/', $this->content, $matches, PREG_PATTERN_ORDER);
        return $matches[1];
    }

    private function parseCzechDate($str)
    {
        list($day, $month, $year) = explode(".", $str);
        return mktime(0, 0, 0, intval($month), intval($day), intval($year));
    }
}
