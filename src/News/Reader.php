<?php
namespace InAdiutorium\News;

/* reads news from a plaintext file */
class Reader implements \Iterator
{
    private $file;
    private $current;

    public function __construct($path)
    {
        $this->file = fopen($path, 'r');
        $this->rewind();
    }

    public function __destruct()
    {
        fclose($this->file);
    }

    public function rewind()
    {
        rewind($this->file);
        $this->current = $this->loadNext();
    }

    public function current()
    {
        return $this->current;
    }

    public function key()
    {
        return $this->current->date;
    }

    public function valid()
    {
        return $this->current != false;
    }

    public function next()
    {
        $this->current = $this->loadNext();
    }

    public function findByTime($time)
    {
        foreach ($this as $n) {
            if ($n->time == $time) {
                return $n;
            }
        }

        return false;
    }

    /* loads and returns next news item, or false if available
       data have been exhausted */
    private function loadNext()
    {
        $dateline = rtrim(fgets($this->file));
        if (!$dateline) {
            return false;
        }

        $content = "";
        while ($l = rtrim(fgets($this->file))) {
            if (!$l) {
                break;
            }

            if ($l === "---") {
                break;
            }

            $content .= " ".$l;
        }

        return new Item($dateline, $content);
    }
}
