<?php
namespace InAdiutorium\controllers;

use InAdiutorium\utils\HttpError;

class HomeController extends Controller
{
    private $downloads_repo;
    private $blog_repo;
    private $news_reader;
    private $books_file;
    private $links_file;
    private $events_file;
    private $liturgical_calendar;

    public function setDownloadsRepo($r)
    {
        $this->downloads_repo = $r;
    }
    public function setBlogRepo($r)
    {
        $this->blog_repo = $r;
    }
    public function setNewsReader($r)
    {
        $this->news_reader = $r;
    }
    public function setBooksFile($f)
    {
        $this->books_file = $f;
    }
    public function setLinksFile($f)
    {
        $this->links_file = $f;
    }
    public function setEventsFile($f)
    {
        $this->events_file = $f;
    }
    public function setLiturgicalCalendar($c)
    {
        $this->liturgical_calendar = $c;
    }

    public function index()
    {
        $news_rss = url_for('news', array('_format' => 'rss'));
        $blog_rss = url_for('blog', array('_format' => 'rss'));

        $season = $this->buildSeason();
        $last_update = $this->lastUpdate();

        $link = $this->link();
        $book = $this->book();

        $this->render('home/index.latte', array(
            'page_rss' => array('Novinky' => $news_rss, 'Nejnovější články na blogu' => $blog_rss),
            'news_rss_url' => $news_rss,
            'blog_rss_url' => $blog_rss,

            'season_id' => $season['id'],
            'season_name' => $season['name'],
            'season_downloads' => $season['downloads'],

            'last_update' => $last_update,
            'last_update_downloads' => $this->lastUpdateDownloads($last_update),
            'days_since_last_update' => $this->daysDifference(time(), $last_update->time),

            'blog_posts' => $this->blogPosts(),
            'link' => $link,
            'link_text' => $this->xmlUnwrap($link->popis->asXML()),
            'book' => $book,
            'book_text' => $this->xmlUnwrap($book->text->asXML()),
            'events' => $this->events(),
        ));
    }

    // prepare data for "downloads for current liturgical season"
    private function buildSeason($max_items=2)
    {
        $season_ids = array(
            'advent' => "advent",
            'christmas' => "vanoce",
            'lent' => "pust",
            'easter' => "velikonoce",
            'ordinary' => "ordo"
        );
        $season_names = array(
            'advent' => "advent",
            'christmas' => "dobu vánoční",
            'lent' => "dobu postní",
            'easter' => "dobu velikonoční",
            'ordinary' => "liturgické mezidobí"
        );

        try {
            $today = $this->liturgical_calendar->today();
        } catch (HttpError $e) {
            return array('id' => false, 'name' => 'aktuální liturgickou dobu', 'downloads' => array());
        }

        $items = $this->downloads_repo->seasonal($today->season);
        if (count($items) <= $max_items) {
            $show_items = $items;
        } else {
            // randomly select two subsequent oness
            $i = rand(0, count($items) - 2);
            $show_items = array_slice($items, $i, 2);
        }

        return array(
            'id' => 'sekce' . $season_ids[$today->season],
            'name' => $season_names[$today->season],
            'downloads' => $show_items,
        );
    }

    private function lastUpdate()
    {
        return $this->news_reader->current();
    }

    private function lastUpdateDownloads($last_update)
    {
        return $this->downloads_repo->updatedSince($last_update->time);
    }

    private function blogPosts($max_posts=4)
    {
        return $this->blog_repo->latest($max_posts)[0];
    }

    private function link()
    {
        $docu = simplexml_load_file($this->links_file);
        $links = $docu->xpath("//stranka");
        $i = rand(0, count($links) -1);
        return $links[$i];
    }

    private function book()
    {
        $docu = simplexml_load_file($this->books_file);
        $books = $docu->xpath("/stranka/obsah/sekce[1]/kniha");
        $i = rand(0, count($books) -1);
        return $books[$i];
    }

    private function events()
    {
        if (file_exists($this->events_file)) {
            $xml = simplexml_load_file($this->events_file);
            return $xml->akce;
        }

        return false;
    }

    private function daysDifference($time1, $time2)
    {
        $day_seconds = 3600 * 24;
        $diff_seconds = abs($time1 - $time2);
        return intval(doubleval($diff_seconds) / $day_seconds);
    }

    private function xmlUnwrap($xml_str)
    {
        // remove first and last line
        // (we magicly know that these contain the opening
        // and closing tag of the wrapping XML element)
        return implode(array_slice(explode("\n", $xml_str), 1, -1), "\n");
    }
}
