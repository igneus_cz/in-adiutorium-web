<?php
namespace InAdiutorium\controllers;

class AboutController extends Controller
{
    private $gravatar_builder;

    public function setGravatarBuilder($b)
    {
        $this->gravatar_builder = $b;
    }

    public function about()
    {
        $this->render(
            'about/about.latte',
            array('gravatar' => $this->gravatar())
        );
    }

    public function english()
    {
        $this->render(
            'about/english.latte',
            array(
                'gravatar' => $this->gravatar(),
                'page_language' => 'en',
            )
        );
    }

    private function gravatar()
    {
        $email = $this->common_template_vars['owner_email'];
        $builder = $this->gravatar_builder;
        return $builder::image($email);
    }
}
