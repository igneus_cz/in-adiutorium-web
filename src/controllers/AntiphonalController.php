<?php
namespace InAdiutorium\controllers;

class AntiphonalController extends Controller
{
    public function index()
    {
        $this->render(
            'antiphonal/index.latte',
            array('navigation' => array())
        );
    }
}
