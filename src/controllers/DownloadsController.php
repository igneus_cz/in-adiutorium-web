<?php
namespace InAdiutorium\controllers;

use PhpKit\Flow\Iterators\LoopIterator;
use InAdiutorium\Downloads\Repository;

class DownloadsController extends Controller
{
    protected $repo;
    protected $news_reader;
    protected $crawler_detector;

    public function setRepo($repo)
    {
        $this->repo = $repo;
    }
    public function setNewsReader($reader)
    {
        $this->news_reader = $reader;
    }
    public function setCrawlerDetector($detector)
    {
        $this->crawler_detector = $detector;
    }

    public function index()
    {
        $rss_url = url_for('news', array('_format' => 'rss'));

        $news = $this->news();
        $last_news_item = end($news);

        $this->render(
            'downloads/index.latte',
            array(
                'rss_url' => $rss_url,
                'page_rss' => array($rss_url),

                'news' => $news,
                'last_onpage_news_time' => $last_news_item->time,

                'downloads' => $this->repo->contents(),

                'detail_link' => function ($item_id) {
                    return url_for('downloads_show', array('id' => $item_id));
                },

                'status_letters' => $this->statusLetters(),
                'status_descriptions' => $this->statusDescriptions(),

                'is_crawler' => $this->isCrawler(),
            )
        );
    }

    public function show()
    {
        // TODO:
        // html - full page
        // html - ajax request, only partial rendered
        // json, xml - for hackety hacks
        $item = $this->repo->findItem($this->params['id']);
        $parents = array();

        $child = $item->xml_node;
        while (true) {
            $pars = $child->xpath('..');
            if (empty($pars)) {
                break;
            }
            if (empty($pars[0]->nazev)) {
                break;
            }
            $child = $pars[0];
            array_unshift($parents, $child);
        }

        $count = $this->repo->downloadCount($item);

        $template = $this->isAjaxRequest() ? 'downloads/item_content.latte' : 'downloads/show.latte';

        $rss_url = url_for('news', array(
            '_format' => 'rss',
            'material' => $item->id
        ));

        $this->render(
            $template,
            array(
                'item' => $item,
                'parents' => $parents,
                'download_count' => $count,
                'is_crawler' => $this->isCrawler(),
                'rss_url' => $rss_url,
                'page_rss' => array($rss_url),
            )
        );
    }

    public function commentary()
    {
        $this->render(
            'downloads/commentary.latte',
            array(
                'status_letters' => $this->statusLetters(),
                'status_descriptions' => $this->statusDescriptions(),
            )
        );
    }

    protected function news()
    {
        $limited = new LoopIterator($this->news_reader);
        $limited->limit(3);
        return iterator_to_array($limited);
    }

    protected function statusLetters()
    {
        return array(
            "alfa" => "&#x03B1;",
            "beta" => "&#x03B2;",
            "gamma" => "&#x03B3;",
            "delta" => "&#x03B4;",
        );
    }

    protected function statusDescriptions()
    {
        return array(
            "alfa" => 'dle autorova soudu způsobilé k použití při společné modlitbě či při liturgii',
            "beta" => 'závažnější chyby jsou již odstraněny',
            "gamma" => 'obsahuje závažnější chyby',
            "delta" => 'materiál může být nekompletní a/nebo obsahovat velmi závažné chyby',
            "" => ''
        );
    }

    protected function isCrawler()
    {
        // TODO: rewrite using request context
        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        return $this->crawler_detector->isCrawler($user_agent);
    }

    protected function isAjaxRequest()
    {
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
}
