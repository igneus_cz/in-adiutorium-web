<?php
namespace InAdiutorium\controllers;

use InAdiutorium\framework\NotFoundException;

abstract class Controller
{
    protected $params;
    protected $templating;
    protected $common_template_vars;
    protected $default_content_type = 'application/xhtml+xml; charset=utf-8';

    public function __construct($templating, $template_vars)
    {
        $this->templating = $templating;
        $this->common_template_vars = $template_vars;

        $this->init();
    }

    protected function init()
    {
    }

    public function run($action, $params)
    {
        $this->params = $params;
        try {
            $this->$action();
        } catch (NotFoundException $e) {
            $this->notFound();
        }
    }

    public function render($template_name, $locals=array(), $content_type=false)
    {
        if ($content_type === false) {
            $content_type = $this->default_content_type;
        }
        header('Content-Type: ' . $content_type);
        $locals['content_type'] = $content_type;
        $templating = $this->templating;
        if (strpos($content_type, 'xhtml') !== false) {
            $templating->setContentType($templating::CONTENT_XHTML);
        } elseif (strpos($content_type, 'xml') !== false) {
            $templating->setContentType($templating::CONTENT_XML);
        }
        echo $templating->render($template_name, array_merge($this->common_template_vars, $locals));
    }

    public function notFound()
    {
        http_response_code(404);
        $this->render('errors/404.latte');
        exit;
    }

    public function redirect($new_url, $status=302)
    {
        header("Location: " . $new_url);
        http_response_code($status);
        exit;
    }
}
