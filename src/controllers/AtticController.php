<?php
namespace InAdiutorium\controllers;

class AtticController extends DownloadsController
{
    public function index()
    {
        $this->render(
            'attic/index.latte',
            array(
                'downloads' => $this->repo->contents(),
                'detail_link' => function ($item_id) {
                    return url_for('downloads_attic_show', array('id' => $item_id));
                },
                'last_onpage_news_time' => strtotime('tomorrow'), // no news on page

                'status_letters' => $this->statusLetters(),
                'status_descriptions' => $this->statusDescriptions(),

                'is_crawler' => $this->isCrawler(),
            )
        );
    }
}
