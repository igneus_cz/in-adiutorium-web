<?php
namespace InAdiutorium\controllers;

use PhpKit\Flow\Iterators\LoopIterator;
use PhpKit\Flow\Iterators\ConditionalIterator;
use InAdiutorium\utils\FilterIterator;
use InAdiutorium\framework\NotFoundException;

class NewsController extends Controller
{
    private $news_reader;
    private $repo;

    public function setNewsReader($r)
    {
        $this->news_reader = $r;
    }
    public function setRepo($repo)
    {
        $this->repo = $repo;
    }

    protected function init()
    {
        $referenced_page_url = url_for('downloads');

        $this->templating->addFilter('replace_local_links', function ($html) use ($referenced_page_url) {
            return str_replace("href=\"#", "href=\"$referenced_page_url#", $html);
        });
    }

    public function index()
    {
        $print_months = true;
        $news = $this->news_reader;

        $materialsym = array_key_exists('material', $_GET) ? $_GET['material'] : false;
        $title = false;
        if ($materialsym) {
            $materialasym = '#' . $materialsym;
            $news = new FilterIterator($news, function ($n) use ($materialasym) {
                return strpos($n->content, $materialasym) !== false;
            });
            $print_months = false;

            try {
                $dwn_item = $this->repo->findItem($materialsym);
                $title = $dwn_item->title;
            } catch (NotFoundException $e) {
                $this->notFound();
            }
        }

        if ($this->params['_format'] == 'rss') {
            header("Content-Type: ");
            $news = new LoopIterator($news);
            $news->limit(10);
            $this->render(
                'news/rss.latte',
                array(
                    'news' => $news,
                    'publication_time' => filemtime("../novinky.txt"),
                    'title' => $title,
                ),
                'application/rss+xml; charset=UTF-8'
            );
        } else {
            $this->render(
                'news/index.latte',
                array(
                    'news' => $news,
                    'print_months' => $print_months,
                    'title' => $title,
                    'page_rss' => array(
                        url_for('news', array('_format' => 'rss'))
                    ),
                )
            );
        }
    }

    public function show()
    {
        $date = $this->params['date'];

        $year = intval(substr($date, 0, 4));
        $month = intval(substr($date, 4, 2));
        $day = intval(substr($date, 6, 2));
        $time = mktime(0, 0, 0, $month, $day, $year);

        $n = $this->news_reader->findByTime($time);
        if ($n === false) {
            $this->notFound();
        }

        $this->render(
            'news/show.latte',
            array(
                'item' => $n,
                'downloads' => $this->downloadsForNewsItem($n),
            )
        );
    }

    private function downloadsForNewsItem($item)
    {
        $downloads = array();
        foreach ($item->mentionedDownloadIds() as $did) {
            try {
                $dwn = $this->repo->findItem($did);
                array_push($downloads, $dwn);
            } catch (NotFoundException $e) {
                // ok, download not found
            }
        }
        return $downloads;
    }
}
