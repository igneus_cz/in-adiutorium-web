<?php
namespace InAdiutorium\controllers;

class EantiphonalController extends Controller
{
    public function index()
    {
        $this->render(
            'ean/index.latte',
            array('navigation' => array())
        );
    }
}
