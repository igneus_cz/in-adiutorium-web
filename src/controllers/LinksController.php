<?php
namespace InAdiutorium\controllers;

class LinksController extends Controller
{
    private $content_resource;
    private $source_path;

    public function setContent($c)
    {
        $this->content_resource = $c;
    }
    public function setSourcePath($p)
    {
        $this->source_path = $p;
    }

    public function index()
    {
        $this->render(
            'links/index.latte',
            array(
                'content' => $this->content_resource->retrieve(),
                'page_xml_source' => basename($this->source_path),
            )
        );
    }
}
