<?php
namespace InAdiutorium\controllers;

use InAdiutorium\Blog\Repository;
use InAdiutorium\Blog\BreviaryParts;

class BlogController extends Controller
{
    private $rss;
    private $repo;
    private $page_size = 10;

    protected function init()
    {
        $this->rss = array('Nejnovější články' => url_for('blog', array('_format' => 'rss')));
    }

    public function setRepo($repo)
    {
        $this->repo = $repo;
    }

    public function legacy()
    {
        if (array_key_exists('clanek', $_GET)) {
            $this->redirectToPost($_GET['clanek']);
        } elseif (array_key_exists('kategorie', $_GET)) {
            $this->redirectToCategory($_GET['kategorie']);
        } else {
            $this->redirect(url_for('blog'));
        }
    }

    // chronological listing of all posts, new first, paginated
    public function index()
    {
        if (array_key_exists('clanek', $_GET) || array_key_exists('kategorie', $_GET)) {
            $this->legacy();
            return;
        }

        $page = $this->getPage();
        list($posts, $pages_count) = $this->repo->latest($this->page_size, $page, true);

        if ($this->params['_format'] == 'rss') {
            $this->render(
                'blog/rss.latte',
                array(
                    'posts' => $posts,
                    'web_url' => url_for('blog'),
                    'rss_url' => url_for('blog', array('_format' => 'rss')),
                    'publication_time' => $posts[0]->time
                ),
                'application/rss+xml; charset=UTF-8'
            );
            return;
        }

        $this->render(
            'blog/index.latte',
            array(
                'posts' => $posts,
                'categories' => $this->repo->allCategories(),
                'tag_cloud' => $this->repo->tagCloud(),
                'page_rss' => $this->rss,
                'current_page' => $page + 1, // the templates number from 1
                'pages_total' => $pages_count,
                'category' => false,
            )
        );
    }

    // chronological listing of a category
    public function category()
    {
        $category = $this->repo->findCategoryBySlug($this->params['slug']);
        if ($category === false) {
            $this->notFound();
        }

        $this->rss['Články v kategorii ' . $category->title] = $category->permalink(array('_format' => 'rss'));

        if ($this->params['_format'] == 'rss') {
            $posts = $this->repo->fromCategory($category)[0];
            $this->render(
                'blog/rss.latte',
                array(
                    'posts' => $posts,
                    'web_url' => $category->permalink(),
                    'rss_url' => $category->permalink(array('_format' => 'rss')),
                    'publication_time' => $posts[0]->time,
                    'title_appendix' => ' - kategorie ' . $category->title
                ),
                'application/rss+xml; charset=UTF-8'
            );

            return;
        }

        $page = $this->getPage();
        list($posts, $pages_count) = $this->repo->fromCategory($category, $this->page_size, $page);

        $this->render(
            'blog/category.latte',
            array(
                'posts' => $posts,
                'categories' => $this->repo->allCategories(),
                'page_rss' => $this->rss,
                'category' => $category,
                'current_page' => $page + 1, // the templates number from 1
                'pages_total' => $pages_count,
            )
        );
    }

    // categories list
    public function categories()
    {
        $this->render(
            'blog/categories.latte',
            array(
                    'categories' => $this->repo->allCategories(),
                    'page_rss' => $this->rss,
                )
        );
    }

    // tags list
    public function tags()
    {
        $tags = $this->repo->allTags();
        usort($tags, function ($a, $b) {
            return strcasecmp($a->title, $b->title);
        });

        $this->render(
            'blog/tags.latte',
            array(
                'categories' => $this->repo->allCategories(),
                'tags' => $tags
            )
        );
    }

    public function tag()
    {
        $tag = $this->repo->findTagBySlug($this->params['slug']);
        if ($tag === null) {
            $this->notFound();
        }

        $posts = $this->repo->havingTag($tag);

        if ($this->params['_format'] == 'rss') {
            $this->render(
                'blog/rss.latte',
                array(
                    'posts' => $posts,
                    'web_url' => $tag->permalink(),
                    'rss_url' => $tag->permalink(array('_format' => 'rss')),
                    'publication_time' => $posts[0]->time,
                    'title_appendix' => ' - štítek ' . $tag->title
                ),
                'application/rss+xml; charset=UTF-8'
            );

            return;
        }

        $this->rss['Články se štítkem ' . $tag->title] = $tag->permalink(array('_format' => 'rss'));
        $this->render(
            'blog/tag.latte',
            array(
                'tag' => $tag,
                'posts' => $posts,
                'page_rss' => $this->rss
            )
        );
    }

    // chronological listing of all posts, but without content,
    // only titles
    public function archive()
    {
        $parts = new BreviaryParts();

        $posts = $this->repo->latest()[0];

        $years = array();
        $posts_decorated = array();

        $last_year = 0;
        $last_part = '';
        foreach ($posts as $p) {
            $decoration = array('post' => $p);
            $year = date('Y', $p->time);
            $part = $parts->breviaryPart($p->time);

            if ($year != $last_year) {
                array_push($years, $year);
                $decoration['year'] = $year;
                $last_year = $year;
                $last_part = '';
            }
            if ($part != $last_part) {
                $decoration['part'] = $part;
                $last_part = $part;
            }

            array_push($posts_decorated, $decoration);
        }

        $this->render(
            'blog/archive.latte',
            array(
                'posts' => $posts_decorated,
                'years' => $years,
                'page_rss' => $this->rss,
            )
        );
    }

    // selection of older posts considered worth reading
    public function selected()
    {
        $post_groups_a = array(
            'Návody - zpívané oficium' => array(
                'zacitzpivatoficium.xml',
                'oratorianskyzpev.xml',
                'zpivanamodely.xml',
            ),
            'Pro liturgickou praxi' => array(
                'krestninespory4.xml',
                'krestninespory5.xml',
                'krestninesporyolomouc.xml',
                'introit.xml',
            ),
            'O projektu In adiutorium' => array(
                'vratkezaklady.xml',
                'choralaprava.xml',
                'antifonarkdmckoncept4.xml',
                'antifonarkdmckoncept3.xml',
                'antifonarkdmckoncept2.xml',
                'antifonarkdmckoncept.xml',
            ),
        );
        $post_groups = array();
        foreach ($post_groups_a as $name => $group) {
            $r = array();
            foreach ($group as $fname) {
                array_push($r, $this->repo->findByFilename($fname, false));
            }
            $post_groups[$name] = $r;
        }

        $this->render(
            'blog/selected.latte',
            array(
                    'post_groups' => $post_groups,
                    'categories' => $this->repo->allCategories(),
                    'page_rss' => $this->rss
                )
        );
    }

    public function abbreviations()
    {
        $this->render('blog/abbreviations.latte');
    }

    // single post
    public function show()
    {
        $post = $this->repo->findBySlug($this->params['slug']);
        if ($post === false) {
            $this->notFound();
        }

        $this->render(
            'blog/show.latte',
            array(
                'post' => $post,
                'categories' => $this->repo->allCategories(),
                'page_rss' => $this->rss
            ),
            // no XHTML here, IntenseDebate wouldn't cope with it
            'text/html; charset=UTF-8'
        );
    }

    // bibliographical citation of a post
    public function biblio()
    {
        $post = $this->repo->findBySlug($this->params['slug']);
        if ($post === false) {
            $this->notFound();
        }

        $format = $this->params['_format'];
        if ($format == 'bib') {
            $content_type = 'application/x-bibtex; charset=UTF-8';
        } elseif ($format == 'xml') {
            // more specific type
            // application/docbook+xml could be used instead,
            // but browsers generally don't do anything useful
            // with it and offer opener application selector;
            // I think it's better for the user to see a pretty
            // XML source instead
            $content_type = 'application/xml; charset=UTF-8';
        } else {
            $content_type = $this->default_content_type;
        }

        $this->render(
            "blog/biblio.$format.latte",
            array('post' => $post),
            $content_type
        );
    }

    private function redirectToPost($post_file_name)
    {
        $post = $this->repo->findByFile($post_file_name);
        if (!$post) {
            $this->notFound();
        }
        $this->redirect($post->permalink(), 301);
    }

    private function redirectToCategory($category_name)
    {
        $category = $this->repo->findCategoryByTitle($category_name);
        if (!$category) {
            $this->notFound();
        }
        $this->redirect($category->permalink(), 301);
    }

    /* requested pager page */
    private function getPage()
    {
        if (array_key_exists('strana', $_GET)) {
            return abs((int)$_GET['strana'] - 1);
        }

        return 0;
    }
}
