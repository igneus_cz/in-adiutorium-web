<?php
namespace InAdiutorium\controllers;

use InAdiutorium\Counter\Counter;

class CounterController extends Controller
{
    private $crawler_detector;

    public function setCrawlerDetector($detector)
    {
        $this->crawler_detector = $detector;
    }

    public function index()
    {
        $time = strtotime('today');
        if (@$_GET['den']) {
            $time = strtotime($_GET['den']);
        }

        $counter = $this->buildCounter($time);

        $sort = 'downloads_total';
        switch (@$_GET['razeni']) {
        case 'den':
            $sort = 'downloads_day'; break;
        case 'celkem':
            $sort = 'downloads_total'; break;
        case 'soubor':
            $sort = 'file'; break;
        default:
            // do nothing, use default set above
        }

        $this->render(
            'counter/index.latte',
            array(
                'day' => ($time == strtotime('today')) ? 'dnes' : date('d.m.Y', $time), // for display
                'date' => date('Y-m-d', $time), // for computing
                'entries' => $counter->entries($sort),
                'total' => $counter->total(),
                'today_total' => 0,
                'last_download_time' => $counter->lastDownloadTime(),
                'summaries' => $counter->recentDaySummaries(),
            )
        );
    }

    public function count()
    {
        $counter = $this->buildCounter();

        $file = $_GET['soubor'];

        if (!$this->validDownloadPath($file)) {
            $this->notFound();
            exit;
        }

        if (!$this->isCrawler()) {
            $counter->increment($file);
        }

        $this->redirect('/' . $file);
    }

    public function current()
    {
        $counter = $this->buildCounter(time());

        header('Access-Control-Allow-Origin: *');
        $this->render(
            'counter/current.latte',
            array(
                'total' => $counter->total(),
                'today_total' => $counter->day_total(),
                'last_download_time' => $counter->lastDownloadTime(),
                'recently_downloaded' => $counter->recentlyDownloaded()
            ),
            'application/xml'
        );
    }

    private function buildCounter($day=false)
    {
        return new Counter($day);
    }

    private function validDownloadPath($path)
    {
        if (preg_match('/^materialy\/[\w\d_-]+\.pdf$/', $path) !== 1) {
            return false;
        }

        $file_realpath = DOWNLOADS_DIR . '/' . basename($path);
        if (!is_file($file_realpath)) {
            return false;
        }

        return true;
    }

    private function isCrawler()
    {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        return $this->crawler_detector->isCrawler($user_agent);
    }
}
