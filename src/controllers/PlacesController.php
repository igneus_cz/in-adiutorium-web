<?php
namespace InAdiutorium\controllers;

class PlacesController extends Controller
{
    private $content_resource;
    private $source_path;

    public function setContent($c)
    {
        $this->content_resource = $c;
    }
    public function setSourcePath($p)
    {
        $this->source_path = $p;
    }

    public function index()
    {
        $this->render(
            'places/index.latte',
            array(
                'content' => $this->content_resource->retrieve(),
                'page_xml_source' => basename($this->source_path),
            ),
            // no XHTML here, mapy.cz js wouldn't cope with it
            'text/html; charset=UTF-8'
        );
    }
}
