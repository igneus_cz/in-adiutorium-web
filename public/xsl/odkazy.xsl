<?xml version="1.0" encoding="utf-8" ?>

<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:include href="inadiutorium.xsl"/>
  
  <xsl:template match="stranka">
    <div class="odkaz">
      <xsl:attribute name="id"><xsl:value-of select="symbol"/></xsl:attribute>

      <xsl:if test="ikonka">
        <a>
          <xsl:attribute name="href"><xsl:value-of select="url"/></xsl:attribute>
          <xsl:attribute name="rel">external</xsl:attribute>
          <img class="odkazikonka">
            <xsl:attribute name="src"><xsl:value-of select="ikonka"/></xsl:attribute>
            <xsl:attribute name="alt"><xsl:value-of select="nazev"/></xsl:attribute>
          </img>
        </a>
      </xsl:if>

      <h3>
        <a>
          <xsl:attribute name="href"><xsl:value-of select="url"/></xsl:attribute>
          <xsl:attribute name="rel">external</xsl:attribute>
          <xsl:value-of select="nazev"/>
        </a>
      </h3>

      <p class="citace">
        <a>
          <xsl:attribute name="href"><xsl:value-of select="url"/></xsl:attribute>
          <xsl:attribute name="rel">external</xsl:attribute>
          <xsl:value-of select="url"/>
        </a>
      </p>

      <xsl:apply-templates select="popis"/>
    </div>
  </xsl:template>
  
  <xsl:template match="popis">
    <xsl:for-each select="./*">
      <xsl:copy-of select="." />
    </xsl:for-each>
  </xsl:template>
  
</xsl:stylesheet>
