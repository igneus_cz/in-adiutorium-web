<?xml version="1.0" encoding="utf-8" ?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:include href="inadiutorium.xsl"/>
  
  <!-- prepise sablonu z inadiutorium.xsl !! -->
  <xsl:template match="obsah">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="kraj">
    <div class="krajlh">
      <h2>
        <xsl:attribute name="id">
          <xsl:value-of select="@id"/>
        </xsl:attribute>
        <xsl:value-of select="@nazev"/>
      </h2>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="misto">
    <div class="mistolh">
      <xsl:attribute name="id">
        <xsl:value-of select="@id"/>
      </xsl:attribute>

      <h3>
        <a href="#mapa" class="map-zoom">
          <xsl:value-of select="./nazev"/>
        </a>
      </h3>

      <p>
        <xsl:apply-templates select="upresneni|lokace|souradnice|cas|web|druhLiturgie|miraZpevu|napevy"/>
      </p>
      <p class="controller">
        <a href="#mapa" class="map-zoom">
          ukázat na mapě
        </a>
      </p>
    </div>
  </xsl:template>
  
  <xsl:template match="upresneni">
    <span class="polozka">kdo, co:</span>
    <xsl:text> </xsl:text>
    <xsl:value-of select="."/>
    <br/>
  </xsl:template>
  
  <xsl:template match="lokace">
    <span class="polozka">kde:</span>
    <xsl:text> </xsl:text>
    <xsl:value-of select="."/>
    <br/>
  </xsl:template>

  <xsl:template match="souradnice">
    <span class="polozka">gps:</span>
    <xsl:text> </xsl:text>
    <span class="h-geo geo">
      <span class="p-latitude latitude"><xsl:value-of select="@n"/></span>,
      <span class="p-longitude longitude"><xsl:value-of select="@e"/></span>
    </span>
    <br/>
  </xsl:template>
  
  <xsl:template match="web">
    <span class="polozka">web:</span><xsl:text> </xsl:text> 
    <a>
      <xsl:attribute name="href"><xsl:value-of select="@url"/></xsl:attribute>
      <xsl:attribute name="rel">external</xsl:attribute>
      <xsl:value-of select="@url"/>
    </a>
    <br/>
  </xsl:template>
  
  <xsl:template match="cas">
    <span class="polozka">čas:</span><xsl:text> </xsl:text> <xsl:value-of select="."/>
    <xsl:if test="@url">
      <xsl:text> </xsl:text>
      <a><xsl:attribute name="href"><xsl:value-of select="@url"/></xsl:attribute><xsl:attribute name="rel">external</xsl:attribute>podrobnosti &gt;&gt;</a>
    </xsl:if>
    <br/>
  </xsl:template>
  
  <xsl:template match="druhLiturgie">
    <span class="polozka">oficium:</span>
    <xsl:text> </xsl:text>
    <xsl:value-of select="."/>
    <br/>
  </xsl:template>
  
  <xsl:template match="miraZpevu">
    <span class="polozka">zpívá se:</span>
    <xsl:text> </xsl:text>
    <xsl:value-of select="."/>
    <br/>
  </xsl:template>
  
  <xsl:template match="napevy">
    <span class="polozka">nápěvy:</span>
    <xsl:text> </xsl:text>
    <xsl:copy-of select="child::node()"/>
    <br/>
  </xsl:template>
  
</xsl:stylesheet>
