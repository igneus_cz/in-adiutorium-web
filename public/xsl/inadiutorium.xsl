<xsl:stylesheet version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" omit-xml-declaration="yes" />
  
  <xsl:template match="/" xml:space="preserve">
    <xsl:apply-templates select="//obsah"/>
  </xsl:template>
  
  <xsl:template match="obsah">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="text">
    <xsl:for-each select="./*">
      <xsl:copy-of select="."/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="sloupek">
    <div class="sloupek">
      <h4>Kategorie:</h4>

      <xsl:for-each select="stranka/obsah/sekce">
        <p>
          <a>
            <xsl:attribute name="href">
              #<xsl:value-of select="@id"/>
            </xsl:attribute>
            <xsl:value-of select="@nazev"/>
          </a>
        </p>
      </xsl:for-each>
    </div>
  </xsl:template>

  <xsl:template match="sekce">
    <h2>
      <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
      <xsl:value-of select="@nazev"/>
    </h2>
    <xsl:apply-templates/>
  </xsl:template>
  
</xsl:stylesheet>