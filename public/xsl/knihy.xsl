<?xml version="1.0" encoding="utf-8" ?>

<xsl:stylesheet version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:include href="inadiutorium.xsl"/>
  
  <xsl:strip-space elements="biblio"/>

  <xsl:template match="kniha">
    <div class="kniha">
      <xsl:attribute name="id">
        <xsl:value-of select="@symbol"/>
      </xsl:attribute>

      <xsl:choose>
        <xsl:when test="nadpis">
          <h3><xsl:value-of select="nadpis"/></h3>
        </xsl:when>
        <xsl:otherwise>
          <h3>
            <xsl:value-of select="biblio/nazev"/> 
            <xsl:if test="biblio/autor">
              <xsl:text> (</xsl:text><xsl:value-of select="biblio/autor"/>)
            </xsl:if>
          </h3>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="biblio|text"/>
      <xsl:if test="odkazy|skcr|blogTag">
        <p class="odkazy">
          <xsl:apply-templates select="blogTag"/>
          <xsl:apply-templates select="skcr"/>
          <xsl:apply-templates select="odkazy"/>
        </p>
      </xsl:if>
    </div>
  </xsl:template>
  
  <xsl:template match="biblio">
    <p class="citace"><xsl:apply-templates/>.</p>
  </xsl:template>
  
  <xsl:template match="biblio/nazev">
    <xsl:apply-templates/>,
  </xsl:template>
  
  <xsl:template match="biblio/autor">
    <xsl:apply-templates/>: 
  </xsl:template>
  
  <xsl:template match="biblio/misto">
    <xsl:apply-templates/><xsl:if test="../vydavatel">:</xsl:if>
    <xsl:text> </xsl:text>
  </xsl:template>
  
  <xsl:template match="biblio/vydavatel">
    <xsl:apply-templates/><xsl:text> </xsl:text>
  </xsl:template>
  
  <xsl:template match="biblio/rok">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="odkazy">
    <span>
      koupit:
      <xsl:apply-templates/>
    </span>
  </xsl:template>
  
  <xsl:template match="url">
    <a>
      <xsl:attribute name="href">
        <xsl:value-of select="."/>
      </xsl:attribute>
      <xsl:attribute name="rel">external</xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>

      <xsl:value-of select="."/>
    </a>
  </xsl:template>

  <xsl:template match="skcr">
    <span>
      půjčit:
      <a>
        <xsl:attribute name="href">
          http://aleph.nkp.cz/F/?func=direct&amp;local_base=SKC&amp;doc_number=<xsl:value-of select="."/>
        </xsl:attribute>
        <xsl:attribute name="rel">external</xsl:attribute>
        <xsl:attribute name="target">_blank</xsl:attribute>
      SKČR</a>
    </span>
  </xsl:template>

  <xsl:template match="blogTag">
    <span>
      <a>
        <xsl:attribute name="href">
          /blog/stitky/<xsl:value-of select="."/>
        </xsl:attribute>
        související články na blogu
      </a>
    </span>
  </xsl:template>
</xsl:stylesheet>
