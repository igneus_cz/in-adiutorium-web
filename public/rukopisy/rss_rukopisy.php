<?php
  header("Content-Type: application/rss+xml; charset=UTF-8"); 
  echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n"; 
  
  function compare_lastmodtime($a, $b)
  {
    if ($a[1] == $b[1]) {
        return 0;
    }
    return ($a[1] < $b[1]) ? -1 : 1;
  }
  
  $soubory = Array();
  
  $d = dir('.');
  while (false !== ($f = $d->read())) {
    if (substr($f, -3) != 'xml') {
      continue;
    }
    $s = Array();
    $s[0] = $f;
    $s[1] = filemtime($f);
    $fxml = simplexml_load_file($f);
    $s[2] = $fxml->identification->name;
    $soubory[count($soubory)] = $s;
  }
  $d->close();
  
  usort($soubory, "compare_lastmodtime");
  
  $nejnovejsi_cas = $soubory[0][1];
?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
   <channel>
      <title>inadiutorium.xf.cz - Obsahy rukopisů</title>
      <link>http://inadiutorium.xf.cz/rukopisy/</link>
      <description>Naposledy aktualisované obsahy rukopisů</description>
      <language>cs-cs</language>
      <pubDate><?php echo date("r", $nejnovejsi_cas); ?></pubDate>

      <lastBuildDate><?php echo date("r", $nejnovejsi_cas); ?></lastBuildDate>
      <docs>http://www.rssboard.org/rss-specification</docs>
      <generator>inadiutorium.xf.cz RSS engine</generator>
      <webMaster>jkb.pavlik@gmail.com (Jakub Pavlík)</webMaster>
      <atom:link href="http://inadiutorium.xf.cz/rukopisy/rss_rukopisy.php" rel="self" type="application/rss+xml" />
      
      <?php
      foreach ($soubory as $s) {
        echo "      <item>\n";
        echo "        <pubDate>".date("r", $s[1])."</pubDate>\n";
        // guid: fake permalink: filename + ? + date
        echo "        <guid>http://inadiutorium.xf.cz/rukopisy/".$s[0]."?".date("Ymd", $s[1])."</guid>\n";
        echo "        <description>".$s[2]."</description>\n";
        echo "      </item>\n\n";
      }
      ?>

  </channel>
</rss>