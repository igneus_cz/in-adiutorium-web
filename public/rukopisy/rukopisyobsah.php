<?xml version="1.0" encoding="utf-8" ?>

<html>
  <head>
    <title>Obsahy rukopisů a starých tisků</title>
    <link rel="alternate" type="application/rss+xml" href="rss_rukopisy.php" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>

  <body>
    <a href="..">Zpět na obsah webu In adiutorium</a>
  
    <h1>Obsahy rukopisů a starých tisků</h1>
    
    <p>
      Následují obsahy některých rukopisů, jejichž facsimile lze najít 
      na internetu, považoval jsem je za relevantní pro svou práci
      na zhudebnění českého oficia a dosud k nim nebylo lze žádný obsah najít
      nebo autoři dostupných obsahů považovali za důležité něco jiného než já.
      Obsahy jsou to proto zákonitě neúplné,
      navíc neodborně vypracované.
    </p>
    <p>
      Změny v obsazích lze jednoduše sledovat pomocí
      <a href="rss_rukopisy.php">RSS kanálu</a>.
      Kanál je ovšem velmi prostý, automatický, a jediné, co dokáže sdělit, je,
      že se obsah některého rukopisu změnil - neřekne už, jaké změny
      se mu dostalo.
    </p>
    <hr/>
    
    <?php
       $d = dir('.');
       while (false !== ($f = $d->read())) {
        if (substr($f, -3) == 'xml') {
          $fxml = simplexml_load_file($f);
          echo "<p>";
	  echo "<a href=\"./$f\">".$fxml->identification->name."</a>";
	  echo " (".date("d.m.Y", filemtime($f)).")";
	  echo " ";
	  if ($fxml->contents) {
	    echo "[O";
	  } else {
	    echo "[-";
	  }
	  if ($fxml->identification->facsimileURL) {
	    echo "F]";
	  } else {
	    echo "-]";
	  }
	  echo "</p>\n";
        }
      }
      $d->close();
    ?>

    <hr/>
    <p>
      Zkratky: O - má popsaný obsah související s oficiem; F - na internetu je facsimile
    </p>
    
    <hr/>
    <p>
      Užitečné internetové zdroje:
    </p>
    <ul>
      <li><a href="http://www.manuscriptorium.com">Manuscriptorium</a></li>
      <li><a href="http://www.clavmon.cz/limup/">LIMUP</a> - databáze obsahu liturgických rukopisů utrakvistické provenience 15.-16. století.</li>
      <li><a href="http://www.firmadat.cz/melodiarium/">katalog Melodiarium hymnologicum Bohemiae</a></li>
    </ul>
  </body>
</html>
