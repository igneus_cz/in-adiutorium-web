<?xml version="1.0" encoding="utf-8" ?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
  <xsl:template match="/">
    <html>
      <head>
	<title><xsl:value-of select="//identification/name" /> (Obsah rukopisu)</title>
	<link rel="stylesheet" type="text/css" href="manuscript.css"/>
      </head>
      <body>
	<xsl:apply-templates/>
      </body>
    </html>
  </xsl:template>

  <!-- identification -->
  <xsl:template match="identification">
    <xsl:apply-templates/>
    <hr/>
  </xsl:template>

  <xsl:template match="name">
    <h1><xsl:value-of select="."/></h1>
  </xsl:template>

  <xsl:template match="descriptionURL">
    <p>
      popis: 
      <a>
	<xsl:attribute name="href"><xsl:value-of select="."/></xsl:attribute>
	<xsl:value-of select="."/>
      </a>
    </p>
  </xsl:template>

  <xsl:template match="facsimileURL">
    <p>
      facsimile: 
      <a>
	<xsl:attribute name="href"><xsl:value-of select="."/></xsl:attribute>
	<xsl:value-of select="."/>
      </a>
    </p>
  </xsl:template>

  <!-- section -->
  <xsl:template match="section">
    <h2><xsl:value-of select="."/></h2>
  </xsl:template>

  <!-- subsection -->
  <xsl:template match="subsection">
    <h3><xsl:value-of select="."/></h3>
  </xsl:template>

  <!-- hora -->
  <xsl:template match="hora">
    <h4 class="hora"><xsl:value-of select="."/></h4>
  </xsl:template>
  
  <!-- item -->
  <xsl:template match="item">
    <div>
      <p>
	<xsl:value-of select="./@page"/>
	<xsl:text>: </xsl:text>
	<xsl:value-of select="./title"/>
	<xsl:text> </xsl:text>
	<i><xsl:value-of select="./incipit"/></i>
      </p>
      <div>
	<xsl:apply-templates select="description" />
      </div>
    </div>
  </xsl:template>

  <xsl:template match="description">
    <xsl:for-each select="./*">
      <xsl:copy-of select="."/>
    </xsl:for-each>
    <xsl:value-of select="text()"/>
  </xsl:template>
  
  <xsl:template match="text">
    <div class="text">
      <xsl:for-each select="./*">
	<xsl:copy-of select="."/>
      </xsl:for-each>
    </div>
  </xsl:template>  
  
</xsl:stylesheet>