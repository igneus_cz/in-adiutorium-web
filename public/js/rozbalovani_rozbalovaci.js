// nastavi handlery rozbalovacich odkazu
function inicialisujRozbalovani() {
  // nastavit ovladac rozbalovacich odkazu
  $("a.rozbalodkaz").click(function(event){
    event.preventDefault();
    var divid = $(this).parents('.soubor, .sekce').first().attr('id');
    prepniRozbaleni(divid);
  });

  // ovladace odkazu pro rozbaleni/sbaleni vsech sekci
  $("a.zabalvse").click(function(event){
    event.preventDefault();
    $("#content").children("div.sekce").each(function(i,d){
      zabal($(this).attr("id"));
    });
  });

  $("a.rozbalvse").click(function(event){
    event.preventDefault();
    $("#content").children("div.sekce").each(function(){
      rekursivneRozbal($(this).attr("id"));
    });
  });

  // pred tim, nez se na cil skoci, zajistit, aby byl viditelny
  $("a[href^='\\#']").click(function(event){
    var href = $(this).attr('href');

    if (href == '#' || $(this).hasClass('rozbalodkaz')) {
      // not a regular link
      return;
    }

    // neskakat na kotvu pred rozbalenim
    event.preventDefault();
    skocNaKotvu(href);
  });

  var cookieChkbx = $('#cookie-opt-in');
  if (cookieExistuje()) {
    cookieChkbx.attr('checked', true); // TODO for newer jQuery update to .prop('checked', true)
  }

  cookieChkbx.change(function(event){
    if (cookieChkbx.is(':checked')) {
      ulozStavRozbalenosti();
    } else {
      smazCookie();
    }
  });

  $(window).unload(function(event){
    if (cookieExistuje()) {
      ulozStavRozbalenosti();
    }
  });
}

// k navigaci v ramci stranky: vola se, kdykoli se stranka nacita.
// Pokud je pozadovana specificka kotva, zajisti rozbalenim prislusnych
// sekci, aby byla viditelna, a pak na ni skoci
function skocNaKotvu(cil) {
  var i = cil.indexOf("#");
  if (i == -1) {
    return; // no anchor requested
  }
  var anchorname = cil.substr(i+1);

  var anchor = $("#" + anchorname);
  if (anchor.length == 0) {
    alert("Cíl '#" + anchorname + "' neexistuje. Zřejmě jste někde (ve starším čánku na blogu?) klikli na odkaz k materiálu, který mezitím už není dostupný.");
    return;
  }

  if (!isNaN(anchorname.charAt(0))) {
    // the anchor name begins with a number - the link points not to
    // a file, but to a day in the overview of recent updates
    var anchorobj = $("#"+anchorname).get(0);
    anchorobj.scrollIntoView(true);
    return;
  }

  var krozbaleni = Array();
  var currentel = anchor;
  var currentid = currentel.attr('id');

  if ($("#"+currentid).hasClass('sekce')) {
    krozbaleni.push(currentid);
  }

  do {
    currentel = $("#"+currentid).parent().parent();
    currentid = currentel.attr('id');

    if (!currentel.hasClass('sekce')) {
      break;
    }
    if (rozbalena(currentid)) {
      break;
    }

    krozbaleni.push(currentid);
  } while (true);

  for (var j = krozbaleni.length-1; j >= 0; j--) {
    rozbal(krozbaleni[j]);
  }

  var anchorobj = $("#"+anchorname).get(0);
  if (krozbaleni.length > 0) {
    // pockat, az bude hotova rozbalovaci animace, a skocit na
    // kotvu
    setTimeout(function(){
      anchorobj.scrollIntoView(true);
    }, 500);
  } else {
    // nic se nerozbalovalo; skocit hned
    anchorobj.scrollIntoView(true);
  }
}

// universalni funkce pro zobrazeni/skryti obsahu souboru i sekce;
// bezAnimace se nastavuje na true jen kdyz se rozbaleni nema animovat -
// vychozi hodnota undefined znamena animovat
function prepniRozbaleni(id, bezAnimace) {
  var element = $("#"+id);
  var obsahdiv = element.children(".obsah");

  if (! obsahdiv.hasClass('rozbalena')) {
    if (obsahdiv.text() == '' && !element.hasClass('sekce')) {
      // soubor; dosud neprohlizeny; potreba nacist data AJAXem
      var href = element.data('href');
      $.ajax(href, {complete: function(data) { obsahdiv.html(data.responseText); }});
    }

    obsahdiv.addClass("rozbalena");
  } else {
    obsahdiv.removeClass("rozbalena");

    if (element.hasClass('sekce')) {
      // sekce; skryt obsah take vsech dcerinych sekci
      var uvnitr = obsahdiv.children("div.sekce, div.soubor");
      uvnitr.each(function(i, c){
        zabal($(this).attr('id'));
      });
    }
  }

  if (element.hasClass('soubor')) {
    var baliciodkaz = $('.vpravo .rozbalodkaz', element);
    if (obsahdiv.hasClass('rozbalena')) {
      baliciodkaz.text('zabalit');
    } else {
      baliciodkaz.text('rozbalit');
    }
  }

  if (typeof(bezAnimace) === 'undefined') {
    // rozbalit s animaci
    obsahdiv.slideToggle();
  } else if (bezAnimace == true) {
    // rozbalit skokem, bez animace
    obsahdiv.show();
  }
}

function rozbalena(id) {
  return $("#"+id).children(".obsah").hasClass("rozbalena");
}

// Zuzeni funkce vyse na jeden smer - pouze zavira, co je otevrene.
// Zavrene nechava byt.
function zabal(id) {
  if (! rozbalena(id)) {
    return;
  }

  prepniRozbaleni(id);
}

// bezAnimace se nastavuje na true jen kdyz se rozbaleni nema animovat -
// vychozi hodnota undefined znamena animovat
function rozbal(id, bezAnimace) {
  if (rozbalena(id)) {
    return;
  }

  prepniRozbaleni(id, bezAnimace);
}

// rozbali sekci a vsechny jeji podsekce
function rekursivneRozbal(id) {
  rozbal(id);
  $("#"+id).children(".obsah").children("div.sekce").each(function(i,s){
    var iid = $(this).attr("id");
    rekursivneRozbal(iid);
  });
}

var cookieName = 'pack_state_' + window.location.pathname;

// projde cookies a zabali, co podle nich nema byt rozbaleno
function rozbalPodleCookies()
{
  $('div.obsah').addClass('skryte');

  if (!cookieExistuje()) {
    return;
  }

  var toUnpack = JSON.parse(readCookie(cookieName));
  for (var key in toUnpack) {
    if (toUnpack[key] == 1) {
      element = document.getElementById(key);
      if (element) {
        // tady se rozbaluje podle animace, protoze rozbalujeme to,
        // co uz uzivatel predtim mel rozbalene
        rozbal(key, true);
      }
    }
  }
}

function cookieExistuje()
{
  return (readCookie(cookieName) ? true: false);
}

function ulozStavRozbalenosti()
{
  var obj = {};
  $('.obsah.rozbalena').each(function(){
    var id = $(this).parent().attr('id');
    obj[id] = 1;
  });
  createCookie(cookieName, JSON.stringify(obj), 1000); // 1000 dni
}

function smazCookie()
{
  eraseCookie(cookieName);
}
