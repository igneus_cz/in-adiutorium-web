/* Kod ktery zarizuje pravidelne obnovovani stranky s poctem stazeni */

var defaultTitle = document.title;
var posledniPocetStazeni = 0;
var noveNahrano = true;

function obnovovaciCyklus()
{
    var oneMinute = 60*1000;
  
    obnov();
    setInterval(function(){ obnov(); }, oneMinute);
}

function obnov()
{
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET","/noty/stahovanost/nyni", true);
    xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState !== XMLHttpRequest.DONE || xmlhttp.status !== 200) {
              return;
            }
	    zpracujPrichoziData(xmlhttp.responseXML);
    };
    xmlhttp.send();
}

function zpracujPrichoziData(xmldoc)
{
    var dnesspan = document.getElementById("dnes");
    var celkemspan = document.getElementById("celkem");
    var casspan = document.getElementById("cas");
    var poslednispan = document.getElementById("posledni");

    var dnesi = xmldoc.getElementsByTagName("dnesCelkem")[0].textContent;
    var celkemi = xmldoc.getElementsByTagName("celkem")[0].textContent;

    var poslednii = parseInt(xmldoc.getElementsByTagName("posledni")[0].textContent);
    var poslednid = new Date(poslednii*1000);
    var d = new Date(); // dnesni datum

    document.title = "("+dnesi+") " + defaultTitle;

    dnesspan.innerHTML = dnesi;
    celkemspan.innerHTML = celkemi;

    if (poslednid.getDate() != d.getDate()) {
	    var rozdil = d.getDate() - poslednid.getDate();

	// zmenil se mesic?
	if (rozdil < 0) {
	    var minulyMesicDni = 0;
	    switch (poslednid.getMonth()+1) {
	    case 1:
	    case 3: 
	    case 5:
	    case 7:
	    case 8:
	    case 10:
	    case 12:
		minulyMesicDni = 31; break;
	    case 2:
		if (poslednid.getYear() % 4 == 0) {
		    if (poslednid.getYear() % 100 == 0) {
			if (poslednid.getYear() % 400 == 0) {
			    minulyMesicDni = 29;
			} else {
			    minulyMesicDni = 28;
			}
		    } else {
			minulyMesicDni = 29;
		    }
		} else {
		    minulyMesicDni = 28;
		} 
		break;
	    default:
		minulyMesicDni = 30; break;
	    }

	    rozdil = (minulyMesicDni - poslednid.getDate()) + d.getDate();
	}

	// hezke datum posledniho stazeni
	if (rozdil == 1) {
	    poslednispan.innerHTML = 'včera';
	} else if (rozdil == 2) {
	    poslednispan.innerHTML = 'předevčírem';
	} else {
	    poslednispan.innerHTML = 'před '+rozdil+' dny';
	}
    } else {
	poslednispan.innerHTML = poslednid.getHours()+':'+poslednid.getMinutes();
    }
    
    casspan.innerHTML = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds(); 

    var dnesii = parseInt(dnesi);
    if (dnesii > posledniPocetStazeni) {
	if (noveNahrano) {
	    noveNahrano = false;
	} else {
	    if (document.getElementById("prepinac_zvoneni").checked) {
		zazvon(dnesii - posledniPocetStazeni);
	    }
	}

        var seznamdiv = document.getElementById("seznamsouboru");
        var stazeneSoubory = xmldoc.getElementsByTagName("soubory")[0].textContent;
        seznamdiv.innerHTML = "<pre>"+stazeneSoubory+"</pre>";
        seznamdiv.parentNode.style.display = "block";

        // systemova notifikace
        if (document.getElementById("prepinac_upozorneni").checked && // switched on
            ("Notification" in window)) { // notification API available

          if (Notification.permission === 'denied') {
            // nic nedelat
          } else if (Notification.permission === 'granted') {
            zobrazUpozorneni(stazeneSoubory)
          } else {
            Notification.requestPermission(function (permission) {
              if (permission === 'granted') {
                zobrazUpozorneni(stazeneSoubory)
              }
            });
          }
        }

	posledniPocetStazeni = dnesii;
    }
}

function zobrazUpozorneni(text)
{
  var notification = new Notification(text.replace("\n", " "));
}

// prehraje zvuk
function zazvon(kolikrat)
{
  var audio = document.getElementById('gong');
  console.log(audio);
  audio.play();
  if (kolikrat > 1) {
    setTimeout(function(){zazvon(kolikrat-1);}, 1400);
  }
}
