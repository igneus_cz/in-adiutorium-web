/* vychazi z predpokladu, ze stranka je strukturovana pomoci nadpisu h3
a ze kazdemu takovemu nadpisu predchazi kotva.
Posbira takove nadpisy a vytvori k nim odkazy z bocniho sloupce. */

function collectSectionNavigation() {
  var navigpane = $('.sloupek').first();
  navigpane.append('<ul></ul>');
  var naviglist = navigpane.children('ul');
  $('h2').each(function(i, h) {
    var previous = $(this).prev();
    var previouselem = previous.get(0);
    var target = '';
    if ($(this).attr('id') != '') {
      target = $(this).attr('id');
    }
       
    if (target != '') {
      var link = "<li><a href=\"#" + target +
                  "\">" + $(this).text() + "</a></li>";
      naviglist.append(link);
    }
  });
}

$(document).ready(function(){ 
  collectSectionNavigation();
});