/* Podpora fungovani stranky s knihami */

var booksPage = { linksBeautified: false };

$(function(){

  // shorten text of links to on-line bookstores
  (function(){
    // for some reason, in FF the onload function gets called twice;
    // don't perform the beautification the second time
    if (booksPage.linksBeautified) return;

    var links = $("div.kniha p.odkazy a");
    links.each(function() {
      var text = $(this).text();
      if (text.substr(0, 4) != 'http') {
        return;
      }
	    var uri = URI(text);
      var replacement = '';
      if (uri.subdomain() == 'www') {
        replacement = uri.domain();
      } else {
        replacement = uri.host();
      }
	    $(this).text(replacement);
    });

    booksPage.linksBeautified = true;
  })();
});
