/*
Upravi obrazek pro Slimbox 2.

- pokud je obrazek v odkazu, doplni odkazu atributy rel a title
- pokud obrazek neni v odkazu, obali ho odkazem
*/
function slimboxifyImage(element)
{
  if (!element.parent().is('a')) {
    element.wrap("<a />");
    element.parent().attr('href', element.attr('src'));
  }

  var wraplink = element.parent();
  wraplink.attr('rel', 'lightbox');

  var title_p = element.parent().next('p')
  if (title_p) {
    var title = title_p.text();
    wraplink.attr('title', title);
  }

  wraplink.slimbox({/* Put custom options here */}, null, function(el) {
    return (this == el) || ((this.rel.length > 8) && (this.rel == el.rel));
  });
}

$(document).ready(function(){
  $("div.obrazek img").each(function(i, element) {
    slimboxifyImage($(this));
  });
  $("div.obrazek a img").each(function(i, element) {
    slimboxifyImage($(this));
  });

  $('a.toggle-biblio').each(function(e) {
    var $this = $(this);
    $this.click(function(e){
      e.preventDefault();
      $('.biblio', $this.parent()).toggle();
    });
  })
});	