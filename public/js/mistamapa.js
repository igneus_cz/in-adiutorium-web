/* pro mista.xml: mapa s misty, kde se slavi zpivana liturgie hodin;
vyuziva mapy.cz API v. 4 - viz http://api4.mapy.cz */

// initial center of the map - chosen so that most of the state is visible
var republicCenter = SMap.Coords.fromWGS84(15.5827111, 49.8162886);

var dataFile = "mistalh.xml";

var view;

$(document).ready(function(){ 
  view = new LOTHPlacesView(dataFile);

  // initialize links zooming map to places
  jQuery('a.map-zoom').click(function() {
    view.zoomTo($(this).parents('.mistolh').first().attr('id'));
  });
});

/* urgh... so _this_way_ classes are defined in JavaScript? #@!*... */
function LOTHPlacesView(dataFile) {
  this.dataFile = dataFile;

  var zoom = 7;
  this.map = new SMap(JAK.gel("mapa"), republicCenter, zoom);

  this.map.addDefaultLayer(SMap.DEF_BASE).enable();
  this.map.addDefaultControls();

  this.markerLayer = new SMap.Layer.Marker();
  this.map.addLayer(this.markerLayer);
  this.markerLayer.enable();

  this.coordinates = {};
  this.loadDataXML();
}

// downloads a JQuery object (XML document) containing the data,
// populates map with markers
LOTHPlacesView.prototype.loadDataXML = function() {
  var view = this;
  $.ajax(this.dataFile).done(function(data) {
    $(data).find("misto").each(function() {
      var misto = $(this);
      if (misto.children("souradnice")) {
        view.mapAddPlace(misto);
      }
    });
  });
};

// placeXML - a jQuery object containing a 'misto' element from
// mista.xml; the place is added to the map
LOTHPlacesView.prototype.mapAddPlace = function(placeXML) {
  var coordn = parseFloat(placeXML.children("souradnice").attr("n")),
    coorde = parseFloat(placeXML.children("souradnice").attr("e"));
  var name = placeXML.children("nazev").text();
  var placeId = placeXML.attr('id');

  var coord = SMap.Coords.fromWGS84(coorde, coordn);
  this.coordinates[placeId] = coord;

  var options = { title: name };
  var marker = new SMap.Marker(coord, name, options);

  var card = new SMap.Card();
  var placeHTML = $('#'+placeId);
  card.getHeader().innerHTML = "<strong>"+placeHTML.children('h3').text()+"</strong>";
  card.getBody().innerHTML = placeHTML.children('p:not(.controller)').html();
  marker.decorate(SMap.Marker.Feature.Card, card);

  this.markerLayer.addMarker(marker);
};

LOTHPlacesView.prototype.loadXMLDoc = function(file) {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET", file, false); // _synchronous_ AJAX request
  xmlhttp.send();
  var xmldoc = $.parseXML( xmlhttp.responseText );
  return xmldoc;
};

LOTHPlacesView.prototype.zoomTo = function(placeId) {
  coordinates = this.coordinates[placeId];
  this.map.setCenter(coordinates);
  this.map.setZoom(20, coordinates);
};
