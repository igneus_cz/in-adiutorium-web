<?php
// main entry point of the application. All requests are served
// by this script.

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/setup.php';
require_once __DIR__ . '/src/container.php';
require_once __DIR__ . '/src/router.php';

use Symfony\Component\Templating;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Templating\Loader\FilesystemLoader;
use Symfony\Component\Templating\Helper\SlotsHelper;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

use InAdiutorium\framework\RequestDispatcher;

function content_type($fname) {
    $parts = explode('.', $fname);
    $ext = end($parts);

    if ($ext === 'xml') {
        return 'application/xml';
    }

    return 'application/octet-stream';
}


try {
    try {
        $parameters = $matcher->match($context->getPathInfo());
    } catch (ResourceNotFoundException $e) {
        if (substr($context->getPathInfo(), -1, 1) == '/') {
            // isn't there a valid route without trailing slash?
            $withoutSlash = rtrim($context->getPathInfo(), '/');
            $parameters = $matcher->match($withoutSlash);
            // redirect there
            $route = $parameters['_route'];
            unset($parameters['_route']);
            $parameters = array(
                'redirect' => $route,
                'parameters' => $parameters,
            );
        } else {
            throw $e;
        }
    }
} catch (ResourceNotFoundException $e) {
    $c = $container->get('error.controller');
    $c->notFound();
    exit;
}

$dispatcher = new RequestDispatcher($container, $generator);
$dispatcher->dispatch($request, $parameters);
